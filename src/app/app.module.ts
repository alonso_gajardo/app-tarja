import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
//import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from './components/components.module';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { PipesModule } from './pipes/pipes.module';
//import { AppVersion } from '@ionic-native/app-version/ngx';
import { FiltrosModule } from './filtros/filtros.module';
import { Network } from'@ionic-native/network/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HttpClientModule } from '@angular/common/http';
import { Proseso } from "./provedor/proseso";
import { Ubicacion } from "./provedor/ubicacion";


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(),
     AppRoutingModule,
     ComponentsModule,
     PipesModule,
     FiltrosModule,
     IonicStorageModule.forRoot(),
     HttpClientModule,
    ],
  providers: [
    StatusBar,
    //SplashScreen,
    BarcodeScanner,
    Network,
    Geolocation,
   // AppVersion,
   Proseso,
   Ubicacion,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
