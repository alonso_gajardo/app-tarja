import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroGeneral'
})
export class FiltroGeneral implements PipeTransform {

  transform(arreglo: any[], texto: string, columna: string): any []{

    if( texto ===''){
      return arreglo
    }

    //texto = texto.toLowerCase();

    return arreglo.filter( item =>{
      return item[columna].toLowerCase().includes(texto);
    });


/*
    users.filter(item=> { 
      item.desplayName.toLocaleLowerCase().includes(nameSearch), 
      item.email.toLocaleLowerCase().includes(eamilSearch), 
      item.description.toLocaleLowerCase().includes(roleSearch) 
    });
*/



    
  }

}