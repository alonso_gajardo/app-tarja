import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroIdusuario'
})
export class FiltroIdusuarioPipe implements PipeTransform {

  transform(arreglo: any[], texto: string): any []{

    if( texto ===''){
      return arreglo
    }

    texto = texto.toLowerCase();


    return arreglo.filter( item =>{
      return item.usuario_idUsuario.toLowerCase().includes(texto),item.estado_idEstado.toLowerCase().includes("1");
    });

    
  }

}
