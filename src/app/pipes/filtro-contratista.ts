import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro_contratista'
})
export class FiltroContratista implements PipeTransform {

  transform(arreglo: any[], texto: string): any []{

    if( texto ===''){
      return arreglo
    }

    texto = texto.toLowerCase();

    return arreglo.filter( item =>{
      return item.contratista_razon_social.toLowerCase()
      .includes(texto);
    });


    
  }

}