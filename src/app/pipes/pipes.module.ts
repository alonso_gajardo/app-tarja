import { NgModule } from '@angular/core';
import { FiltroPipe } from './filtro.pipe';
import { FiltroOTPipe } from './filtro-ot.pipe';
import { FiltroOTlaborPipe } from './filtro-otlabor';
import { FiltroGeneral } from './filtro-general';
import { FiltroContratista } from './filtro-contratista';
import { FiltroIdusuarioPipe } from './filtro-idusuario.pipe';
import { FiltroRutPipe } from './filtro-rutestado';


@NgModule({
  declarations: [FiltroPipe, FiltroOTPipe, FiltroOTlaborPipe, FiltroGeneral, FiltroContratista, FiltroIdusuarioPipe, FiltroRutPipe],
  exports: [FiltroPipe, FiltroOTPipe, FiltroOTlaborPipe, FiltroGeneral, FiltroContratista, FiltroIdusuarioPipe, FiltroRutPipe]
})
export class PipesModule { }
