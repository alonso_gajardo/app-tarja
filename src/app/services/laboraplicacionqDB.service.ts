import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalRest } from './global';


@Injectable({
  providedIn: 'root'
})
export class DBlaboraplicacionqService {


  public urlrest: string;

//url ='https://www.digital5.cl/php_generis/restfull/index.php/';

  constructor(private http: HttpClient) { 
    this.urlrest= GlobalRest.url;
  }


  //GET FUNCIONA 100%
  getLaborAplicacionQ():Observable<any>{
return this.http.get(this.urlrest+'LaborAplicacionQ/laboraplicacionq_all');
  }

  
   //INSERT FUNCIONA 100%
  CrearPostLaborAplicacionQ(laboraplicacionq):Observable<any>{
    //console.log(dblatLong);
    let params = JSON.stringify(laboraplicacionq);
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let Headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.urlrest+'LaborAplicacionQ/laboraplicacionq_insertar',params,{headers: Headers});
  }


}