import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalRest } from './global';


@Injectable({
  providedIn: 'root'
})
export class DBcontratistaOKService {


  public urlrest: string;

//url ='https://www.digital5.cl/php_generis/restfull/index.php/';

  constructor(private http: HttpClient) { 
    this.urlrest= GlobalRest.url;
  }


  //GET FUNCIONA 100%
  getContratista():Observable<any>{
return this.http.get(this.urlrest+'Contratista/Contratista_all');
  }

    //GET FUNCIONA 100%
    getContratista_x_fundo(idFundo):Observable<any>{
      return this.http.get(this.urlrest+'Contratista/Contratista_x_fundo/'+idFundo);
        }

    //CREAR CONTRATISTA
    CrearPostContratista(data):Observable<any>{
      console.log(data);
      let params = JSON.stringify(data);
      //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
      let Headers = new HttpHeaders().set('Content-Type','application/json');
      return this.http.post(this.urlrest+'Contratista/contratista_insertar/',params,{headers: Headers});
    }



}