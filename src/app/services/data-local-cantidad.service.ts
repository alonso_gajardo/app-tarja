import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { RegistroCantidad } from '../models/registroCantidad.models';

@Injectable({
  providedIn: 'root'
})
export class DataLocalLaboresCantidad{

  guardadosCantidad: RegistroCantidad[] =[];

  constructor(public storage: Storage) { 


//cargar registros
    this.cargarStorageCantidad();

  }

  async cargarStorageCantidad(){
this.guardadosCantidad=(await this.storage.get('registrosCantidad')) || [];
  }

  async guardarRegistroCantidad( 
     numeroOtCantidad: string,
     rutCantidad: string,
     unidadCantidad: string,
     cantidadCantidad: string,
     fechaCantidad: string,
     nombreProductoCantidad:string,
     cantidadProductoCantidad:string,
     estadoCantidad:string,
     
    nombreProductoQuimico1:string,
    nombreProductoQuimico2:string,
    nombreProductoQuimico3:string,
    nombreProductoQuimico4:string,
    nombreProductoQuimico5:string,
    nombreProductoQuimico6:string,
    nombreProductoQuimico7:string,
    nombreProductoQuimico8:string,
    nombreProductoQuimico9:string,
    cantidadProductoQuimico1:string,
    cantidadProductoQuimico2:string,
    cantidadProductoQuimico3:string,
    cantidadProductoQuimico4:string,
    cantidadProductoQuimico5:string,
    cantidadProductoQuimico6:string,
    cantidadProductoQuimico7:string,
    cantidadProductoQuimico8:string,
    cantidadProductoQuimico9:string,
    medidaProductoQuimico1:string,
    medidaProductoQuimico2:string,
    medidaProductoQuimico3:string,
    medidaProductoQuimico4:string,
    medidaProductoQuimico5:string,
    medidaProductoQuimico6:string,
    medidaProductoQuimico7:string,
    medidaProductoQuimico8:string,
    medidaProductoQuimico9:string,
    numeroBombada:string,
    litrosAguaxHectarea:string

          ){

    await this.cargarStorageCantidad();


    const nuevoRegistroCantidad = new RegistroCantidad(
        numeroOtCantidad,
        rutCantidad,
        unidadCantidad,
        cantidadCantidad,
        fechaCantidad,
        nombreProductoCantidad,
        cantidadProductoCantidad,
        estadoCantidad,

       nombreProductoQuimico1,
       nombreProductoQuimico2,
       nombreProductoQuimico3,
       nombreProductoQuimico4,
       nombreProductoQuimico5,
       nombreProductoQuimico6,
       nombreProductoQuimico7,
       nombreProductoQuimico8,
       nombreProductoQuimico9,
       cantidadProductoQuimico1,
       cantidadProductoQuimico2,
       cantidadProductoQuimico3,
       cantidadProductoQuimico4,
       cantidadProductoQuimico5,
       cantidadProductoQuimico6,
       cantidadProductoQuimico7,
       cantidadProductoQuimico8,
       cantidadProductoQuimico9,
       medidaProductoQuimico1,
       medidaProductoQuimico2,
       medidaProductoQuimico3,
       medidaProductoQuimico4,
       medidaProductoQuimico5,
       medidaProductoQuimico6,
       medidaProductoQuimico7,
       medidaProductoQuimico8,
       medidaProductoQuimico9,
       numeroBombada,
       litrosAguaxHectarea       

        );

    //ordena y deja el ultimo ingresado en primer lugar
    this.guardadosCantidad.unshift(nuevoRegistroCantidad);

    this.storage.set('registrosCantidad',this.guardadosCantidad);
  }


}