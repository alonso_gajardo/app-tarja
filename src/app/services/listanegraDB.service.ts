import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalRest } from './global';


@Injectable({
  providedIn: 'root'
})
export class ListaNegraDB {


  public urlrest: string;

//url ='https://www.digital5.cl/php_generis/restfull/index.php/';

  constructor(private http: HttpClient) { 
    this.urlrest= GlobalRest.url;
  }


  //GET FUNCIONA 100%
  getListaNegra(id):Observable<any>{
return this.http.get(this.urlrest+'ListaNegra/listanegra_single/'+id);
  }
  

  
   //INSERT FUNCIONA 100%
  CrearPostAvance(data):Observable<any>{
    console.log(data);
    let params = JSON.stringify(data);
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let Headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.urlrest+'AvanceLaborG/avancelaborg_insertar/',params,{headers: Headers});
  }



}