import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ProductosQFusionModel } from '../models/productosQFusion.model';



@Injectable({
  providedIn: 'root'
})
export class ProductosQFusionLocal{

  productosQFusiondata: ProductosQFusionModel [] =[];

  constructor(
      public storage: Storage
      ) { 

//cargar registros
    this.cargarProductosQFusion();

  }

  async cargarProductosQFusion(){
this.productosQFusiondata=(await this.storage.get('productosQFusionLocal')) || [];
  }


  async guardarProductosQFusion( 

    rut: string,
    idLabor: string,
    nombre: string,
    apellido: string,
    formatoEntrada: string,
    celular: string,
    idFecha: string,
    productosQCantidad: string,
    productosQNombre: string,
    unidadmedida_idUnidadMedida: string,
    estado: string

          ){ 

    //await this.cargarProductosQFusion();

    const nuevoRegistroProductosQFusion = new ProductosQFusionModel(

        rut,
        idLabor,
        nombre,
        apellido,
        formatoEntrada,
        celular,
        idFecha,
        productosQCantidad,
        productosQNombre,
        unidadmedida_idUnidadMedida,
        estado
        );


  //this.productosQdata.push(nuevoRegistroProductosQ);
//this.storage.set('avanceLaborGLocal',this.avanceLaborQdata);


    //ordena y deja el ultimo ingresado en primer lugar
   this.productosQFusiondata.unshift(nuevoRegistroProductosQFusion);
   this.storage.set('productosQFusionLocal',this.productosQFusiondata);
  }


}