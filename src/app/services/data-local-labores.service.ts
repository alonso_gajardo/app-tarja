import { Injectable } from '@angular/core';
import {RegistroLabores} from '../models/registroLabores.model'
import {Storage} from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class DataLocalLaboresService {

  guardadosLabores: RegistroLabores[] =[];

  constructor(private storage: Storage) 
  { 
    //cargar registros
    this.cargarStorageLabores();
  }

/////////////////////////
/*
async guardarRegistroLabores( 
  nombreLabor:string, 
  tipoLabor:string
    ){
  await this.cargarStorageLabores();
  const nuevoRegistroLabores = new RegistroLabores(nombreLabor, tipoLabor);
  //ordena y deja el ultimo ingresado en primer lugar
  this.guardadosLabores.unshift(nuevoRegistroLabores);
  this.storage.set('registrosLabores',this.guardadosLabores);
}
*/
////////////////////////


  async cargarStorageLabores(){
this.guardadosLabores=(await this.storage.get('registrosLabores')) || [];
  }

  async guardarRegistroLabores( 
    
    idLabor: string,
    laborNombre: string,
    laborDescripcion: string,
    laborValorTratoInicial: string,
    laborValorTratoFinal: string,
    laborObjetivo: string,
    laborLat: string,
    laborLng: string,
    laborFecha: string,
    laborIconoAsignacion: string,
    laborIcono: string,
    laborCuartel: string,
    laborDescripcionCierre: string,
    estado_idEstado: string,
    tipoLabor_idTipoLabor: string,
    laborAplicacionQ_idLaborAplicacionQ: string,
    contratista_idContratista: string,
    usuario_idUsuario: string

      ){

    await this.cargarStorageLabores();


    const nuevoRegistroLabores = new RegistroLabores(

      idLabor,
      laborNombre,
      laborDescripcion,
      laborValorTratoInicial,
      laborValorTratoFinal,
      laborObjetivo,
      laborLat,
      laborLng,
      laborFecha,
      laborIconoAsignacion,
      laborIcono,
      laborCuartel,
      laborDescripcionCierre,
      estado_idEstado,
      tipoLabor_idTipoLabor,
      laborAplicacionQ_idLaborAplicacionQ,
      contratista_idContratista,
      usuario_idUsuario
      );

    //ordena y deja el ultimo ingresado en primer lugar
    this.guardadosLabores.unshift(nuevoRegistroLabores);

    this.storage.set('registrosLabores',this.guardadosLabores);
  }


  async borrarRegistroLabores(index){

    await this.cargarStorageLabores();

    this.guardadosLabores.splice(index,1);
    this.storage.set('registrosLabores',this.guardadosLabores);

  }







    async editarRegistroLabores(
      /*
      ot:any,
      descripcion:any,
      tratoFinal:any,
      sobrante1:any,
      sobrante2:any,
      sobrante3:any,
      sobrante4:any,
      sobrante5:any,
      sobrante6:any,
      sobrante7:any,
      sobrante8:any,
      sobrante9:any,
      estadoCierre:any
*/
      ){

      //aqui cargo el listado  guardadosLabores
      await this.cargarStorageLabores();
  /*
      const editLabores = ((ot) =>{
      //aquí lo recorro
        this.guardadosLabores.map(item => {

          //index es mi numero de OT       
          if (item.numeroOt === ot) {
            //console.log(item.numeroOt);
            //estadolabor : 1 activo  - 2 inactivo
            item.estadoLabor = "2";
            item.cierreDescripcion=descripcion;
            item.cierreTratoFinal = tratoFinal;
            item.cierreSobrante1 = sobrante1;
            item.cierreSobrante2 = sobrante2;
            item.cierreSobrante3 = sobrante3;
            item.cierreSobrante4 = sobrante4;
            item.cierreSobrante5 = sobrante5;
            item.cierreSobrante6 = sobrante6;
            item.cierreSobrante7 = sobrante7;
            item.cierreSobrante8 = sobrante8;
            item.cierreSobrante9 = sobrante9;
            item.estadoCierre = estadoCierre;
            //console.log(this.guardadosLabores);
            this.storage.set('registrosLabores',this.guardadosLabores);
            //return item.estadoLabor;
          }

        });

      });

      editLabores(ot);
      */
  }













}
