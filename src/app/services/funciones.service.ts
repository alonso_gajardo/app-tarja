import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Injectable({
  providedIn: 'root'
})
export class FuncionesService {
  
  today:Date;

  constructor(
    private geolocation: Geolocation
  ) { }



  fecha_hora_actual(){
    //fecha
    var x = "";
    var x2 = "";
    var x3 = "";
  
    var tiempo = new Date();
    var hora = tiempo.getHours();
    var minuto = tiempo.getMinutes();
    var segundo = tiempo.getSeconds();
  
    if (hora <= 9) { x = "0"; }
    if (minuto <= 9) { x2 = "0"; }
    if (segundo <= 9) { x3 = "0"; }
  
    var hora_actual = x + hora + ":" + x2 + minuto + ":" + x3 + segundo;
  
    //hora
      var fecha_1 = "";
      var fecha_2 = "";
  
      var fecha_actual = new Date(); //fecha de hoy
      var a = fecha_actual.getMonth() + 1; //sacar mes
      var d = fecha_actual.getDate(); //sacar dias
  
      if (a <= 9) { fecha_1 = "0"; }
  
      if (d <= 9) { fecha_2 = "0"; }
  
      //res_x =  fecha_actual.getFullYear() + fecha_1 + (fecha_actual.getMonth() + 1 )  + fecha_2 + fecha_actual.getDate();
      var fecha = fecha_actual.getFullYear() + "-" + fecha_1 + (fecha_actual.getMonth() + 1) + "-" + fecha_2 + fecha_actual.getDate();
  
      var res = fecha+" "+hora_actual;
      return res;
  }



  obtener_iconoLabor(tipificacionLabor){

    /*
  1	Labor General Autoasignada
  2	Labor General Interna
  3	Labor General Contratista
  4	Aplicación Química Autoasignada
  5	Aplicación Química Interna
  6	Aplicación Química Contratista
    */
  
    if(tipificacionLabor == "1"){
  return "leaf-outline";
    }
  
    if(tipificacionLabor == "2"){
      return "leaf-outline";
    }
  
    if(tipificacionLabor == "3"){
      return "leaf-outline";
    }
    
    if(tipificacionLabor == "4"){
      return "logo-react";
        }
      
        if(tipificacionLabor == "5"){
          return "logo-react";
        }
      
        if(tipificacionLabor == "6"){
          return "logo-react";
        }
  }
  
  obtener_iconoAsignacion(tipificacionLabor){
    /*
  1	Labor General Autoasignada
  2	Labor General Interna
  3	Labor General Contratista
  4	Aplicación Química Autoasignada
  5	Aplicación Química Interna
  6	Aplicación Química Contratista
    */
  
    if(tipificacionLabor == "1"){
      return "person-outline";
        }
      
        if(tipificacionLabor == "2"){
          return "people-outline";
        }
      
        if(tipificacionLabor == "3"){
          return "construct-outline";
        }
        
        if(tipificacionLabor == "4"){
          return "person-outline";
            }
          
            if(tipificacionLabor == "5"){
              return "people-outline";
            }
          
            if(tipificacionLabor == "6"){
              return "construct-outline";
            }
  }





  obtener_hora(){
    /*sacar fecha*/
    var meses = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
    var f = new Date();
    var fec = (f.getFullYear() + "" + meses[f.getMonth()] + "" +f.getDate());
    /**/
  
    /*sacar hora*/
    this.today = new Date();
    var h = this.today.getHours();
    var m = this.today.getMinutes();
    var s = this.today.getSeconds();
    var mm = this.today.getMilliseconds();
    var h1 = 0;
    var m1 = 0;
    var s1 = 0;
  
    if (h < 10) { h1 = 0 + h; } else { h1 = h; }
    if (m < 10) { m1 = 0 + m; } else { m1 = m; }
    if (s < 10) { s1 = 0 + s; } else { s1 = s; }
    var hora = h1 + ":" + m1 + ":" + s1 + ":" + mm;
    /**/
  
    return fec + "|" + hora;
  }

obtenerGeolocalizacion(){

  this.geolocation.getCurrentPosition().then((resp) => {
    const gpsUsuarioLatLngLat = `${resp.coords.latitude}`;
    const gpsUsuarioLatLngLng = `${resp.coords.longitude}`;
    console.log(resp.coords.latitude+" - "+resp.coords.longitude);
    sessionStorage.lat = resp.coords.latitude;
    sessionStorage.lng = resp.coords.longitude;
  }).catch((error) => {
    console.log('Error getting lat', error);
  });

}





  
}
