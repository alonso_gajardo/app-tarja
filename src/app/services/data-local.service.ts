import { Injectable } from '@angular/core';
import {Registro} from '../models/registro.model';
import {Storage} from '@ionic/storage';
import { FuncionesService } from 'src/app/services/funciones.service';


@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  guardados:Registro[] = [];
  funcionesService: FuncionesService;


  constructor(private storage: Storage) { 
/*
    this.storage.get('registros')
    .then(registros =>{
      this.guardados = registros || [];
    });*/
//cargar registros
    this.cargarStorage();

  }

  async cargarStorage(){
this.guardados=(await this.storage.get('registros')) || [];
  }



  async guardarRegistro( format: string, text: string, idlabor:string, estado: string){


const created = this.fecha_hora_actual();
const estado1 = "1";

    await this.cargarStorage();
    const nuevoRegistro = new Registro(format, text, created, idlabor, estado1);
    this.guardados.unshift(nuevoRegistro);
    console.log(this.guardados);

    this.storage.set('registros',this.guardados);
    
  }






limpiar_formulario(){
  this.guardados = [];
}



  async borrarRegistro(index){

    await this.cargarStorage();

    this.guardados.splice(index,1);
    this.storage.set('registros',this.guardados);

  }




  fecha_hora_actual(){
    //fecha
    var x = "";
    var x2 = "";
    var x3 = "";
  
    var tiempo = new Date();
    var hora = tiempo.getHours();
    var minuto = tiempo.getMinutes();
    var segundo = tiempo.getSeconds();
  
    if (hora <= 9) { x = "0"; }
    if (minuto <= 9) { x2 = "0"; }
    if (segundo <= 9) { x3 = "0"; }
  
    var hora_actual = x + hora + ":" + x2 + minuto + ":" + x3 + segundo;
  
    //hora
      var fecha_1 = "";
      var fecha_2 = "";
  
      var fecha_actual = new Date(); //fecha de hoy
      var a = fecha_actual.getMonth() + 1; //sacar mes
      var d = fecha_actual.getDate(); //sacar dias
  
      if (a <= 9) { fecha_1 = "0"; }
  
      if (d <= 9) { fecha_2 = "0"; }
  
      //res_x =  fecha_actual.getFullYear() + fecha_1 + (fecha_actual.getMonth() + 1 )  + fecha_2 + fecha_actual.getDate();
      var fecha = fecha_actual.getFullYear() + "-" + fecha_1 + (fecha_actual.getMonth() + 1) + "-" + fecha_2 + fecha_actual.getDate();
  
      var res = fecha+" "+hora_actual;
      return res;
  }



}



