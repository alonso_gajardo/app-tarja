import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalRest } from './global';


@Injectable({
  providedIn: 'root'
})
export class DBrutUsuariosService {


  public urlrest: string;

  //url ='https://www.digital5.cl/php_generis/restfull/index.php/';

  constructor(private http: HttpClient) {
    this.urlrest = GlobalRest.url;
  }


  //GET FUNCIONA 100%
  getRut(): Observable<any> {
    return this.http.get(this.urlrest + 'Registros_rut_usuarios/registros_rut_usuarios_all');
  }


  //INSERT FUNCIONA 100%
  CrearPostRutUsuarios(dbrut): Observable<any> {
    //console.log(dblatLong);
    let params = JSON.stringify(dbrut);
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let Headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(this.urlrest + 'Registros_rut_usuarios/registros_rut_usuarios_insertar', params, { headers: Headers });
  }


  //GET FUNCIONA 100%
  getRutUsuarios(rut): Observable<any> {
    return this.http.get(this.urlrest + 'Registros_rut_usuarios/registros_rut_usuarios_por_rut/' + rut);
  }

  //validar rut FUNCIONA 100%
  ValidarPostRut(dbrut): Observable<any> {
    console.log(dbrut);
    console.log("check db")
    let params = JSON.stringify(dbrut);
    console.log(params);
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let Headers = new HttpHeaders().set('Content-Type', 'application/json');
    let RESULT = this.http.post(this.urlrest + 'Registros_rut_usuarios/registros_rut_validar', params, { headers: Headers })
    console.log(RESULT)
    return RESULT;
  }



  //INSERT FUNCIONA 100%
  ModificarPostRutUsuarios(dbrut): Observable<any> {
    //console.log(dblatLong);
    let params = JSON.stringify(dbrut);
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let Headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(this.urlrest + 'Registros_rut_usuarios/registros_rut_usuarios_modificar', params, { headers: Headers });
  }





}