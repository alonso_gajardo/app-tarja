import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalRest } from './global';


@Injectable({
  providedIn: 'root'
})
export class DBmedidaService {


  public urlrest: string;

//url ='https://www.digital5.cl/php_generis/restfull/index.php/';

  constructor(private http: HttpClient) { 
    this.urlrest= GlobalRest.url;
  }


  //GET FUNCIONA 100%
  getMedida():Observable<any>{
return this.http.get(this.urlrest+'UnidadMedida/unidadmedida_all');
  }

  
   //INSERT FUNCIONA 100%
  CrearPostMedida(dbmedida):Observable<any>{
    //console.log(dblatLong);
    let params = JSON.stringify(dbmedida);
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let Headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.urlrest+'UnidadMedida/unidadmedida_insertar',params,{headers: Headers});
  }


}