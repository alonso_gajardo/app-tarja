
import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage';
import { RegistroOT } from '../models/ot.model';

@Injectable({
  providedIn: 'root'
})
export class DataLocalLaboresOT {

  guardadosOT: RegistroOT[] =[];

  constructor(public storage: Storage) { 
/*
    this.storage.get('registros')
    .then(registros =>{
      this.guardados = registros || [];
    });*/





//cargar registros
    this.cargarStorageOT();

  }

  async cargarStorageOT(){
this.guardadosOT=(await this.storage.get('registrosOT')) || [];
  }

  async guardarRegistroOT( idOT: string,usuarioRut: string,nombreLabor: string,tipoLabor: string,contratista: string,trato: string,estadoOT: string,descripcionLabor: string,fechaOT: Date,grupoRut: string,grupoNumero: string){

    await this.cargarStorageOT();


    const nuevoRegistroOT = new RegistroOT(idOT,usuarioRut,nombreLabor,tipoLabor,contratista,trato,estadoOT,descripcionLabor,fechaOT,grupoRut,grupoNumero);

    //ordena y deja el ultimo ingresado en primer lugar
    this.guardadosOT.unshift(nuevoRegistroOT);

    this.storage.set('registrosOT',this.guardadosOT);
  }


}