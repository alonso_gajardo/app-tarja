
import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage';
import { RegistroSobrantesQ } from '../models/sobrantesQ.model';

@Injectable({
  providedIn: 'root'
})
export class DataLocalLaboresSobrantesQ {

  guardadosSobrantesQ: RegistroSobrantesQ[] =[];

  constructor(public storage: Storage) { 
/*
    this.storage.get('registros')
    .then(registros =>{
      this.guardados = registros || [];
    });*/

//cargar registros
    this.cargarStorageSobrantesQ();
  }

  async cargarStorageSobrantesQ(){
this.guardadosSobrantesQ=(await this.storage.get('registroSobrantesQ')) || [];
  }


  async guardarRegistroSobrantesQ( 

    sobrantesQCantidad: string,
    sobrantesQNombreProducto: string,
    sobrantesQObservacion: string,
    labor_idLabor: string,
    estado: string
          ){

    await this.cargarStorageSobrantesQ();
    const nuevoRegistroSobrantesQ = new RegistroSobrantesQ(
      sobrantesQCantidad,
      sobrantesQNombreProducto,
      sobrantesQObservacion,
      labor_idLabor,
      estado
        );

    //ordena y deja el ultimo ingresado en primer lugar
    this.guardadosSobrantesQ.unshift(nuevoRegistroSobrantesQ);
    //guarda en storage el registro
    this.storage.set('registroSobrantesQ',this.guardadosSobrantesQ);
  }


}