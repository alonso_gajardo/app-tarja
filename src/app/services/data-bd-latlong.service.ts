import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalRest } from './global';


@Injectable({
  providedIn: 'root'
})
export class DBlatlongService {


  public urlrest: string;

//url ='https://www.digital5.cl/php_generis/restfull/index.php/';

  constructor(private http: HttpClient) { 
    this.urlrest= GlobalRest.url;
  }


  //GET FUNCIONA 100%
  getLatLong():Observable<any>{
return this.http.get(this.urlrest+'Registros_lat_long/registros_lat_long_all');
  }



  
   //INSERT FUNCIONA 100%
  CrearPostLatLong(dblatLong):Observable<any>{
    console.log(dblatLong);
    let params = JSON.stringify(dblatLong);
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let Headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.urlrest+'Registros_lat_long/registros_lat_long_insertar',params,{headers: Headers});
  }











}