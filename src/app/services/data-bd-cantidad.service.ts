import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalRest } from './global';


@Injectable({
  providedIn: 'root'
})
export class DBcantidadService {


  public urlrest: string;

//url ='https://www.digital5.cl/php_generis/restfull/index.php/';

  constructor(private http: HttpClient) { 
    this.urlrest= GlobalRest.url;
  }


  //GET FUNCIONA 100%
  getCantidad():Observable<any>{
return this.http.get(this.urlrest+'Registros_cantidad/registros_cantidad_all');
  }

  
   //INSERT FUNCIONA 100%
  CrearPostCantidad(dbcantidad):Observable<any>{
    //console.log(dblatLong);
    let params = JSON.stringify(dbcantidad);
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let Headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.urlrest+'Registros_cantidad/registros_cantidad_insertar',params,{headers: Headers});
  }











}