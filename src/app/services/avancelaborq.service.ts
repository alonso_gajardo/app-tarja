import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AvanceLaborQModel } from '../models/avancelaborq.model';


@Injectable({
  providedIn: 'root'
})
export class AvanceLaborQLocal{

  avanceLaborQdata: AvanceLaborQModel[] =[];

  constructor(public storage: Storage
      ) { 

//cargar registros
    this.cargarAvanceLaborQ();

  }

  async cargarAvanceLaborQ(){
this.avanceLaborQdata=(await this.storage.get('avanceLaborQLocal')) || [];
  }



  async guardarAvanceLaborQ( 

    avanceLaborQCantidad: string,
    avanceLaborQProducto: string,
    avanceLaborQBombada: string,
    avanceLaborQLat: string,
    avanceLaborQLng: string,
    avanceLaborQFecha: string,
    avanceLaborQRutTrabajador: string,
    avanceLaborQUnidadMedida: string,
    labor_idLabor: string,
    envio: string

          ){

    await this.cargarAvanceLaborQ();

    let nuevoRegistroCantidad = new AvanceLaborQModel(

        avanceLaborQCantidad,
        avanceLaborQProducto,
        avanceLaborQBombada,
        avanceLaborQLat,
        avanceLaborQLng,
        avanceLaborQFecha,
        avanceLaborQRutTrabajador,
        avanceLaborQUnidadMedida,
        labor_idLabor,
        envio
        );


  //      this.avanceLaborQdata.push(nuevoRegistroCantidad);
//this.storage.set('avanceLaborGLocal',this.avanceLaborQdata);


    //ordena y deja el ultimo ingresado en primer lugar
    this.avanceLaborQdata.unshift(nuevoRegistroCantidad);
    this.storage.set('avanceLaborQLocal',this.avanceLaborQdata);

  }


}