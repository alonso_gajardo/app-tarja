import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AvanceLaborGModel } from '../models/avancelaborg.model';


@Injectable({
  providedIn: 'root'
})
export class AvanceLaborGLocal{

  avanceLaborGdata: AvanceLaborGModel[] =[];

  constructor(public storage: Storage
      ) { 

//cargar registros
    this.cargarAvanceLaborG();

  }

  async cargarAvanceLaborG(){
this.avanceLaborGdata=(await this.storage.get('avanceLaborGLocal')) || [];
  }



  async guardarAvanceLaborG( 

avanceLaborGCantidad:string,
avanceLaborGRutTrabajador:string,
avanceLaborGObjetivoLabor:string,
avanceLaborGLat:string,
avanceLaborGLng:string,
avanceLaborGFecha:string,
avanceLaborGCantGrupo:string,
avanceLaborGFormato:string,
labor_idLabor:string,
envio:string
          ){

    await this.cargarAvanceLaborG();

    let nuevoRegistroCantidad = new AvanceLaborGModel(

avanceLaborGCantidad,
avanceLaborGRutTrabajador,
avanceLaborGObjetivoLabor,
avanceLaborGLat,
avanceLaborGLng,
avanceLaborGFecha,
avanceLaborGCantGrupo,
avanceLaborGFormato,
labor_idLabor,
envio

        );


  //this.avanceLaborGdata.push(nuevoRegistroCantidad);
//this.storage.set('avanceLaborGLocal',this.avanceLaborGdata);


    //ordena y deja el ultimo ingresado en primer lugar
    this.avanceLaborGdata.unshift(nuevoRegistroCantidad);
    this.storage.set('avanceLaborGLocal',this.avanceLaborGdata);

  }


}