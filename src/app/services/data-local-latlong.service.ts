
import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage';
import { RegistroLatLong } from '../models/latLong.model';

@Injectable({
  providedIn: 'root'
})
export class DataLocalLaboresLatLong {

  guardadosLatLong: RegistroLatLong[] =[];

  constructor(public storage: Storage) { 
/*
    this.storage.get('registros')
    .then(registros =>{
      this.guardados = registros || [];
    });*/

//cargar registros
    this.cargarStorageLatLong();
  }

  async cargarStorageLatLong(){
this.guardadosLatLong=(await this.storage.get('registrosLatLong')) || [];
  }


  async guardarRegistroLatLong( 
    //idgpsUsuarioLatLng: string,
    gpsUsuarioLatLngLat: string,
    gpsUsuarioLatLngLng: string,
    gpsUsuarioLatLngFecha: string,
    gpsUsuarioLatLngProcedencia: string,
    usuario_idUsuario: string,
    estadoLatLong:string
          ){

    await this.cargarStorageLatLong();
    const nuevoRegistroLatLong = new RegistroLatLong(
      //idgpsUsuarioLatLng,
      gpsUsuarioLatLngLat,
      gpsUsuarioLatLngLng,
      gpsUsuarioLatLngFecha,
      gpsUsuarioLatLngProcedencia,
      usuario_idUsuario,
      estadoLatLong
        );

    //ordena y deja el ultimo ingresado en primer lugar
    this.guardadosLatLong.unshift(nuevoRegistroLatLong);
    //guarda en storage el registro
    this.storage.set('registrosLatLong',this.guardadosLatLong);
  }


}