import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ProductosQModel } from '../models/productosQ.model';



@Injectable({
  providedIn: 'root'
})
export class ProductosQLocal{

  productosQdata: ProductosQModel[] =[];

  constructor(
      public storage: Storage
      ) { 

//cargar registros
    this.cargarProductosQ();

  }

  async cargarProductosQ(){
this.productosQdata=(await this.storage.get('productosQLocal')) || [];
  }


  async guardarProductosQ( 

    productosQCantidad: string,
    productosQNombre: string,
    labor_idLabor: string,
    unidadmedida_idUnidadMedida: string,
    idFecha: string,
    estado: string

          ){

    await this.cargarProductosQ();

    const nuevoRegistroProductosQ = new ProductosQModel(

        productosQCantidad,
        productosQNombre,
        labor_idLabor,
        unidadmedida_idUnidadMedida,
        idFecha,
        estado
        );


 // this.productosQdata.push(nuevoRegistroProductosQ);
//this.storage.set('avanceLaborGLocal',this.avanceLaborQdata);


    //ordena y deja el ultimo ingresado en primer lugar
   this.productosQdata.unshift(nuevoRegistroProductosQ);
   this.storage.set('productosQLocal',this.productosQdata);
  }


}



