import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalRest } from './global';


@Injectable({
  providedIn: 'root'
})
export class ProductosQDB {


  public urlrest: string;

//url ='https://www.digital5.cl/php_generis/restfull/index.php/';

  constructor(private http: HttpClient) { 
    this.urlrest= GlobalRest.url;
  }


  //GET FUNCIONA 100%
  getProductosQDB():Observable<any>{
return this.http.get(this.urlrest+'Productos/productos_all');
  }
  
/*
    //GET FUNCIONA 100%
    getLaboresXidCliente(idCliente):Observable<any>{
      return this.http.get(this.urlrest+'Registros_labores/registros_labores_x_idcliente/'+idCliente);
        }
*/
  
   //INSERT FUNCIONA 100%
  CrearPostProductosQDB(data):Observable<any>{
    console.log(data);
    let params = JSON.stringify(data);
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let Headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.urlrest+'Productos/productos_insertar/',params,{headers: Headers});
  }

/*
  //CREAR UN ACTUALIZADOR DE LABORES
  //para los datos que se modifican con el cierre de labor, ya que son datos extras que van en la tabla LABOR   registros_labores_modificar

  UpdatePostLabores(dblabores):Observable<any>{
    //console.log(dblatLong);
    let params = JSON.stringify(dblabores);
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let Headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.urlrest+'Registros_labores/registros_labores_modificar',params,{headers: Headers});
  }
  */

}