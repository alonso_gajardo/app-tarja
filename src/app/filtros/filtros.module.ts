import { NgModule } from '@angular/core';
import { FiltrootPipe } from './filtroot.pipe';

@NgModule({
  declarations: [FiltrootPipe],
  exports:[FiltrootPipe]
})
export class FiltrosModule { }
