import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import "rxjs/add/operator/map";
import "rxjs/add/operator/timeout";

@Injectable()
export class Proseso {
  // url para la conecion de la api
  // server: string = "http://192.168.0.106/phpionic/versiones/servisioradio/";
  server: string = "http://192.168.1.99/api/php_generis/prueva/";

  constructor(public http: HttpClient) {}

  postData(body, file) {
    let headers = new HttpHeaders({
      "Content-Type": "application/json; charset=UTF-8"
    });
    let options = {
      headers: headers
    };
    return this.http
      .post(this.server + file, JSON.stringify(body), options)
      .timeout(59000) // 59 segundos
      .map(res => res);
  }
}
