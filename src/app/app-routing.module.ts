import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'inicio', loadChildren: './pages/inicio/inicio.module#InicioPageModule' },
  { path: 'alert', loadChildren: './pages/alert/alert.module#AlertPageModule' },
  { path: 'action-sheet', loadChildren: './pages/action-sheet/action-sheet.module#ActionSheetPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'genera-apq', loadChildren: './pages/genera-apq/genera-apq.module#GeneraAPQPageModule' },
  { path: 'apq-interna', loadChildren: './pages/apq-interna/apq-interna.module#ApqInternaPageModule' },
  { path: 'apq-contratista', loadChildren: './pages/apq-contratista/apq-contratista.module#ApqContratistaPageModule' },
  { path: 'genera', loadChildren: './pages/genera/genera.module#GeneraPageModule' },
  { path: 'genera-generales', loadChildren: './pages/genera-generales/genera-generales.module#GeneraGeneralesPageModule' },
  { path: 'generales-interna', loadChildren: './pages/generales-interna/generales-interna.module#GeneralesInternaPageModule' },
  { path: 'generales-contratista', loadChildren: './pages/generales-contratista/generales-contratista.module#GeneralesContratistaPageModule' },
  { path: 'entrada', loadChildren: './pages/entrada/entrada.module#EntradaPageModule' },
  { path: 'labor-general', loadChildren: './pages/labor-general/labor-general.module#LaborGeneralPageModule' },
  { path: 'aplicacion-quimica', loadChildren: './pages/aplicacion-quimica/aplicacion-quimica.module#AplicacionQuimicaPageModule' },
  //{ path: 'principal', loadChildren: './pages/principal/principal.module#PrincipalPageModule' },
  { path: 'playpause', loadChildren: './pages/playpause/playpause.module#PlaypausePageModule' },
  { path: 'listado-labores', loadChildren: './pages/listado-labores/listado-labores.module#ListadoLaboresPageModule' },
  { path: 'ingreso-general', loadChildren: './pages/ingreso-general/ingreso-general.module#IngresoGeneralPageModule' },
  {
    path: 'aplicacion-quimica',
    loadChildren: () => import('./pages/aplicacion-quimica/aplicacion-quimica.module').then( m => m.AplicacionQuimicaPageModule)
  },
  {
    path: 'ingreso-quimica',
    loadChildren: () => import('./pages/ingreso-quimica/ingreso-quimica.module').then( m => m.IngresoQuimicaPageModule)
  },
  {
    path: 'modal',
    loadChildren: () => import('./pages/modal/modal.module').then( m => m.ModalPageModule)
  },  {
    path: 'lovi',
    loadChildren: () => import('./pages/lovi/lovi.module').then( m => m.LoviPageModule)
  },
  {
    path: 'lislabor',
    loadChildren: () => import('./pages/lislabor/lislabor.module').then( m => m.LislaborPageModule)
  },
  {
    path: 'lislabor2',
    loadChildren: () => import('./pages/lislabor2/lislabor2.module').then( m => m.Lislabor2PageModule)
  },
  {
    path: 'lislabor3',
    loadChildren: () => import('./pages/lislabor3/lislabor3.module').then( m => m.Lislabor3PageModule)
  },
  {
    path: 'lislabor1',
    loadChildren: () => import('./pages/lislabor1/lislabor1.module').then( m => m.Lislabor1PageModule)
  },






];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
