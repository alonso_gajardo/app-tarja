import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';

import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Proseso } from "./provedor/proseso";
//import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Geolocation, Geoposition } from "@ionic-native/geolocation/ngx";
import { FuncionesService } from 'src/app/services/funciones.service';
import { DataLocalLaboresLatLong } from 'src/app/services/data-local-latlong.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {




  constructor(

    public platform: Platform,
    private statusBar: StatusBar,
    private network: Network,
    private prosecnt: Proseso,
    private geolocation: Geolocation,
    public funcionesService: FuncionesService,
    private latLong: DataLocalLaboresLatLong,
    //public splashScreen: SplashScreen
  ) {
    this.initializeApp();


/*
    setInterval(() => {



      this.geolocation.getCurrentPosition().then((resp) => {
        // resp.coords.latitude
        // resp.coords.longitude
        //const idgpsUsuarioLatLng = "";
        const gpsUsuarioLatLngLat = `${resp.coords.latitude}`;
        const gpsUsuarioLatLngLng = `${resp.coords.longitude}`;
        const gpsUsuarioLatLngFecha = this.funcionesService.fecha_hora_actual();

        const usuario_idUsuario = sessionStorage.idUsuario;
        const estadoLatLong = "1";


      }).catch((error) => {
        console.log('Error getting location', error);
      });
    }, 60000);

*/
  }





  initializeApp() {
    /*
    
        this.platform.ready().then(() => {
          this.statusBar.styleDefault();
          setTimeout(() => {
          //this.splashScreen.hide();
        }, 10000);
        });
    
     */


    this.platform.ready().then(() => {
      this.statusBar.styleDefault();

    });

    this.network.onDisconnect().subscribe(() => {
      console.log(this.network.type + ' tipo de conexion3');
      if (this.network.type === 'none') {
        alert('Sin conexion');
      }
    }
    );

    this.network.onConnect().subscribe(() => {
      //console.log(this.network.type+' tipo de conexion1');
      setTimeout(() => {
        console.log(this.network.type + ' tipo de conexion2');
      }, 3000);
    });

    //eliminar botón volver atras del Hardware
    this.platform.ready().then(() => {
      this.platform.backButton.subscribeWithPriority(9999, () => {
        document.addEventListener('backbutton', function (event) {
          event.preventDefault();
          event.stopPropagation();
          //console.log('hello');
        }, false);
      });
      this.statusBar.styleDefault();
    });



  }



}
