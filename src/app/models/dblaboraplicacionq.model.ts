export class DBlaboraplicacionqModel{
    /*
        public id_medida: number;
        public medida_nombre: string;
        public medida_estado: string;
    */
        
        public idLaborAplicacionQ: number;
        public laborAplicacionQMaquina: string;
        public laborAplicacionQMojamiento: string;
        public laborAplicacionQSuperficie: string;
        
        constructor(
        
            idLaborAplicacionQ: number,
            laborAplicacionQMaquina: string,
            laborAplicacionQMojamiento: string,
            laborAplicacionQSuperficie: string
        
                ){
                    this.idLaborAplicacionQ=idLaborAplicacionQ;
                    this.laborAplicacionQMaquina=laborAplicacionQMaquina;
                    this.laborAplicacionQMojamiento=laborAplicacionQMojamiento;
                    this.laborAplicacionQSuperficie=laborAplicacionQSuperficie;        
                }
        
        }