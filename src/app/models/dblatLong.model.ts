export class DBlatlongModel{

public gpsUsuarioLatLngLat: string;
public gpsUsuarioLatLngLng: string;
public gpsUsuarioLatLngFecha: string;
public gpsUsuarioLatLngProcedencia: string;
public usuario_idUsuario: number;

constructor(
    gpsUsuarioLatLngLat: string,
    gpsUsuarioLatLngLng: string,
    gpsUsuarioLatLngFecha: string,
    gpsUsuarioLatLngProcedencia: string,
    usuario_idUsuario: number
){

this.gpsUsuarioLatLngLat = gpsUsuarioLatLngLat;  
this.gpsUsuarioLatLngLng = gpsUsuarioLatLngLng;  
this.gpsUsuarioLatLngFecha = gpsUsuarioLatLngFecha;
this.gpsUsuarioLatLngProcedencia = gpsUsuarioLatLngProcedencia;  
this.usuario_idUsuario = usuario_idUsuario;  

}



}