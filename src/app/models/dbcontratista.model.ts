export class DBcontratistaModel{

public idContratista: number;
public contratistaRut: string;
public contratistaNombre: string;
public contratistaGiro: string;
public contratistaFono: string;
public contratistaMail: string;
public contratistaDireccion: string;
public contratistaRazonSocial: string;
public estado_idEstado: string;
public fundo_idFundo: string;

constructor(

idContratista:number,
contratistaRut:string,
contratistaNombre:string,
contratistaGiro:string,
contratistaFono:string,
contratistaMail:string,
contratistaDireccion:string,
contratistaRazonSocial:string,
estado_idEstado:string,
fundo_idFundo:string

        ){

this.idContratista=idContratista;
this.contratistaRut=contratistaRut;
this.contratistaNombre=contratistaNombre;
this.contratistaGiro=contratistaGiro;
this.contratistaFono=contratistaFono;
this.contratistaMail=contratistaMail;
this.contratistaDireccion=contratistaDireccion;
this.contratistaRazonSocial=contratistaRazonSocial;
this.estado_idEstado=estado_idEstado;
this.fundo_idFundo=fundo_idFundo;

        }

}