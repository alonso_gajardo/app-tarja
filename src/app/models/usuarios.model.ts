export class Usuarios{

    public idUsuario: string;
    public usuarioNombre: string;
    public usuarioApellidoPaterno: string;
    public usuarioApellidoMaterno: string;
    public usuarioCelularCodigo: string;
    public usuarioCelular: string;
    public usuarioEmail: string;
    public usuarioRut: string;
    public usuarioNivelPermiso: string; //A - Administrador, B - Encargado C - Agricola, D - Encargado Campo, E - Jefe Cuadrilla , F - Jefe Aplicadores, G - Trabajador
    public usuarioEstado: string; //Activo - Inactivo -  
    public usuarioCategoria: string; //aplicador quimica, operario, 
    public usuarioHistorial: string; //datos importantes
    public usuarioTipoContrato: string; //Contratado, Temporal, Trato
    public usuarioListaNegra: string; //1 en lista negra   0 sin lista negra
    public usuarioNombreCliente: string; //
    public usuarioOrigen: string; //Contratista, Local
    public usuarioIngresadoPorRut: string; //Rut de encargado que ingreso a usuario
    public usuarioFechaIngresoContrato: string; //Rut de encargado que ingreso a usuario


constructor(
    idUsuario: string,
    usuarioNombre: string,
    usuarioApellidoPaterno: string,
    usuarioApellidoMaterno: string,
    usuarioCelularCodigo: string,
    usuarioCelular: string,
    usuarioEmail: string,
    usuarioRut: string,
    usuarioNivelPermiso: string,
    usuarioEstado: string,
    usuarioCategoria: string,
    usuarioHistorial: string,
    usuarioTipoContrato: string,
    usuarioListaNegra: string,
    usuarioNombreCliente: string,
    usuarioOrigen: string,
    usuarioFechaIngresoContrato: string
    ){
    this.idUsuario=idUsuario;
    this.usuarioNombre=usuarioNombre;
    this.usuarioApellidoPaterno=usuarioApellidoPaterno;
    this.usuarioApellidoMaterno=usuarioApellidoMaterno;
    this.usuarioCelularCodigo=usuarioCelularCodigo;
    this.usuarioCelular=usuarioCelular;
    this.usuarioEmail=usuarioEmail;
    this.usuarioRut=usuarioRut;
    this.usuarioNivelPermiso=usuarioNivelPermiso;
    this.usuarioEstado=usuarioEstado;
    this.usuarioCategoria=usuarioCategoria;
    this.usuarioHistorial=usuarioHistorial;
    this.usuarioTipoContrato=usuarioTipoContrato;
    this.usuarioListaNegra=usuarioListaNegra;
    this.usuarioNombreCliente=usuarioNombreCliente;
    this.usuarioOrigen=usuarioOrigen;
    this.usuarioFechaIngresoContrato=usuarioFechaIngresoContrato;
}

}