export class ProductosQModel{

    public productosQCantidad: string;
    public productosQNombre: string;
    public labor_idLabor: string;
    public unidadmedida_idUnidadMedida: string;
    public idFecha: string;
    public estado: string;
        
        constructor(

        productosQCantidad: string,
        productosQNombre: string,
        labor_idLabor: string,
        unidadmedida_idUnidadMedida: string,
        idFecha: string,
        estado: string
    
            )
        
            {

this.productosQCantidad=productosQCantidad;
this.productosQNombre=productosQNombre;
this.labor_idLabor=labor_idLabor;
this.unidadmedida_idUnidadMedida=unidadmedida_idUnidadMedida;
this.idFecha=idFecha;
this.estado=estado;
    
            }
        
        }