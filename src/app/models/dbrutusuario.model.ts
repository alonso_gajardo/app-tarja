export class DBrutusuarioModel{



/*
public idUsuario:number;
public usuarioNombre:string;
public usuarioApellidoPaterno:string;
public usuarioApellidoMaterno:string;
public usuarioCelularCodigo:string;
public usuarioCelular:string;
public usuarioEmail:string;
public usuarioRut:string;
public usuarioNivelPermiso:string;
public usuarioEstado:string;
public usuarioCategoria:string;
public usuarioHistorial:string;
public usuarioTipoContrato:string;
public usuarioListaNegra:string;
public usuarioNombreCliente:string;
public usuarioOrigen:string;
public usuarioFechaIngreso:any;
public usuarioPass:any;
*/


public idUsuario:number;
public usuarioRut:string;
public usuarioPass:string;
public usuarioNombres:string;
public usuarioApellidoPaterno:string;
public usuarioApellidoMaterno:string;
public usuarioFono:string;
public usuarioMail:string;
public contrato_idContrato:string;
public perfil_idPerfil:string;
public dispositivo_idDispositivo:string;
public estado_idEstado:string;
public nivelAcceso_idNivelAcceso:string;
public fundo_idFundo:string;
public urlfoto:string;
public passvisible:string;
    
    
    
    constructor(
    
idUsuario:number,
usuarioRut:string,
usuarioPass:string,
usuarioNombres:string,
usuarioApellidoPaterno:string,
usuarioApellidoMaterno:string,
usuarioFono:string,
usuarioMail:string,
contrato_idContrato:string,
perfil_idPerfil:string,
dispositivo_idDispositivo:string,
estado_idEstado:string,
nivelAcceso_idNivelAcceso:string,
fundo_idFundo:string,
urlfoto:string,
passvisible:string
   
            ){

this.idUsuario = idUsuario;
this.usuarioRut = usuarioRut;
this.usuarioPass = usuarioPass;
this.usuarioNombres = usuarioNombres;
this.usuarioApellidoPaterno = usuarioApellidoPaterno;
this.usuarioApellidoMaterno = usuarioApellidoMaterno;
this.usuarioFono = usuarioFono;
this.usuarioMail = usuarioMail;
this.contrato_idContrato = contrato_idContrato;
this.perfil_idPerfil = perfil_idPerfil;
this.dispositivo_idDispositivo = dispositivo_idDispositivo;
this.estado_idEstado = estado_idEstado;
this.nivelAcceso_idNivelAcceso = nivelAcceso_idNivelAcceso;
this.fundo_idFundo = fundo_idFundo;
this.urlfoto = urlfoto;
this.passvisible = passvisible ; 


            }
    
    }