export class RegistroLabores{

    public idLabor: string;
    public laborNombre: string;
    public laborDescripcion: string;
    public laborValorTratoInicial: string;
    public laborValorTratoFinal: string;
    public laborObjetivo: string;
    public laborLat: string;
    public laborLng: string;
    public laborFecha: string;
    public laborIconoAsignacion: string;
    public laborIcono: string;
    public laborCuartel: string;
    public laborDescripcionCierre: string;
    public estado_idEstado: string;
    public tipoLabor_idTipoLabor: string;
    public laborAplicacionQ_idLaborAplicacionQ: string;
    public contratista_idContratista: string;
    public usuario_idUsuario: string;


constructor(
    idLabor:string,
    laborNombre: string,
    laborDescripcion: string,
    laborValorTratoInicial: string,
    laborValorTratoFinal: string,
    laborObjetivo: string,
    laborLat: string,
    laborLng: string,
    laborFecha: string,
    laborIconoAsignacion: string,
    laborIcono: string,
    laborCuartel: string,
    laborDescripcionCierre: string,
    estado_idEstado: string,
    tipoLabor_idTipoLabor: string,
    laborAplicacionQ_idLaborAplicacionQ: string,
    contratista_idContratista: string,
    usuario_idUsuario: string
    
    ){

this.idLabor = idLabor;
this.laborNombre = laborNombre;
this.laborDescripcion = laborDescripcion;
this.laborValorTratoInicial = laborValorTratoInicial;
this.laborValorTratoFinal = laborValorTratoFinal;
this.laborObjetivo = laborObjetivo;
this.laborLat = laborLat;
this.laborLng = laborLng;
this.laborFecha = laborFecha;
this.laborIconoAsignacion = laborIconoAsignacion;
this.laborIcono = laborIcono;
this.laborCuartel = laborCuartel;
this.laborDescripcionCierre = laborDescripcionCierre;
this.estado_idEstado = estado_idEstado;
this.tipoLabor_idTipoLabor = tipoLabor_idTipoLabor;
this.laborAplicacionQ_idLaborAplicacionQ = laborAplicacionQ_idLaborAplicacionQ;
this.contratista_idContratista = contratista_idContratista;
this.usuario_idUsuario = usuario_idUsuario;


}


}