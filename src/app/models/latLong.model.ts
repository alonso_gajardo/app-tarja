export class RegistroLatLong{
/*
    public idLatLong: string;
    public latitud: string;
    public longitud: string;
    public fechaLatLong: string;
    public estadoLatLong: string;
    public accionLatLong: string;
    public idUsuario: string;
*/
    public gpsUsuarioLatLngLat: string;
    public gpsUsuarioLatLngLng: string;
    public gpsUsuarioLatLngFecha: string;
    public gpsUsuarioLatLngProcedencia: string;
    public usuario_idUsuario: string;
    public estadoLatLong: string;


constructor(
    gpsUsuarioLatLngLat: string,
    gpsUsuarioLatLngLng: string,
    gpsUsuarioLatLngFecha: string,
    gpsUsuarioLatLngProcedencia: string,
    usuario_idUsuario: string,
    estadoLatLong: string
){

this.gpsUsuarioLatLngLat=gpsUsuarioLatLngLat;
this.gpsUsuarioLatLngLng=gpsUsuarioLatLngLng;
this.gpsUsuarioLatLngFecha=gpsUsuarioLatLngFecha;
this.gpsUsuarioLatLngProcedencia=gpsUsuarioLatLngProcedencia;
this.usuario_idUsuario=usuario_idUsuario;
this.estadoLatLong = estadoLatLong;

}


}