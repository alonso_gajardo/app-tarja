export class ProductosQFusionModel{

public rut: string;
public idLabor: string;
public nombre: string;
public apellido: string;
public formatoEntrada: string;
public celular: string;
public idFecha: string;
public productosQCantidad: string;
public productosQNombre: string;
public unidadmedida_idUnidadMedida: string;
public estado: string;
        
        constructor(

rut: string,
idLabor: string,
nombre: string,
apellido: string,
formatoEntrada: string,
celular: string,
idFecha: string,
productosQCantidad: string,
productosQNombre: string,
unidadmedida_idUnidadMedida: string,
estado: string
    
            )
        
            {

this.rut=rut;
this.idLabor=idLabor;
this.nombre=nombre;
this.apellido=apellido;
this.formatoEntrada=formatoEntrada;
this.celular=celular;
this.idFecha=idFecha;
this.productosQCantidad=productosQCantidad;
this.productosQNombre=productosQNombre;
this.unidadmedida_idUnidadMedida=unidadmedida_idUnidadMedida;
this.estado=estado;

            }
        
        }