export class AvanceLaborGModel{

public avanceLaborGCantidad: string;
public avanceLaborGRutTrabajador: string;
public avanceLaborGObjetivoLabor: string;
public avanceLaborGLat: string;
public avanceLaborGLng: string;
public avanceLaborGFecha: string;
public avanceLaborGCantGrupo: string;
public avanceLaborGFormato: string;
public labor_idLabor: string;
public envio: string;

constructor(

avanceLaborGCantidad: string,
avanceLaborGRutTrabajador: string,
avanceLaborGObjetivoLabor: string,
avanceLaborGLat: string,
avanceLaborGLng: string,
avanceLaborGFecha: string,
avanceLaborGCantGrupo: string,
avanceLaborGFormato: string,
labor_idLabor: string,
envio:string
    )

    {
//this.idAvanceLaborG = idAvanceLaborG; 
this.avanceLaborGCantidad = avanceLaborGCantidad; 
this.avanceLaborGRutTrabajador = avanceLaborGRutTrabajador; 
this.avanceLaborGObjetivoLabor = avanceLaborGObjetivoLabor; 
this.avanceLaborGLat = avanceLaborGLat; 
this.avanceLaborGLng = avanceLaborGLng; 
this.avanceLaborGFecha = avanceLaborGFecha; 
this.avanceLaborGCantGrupo = avanceLaborGCantGrupo; 
this.avanceLaborGFormato = avanceLaborGFormato; 
this.labor_idLabor = labor_idLabor; 
this.envio = envio; 
    }

}