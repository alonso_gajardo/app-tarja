export class RegistroOT{

    public idOT: string;
    public usuarioRut: string;
    public nombreLabor: string;
    public tipoLabor: string;
    public contratista: string;
    public trato: string;
    public estadoOT: string;
    public descripcionLabor: string;
    public fechaOT: Date;
    public grupoRut: string;
    public grupoNumero: string;



constructor(idOT: string,usuarioRut: string,nombreLabor: string,tipoLabor: string,contratista: string,trato: string,estadoOT: string,descripcionLabor: string,fechaOT: Date,grupoRut: string,grupoNumero: string){


    this.idOT=idOT;
    this.usuarioRut=usuarioRut;
    this.nombreLabor=nombreLabor;
    this.tipoLabor=tipoLabor;
    this.contratista=contratista;
    this.trato=trato;
    this.estadoOT=estadoOT;
    this.descripcionLabor=descripcionLabor;
    this.fechaOT=new Date();
    this.grupoRut=grupoRut;
    this.grupoNumero=grupoNumero;

}


}