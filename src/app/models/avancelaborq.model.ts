export class AvanceLaborQModel{

public avanceLaborQCantidad: string;
public avanceLaborQProducto: string;
public avanceLaborQBombada: string;
public avanceLaborQLat: string;
public avanceLaborQLng: string;
public avanceLaborQFecha: string;
public avanceLaborQRutTrabajador: string;
public avanceLaborQUnidadMedida: string;
public labor_idLabor: string;
public envio: string;
    
    constructor(

    avanceLaborQCantidad: string,
    avanceLaborQProducto: string,
    avanceLaborQBombada: string,
    avanceLaborQLat: string,
    avanceLaborQLng: string,
    avanceLaborQFecha: string,
    avanceLaborQRutTrabajador: string,
    avanceLaborQUnidadMedida: string,
    labor_idLabor: string,
    envio: string

        )
    
        {

this.avanceLaborQCantidad = avanceLaborQCantidad;
this.avanceLaborQProducto = avanceLaborQProducto;
this.avanceLaborQBombada = avanceLaborQBombada;
this.avanceLaborQLat = avanceLaborQLat;
this.avanceLaborQLng = avanceLaborQLng;
this.avanceLaborQFecha = avanceLaborQFecha;
this.avanceLaborQRutTrabajador = avanceLaborQRutTrabajador;
this.avanceLaborQUnidadMedida = avanceLaborQUnidadMedida;
this.labor_idLabor = labor_idLabor;
this.envio = envio;

        }
    
    }