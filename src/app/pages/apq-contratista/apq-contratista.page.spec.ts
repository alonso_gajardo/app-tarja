import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApqContratistaPage } from './apq-contratista.page';

describe('ApqContratistaPage', () => {
  let component: ApqContratistaPage;
  let fixture: ComponentFixture<ApqContratistaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApqContratistaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApqContratistaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
