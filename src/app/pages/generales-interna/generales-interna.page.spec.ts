import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralesInternaPage } from './generales-interna.page';

describe('GeneralesInternaPage', () => {
  let component: GeneralesInternaPage;
  let fixture: ComponentFixture<GeneralesInternaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralesInternaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralesInternaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
