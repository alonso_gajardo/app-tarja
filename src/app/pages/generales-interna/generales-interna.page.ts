import { Component, OnInit, Input } from '@angular/core';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';


import { DataLocalService } from '../../services/data-local.service';
import { DataLocalLaboresService } from '../../services/data-local-labores.service';
import { DataLocalLaboresOT } from '../../services/data-local-ot.service';
import { Registro } from 'src/app/models/registro.model';
import { AlertController } from '@ionic/angular';

import { Router } from '@angular/router';
import { DBrutUsuariosService } from 'src/app/services/data-db-rut-usuarios.service';




@Component({
  selector: 'app-generales-interna',
  templateUrl: './generales-interna.page.html',
  styleUrls: ['./generales-interna.page.scss']

})


export class GeneralesInternaPage implements OnInit {

  mostrar: true;
  selNumero: true;

  caja_rut: boolean = false;
  boton_rut: boolean = true;
  caja_rut_datos: boolean = false;

  datosUsuario2 = {
    rut2: ''
  };

  estado = "1";


  datosUsuario = {
    idUsuario: "",
    usuarioNombre: "",
    usuarioApellidoPaterno: "",
    usuarioApellidoMaterno: "",
    usuarioCelularCodigo: "",
    usuarioCelular: "",
    usuarioEmail: "",
    usuarioRut: "",
    usuarioNivelPermiso: "",
    usuarioEstado: "",
    usuarioCategoria: "",
    usuarioHistorial: "",
    usuarioTipoContrato: "",
    usuarioListaNegra: "",
    usuarioNombreCliente: "",
    usuarioOrigen: "",
    usuarioFechaIngreso: ""
  };

  listaNegra = "";

  @Input('entrada') entrada = "default";

  swiperOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };


  //carga al cargar la página completa
  ionViewDidEnter() {
    console.log('ionViewDidEnter');
  }

  //carga al salir de la página completa
  ionViewDidLeave() {
    console.log('ionViewDidLeave');
  }

  //carga antes de cargar la página completa
  ionViewWillEnter() {
    console.log('ionViewDidEnter');
    //this.scan();
  }

  //carga antes de salir de la página completa
  ionViewWillLeave() {
    console.log('ionViewDidLeave');
  }






  labor: any = [];
  laborOT: any;
  laborNombre: any;
  laborDescripcion: any;
  toggleEstado: boolean;
  toggleEstadoBoton: boolean;
  numero: any;

  escanearBoton: boolean;
  grupoBoton: boolean;
  manualBoton: boolean;






  constructor(
    private barcodeScanner: BarcodeScanner,
    public dataLocal: DataLocalService,
    public dataLocalLabor: DataLocalLaboresService,
    public dataLocalOT: DataLocalLaboresOT,
    public alertController: AlertController,
    private router: Router,
    private dbrutusuarios: DBrutUsuariosService
  ) {

    this.labor = [{
      "tipo": "cosechar"
    },
    {
      "tipo": "limpiar"
    },
    {
      "tipo": "ordenar"
    }];




  }









  enviarCorreo() {
    console.log('Enviando Correo...');
  }





  ngOnInit() {
    //this.laborOT = sessionStorage.usuarioNumberOT;
    this.laborNombre = sessionStorage.laboresNombre;
    this.laborDescripcion = sessionStorage.laboresDescripcion;
    this.toggleEstado = false;
    this.toggleEstadoBoton = false;
    this.numero;
    this.escanearBoton = true;
    this.grupoBoton = true;
    this.manualBoton = true;
    sessionStorage.rutValEstado = false;
  }



  ot = {
    idOT: '',
    usuarioRut: '',
    nombreLabor: '',
    tipoLabor: '',
    contratista: '',
    trato: '',
    estadoOT: '',
    descripcionLabor: '',
    fechaOT: null,
    grupoRut: '',
    grupoNumero: ''
  };


  leer_rutValEstado(){

    return sessionStorage.rutValEstado;

 }

  agregarOT() {

    this.dataLocalOT.guardarRegistroOT(this.ot.idOT, this.ot.usuarioRut, this.ot.nombreLabor, this.ot.tipoLabor, this.ot.contratista, this.ot.trato, this.ot.estadoOT, this.ot.descripcionLabor, this.ot.fechaOT, this.ot.grupoRut, this.ot.grupoNumero);

    this.ot.idOT;
    this.ot.usuarioRut;
    this.ot.nombreLabor;
    this.ot.tipoLabor;
    this.ot.contratista;
    this.ot.trato;
    this.ot.estadoOT;
    this.ot.descripcionLabor;
    this.ot.fechaOT;
    this.ot.grupoRut;
    this.ot.grupoNumero;

    console.log(this.dataLocalOT);

  }



  scan2() {
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
    }).catch(err => {
      console.log('Error', err);
    });
  }


      formato_rut(rut){
        let value = rut.replace(/\./g, '').replace('-', '');
  
        if (value.match(/^(\d{2})(\d{3}){2}(\w{1})$/)) {
          value = value.replace(/^(\d{2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4');
        }
        else if (value.match(/^(\d)(\d{3}){2}(\w{0,1})$/)) {
          value = value.replace(/^(\d)(\d{3})(\d{3})(\w{0,1})$/, '$1.$2.$3-$4');
        }
        else if (value.match(/^(\d)(\d{3})(\d{0,2})$/)) {
          value = value.replace(/^(\d)(\d{3})(\d{0,2})$/, '$1.$2.$3');
        }
        else if (value.match(/^(\d)(\d{0,2})$/)) {
          value = value.replace(/^(\d)(\d{0,2})$/, '$1.$2');
        }
        return value;
      }




  scan3(laborOT){
            //conectarse y verificar rut con lista negra rut
            var rut ="124845912";
            var estado = "1";







            let value = rut.replace(/\./g, '').replace('-', '');
  
            if (value.match(/^(\d{2})(\d{3}){2}(\w{1})$/)) {
              value = value.replace(/^(\d{2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4');
            }
            else if (value.match(/^(\d)(\d{3}){2}(\w{0,1})$/)) {
              value = value.replace(/^(\d)(\d{3})(\d{3})(\w{0,1})$/, '$1.$2.$3-$4');
            }
            else if (value.match(/^(\d)(\d{3})(\d{0,2})$/)) {
              value = value.replace(/^(\d)(\d{3})(\d{0,2})$/, '$1.$2.$3');
            }
            else if (value.match(/^(\d)(\d{0,2})$/)) {
              value = value.replace(/^(\d)(\d{0,2})$/, '$1.$2');
            }

            alert(value);



return false





            this.dbrutusuarios.getRutUsuarios(rut)
            .subscribe(
              response => {
    
    
                // console.log(response);
                //
                this.listaNegra = response;
    
                //
                var respuesta = Object.values(response.registros_rut_usuarios)
                console.log(respuesta);
                //1 ok 2lista negra
                if (respuesta[13] == '2') {
                  alert("CON ANTECEDENTES - " + respuesta[1] + " " + respuesta[2] + " " + respuesta[3] + " NO puede trabajar en la empresa está en LISTA NEGRA");
                  sessionStorage.rutValEstado = false;
                  return false;
                }
      
                if (respuesta[13] == '1') {
                  alert("SIN ANTECEDENTES OK- " + respuesta[1] + " " + respuesta[2] + " " + respuesta[3] + " SI puede trabajar en la Empresa");
                  //EVITA INGRESAR RUT DUPLICADO
                  var total = "";
                  var obj = this.dataLocal.guardados;
                  for (const prop in obj) {
      
                    console.log(`${obj[prop].text}`);
      
                    if (`${obj[prop].text}` == rut && `${obj[prop].idlabor}` == laborOT) {
      
                      alert("Este Rut ya fue ingresado: " + `${obj[prop].text}`);
                      sessionStorage.rutValEstado = false;
                      return false;
                    } else {
                      //total+=" \n "+`${obj[prop].text}`;
                    }
                  }
                  console.log("TOTAL: " + total);
                  //
      
                  //alert(this.dataLocal.guardados);
                  this.caja_rut = false;
                  this.boton_rut = true;
                  //Validar rut en BD
      
                  this.dataLocal.guardarRegistro("Manual", rut, laborOT, estado);
                }
                //
    
    
              },
              error => {
    
    
    
                //console.log(error);
                //
                console.log(error);
                console.log(error.registros_rut_usuarios);
      
                console.log(error.status);//404 rut no encontrado   // 0  sin internet
                console.log(error.statusText);//ok rut no encontrado  // Unknown Error sin internet
      
                if(error.status == 0 || error.statusText == "Unknown Error"){
                  alert("No se puede confirmar RUT. No hay conexión con el Servidor");
                  sessionStorage.rutValEstado = false;
                  return false;
                }
      
                if(error.status == 404 || error.statusText == "OK"){
      
                    //EVITA INGRESAR RUT DUPLICADO
                    alert("Este RUT es Nuevo en nuestra BD");
          
                    //GUARDAR RUT EN BD NOMBRE APELLIDO TELEFONO
                    //this.info = {fechaLatLong:1,idLatLong:2,latitud:3,longitud:4,fechaEntrada:5};
                    this.caja_rut_datos = true;
          
          
                    var total = "";
                    var obj = this.dataLocal.guardados;
                    for (const prop in obj) {
          
                      console.log(`${obj[prop].text}`);
          
                      if (`${obj[prop].text}` == rut && `${obj[prop].idlabor}` == laborOT) {
          
                        alert("Este Rut ya fue ingresado: " + `${obj[prop].text}`);
                        sessionStorage.rutValEstado = false;
                        return false;
                      } else {
                        //total+=" \n "+`${obj[prop].text}`;
                      }
                    }
                    console.log("TOTAL: " + total);
                    //
          
                    //alert(this.dataLocal.guardados);
                    this.caja_rut = false;
                    this.boton_rut = true;
                    //Validar rut en BD
          
                    this.dataLocal.guardarRegistro("Manual", rut, laborOT, estado);
          
                    console.log(error);
                    //return false;
                }
    
    
    
    
              }
            )
  
  
  /*
          if (rut === "") {
            alert("El código QR está ilegible o no corresponde a un rut válido.");
            return false;
          }
          let estado = "";
          this.dataLocal.guardarRegistro("QR_CODE2", rut, sessionStorage.usuarioNumberOT, estado);
  */
          //this.toggleEstado =true;
          //this.toggleEstadoBoton =false;
  
  
          this.escanearBoton = true;
          this.grupoBoton = false;
          this.manualBoton = true;
  }



  scan(laborOT) {

    //alert("scan");

    this.barcodeScanner.scan().then(barcodeData => {
      console.log('barcode data', barcodeData);

      if (!barcodeData.cancelled) {


        //
        var rut1 = Array.from(barcodeData.text);
        var rutOk = "";

        for (var i = 0; i <= rut1.length; i++) {
          var xx = rut1[i];

          if ((i >= 52 && i <= 62) && (xx != "=" && xx != "&" && xx != "-" && xx != "t" && xx != "<" && xx != "L")) {
            console.log(i + " - " + rut1[i]);
            rutOk += rut1[i];
            console.log(i + " - " + rut1[i] + " - " + rutOk);
          }

        }
        var rut = rutOk;
        var estado = "1";
        //conectarse y verificar rut con lista negra rut


        var rut_formateado = this.formato_rut(rut);



        //alert(rut_formateado);
        this.dbrutusuarios.getRutUsuarios(rut_formateado)
        .subscribe(
          response => {


            // console.log(response);
            //
            this.listaNegra = response;

            //
            var respuesta = Object.values(response.registros_rut_usuarios)
            console.log(respuesta);
            //1 ok 2lista negra
            if (respuesta[13] == '2') {
              alert("CON ANTECEDENTES - " + respuesta[1] + " " + respuesta[2] + " " + respuesta[3] + " NO puede trabajar en la empresa está en LISTA NEGRA");
              sessionStorage.rutValEstado = false;
              return false;
            }
  
            if (respuesta[13] == '1') {
              alert("SIN ANTECEDENTES OK- " + respuesta[1] + " " + respuesta[2] + " " + respuesta[3] + " SI puede trabajar en la Empresa");
              //EVITA INGRESAR RUT DUPLICADO
              var total = "";
              var obj = this.dataLocal.guardados;
              for (const prop in obj) {
  
                console.log(`${obj[prop].text}`);
  
                if (`${obj[prop].text}` == rut && `${obj[prop].idlabor}` == laborOT) {
  
                  alert("Este Rut ya fue ingresado: " + `${obj[prop].text}`);
                  sessionStorage.rutValEstado = false;
                  return false;
                } else {
                  //total+=" \n "+`${obj[prop].text}`;
                }
              }
              console.log("TOTAL: " + total);
              //
  
              //alert(this.dataLocal.guardados);
              this.caja_rut = false;
              this.boton_rut = true;
              //Validar rut en BD
  
              this.dataLocal.guardarRegistro("Manual", rut, laborOT, estado);
            }
            //


          },
          error => {



            //console.log(error);
            //
            console.log(error);
            console.log(error.registros_rut_usuarios);
  
            console.log(error.status);//404 rut no encontrado   // 0  sin internet
            console.log(error.statusText);//ok rut no encontrado  // Unknown Error sin internet
  
            if(error.status == 0 || error.statusText == "Unknown Error"){
              alert("No se puede confirmar RUT. No hay conexión con el Servidor");
              sessionStorage.rutValEstado = false;
              return false;
            }
  
            if(error.status == 404 || error.statusText == "OK"){
  
                //EVITA INGRESAR RUT DUPLICADO
                alert("Este RUT es Nuevo en nuestra BD");
      
                //GUARDAR RUT EN BD NOMBRE APELLIDO TELEFONO
                //this.info = {fechaLatLong:1,idLatLong:2,latitud:3,longitud:4,fechaEntrada:5};
                this.caja_rut_datos = true;
      
      
                var total = "";
                var obj = this.dataLocal.guardados;
                for (const prop in obj) {
      
                  console.log(`${obj[prop].text}`);
      
                  if (`${obj[prop].text}` == rut && `${obj[prop].idlabor}` == laborOT) {
      
                    alert("Este Rut ya fue ingresado: " + `${obj[prop].text}`);
                    sessionStorage.rutValEstado = false;
                    return false;
                  } else {
                    //total+=" \n "+`${obj[prop].text}`;
                  }
                }
                console.log("TOTAL: " + total);
                //
      
                //alert(this.dataLocal.guardados);
                this.caja_rut = false;
                this.boton_rut = true;
                //Validar rut en BD
      
                this.dataLocal.guardarRegistro("Manual", rut, laborOT, estado);
      
                console.log(error);
                //return false;
            }




          }
        )


/*
      if (rut === "") {
        alert("El código QR está ilegible o no corresponde a un rut válido.");
        return false;
      }
      let estado = "";
      this.dataLocal.guardarRegistro("QR_CODE2", rut, sessionStorage.usuarioNumberOT, estado);
*/
      //this.toggleEstado =true;
      //this.toggleEstadoBoton =false;


      this.escanearBoton = true;
      this.grupoBoton = false;
      this.manualBoton = true;

      }

    }).catch(err => {


      /*
         //
         var rutOk='https://www.digital5.cl';
         var total="";
         var obj = this.dataLocal.guardados;
         for(const prop in obj){
         
           console.log(`${obj[prop].text}`);
         
         
             if(`${obj[prop].text}`==rutOk && `${obj[prop].idlabor}`==sessionStorage.usuarioNumberOT){
         
               alert("Este Rut ya fue ingresado: "+`${obj[prop].text}`);
             return false;
             }else{
               //total+=" \n "+`${obj[prop].text}`;
             }
         
         
         }
         console.log("TOTAL: "+ total);
      //
      */


      console.log('Error', err);
      //let estado = "";
      alert("Error al escanear: " + err);
      //this.dataLocal.guardarRegistro('QRCode', 'https://www.digital5.cl', sessionStorage.usuarioNumberOT, estado);
      this.toggleEstado = true;
      this.toggleEstadoBoton = false;

      this.escanearBoton = true;
      this.grupoBoton = false;
      this.manualBoton = false;

    });
  }



  abrirRegistro(registro) {

    alert(
      "OT :" +
      registro.idlabor
      + "\nR.U.T :" +
      registro.text


    );
    console.log('Registro: ', registro);
  }


  cerrarLabor(index: number) {

    //alert(index);
    //1 = activo 2= inactivo
    //this.dataLocalLabor.splice(index, 4);

    //console.log(this.dataLocalLabor);
    this.dataLocal.borrarRegistro(index);


  }


  /*
  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      header: 'Ingreso Manual de Rut',
      inputs: [
        {
          name: 'name1',
          type: 'text',
          placeholder: 'Ingrese Rut:'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (aa) => {
            var estado ="";
            console.log('Confirm Ok '+aa.name1);
            this.dataLocal.guardarRegistro(
              "Manual", 
              aa.name1, 
              this.laborOT,
              estado
               );
               this.toggleEstado =true;
               this.toggleEstadoBoton =false;
  
               this.escanearBoton =true;
               this.grupoBoton =false;
               this.manualBoton =true;
               
          }
        }
      ]
    });
  
    await alert.present();
  }
  */

  async guardarRegistroNgrupo() {
    var estado = "";
    this.dataLocal.guardarRegistro(
      "Grupo",
      this.numero,
      this.laborOT,
      estado
    );
    //    this.toggleEstado =true;
    //   document.getElementById('botonNumero').style.display="none";
    this.numero = null;
    //   this.toggleEstado =true;
    //   this.toggleEstadoBoton =true;


    var escanearBoton = true;
    var grupoBoton = true;
    var manualBoton = true;

    this.volver_operarios(escanearBoton, grupoBoton, manualBoton);
  }

  volver_operarios(x1, x2, x3) {

    this.dataLocal.guardados = [];

    this.escanearBoton = x1;
    this.grupoBoton = x2;
    this.manualBoton = x3;

    this.router.navigate(['/entrada']);

  }

  volver() {
    this.router.navigate(['/entrada']);

  }




  validar_rut() {
    this.toggleEstado = true;
    this.toggleEstadoBoton = false;

    this.escanearBoton = true;
    this.grupoBoton = false;
    this.manualBoton = true;

    this.caja_rut = true;
    this.boton_rut = false;
  }

  ingresar_rut(manual, laborOT, estado) {
        
    var rut = "";
    // alert(this.datosUsuario2.rut2);
    // alert(manual+" "+rut2+" "+laborOT+" "+estado);
    this.toggleEstado = true;
    this.toggleEstadoBoton = false;

    this.escanearBoton = true;
    this.grupoBoton = false;
    this.manualBoton = true;

    if (this.datosUsuario2.rut2) {
      rut = this.datosUsuario2.rut2;
    } else {
      alert("Ingrese un Rut válido");
      return false
    }

    //validar Rut con BD lista negra
    //conectarse y verificar rut con lista negra rut
    this.dbrutusuarios.getRutUsuarios(rut)
      .subscribe(
        response => {

          this.listaNegra = response;

          //
          var respuesta = Object.values(response.registros_rut_usuarios)
          console.log(respuesta);
          //1 ok 2lista negra
          if (respuesta[13] == '2') {
            alert("CON ANTECEDENTES - " + respuesta[1] + " " + respuesta[2] + " " + respuesta[3] + " NO puede trabajar en la empresa está en LISTA NEGRA");
            sessionStorage.rutValEstado = false;
            return false;
          }

          if (respuesta[13] == '1') {
            alert("SIN ANTECEDENTES OK- " + respuesta[1] + " " + respuesta[2] + " " + respuesta[3] + " SI puede trabajar en la Empresa");
            //EVITA INGRESAR RUT DUPLICADO
            var total = "";
            var obj = this.dataLocal.guardados;
            for (const prop in obj) {

              console.log(`${obj[prop].text}`);

              if (`${obj[prop].text}` == rut && `${obj[prop].idlabor}` == laborOT) {

                alert("Este Rut ya fue ingresado: " + `${obj[prop].text}`);
                sessionStorage.rutValEstado = false;
                return false;
              } else {
                //total+=" \n "+`${obj[prop].text}`;
              }
            }
            console.log("TOTAL: " + total);
            //

            //alert(this.dataLocal.guardados);
            this.caja_rut = false;
            this.boton_rut = true;
            //Validar rut en BD

            this.dataLocal.guardarRegistro("Manual", rut, laborOT, estado);
          }

          //console.log("respuesta validador rut en BD: "+respuesta[13]);
        },
        error => {
          console.log(error);
          console.log(error.registros_rut_usuarios);

          console.log(error.status);//404 rut no encontrado   // 0  sin internet
          console.log(error.statusText);//ok rut no encontrado  // Unknown Error sin internet

          if(error.status == 0 || error.statusText == "Unknown Error"){
            alert("No se puede confirmar RUT. No hay conexión con el Servidor");
            sessionStorage.rutValEstado = false;
            return false;
          }

          if(error.status == 404 || error.statusText == "OK"){

              //EVITA INGRESAR RUT DUPLICADO
              alert("Este RUT es Nuevo en nuestra BD");
    
              //GUARDAR RUT EN BD NOMBRE APELLIDO TELEFONO
              //this.info = {fechaLatLong:1,idLatLong:2,latitud:3,longitud:4,fechaEntrada:5};
              this.caja_rut_datos = true;
    
    
              var total = "";
              var obj = this.dataLocal.guardados;
              for (const prop in obj) {
    
                console.log(`${obj[prop].text}`);
    
                if (`${obj[prop].text}` == rut && `${obj[prop].idlabor}` == laborOT) {
    
                  alert("Este Rut ya fue ingresado: " + `${obj[prop].text}`);
                  sessionStorage.rutValEstado = false;
                  return false;
                } else {
                  //total+=" \n "+`${obj[prop].text}`;
                }
              }
              console.log("TOTAL: " + total);
              //
    
              //alert(this.dataLocal.guardados);
              this.caja_rut = false;
              this.boton_rut = true;
              //Validar rut en BD
    
              this.dataLocal.guardarRegistro("Manual", rut, laborOT, estado);
    
              console.log(error);
              //return false;
          }

        })

    //

  }



  guardar_nuevo_rut_usuario(datosUsuario, rut) {
/*
    var data_cruda = {
      idUsuario: "",
      usuarioNombre: datosUsuario.usuarioNombre,
      usuarioApellidoPaterno: datosUsuario.usuarioApellidoPaterno,
      usuarioApellidoMaterno: "",
      usuarioCelularCodigo: "",
      usuarioCelular: datosUsuario.usuarioCelular,
      usuarioEmail: "",
      usuarioRut: rut,
      usuarioNivelPermiso: "",
      usuarioEstado: 1,
      usuarioCategoria: "",
      usuarioHistorial: "",
      usuarioTipoContrato: "",
      usuarioListaNegra: 1,
      usuarioNombreCliente: "",
      usuarioOrigen: "",
      usuarioFechaIngreso: "",
      usuarioPass: "12345"
    };*/


    var data_cruda = {
      idUsuario:"",
      usuarioRut:rut,
      usuarioPass:"",
      usuarioNombres:datosUsuario.usuarioNombre,
      usuarioApellidoPaterno:datosUsuario.usuarioApellidoPaterno,
      usuarioApellidoMaterno:"",
      usuarioFono:datosUsuario.usuarioCelular,
      usuarioMail:"",
      contrato_idContrato:"",
      perfil_idPerfil:"10",
      dispositivo_idDispositivo:"",
      estado_idEstado:"1",
      nivelAcceso_idNivelAcceso:"4",
      fundo_idFundo:"",
      urlfoto:"",
      passvisible:""
    };



    this.dbrutusuarios.CrearPostRutUsuarios(data_cruda).subscribe(
      response => {
        alert("Agregado a BD Exitosamente");
        console.log("Respuesta ingreso de rut usuario: " + response);
        this.caja_rut_datos = false;
        datosUsuario = null;
        data_cruda = null;
      },
      error => {
        console.log("Error ingreso de rut usuario: " + error);
      }
    )
  }
















  

}






