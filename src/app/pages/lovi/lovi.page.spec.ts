import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoviPage } from './lovi.page';

describe('LoviPage', () => {
  let component: LoviPage;
  let fixture: ComponentFixture<LoviPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoviPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoviPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
