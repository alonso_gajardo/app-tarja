import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoviPageRoutingModule } from './lovi-routing.module';

import { LoviPage } from './lovi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoviPageRoutingModule
  ],
  declarations: [LoviPage]
})
export class LoviPageModule {}
