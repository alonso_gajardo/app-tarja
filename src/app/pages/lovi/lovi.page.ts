import { Component, OnInit } from '@angular/core';
import { ToastController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { FuncionesService } from "../../services/funciones.service";
import { DataLocalLaboresLatLong } from "../../services/data-local-latlong.service";
import { DBlaboresService } from "../../services/data-bd-labores.service";
import { DataLocalLaboresService } from "../../services/data-local-labores.service";
import { RegistroLabores } from "../../models/registroLabores.model";
import { DBcontratistaOKService } from "../../services/contratista.service";
import { LoadingController, AlertController } from "@ionic/angular";
import { Router } from "@angular/router";
import { ProductosQFusionLocal } from "src/app/services/productosQFusion.service";
import { AvanceLaborQLocal } from "src/app/services/avancelaborq.service";
import { RegistroSobrantesQ } from "src/app/models/sobrantesQ.model";
import { DataLocalLaboresSobrantesQ } from "src/app/services/data-local-sobrantesQ.service";
import { DBrutUsuariosService } from "src/app/services/data-db-rut-usuarios.service";
import {Proseso} from "../../provedor/proseso";
import { AvanceLaborGLocal } from "src/app/services/avancelaborg.service";
@Component({
  selector: 'app-lovi',
  templateUrl: './lovi.page.html',
  styleUrls: ['./lovi.page.scss'],
})
export class LoviPage implements OnInit {
  botonIngresar: boolean = false;
  botonPlay: boolean = false;
  botonPausa: boolean = true;
  botonSalida: boolean = true;
  disabledIngresar: boolean = false;
  disabledPlay: boolean = true;
  disabledPausa: boolean = false;
  disabledSalida: boolean = false;
  textoIngresar: boolean = true;

  disabledEntrada: boolean = true;

  on: boolean = false;
  off: boolean = true;
  idUsuario: string = sessionStorage.idUsuario;
  geo = "GPS x Tiempo";
  lat: number;
  lon: number;
  geot2: string = "Termina de Trabajar";
  geot3: string = "Sale del Trabajo";
  proce: string = "1";
  estado: string = "1";

  guardadosLabores: RegistroLabores[] = [];

  //cargar datos de tabla arreglo local
  arregloLocal2: [] = JSON.parse(localStorage.getItem("arregloLocal"));

  labores = {
    idLabor: "",
    laborNombre: "",
    laborDescripcion: "",
    laborValorTratoInicial: "",
    laborValorTratoFinal: "",
    laborObjetivo: "",
    laborLat: "",
    laborLng: "",
    laborFecha: "",
    laborIconoAsignacion: "",
    laborIcono: "",
    laborCuartel: "",
    laborDescripcionCierre: "",
    estado_idEstado: "",
    tipoLabor_idTipoLabor: "",
    laborAplicacionQ_idLaborAplicacionQ: "",
    contratista_idContratista: "",
    usuario_idUsuario: "",
  };
  constructor(
    private toastController: ToastController,
    private router: Router,
    private storage: Storage,

    private geolocation: Geolocation,
    public funcionesService: FuncionesService,
    private latLong: DataLocalLaboresLatLong,
    private dblaboresService: DBlaboresService,
    public dataLocalLaborGeneral: DataLocalLaboresService,

    private dbcontratistaService: DBcontratistaOKService,
    public loadingController: LoadingController,

    // otra pagina

    private avancelaborglocal: AvanceLaborGLocal,

    public productosQFusion: ProductosQFusionLocal,

    public avancelaborq: AvanceLaborQLocal,
    public sobrantesq: DataLocalLaboresSobrantesQ,
    public dbrutusuarios: DBrutUsuariosService,
    public alertController: AlertController,
    private prosecnt: Proseso,
  ) {
    setInterval(() =>{
      this.agregar1();
     
        }, 15000)
        this.iniciarGeolocation();
  }

  ngOnInit() {
    this.obtener_laboresDB();

    this.obtener_contratistaDB();
  }
  enviar_playpause(x) {
    if (x == "botonIngresar") {
      this.botonIngresar = this.on;
      this.botonPlay = this.on;
      this.botonPausa = this.off;
      this.botonSalida = this.on;

      this.disabledIngresar = true;
      this.disabledPlay = false;
      this.disabledPausa = true;
      this.disabledSalida = false;

      this.disabledEntrada = true;

      this.botones_estado_latlong("ingresar");
    }

    if (x == "botonPlay") {
      this.botonIngresar = this.on;
      this.botonPlay = this.off;
      this.botonPausa = this.on;
      this.botonSalida = this.on;

      this.disabledIngresar = true;
      this.disabledPlay = false;
      this.disabledPausa = false;
      this.disabledSalida = false;

      this.disabledEntrada = false;
      this.botones_estado_latlong("play");
    }

    if (x == "botonPausa") {
      this.botonIngresar = this.on;
      this.botonPlay = this.on;
      this.botonPausa = this.off;
      this.botonSalida = this.on;

      this.disabledIngresar = true;
      this.disabledPlay = false;
      this.disabledPausa = false;
      this.disabledSalida = false;

      this.disabledEntrada = true;
      this.botones_estado_latlong("pausa");
    }

    if (x == "botonSalida") {
      this.botonIngresar = this.on;
      this.botonSalida = this.on;

      this.disabledIngresar = false;
      this.disabledPlay = true;
      this.disabledSalida = true;

      this.disabledEntrada = true;
      this.botones_estado_latlong("salida");
    }
  }

  ingreso_a_principal() {
    //this.router.navigate(['/entrada']);
    this.cargando();

    this.router.navigate(["/listado-labores"]);
  }

  botones_estado_latlong(x) {
    //CREAR LATLONG LOCAL
    if (!sessionStorage.usuarioRut) {
      console.log("No ingresó rut...");
      return false;
    }

    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        // resp.coords.latitude
        // resp.coords.longitude
        /*
        const coords =`${resp.coords.latitude},${resp.coords.longitude}`;
        const lat =`${resp.coords.latitude}`;
        const long =`${resp.coords.longitude}`;
        const fecha = this.funcionesService.fecha_hora_actual();
        const estado ="1"; //1 sin enviar   //2 enviado
        const accion =x;
        const idUsuario = sessionStorage.idUsuario;
        console.log(coords);
        */

        const gpsUsuarioLatLngLat = `${resp.coords.latitude}`;
        const gpsUsuarioLatLngLng = `${resp.coords.longitude}`;
        const gpsUsuarioLatLngFecha = this.funcionesService.fecha_hora_actual();
        const gpsUsuarioLatLngProcedencia = "lovi";
        const usuario_idUsuario = sessionStorage.idUsuario;
        const estadoLatLong = "1";

        //this.latLong.guardarRegistroLatLong(sessionStorage.usuarioRut, lat, long, fecha, estado, accion, idUsuario);
        this.latLong.guardarRegistroLatLong(
          gpsUsuarioLatLngLat,
          gpsUsuarioLatLngLng,
          gpsUsuarioLatLngFecha,
          gpsUsuarioLatLngProcedencia,
          usuario_idUsuario,
          estadoLatLong
        );
      })
      .catch((error) => {
        console.log("Error getting location", error);
      });
  }

  obtener_laboresDB() {
    //return  false;
    const arregloLocal = [];
    //alert("obtener_laboresDB");
    //DB LLAMADA a tabla LABORES x idUsuario y agregar datos en BD LOCAL 100%
    this.dblaboresService.getLaboresXidCliente(this.idUsuario).subscribe(
      (response) => {
        for (let i = 0; i < response.registros_labores.length; i++) {
          const element = response.registros_labores[i];
          // console.log(element.idLabor);

          arregloLocal.push({
            idLabor: element.idLabor,
            laborNombre: element.laborNombre,
            laborDescripcion: element.laborDescripcion,
            laborValorTratoInicial: element.laborValorTratoInicial,
            laborValorTratoFinal: element.laborValorTratoFinal,
            laborObjetivo: element.laborObjetivo,
            laborLat: element.laborLat,
            laborLng: element.laborLng,
            laborFecha: element.laborFecha,
            laborIconoAsignacion: element.laborIconoAsignacion,
            laborIcono: element.laborIcono,
            laborCuartel: element.laborCuartel,
            laborDescripcionCierre: element.laborDescripcionCierre,
            estado_idEstado: element.estado_idEstado,
            tipoLabor_idTipoLabor: element.tipoLabor_idTipoLabor,
            laborAplicacionQ_idLaborAplicacionQ:
              element.laborAplicacionQ_idLaborAplicacionQ,
            contratista_idContratista: element.contratista_idContratista,
            usuario_idUsuario: element.usuario_idUsuario,
          });
        }
        //ORDENA LA LISTA EN FORMA DESCENDENTE
        arregloLocal.sort().reverse();
        localStorage.arregloLocal = JSON.stringify(arregloLocal);
      },
      (error) => {
        console.log(error);
      }
    );
    //location.reload();
    //alert("Sincronizando con BD, esto tardará unos segundos");
    //this.router.navigate(['/entrada']);
  }

  obtener_contratistaDB() {
    const idFundo = sessionStorage.idFundo;

    //return  false;
    const contratistaLocal = [];
    //alert("obtener_laboresDB");
    //DB LLAMADA a tabla LABORES x idUsuario y agregar datos en BD LOCAL 100%
    this.dbcontratistaService.getContratista_x_fundo(idFundo).subscribe(
      (response) => {
        console.log(response);

        //return  false;

        for (let i = 0; i < response.contratistas.length; i++) {
          const element = response.contratistas[i];
          console.log(element.idContratista);

          contratistaLocal.push({
            idContratista: element.idContratista,
            contratistaRut: element.contratistaRut,
            contratistaNombre: element.contratistaNombre,
            contratistaGiro: element.contratistaGiro,
            contratistaFono: element.contratistaFono,
            contratistaMail: element.contratistaMail,
            contratistaDireccion: element.contratistaDireccion,
            contratistaRazonSocial: element.contratistaRazonSocial,
            estado_idEstado: element.estado_idEstado,
            fundo_idFundo: element.fundo_idFundo,
          });
        }
        //ORDENA LA LISTA EN FORMA DESCENDENTE
        contratistaLocal.sort().reverse();
        localStorage.contratistaLocal = JSON.stringify(contratistaLocal);
      },
      (error) => {
        console.log(error);
      }
    );
    //location.reload();
    //alert("Sincronizando con BD, esto tardará unos segundos");
    //this.router.navigate(['/entrada']);
  }

  async cargando() {
    const loading = await this.loadingController.create({
      message: "Procesando...",
      duration: 3000,
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log("Loading dismissed!");
  }


  iniciarGeolocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat =   resp.coords.latitude
      this.lon = resp.coords.longitude

        console.log(resp.coords);

        let watch = this.geolocation.watchPosition();
watch.subscribe((data) => {
 // data can be a set of coordinates, or an error (if an error occurred).
 // data.coords.latitude
 // data.coords.longitude

 this.lat =   data.coords.latitude
 this.lon = data.coords.longitude
 console.log('Watch: ', data);


});
       }).catch((error) => {
         console.log('Error getting location', error);
       });

       60000    
  }


  agregar1() {
    return new Promise((resolve) => {
      let body = {
        datos: "insertar",
        longitud: this.lon,
        latitud: this.lat,
     
        proce1: this.proce,
        user1 :this.idUsuario,
        estado1: this.estado,
       
        geo1: this.geo,
        fecha: this.funcionesService.fecha_hora_actual()
      };

      this.prosecnt.postData(body, "geo2.php").subscribe((data) => {
        console.log("OK");
      });
    });
}
}
