import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoviPage } from './lovi.page';

const routes: Routes = [
  {
    path: '',
    component: LoviPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoviPageRoutingModule {}
