import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { DataLocalLaboresService } from '../../services/data-local-labores.service';
import { FuncionesService } from 'src/app/services/funciones.service';
import { DBmedidaService } from 'src/app/services/data-bd-medida.service';
import { DBcontratistaService } from 'src/app/services/data-bd-contratista.services';


@Component({
  selector: 'app-aplicacion-quimica',
  templateUrl: './aplicacion-quimica.page.html',
  styleUrls: ['./aplicacion-quimica.page.scss'],
})
export class AplicacionQuimicaPage implements OnInit {

  pin1: boolean = false;
  pin2: boolean = false;
  pin3: boolean = false;

  opcionInterna:true;
  opcionAuto:true;

  formularioAplicacionQuimicaContratista:any;

  listaMedida: any[] = [];
  listaContratista: any[] = [];

  permiso:string;

  segmento: string;

  labores={
idLabor:'',
laborNombre:'',
laborDescripcion:'',
laborValorTratoInicial:'',
laborValorTratoFinal:'',
laborObjetivo:'',
laborLat:'',
laborLng:'',
laborFecha:'',
laborIconoAsignacion:'',
laborIcono:'',
laborCuartel:'',
laborDescripcionCierre:'',
estado_idEstado:'',
tipoLabor_idTipoLabor:'',
laborAplicacionQ_idLaborAplicacionQ:'',
contratista_idContratista:'',
usuario_idUsuario:''



  };

  today:Date;

  ionViewWillEnter(){
    sessionStorage.laboresNombre = this.labores.laborNombre;
    sessionStorage.laboresDescripcion = this.labores.laborDescripcion;
          console.log(this.labores.laborNombre+" - "+ this.labores.laborDescripcion);
  }

  constructor(
    private router: Router,
    public dataLocalAplicacionQuimica: DataLocalLaboresService ,
    public funcionesService: FuncionesService,
    public dbmedidaService: DBmedidaService,
    public dbcontratistaService: DBcontratistaService
  ) { }

  ngOnInit() {
    /*
    this.dbcontratistaService.getContratista()
    .subscribe( listaContratista => {
      console.log(listaContratista);
      this.listaContratista = listaContratista;
    } )
    */

    /*
    this.dbmedidaService.getMedida()
    .subscribe( listaMedida => {
      console.log(listaMedida);
      this.listaMedida = listaMedida;
    } )
*/

    this.permiso = sessionStorage.usuarioNivelPermiso;
  }

  guardar_nombre(nombre){
    sessionStorage.laboresNombre = nombre;
  }
  guardar_descripcion(descripcion){
    sessionStorage.laboresDescripcion = descripcion;
  }


  obtener_hora(){
    /*sacar fecha*/
    var meses = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
    var f = new Date();
    var fec = (f.getFullYear() + "" + meses[f.getMonth()] + "" +f.getDate());
    /**/
  
    /*sacar hora*/
    this.today = new Date();
    var h = this.today.getHours();
    var m = this.today.getMinutes();
    var s = this.today.getSeconds();
    var mm = this.today.getMilliseconds();
    var h1 = 0;
    var m1 = 0;
    var s1 = 0;
  
    if (h < 10) { h1 = 0 + h; } else { h1 = h; }
    if (m < 10) { m1 = 0 + m; } else { m1 = m; }
    if (s < 10) { s1 = 0 + s; } else { s1 = s; }
    var hora = h1 + ":" + m1 + ":" + s1 + ":" + mm;
    /**/
  
    return fec + "|" + hora;
  }




  obtener_iconoLabor(tipificacionLabor){

    //alert(tipificacionLabor);

    if(tipificacionLabor == "Labor General Autoasignada"){
  return "leaf-outline";
    }
  
    if(tipificacionLabor == "Labor General Interna"){
      return "leaf-outline";
    }
  
    if(tipificacionLabor == "Labor General Contratista"){
      return "leaf-outline";
    }
    
    if(tipificacionLabor == "Aplicación Química Autoasignada"){
      return "logo-react";
        }
      
        if(tipificacionLabor == "Aplicación Química Interna"){
          return "logo-react";
        }
      
        if(tipificacionLabor == "Aplicación Química Contratista"){
          return "logo-react";
        }
  }
  
  obtener_iconoAsignacion(tipificacionLabor){

    //alert(tipificacionLabor);
  
    if(tipificacionLabor == "Labor General Autoasignada"){
      return "person-outline";
        }
      
        if(tipificacionLabor == "Labor General Interna"){
          return "people-outline";
        }
      
        if(tipificacionLabor == "Labor General Contratista"){
          return "construct-outline";
        }
        
        if(tipificacionLabor == "Aplicación Química Autoasignada"){
          return "person-outline";
            }
          
            if(tipificacionLabor == "Aplicación Química Interna"){
              return "people-outline";
            }
          
            if(tipificacionLabor == "Aplicación Química Contratista"){
              return "construct-outline";
            }
  }
  
  



  ruta_aplicacionQuimicaAutoasignada(){
  
    var hora = this.obtener_hora();
    //sessionStorage.usuarioNumberOT= sessionStorage.usuarioRut+"|"+hora;
    
    //1 = activo 2= inactivo
    this.dataLocalAplicacionQuimica.guardarRegistroLabores(

this.labores.idLabor,
this.labores.laborNombre,
this.labores.laborDescripcion,
this.labores.laborValorTratoInicial,
this.labores.laborValorTratoFinal,
this.labores.laborObjetivo,
this.labores.laborLat,
this.labores.laborLng,
this.labores.laborFecha,
this.labores.laborIconoAsignacion,
this.labores.laborIcono,
this.labores.laborCuartel,
this.labores.laborDescripcionCierre,
this.labores.estado_idEstado = "1",
this.labores.tipoLabor_idTipoLabor = "4",
this.labores.laborAplicacionQ_idLaborAplicacionQ,
this.labores.contratista_idContratista,
this.labores.usuario_idUsuario
      /*
      this.labores.nombre, 
      this.labores.tipo, 
      this.labores.descripcion,  
      "1",
      this.labores.createdLabor = this.funcionesService.fecha_hora_actual(),
      this.labores.numeroOT = sessionStorage.usuarioNumberOT,
     this.labores.tipificacionLabor = "Aplicación Química Autoasignada",
  
     this.labores.nombreContratista,

     this.labores.tipoTratoValor,
     this.labores.tipoTratoUnidadMedida,
     
  
  this.labores.nombreCuartel,
  this.labores.nombreMaquina,

  this.labores.cantidadAgua,

this.labores.nombreProductoQuimico1 = this.labores.nombreProductoQuimico1,
this.labores.cantidadProductoQuimico1 = this.labores.cantidadProductoQuimico1,
this.labores.medidaProductoQuimico1 = this.labores.medidaProductoQuimico1,
this.labores.cierreSobrante1,

this.labores.nombreProductoQuimico2 = this.labores.nombreProductoQuimico2,
this.labores.cantidadProductoQuimico2 = this.labores.cantidadProductoQuimico2,
this.labores.medidaProductoQuimico2 = this.labores.medidaProductoQuimico2,
this.labores.cierreSobrante2,

this.labores.nombreProductoQuimico3 = this.labores.nombreProductoQuimico3,
this.labores.cantidadProductoQuimico3 = this.labores.cantidadProductoQuimico3,
this.labores.medidaProductoQuimico3 = this.labores.medidaProductoQuimico3,
this.labores.cierreSobrante3,

this.labores.nombreProductoQuimico4 = this.labores.nombreProductoQuimico4,
this.labores.cantidadProductoQuimico4 = this.labores.cantidadProductoQuimico4,
this.labores.medidaProductoQuimico4 = this.labores.medidaProductoQuimico4,
this.labores.cierreSobrante4,

this.labores.nombreProductoQuimico5 = this.labores.nombreProductoQuimico5,
this.labores.cantidadProductoQuimico5 = this.labores.cantidadProductoQuimico5,
this.labores.medidaProductoQuimico5 = this.labores.medidaProductoQuimico5,
this.labores.cierreSobrante5,

this.labores.nombreProductoQuimico6 = this.labores.nombreProductoQuimico6,
this.labores.cantidadProductoQuimico6 = this.labores.cantidadProductoQuimico6,
this.labores.medidaProductoQuimico6 = this.labores.medidaProductoQuimico6,
this.labores.cierreSobrante6,

this.labores.nombreProductoQuimico7 = this.labores.nombreProductoQuimico7,
this.labores.cantidadProductoQuimico7 = this.labores.cantidadProductoQuimico7,
this.labores.medidaProductoQuimico7 = this.labores.medidaProductoQuimico7,
this.labores.cierreSobrante7,

this.labores.nombreProductoQuimico8 = this.labores.nombreProductoQuimico8,
this.labores.cantidadProductoQuimico8 = this.labores.cantidadProductoQuimico8,
this.labores.medidaProductoQuimico8 = this.labores.medidaProductoQuimico8,
this.labores.cierreSobrante8,

this.labores.nombreProductoQuimico9 = this.labores.nombreProductoQuimico9,
this.labores.cantidadProductoQuimico9 = this.labores.cantidadProductoQuimico9,
this.labores.medidaProductoQuimico9 = this.labores.medidaProductoQuimico9,
this.labores.cierreSobrante9,

this.labores.cierreDescripcion,
this.labores.cierreTratoFinal,

  this.labores.iconoLabor = this.obtener_iconoLabor(this.labores.tipificacionLabor),
  this.labores.iconoAsignacion = this.obtener_iconoAsignacion(this.labores.tipificacionLabor),
  this.labores.estadoCierre="1"
*/
  

      );
    
    this.limpiar_form();
    
    console.log(this.dataLocalAplicacionQuimica);

    
    
      this.router.navigate(['/entrada']);
    }





  
  ruta_aplicacionQuimicaInterna(){
  

/*
  var hora = this.obtener_hora();

  sessionStorage.usuarioNumberOT= sessionStorage.usuarioRut+"|"+hora;
  */
  //1 = activo 2= inactivo
  this.dataLocalAplicacionQuimica.guardarRegistroLabores(
this.labores.idLabor,
this.labores.laborNombre,
this.labores.laborDescripcion,
this.labores.laborValorTratoInicial,
this.labores.laborValorTratoFinal,
this.labores.laborObjetivo,
this.labores.laborLat,
this.labores.laborLng,
this.labores.laborFecha,
this.labores.laborIconoAsignacion,
this.labores.laborIcono,
this.labores.laborCuartel,
this.labores.laborDescripcionCierre,
this.labores.estado_idEstado = "1",
this.labores.tipoLabor_idTipoLabor = "5",
this.labores.laborAplicacionQ_idLaborAplicacionQ,
this.labores.contratista_idContratista,
this.labores.usuario_idUsuario

    /*
    this.labores.nombre, 
    this.labores.tipo, 
    this.labores.descripcion,  
    "1",
    this.labores.createdLabor = this.funcionesService.fecha_hora_actual(),
    this.labores.numeroOT = sessionStorage.usuarioNumberOT,
   this.labores.tipificacionLabor="Aplicación Química Interna",

   this.labores.nombreContratista,

   this.labores.tipoTratoValor,
   this.labores.tipoTratoUnidadMedida,
   

this.labores.nombreCuartel,
this.labores.nombreMaquina,

this.labores.cantidadAgua,

this.labores.nombreProductoQuimico1 = this.labores.nombreProductoQuimico1,
this.labores.cantidadProductoQuimico1 = this.labores.cantidadProductoQuimico1,
this.labores.medidaProductoQuimico1 = this.labores.medidaProductoQuimico1,
this.labores.cierreSobrante1,

this.labores.nombreProductoQuimico2 = this.labores.nombreProductoQuimico2,
this.labores.cantidadProductoQuimico2 = this.labores.cantidadProductoQuimico2,
this.labores.medidaProductoQuimico2 = this.labores.medidaProductoQuimico2,
this.labores.cierreSobrante2,

this.labores.nombreProductoQuimico3 = this.labores.nombreProductoQuimico3,
this.labores.cantidadProductoQuimico3 = this.labores.cantidadProductoQuimico3,
this.labores.medidaProductoQuimico3 = this.labores.medidaProductoQuimico3,
this.labores.cierreSobrante3,

this.labores.nombreProductoQuimico4 = this.labores.nombreProductoQuimico4,
this.labores.cantidadProductoQuimico4 = this.labores.cantidadProductoQuimico4,
this.labores.medidaProductoQuimico4 = this.labores.medidaProductoQuimico4,
this.labores.cierreSobrante4,

this.labores.nombreProductoQuimico5 = this.labores.nombreProductoQuimico5,
this.labores.cantidadProductoQuimico5 = this.labores.cantidadProductoQuimico5,
this.labores.medidaProductoQuimico5 = this.labores.medidaProductoQuimico5,
this.labores.cierreSobrante5,

this.labores.nombreProductoQuimico6 = this.labores.nombreProductoQuimico6,
this.labores.cantidadProductoQuimico6 = this.labores.cantidadProductoQuimico6,
this.labores.medidaProductoQuimico6 = this.labores.medidaProductoQuimico6,
this.labores.cierreSobrante6,

this.labores.nombreProductoQuimico7 = this.labores.nombreProductoQuimico7,
this.labores.cantidadProductoQuimico7 = this.labores.cantidadProductoQuimico7,
this.labores.medidaProductoQuimico7 = this.labores.medidaProductoQuimico7,
this.labores.cierreSobrante7,

this.labores.nombreProductoQuimico8 = this.labores.nombreProductoQuimico8,
this.labores.cantidadProductoQuimico8 = this.labores.cantidadProductoQuimico8,
this.labores.medidaProductoQuimico8 = this.labores.medidaProductoQuimico8,
this.labores.cierreSobrante8,

this.labores.nombreProductoQuimico9 = this.labores.nombreProductoQuimico9,
this.labores.cantidadProductoQuimico9 = this.labores.cantidadProductoQuimico9,
this.labores.medidaProductoQuimico9 = this.labores.medidaProductoQuimico9,
this.labores.cierreSobrante9,

this.labores.cierreDescripcion,
this.labores.cierreTratoFinal,

this.labores.iconoLabor = this.obtener_iconoLabor(this.labores.tipificacionLabor),
this.labores.iconoAsignacion = this.obtener_iconoAsignacion(this.labores.tipificacionLabor),
this.labores.estadoCierre="1"
*/
    );
  
    this.limpiar_form();
  
  console.log(this.dataLocalAplicacionQuimica);
  
    this.router.navigate(['/apq-interna']);
  }



  ruta_aplicacionQuimicaContratista(){
  /*
    var hora = this.obtener_hora();
    sessionStorage.usuarioNumberOT= sessionStorage.usuarioRut+"|"+hora;
    */

    //1 = activo 2= inactivo
    this.dataLocalAplicacionQuimica.guardarRegistroLabores(
this.labores.idLabor,
this.labores.laborNombre,
this.labores.laborDescripcion,
this.labores.laborValorTratoInicial,
this.labores.laborValorTratoFinal,
this.labores.laborObjetivo,
this.labores.laborLat,
this.labores.laborLng,
this.labores.laborFecha,
this.labores.laborIconoAsignacion,
this.labores.laborIcono,
this.labores.laborCuartel,
this.labores.laborDescripcionCierre,
this.labores.estado_idEstado = "1",
this.labores.tipoLabor_idTipoLabor = "6",
this.labores.laborAplicacionQ_idLaborAplicacionQ,
this.labores.contratista_idContratista,
this.labores.usuario_idUsuario
/*
      this.labores.nombre, 
      this.labores.tipo, 
      this.labores.descripcion,  
      "1",
      this.labores.createdLabor = this.funcionesService.fecha_hora_actual(),
      this.labores.numeroOT = sessionStorage.usuarioNumberOT,
     this.labores.tipificacionLabor="Aplicación Química Contratista",
     
     this.labores.nombreContratista,

     this.labores.tipoTratoValor,
     this.labores.tipoTratoUnidadMedida,

     this.labores.nombreCuartel,
this.labores.nombreMaquina,

this.labores.cantidadAgua,

this.labores.nombreProductoQuimico1 = this.labores.nombreProductoQuimico1,
this.labores.cantidadProductoQuimico1 = this.labores.cantidadProductoQuimico1,
this.labores.medidaProductoQuimico1 = this.labores.medidaProductoQuimico1,
this.labores.cierreSobrante1,

this.labores.nombreProductoQuimico2 = this.labores.nombreProductoQuimico2,
this.labores.cantidadProductoQuimico2 = this.labores.cantidadProductoQuimico2,
this.labores.medidaProductoQuimico2 = this.labores.medidaProductoQuimico2,
this.labores.cierreSobrante2,

this.labores.nombreProductoQuimico3 = this.labores.nombreProductoQuimico3,
this.labores.cantidadProductoQuimico3 = this.labores.cantidadProductoQuimico3,
this.labores.medidaProductoQuimico3 = this.labores.medidaProductoQuimico3,
this.labores.cierreSobrante3,

this.labores.nombreProductoQuimico4 = this.labores.nombreProductoQuimico4,
this.labores.cantidadProductoQuimico4 = this.labores.cantidadProductoQuimico4,
this.labores.medidaProductoQuimico4 = this.labores.medidaProductoQuimico4,
this.labores.cierreSobrante4,

this.labores.nombreProductoQuimico5 = this.labores.nombreProductoQuimico5,
this.labores.cantidadProductoQuimico5 = this.labores.cantidadProductoQuimico5,
this.labores.medidaProductoQuimico5 = this.labores.medidaProductoQuimico5,
this.labores.cierreSobrante5,

this.labores.nombreProductoQuimico6 = this.labores.nombreProductoQuimico6,
this.labores.cantidadProductoQuimico6 = this.labores.cantidadProductoQuimico6,
this.labores.medidaProductoQuimico6 = this.labores.medidaProductoQuimico6,
this.labores.cierreSobrante6,

this.labores.nombreProductoQuimico7 = this.labores.nombreProductoQuimico7,
this.labores.cantidadProductoQuimico7 = this.labores.cantidadProductoQuimico7,
this.labores.medidaProductoQuimico7 = this.labores.medidaProductoQuimico7,
this.labores.cierreSobrante7,

this.labores.nombreProductoQuimico8 = this.labores.nombreProductoQuimico8,
this.labores.cantidadProductoQuimico8 = this.labores.cantidadProductoQuimico8,
this.labores.medidaProductoQuimico8 = this.labores.medidaProductoQuimico8,
this.labores.cierreSobrante8,

this.labores.nombreProductoQuimico9 = this.labores.nombreProductoQuimico9,
this.labores.cantidadProductoQuimico9 = this.labores.cantidadProductoQuimico9,
this.labores.medidaProductoQuimico9 = this.labores.medidaProductoQuimico9,
this.labores.cierreSobrante9,

this.labores.cierreDescripcion,
this.labores.cierreTratoFinal,

this.labores.iconoLabor = this.obtener_iconoLabor(this.labores.tipificacionLabor),
this.labores.iconoAsignacion = this.obtener_iconoAsignacion(this.labores.tipificacionLabor),
this.labores.estadoCierre="1"
*/
      );
    

    
      this.limpiar_form();
   
    
    console.log(this.dataLocalAplicacionQuimica);

    //this.formularioAplicacionQuimicaContratista.reset();
    
      this.router.navigate(['/apq-contratista']);
    }

    pin1value=false;
    pin2value=false;
    pin3value=false;

  toggle(): void {

//alert(this.pin1value);
    if (this.pin1value == true ) {
this.pin1 = false;
this.pin2 = true;
this.pin3 = true;
    }else{
      if (this.pin2value == true) {
        this.pin1 = true;
        this.pin2 = false;
        this.pin3 = true;
            }else{
              if (this.pin3value == true) {
                this.pin1 = true;
                this.pin2 = true;
                this.pin3 = false;
                    }else{
                      this.pin1 = false;
                      this.pin2 = false;
                      this.pin3 = false;
                    }
            }
    }
  }


  seleccionQuimica: string;



  segmentChanged(ev: any) {
    console.log('Segment changed', ev.detail.value);
    this.seleccionQuimica = ev.detail.value;



  }




  limpiar_form(){
    this.labores.idLabor="";
    this.labores.laborNombre = "",
    this.labores.laborDescripcion = "",
    this.labores.laborValorTratoInicial = "",
    this.labores.laborValorTratoFinal = "",
    this.labores.laborObjetivo = "",
    this.labores.laborLat = "",
    this.labores.laborLng = "",
    this.labores.laborFecha = "",
    this.labores.laborIconoAsignacion = "",
    this.labores.laborIcono = "",
    this.labores.laborCuartel = "",
    this.labores.laborDescripcionCierre = "",
    this.labores.estado_idEstado = "",
    this.labores.tipoLabor_idTipoLabor = "",
    this.labores.laborAplicacionQ_idLaborAplicacionQ = "",
    this.labores.contratista_idContratista = "",
    this.labores.usuario_idUsuario =""

   }



}
