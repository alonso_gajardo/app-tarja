import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataLocalService } from 'src/app/services/data-local.service';
import { Registro } from 'src/app/models/registro.model';
import { AlertController } from '@ionic/angular';
import { DataLocalLaboresService } from 'src/app/services/data-local-labores.service';
import { DataLocalLaboresOT } from 'src/app/services/data-local-ot.service';
import { DataLocalLaboresCantidad } from 'src/app/services/data-local-cantidad.service';
import { isNull } from 'util';
import { FuncionesService } from 'src/app/services/funciones.service';
import { RegistroLabores } from 'src/app/models/registroLabores.model';
import { Storage } from '@ionic/storage';
import { DBlaboresService } from 'src/app/services/data-bd-labores.service';


@Component({
  selector: 'app-entrada',
  templateUrl: './entrada.page.html',
  styleUrls: ['./entrada.page.scss']

})
export class EntradaPage implements OnInit {

  segmento: string;
  nombre: string;
  tipoTratoUnidadMedida: string;
  descripcion: string;
  listadoRegistros: boolean;
  tipificacionLabor: string;
  numeroOt: string;

  cuartel: string;
  maquina: string;

  numeroOtCantidad: string;
  rutCantidad: string;
  unidadCantidad: string;

  cantidadCantidad: string;
  cierreDescripcion: string;
  cierreTratoFinal: string;

  fechaCantidad: any;

  nombreProductoCantidad: string;
  cantidadProductoCantidad: string;
  estadoCantidad: string;

  today: any;


  nombreContratista: string;
  tipoTratoValor: string;

  cantidadAgua: string;


  nombreProductoQuimico1: string;
  nombreProductoQuimico2: string;
  nombreProductoQuimico3: string;
  nombreProductoQuimico4: string;
  nombreProductoQuimico5: string;
  nombreProductoQuimico6: string;
  nombreProductoQuimico7: string;
  nombreProductoQuimico8: string;
  nombreProductoQuimico9: string;

  medidaProductoQuimico1: string;
  medidaProductoQuimico2: string;
  medidaProductoQuimico3: string;
  medidaProductoQuimico4: string;
  medidaProductoQuimico5: string;
  medidaProductoQuimico6: string;
  medidaProductoQuimico7: string;
  medidaProductoQuimico8: string;
  medidaProductoQuimico9: string;

  cantidadProductoQuimico1: string;
  cantidadProductoQuimico2: string;
  cantidadProductoQuimico3: string;
  cantidadProductoQuimico4: string;
  cantidadProductoQuimico5: string;
  cantidadProductoQuimico6: string;
  cantidadProductoQuimico7: string;
  cantidadProductoQuimico8: string;
  cantidadProductoQuimico9: string;

  cierreSobrante1: string;
  cierreSobrante2: string;
  cierreSobrante3: string;
  cierreSobrante4: string;
  cierreSobrante5: string;
  cierreSobrante6: string;
  cierreSobrante7: string;
  cierreSobrante8: string;
  cierreSobrante9: string;
  estadoCierre: string;


  numeroBombada: string;
  litrosAguaxHectarea: string;


  itemLaboresAbiertas: boolean = true;
  on: boolean = false;
  off: boolean = true;

  botonesEntrada: boolean = true;

  selectUnidad = ["Litros", "Gramos", "Centimetros", "Onzas"];


  permiso: string;



  idUsuario: string = sessionStorage.idUsuario;

  guardadosLabores: RegistroLabores[] = [];

  //cargar datos de tabla arreglo local
  arregloLocal2: [] = JSON.parse(localStorage.getItem('arregloLocal'));


  //arregloLocal3:string[];

  //arregloLocal3.unshift(this.arregloLocal2);

  // let misDatos = JSON.parse( localStorage.getItem('arregloLocal') );
  //console.log(misDatos[0].idLabor);

  //this.arregloLocal2.unshift(JSON.parse( localStorage.getItem('arregloLocal') ));


  /*
    labores = {
      idLabor:"",
      laborNombre: "",
      laborDescripcion: "",
      laborValorTratoInicial: "",
      laborValorTratoFinal: "",
      laborObjetivo: "",
      laborLat: "",
      laborLng: "",
      laborFecha: "",
      laborIconoAsignacion: "",
      laborIcono: "",
      laborCuartel: "",
      laborDescripcionCierre: "",
      estado_idEstado: "",
      tipoLabor_idTipoLabor: "",
      laborAplicacionQ_idLaborAplicacionQ: "",
      contratista_idContratista: "",
      usuario_idUsuario: ""
    };
  */
  constructor(
    private router: Router,
    public dataLocal: DataLocalService,
    public dataLocalLabor: DataLocalLaboresService,
    public dataLocalOT: DataLocalLaboresOT,
    public alertController: AlertController,
    public dataLocalCantidad: DataLocalLaboresCantidad,
    public funcionesService: FuncionesService,
    private storage: Storage,
    private dblaboresService: DBlaboresService


  ) { }










  ngOnInit() {

    this.obtener_laboresDB();

    this.segmento = "inicial";
    this.listadoRegistros = true;

    this.botonesEntrada = true;

    this.permiso = sessionStorage.usuarioNivelPermiso;

    console.log(this.idUsuario);



  } // fin ng-oninit





  obtener_laboresLocal() {

    /*
      //LEER registrosLabores LOCAL
      let key = 'registrosLabores';
      //leer data local
      this.storage.get(key).then((val) => {
        // console.log('get ' + key + ' ', val);
    
        if (!val.length) {
          console.log("registrosLabores sin datos")
          return false;
        }
    
        if (val == null) {
          console.log("1 registrosLabores sin datos")
          return false;
        }
    
        //recorrer data local
        for (var i = 0; i < val.length; i++) {
    
            console.log(val[i].idLabor);
            arregloLocal.push({
              idLabor: val[i].idLabor,
              laborNombre: val[i].laborNombre,
              laborDescripcion: val[i].laborDescripcion,
              laborValorTratoInicial: val[i].laborValorTratoInicial,
              laborValorTratoFinal: val[i].laborValorTratoFinal,
              laborObjetivo: val[i].laborObjetivo,
              laborLat: val[i].laborLat,
              laborLng: val[i].laborLng,
              laborFecha: val[i].laborFecha,
              laborIconoAsignacion: val[i].laborIconoAsignacion,
              laborIcono: val[i].laborIcono,
              laborCuartel: val[i].laborCuartel,
              laborDescripcionCierre: val[i].laborDescripcionCierre,
              estado_idEstado: val[i].estado_idEstado,
              tipoLabor_idTipoLabor: val[i].tipoLabor_idTipoLabor,
              laborAplicacionQ_idLaborAplicacionQ: val[i].laborAplicacionQ_idLaborAplicacionQ,
              contratista_idContratista: val[i].contratista_idContratista,
              usuario_idUsuario: val[i].usuario_idUsuario});
    
    }
    console.log(arregloLocal);
    
    localStorage.arregloLocal = arregloLocal;
    */
    //console.log(JSON.stringify(localStorage.arregloLocal));

    //console.log(localStorage.arregloLocal);

    let misDatos = JSON.parse(localStorage.getItem('arregloLocal'));
    console.log(misDatos[0].idLabor);

    //this.arregloLocal2.unshift(misDatos);

    /*
    for (const key in localStorage.arregloLocal) {
      if (arregloLocal2.hasOwnProperty(key)) {
        const element = arregloLocal2[key];
        console.log(element.idLabor);
      }
    }
    */
    /*
      }).catch((error) => {
        console.log('get error for ' + key + '', error);
      });
    */
  }




  obtener_laboresDB() {

    //return  false;
    const arregloLocal = [];

    //alert("obtener_laboresDB");
    //DB LLAMADA a tabla LABORES x idUsuario y agregar datos en BD LOCAL 100%
    this.dblaboresService.getLaboresXidCliente(this.idUsuario).subscribe(
      response => {

        for (let i = 0; i < response.registros_labores.length; i++) {
          const element = response.registros_labores[i];
          console.log(element.idLabor);

          arregloLocal.push({
            idLabor: element.idLabor,
            laborNombre: element.laborNombre,
            laborDescripcion: element.laborDescripcion,
            laborValorTratoInicial: element.laborValorTratoInicial,
            laborValorTratoFinal: element.laborValorTratoFinal,
            laborObjetivo: element.laborObjetivo,
            laborLat: element.laborLat,
            laborLng: element.laborLng,
            laborFecha: element.laborFecha,
            laborIconoAsignacion: element.laborIconoAsignacion,
            laborIcono: element.laborIcono,
            laborCuartel: element.laborCuartel,
            laborDescripcionCierre: element.laborDescripcionCierre,
            estado_idEstado: element.estado_idEstado,
            tipoLabor_idTipoLabor: element.tipoLabor_idTipoLabor,
            laborAplicacionQ_idLaborAplicacionQ: element.laborAplicacionQ_idLaborAplicacionQ,
            contratista_idContratista: element.contratista_idContratista,
            usuario_idUsuario: element.usuario_idUsuario
          });

          // const element = response.registros_labores[i];
          // console.log(element.idLabor);
          /*
           var idLabor = element.idLabor;
           var laborNombre = element.laborNombre;
           var laborDescripcion = element.laborDescripcion;
           var laborValorTratoInicial = element.laborValorTratoInicial;
           var laborValorTratoFinal = element.laborValorTratoFinal;
           var laborObjetivo = element.laborObjetivo;
           var laborLat = element.laborLat;
           var laborLng = element.laborLng;
           var laborFecha = element.laborFecha;
           var laborIconoAsignacion = element.laborIconoAsignacion;
           var laborIcono = element.laborIcono;
           var laborCuartel = element.laborCuartel;
           var laborDescripcionCierre = element.laborDescripcionCierre;
           var estado_idEstado = element.estado_idEstado;
           var tipoLabor_idTipoLabor = element.tipoLabor_idTipoLabor;
           var laborAplicacionQ_idLaborAplicacionQ = element.laborAplicacionQ_idLaborAplicacionQ;
           var contratista_idContratista = element.contratista_idContratista;
           var usuario_idUsuario = element.usuario_idUsuario;
           */
          /*
                    const nuevoRegistroLabores = new RegistroLabores(
                      idLabor,
                      laborNombre,
                      laborDescripcion,
                      laborValorTratoInicial,
                      laborValorTratoFinal,
                      laborObjetivo,
                      laborLat,
                      laborLng,
                      laborFecha,
                      laborIconoAsignacion,
                      laborIcono,
                      laborCuartel,
                      laborDescripcionCierre,
                      estado_idEstado,
                      tipoLabor_idTipoLabor,
                      laborAplicacionQ_idLaborAplicacionQ,
                      contratista_idContratista,
                      usuario_idUsuario
                      );*/

          /*
        //ordena y deja el ultimo ingresado en primer lugar
        this.guardadosLabores.unshift(nuevoRegistroLabores);
        //guarda los datos en local
        
        //this.storage.clear(); //borra todas las BD locales
        //console.log(this.storage.keys() );
       this.storage.set('registrosLabores',this.guardadosLabores);
*/

          //arregloLocal.push( nuevoRegistroLabores );

        }
        //ORDENA LA LISTA EN FORMA DESCENDENTE
        arregloLocal.sort().reverse();

        localStorage.arregloLocal = JSON.stringify(arregloLocal);
        //this.obtener_laboresLocal();

      },
      error => {
        console.log(error);
      }
    );

    //location.reload();
    //alert("Sincronizando con BD, esto tardará unos segundos");
    //this.router.navigate(['/entrada']);

  }









  abrirRegistro2() {

    alert("prueba");

  }

  cerrarRegistro(registro) {
    /*
        this.dataLocalLabor.borrarRegistroLabores(
          this.numeroOtCantidad = registro.numeroOt
        );
    */

    console.log(registro.tipificacionLabor);

    this.listadoRegistros = false;


    if (registro.tipificacionLabor == "Labor General Autoasignada") {
      this.segmento = "cerrarAutoasignado";
      this.nombre = registro.nombreLabor;
      this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      this.descripcion = registro.descripcionLabor;
      this.tipificacionLabor = registro.tipificacionLabor;
      this.numeroOt = registro.numeroOt;

      //alert(tipificacionLabor+" "+this.nombre);

      //return "leaf-outline";
    }


    if (registro.tipificacionLabor == "Labor General Interna") {
      this.segmento = "cerrarInterna";
      this.nombre = registro.nombreLabor;
      this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      this.descripcion = registro.descripcionLabor;
      this.tipificacionLabor = registro.tipificacionLabor;
      this.numeroOt = registro.numeroOt;

    }


    if (registro.tipificacionLabor == "Labor General Contratista") {
      this.segmento = "cerrarContratista";
      this.nombre = registro.nombreLabor;

      this.descripcion = registro.descripcionLabor;
      this.tipificacionLabor = registro.tipificacionLabor;
      this.numeroOt = registro.numeroOt;

      this.nombreContratista = registro.nombreContratista;
      this.tipoTratoValor = registro.tipoTratoValor;
      this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      //alert(registro.tipificacionLabor);
      // return "leaf-outline";
    }


    if (registro.tipificacionLabor == "Aplicación Química Autoasignada") {
      this.segmento = "cerrarQuimica_autoasignado";
      this.nombre = registro.nombreLabor;
      this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      this.descripcion = registro.descripcionLabor;
      this.tipificacionLabor = registro.tipificacionLabor;
      this.numeroOt = registro.numeroOt;
      this.nombreProductoQuimico1 = registro.nombreProductoQuimico1;
      this.cantidadProductoQuimico1 = registro.cantidadProductoQuimico1;
      this.medidaProductoQuimico1 = registro.medidaProductoQuimico1;
    }


    if (registro.tipificacionLabor == "Aplicación Química Interna") {
      this.segmento = "cerrarQuimica_interna";
      this.nombre = registro.nombreLabor;
      this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      this.descripcion = registro.descripcionLabor;
      this.tipificacionLabor = registro.tipificacionLabor;
      this.numeroOt = registro.numeroOt;

      this.cuartel = registro.nombreCuartel;
      this.maquina = registro.nombreMaquina;

      this.nombreProductoQuimico1 = registro.nombreProductoQuimico1;
      this.cantidadProductoQuimico1 = registro.cantidadProductoQuimico1;
      this.medidaProductoQuimico1 = registro.medidaProductoQuimico1;

      this.nombreProductoQuimico2 = registro.nombreProductoQuimico2;
      this.cantidadProductoQuimico2 = registro.cantidadProductoQuimico2;
      this.medidaProductoQuimico2 = registro.medidaProductoQuimico2;

      this.nombreProductoQuimico3 = registro.nombreProductoQuimico3;
      this.cantidadProductoQuimico3 = registro.cantidadProductoQuimico3;
      this.medidaProductoQuimico3 = registro.medidaProductoQuimico3;

      this.nombreProductoQuimico4 = registro.nombreProductoQuimico4;
      this.cantidadProductoQuimico4 = registro.cantidadProductoQuimico4;
      this.medidaProductoQuimico4 = registro.medidaProductoQuimico4;

      this.nombreProductoQuimico5 = registro.nombreProductoQuimico5;
      this.cantidadProductoQuimico5 = registro.cantidadProductoQuimico5;
      this.medidaProductoQuimico5 = registro.medidaProductoQuimico5;

      this.nombreProductoQuimico6 = registro.nombreProductoQuimico6;
      this.cantidadProductoQuimico6 = registro.cantidadProductoQuimico6;
      this.medidaProductoQuimico6 = registro.medidaProductoQuimico6;

      this.nombreProductoQuimico7 = registro.nombreProductoQuimico7;
      this.cantidadProductoQuimico7 = registro.cantidadProductoQuimico7;
      this.medidaProductoQuimico7 = registro.medidaProductoQuimico7;

      this.nombreProductoQuimico8 = registro.nombreProductoQuimico8;
      this.cantidadProductoQuimico8 = registro.cantidadProductoQuimico8;
      this.medidaProductoQuimico8 = registro.medidaProductoQuimico8;

      this.nombreProductoQuimico9 = registro.nombreProductoQuimico9;
      this.cantidadProductoQuimico9 = registro.cantidadProductoQuimico9;
      this.medidaProductoQuimico9 = registro.medidaProductoQuimico9;
    }



    if (registro.tipificacionLabor == "Aplicación Química Contratista") {
      this.segmento = "cerrarQuimica_contratista";
      this.nombre = registro.nombreLabor;
      this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      this.descripcion = registro.descripcionLabor;
      this.tipificacionLabor = registro.tipificacionLabor;
      this.numeroOt = registro.numeroOt;

      this.tipoTratoValor = registro.tipoTratoValor;

      this.cuartel = registro.nombreCuartel;
      this.maquina = registro.nombreMaquina;

      this.nombreProductoQuimico1 = registro.nombreProductoQuimico1;
      this.cantidadProductoQuimico1 = registro.cantidadProductoQuimico1;
      this.medidaProductoQuimico1 = registro.medidaProductoQuimico1;

      this.nombreProductoQuimico2 = registro.nombreProductoQuimico2;
      this.cantidadProductoQuimico2 = registro.cantidadProductoQuimico2;
      this.medidaProductoQuimico2 = registro.medidaProductoQuimico2;

      this.nombreProductoQuimico3 = registro.nombreProductoQuimico3;
      this.cantidadProductoQuimico3 = registro.cantidadProductoQuimico3;
      this.medidaProductoQuimico3 = registro.medidaProductoQuimico3;

      this.nombreProductoQuimico4 = registro.nombreProductoQuimico4;
      this.cantidadProductoQuimico4 = registro.cantidadProductoQuimico4;
      this.medidaProductoQuimico4 = registro.medidaProductoQuimico4;

      this.nombreProductoQuimico5 = registro.nombreProductoQuimico5;
      this.cantidadProductoQuimico5 = registro.cantidadProductoQuimico5;
      this.medidaProductoQuimico5 = registro.medidaProductoQuimico5;

      this.nombreProductoQuimico6 = registro.nombreProductoQuimico6;
      this.cantidadProductoQuimico6 = registro.cantidadProductoQuimico6;
      this.medidaProductoQuimico6 = registro.medidaProductoQuimico6;

      this.nombreProductoQuimico7 = registro.nombreProductoQuimico7;
      this.cantidadProductoQuimico7 = registro.cantidadProductoQuimico7;
      this.medidaProductoQuimico7 = registro.medidaProductoQuimico7;

      this.nombreProductoQuimico8 = registro.nombreProductoQuimico8;
      this.cantidadProductoQuimico8 = registro.cantidadProductoQuimico8;
      this.medidaProductoQuimico8 = registro.medidaProductoQuimico8;

      this.nombreProductoQuimico9 = registro.nombreProductoQuimico9;
      this.cantidadProductoQuimico9 = registro.cantidadProductoQuimico9;
      this.medidaProductoQuimico9 = registro.medidaProductoQuimico9;
    }

    /*
      this.dataLocalLabor.editarRegistroLabores(
      this.numeroOtCantidad = registro.numeroOt
        );
    */

    /*
       this.router.navigate(['/entrada']);

   this.segmento="inicial";
   this.listadoRegistros= true;
   */

  }

  cerrarRegistroLabor(tipificacionLabor) {

    alert(this.numeroOt + " - " + this.cierreDescripcion + " - " + this.cantidadCantidad + " - " + tipificacionLabor);

    if (tipificacionLabor == "Labor General Autoasignada") {

      this.dataLocalLabor.editarRegistroLabores(
        /*
         this.numeroOtCantidad = this.numeroOt,
         this.cierreDescripcion = this.cierreDescripcion,
         this.cierreTratoFinal ="",
         this.cierreSobrante1 = "",
         this.cierreSobrante2 = "",
         this.cierreSobrante3 = "",
         this.cierreSobrante4 = "",
         this.cierreSobrante5 = "",
         this.cierreSobrante6 = "",
         this.cierreSobrante7 = "",
         this.cierreSobrante8 = "",
         this.cierreSobrante9 = "",
         this.estadoCierre="2"
         */
      );
      this.guardar_avance();
    }



    if (tipificacionLabor == "Labor General Interna") {

      this.dataLocalLabor.editarRegistroLabores(
        /*
         this.numeroOtCantidad = this.numeroOt,
         this.cierreDescripcion = this.cierreDescripcion,
         this.cierreTratoFinal ="",
         this.cierreSobrante1 = "",
         this.cierreSobrante2 = "",
         this.cierreSobrante3 = "",
         this.cierreSobrante4 = "",
         this.cierreSobrante5 = "",
         this.cierreSobrante6 = "",
         this.cierreSobrante7 = "",
         this.cierreSobrante8 = "",
         this.cierreSobrante9 = "",
         this.estadoCierre="2"
         */
      );
      this.volver();
    }

    if (tipificacionLabor == "Labor General Contratista") {

      this.dataLocalLabor.editarRegistroLabores(
        /*
         this.numeroOtCantidad = this.numeroOt,
         this.cierreDescripcion = this.cierreDescripcion,
         this.cierreTratoFinal = this.cierreTratoFinal,
         this.cierreSobrante1 = "",
         this.cierreSobrante2 = "",
         this.cierreSobrante3 = "",
         this.cierreSobrante4 = "",
         this.cierreSobrante5 = "",
         this.cierreSobrante6 = "",
         this.cierreSobrante7 = "",
         this.cierreSobrante8 = "",
         this.cierreSobrante9 = "",
         this.estadoCierre="2"
         */
      );
      this.volver();
    }


    if (tipificacionLabor == "Aplicación Química Autoasignada") {

      this.dataLocalLabor.editarRegistroLabores(
        /*
         this.numeroOtCantidad = this.numeroOt,
         this.cierreDescripcion = this.cierreDescripcion,
         this.cierreTratoFinal = "",
         this.cierreSobrante1 = this.cierreSobrante1,
         this.cierreSobrante2 = "",
         this.cierreSobrante3 = "",
         this.cierreSobrante4 = "",
         this.cierreSobrante5 = "",
         this.cierreSobrante6 = "",
         this.cierreSobrante7 = "",
         this.cierreSobrante8 = "",
         this.cierreSobrante9 = "",
         this.estadoCierre="2"
         */
      );
      this.guardar_avance();
      this.volver();
    }



    if (tipificacionLabor == "Aplicación Química Interna") {

      this.dataLocalLabor.editarRegistroLabores(
        /*
         this.numeroOtCantidad = this.numeroOt,
         this.cierreDescripcion = this.cierreDescripcion,
         this.cierreTratoFinal = "",
         this.cierreSobrante1 = this.cierreSobrante1,
         this.cierreSobrante2 = this.cierreSobrante2,
         this.cierreSobrante3 = this.cierreSobrante3,
         this.cierreSobrante4 = this.cierreSobrante4,
         this.cierreSobrante5 = this.cierreSobrante5,
         this.cierreSobrante6 = this.cierreSobrante6,
         this.cierreSobrante7 = this.cierreSobrante7,
         this.cierreSobrante8 = this.cierreSobrante8,
         this.cierreSobrante9 = this.cierreSobrante9,
         this.estadoCierre="2"
         */
      );
      this.volver();
    }


    if (tipificacionLabor == "Aplicación Química Contratista") {

      this.dataLocalLabor.editarRegistroLabores(
        /*
         this.numeroOtCantidad = this.numeroOt,
         this.cierreDescripcion = this.cierreDescripcion,
         this.cierreTratoFinal = this.cierreTratoFinal,
         this.cierreSobrante1 = this.cierreSobrante1,
         this.cierreSobrante2 = this.cierreSobrante2,
         this.cierreSobrante3 = this.cierreSobrante3,
         this.cierreSobrante4 = this.cierreSobrante4,
         this.cierreSobrante5 = this.cierreSobrante5,
         this.cierreSobrante6 = this.cierreSobrante6,
         this.cierreSobrante7 = this.cierreSobrante7,
         this.cierreSobrante8 = this.cierreSobrante8,
         this.cierreSobrante9 = this.cierreSobrante9,
         this.estadoCierre="2"
         */
      );
      this.volver();
    }



  }




  abrirRegistro(registro) {

    //console.log(registro);

    this.listadoRegistros = false;

    this.botonesEntrada = false;


    if (registro.tipoLabor_idTipoLabor == "1") {
      this.segmento = "autoasignado";
      this.nombre = registro.laborNombre;
      this.tipoTratoUnidadMedida = "";
      this.descripcion = registro.laborDescripcion;
      this.tipificacionLabor = registro.tipoLabor_idTipoLabor;
      this.numeroOt = "";

      //alert(tipificacionLabor+" "+this.nombre);

      //return "leaf-outline";
    }

    if (registro.tipificacionLabor == "Labor General Interna") {
      this.segmento = "interna";
      this.nombre = registro.nombreLabor;
      this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      this.descripcion = registro.descripcionLabor;
      this.tipificacionLabor = registro.tipificacionLabor;
      this.numeroOt = registro.numeroOt;
      //alert(tipificacionLabor);
      // return "leaf-outline";
    }

    if (registro.tipificacionLabor == "Labor General Contratista") {
      this.segmento = "contratista";
      this.nombre = registro.nombreLabor;

      this.descripcion = registro.descripcionLabor;
      this.tipificacionLabor = registro.tipificacionLabor;
      this.numeroOt = registro.numeroOt;

      this.nombreContratista = registro.nombreContratista;
      this.tipoTratoValor = registro.tipoTratoValor;
      this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      // alert(tipificacionLabor);
      // return "leaf-outline";
    }

    if (registro.tipificacionLabor == "Aplicación Química Autoasignada") {
      this.segmento = "quimica_autoasignado";

      this.nombre = registro.nombreLabor;
      this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      this.descripcion = registro.descripcionLabor;
      this.tipificacionLabor = registro.tipificacionLabor;
      this.numeroOt = registro.numeroOt;
      //alert(tipificacionLabor);
      // return "logo-react";
    }

    if (registro.tipificacionLabor == "Aplicación Química Interna") {
      this.segmento = "quimica_interna";

      this.nombre = registro.nombreLabor;
      this.descripcion = registro.descripcionLabor;
      this.tipificacionLabor = registro.tipificacionLabor;
      this.numeroOt = registro.numeroOt;

      this.cuartel = registro.nombreCuartel;
      this.maquina = registro.nombreMaquina;

      this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;

      this.cantidadAgua = registro.cantidadAgua;

      this.nombreProductoQuimico1 = registro.nombreProductoQuimico1;
      this.nombreProductoQuimico2 = registro.nombreProductoQuimico2;
      this.nombreProductoQuimico3 = registro.nombreProductoQuimico3;
      this.nombreProductoQuimico4 = registro.nombreProductoQuimico4;
      this.nombreProductoQuimico5 = registro.nombreProductoQuimico5;
      this.nombreProductoQuimico6 = registro.nombreProductoQuimico6;
      this.nombreProductoQuimico7 = registro.nombreProductoQuimico7;
      this.nombreProductoQuimico8 = registro.nombreProductoQuimico8;
      this.nombreProductoQuimico9 = registro.nombreProductoQuimico9;

      this.medidaProductoQuimico1 = registro.medidaProductoQuimico1;
      this.medidaProductoQuimico2 = registro.medidaProductoQuimico2;
      this.medidaProductoQuimico3 = registro.medidaProductoQuimico3;
      this.medidaProductoQuimico4 = registro.medidaProductoQuimico4;
      this.medidaProductoQuimico5 = registro.medidaProductoQuimico5;
      this.medidaProductoQuimico6 = registro.medidaProductoQuimico6;
      this.medidaProductoQuimico7 = registro.medidaProductoQuimico7;
      this.medidaProductoQuimico8 = registro.medidaProductoQuimico8;
      this.medidaProductoQuimico9 = registro.medidaProductoQuimico9;

      this.cantidadProductoQuimico1 = registro.cantidadProductoQuimico1;
      this.cantidadProductoQuimico2 = registro.cantidadProductoQuimico2;
      this.cantidadProductoQuimico3 = registro.cantidadProductoQuimico3;
      this.cantidadProductoQuimico4 = registro.cantidadProductoQuimico4;
      this.cantidadProductoQuimico5 = registro.cantidadProductoQuimico5;
      this.cantidadProductoQuimico6 = registro.cantidadProductoQuimico6;
      this.cantidadProductoQuimico7 = registro.cantidadProductoQuimico7;
      this.cantidadProductoQuimico8 = registro.cantidadProductoQuimico8;
      this.cantidadProductoQuimico9 = registro.cantidadProductoQuimico9;
      //alert(tipificacionLabor);
      //return "logo-react";
    }

    if (registro.tipificacionLabor == "Aplicación Química Contratista") {
      this.segmento = "quimica_contratista";

      this.nombre = registro.nombreLabor;
      this.descripcion = registro.descripcionLabor;
      this.tipificacionLabor = registro.tipificacionLabor;
      this.numeroOt = registro.numeroOt;

      this.nombreContratista = registro.nombreContratista;
      this.tipoTratoValor = registro.tipoTratoValor;
      this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;


      this.cantidadAgua = registro.cantidadAgua;
      this.cuartel = registro.nombreCuartel;
      this.maquina = registro.nombreMaquina;

      this.nombreProductoQuimico1 = registro.nombreProductoQuimico1;
      this.nombreProductoQuimico2 = registro.nombreProductoQuimico2;
      this.nombreProductoQuimico3 = registro.nombreProductoQuimico3;
      this.nombreProductoQuimico4 = registro.nombreProductoQuimico4;
      this.nombreProductoQuimico5 = registro.nombreProductoQuimico5;
      this.nombreProductoQuimico6 = registro.nombreProductoQuimico6;
      this.nombreProductoQuimico7 = registro.nombreProductoQuimico7;
      this.nombreProductoQuimico8 = registro.nombreProductoQuimico8;
      this.nombreProductoQuimico9 = registro.nombreProductoQuimico9;

      this.medidaProductoQuimico1 = registro.medidaProductoQuimico1;
      this.medidaProductoQuimico2 = registro.medidaProductoQuimico2;
      this.medidaProductoQuimico3 = registro.medidaProductoQuimico3;
      this.medidaProductoQuimico4 = registro.medidaProductoQuimico4;
      this.medidaProductoQuimico5 = registro.medidaProductoQuimico5;
      this.medidaProductoQuimico6 = registro.medidaProductoQuimico6;
      this.medidaProductoQuimico7 = registro.medidaProductoQuimico7;
      this.medidaProductoQuimico8 = registro.medidaProductoQuimico8;
      this.medidaProductoQuimico9 = registro.medidaProductoQuimico9;

      this.cantidadProductoQuimico1 = registro.cantidadProductoQuimico1;
      this.cantidadProductoQuimico2 = registro.cantidadProductoQuimico2;
      this.cantidadProductoQuimico3 = registro.cantidadProductoQuimico3;
      this.cantidadProductoQuimico4 = registro.cantidadProductoQuimico4;
      this.cantidadProductoQuimico5 = registro.cantidadProductoQuimico5;
      this.cantidadProductoQuimico6 = registro.cantidadProductoQuimico6;
      this.cantidadProductoQuimico7 = registro.cantidadProductoQuimico7;
      this.cantidadProductoQuimico8 = registro.cantidadProductoQuimico8;
      this.cantidadProductoQuimico9 = registro.cantidadProductoQuimico9;

      //alert(tipificacionLabor);
      // return "logo-react";
    }
    /*
        alert(
          +"\n "+registro.iconoLabor
          +"\n "+registro.iconoAsignacion
          +"\n "+registro.cantidadProductoQuimico1
          +"\n "+registro.cantidadProductoQuimico2
          +"\n "+registro.cantidadProductoQuimico3
          +"\n "+registro.cantidadProductoQuimico4
          +"\n "+registro.cantidadProductoQuimico5
          +"\n "+registro.createdLabor
          +"\n "+registro.descripcionLabor
          +"\n "+registro.estadoLabor
          +"\n "+registro.formulaProductoQuimico1
          +"\n "+registro.formulaProductoQuimico2
          +"\n "+registro.formulaProductoQuimico3
          +"\n "+registro.formulaProductoQuimico4
          +"\n "+registro.formulaProductoQuimico5
          +"\n "+registro.medidaProductoQuimico1
          +"\n "+registro.medidaProductoQuimico2
          +"\n "+registro.medidaProductoQuimico3
          +"\n "+registro.medidaProductoQuimico4
          +"\n "+registro.medidaProductoQuimico5
          +"\n "+registro.nombreContratista
          +"\n "+registro.nombreCuartel
          +"\n "+registro.nombreLabor
          +"\n "+registro.nombreMaquina
          +"\n "+registro.nombreProductoQuimico1
          +"\n "+registro.nombreProductoQuimico2
          +"\n "+registro.nombreProductoQuimico3
          +"\n "+registro.nombreProductoQuimico4
          +"\n "+registro.nombreProductoQuimico5
          +"\n "+registro.numeroOt
          +"\n "+registro.tipificacionLabor
          +"\n "+registro.tipoLabor
          +"\n "+registro.tipoTrato
    
        );*/

    console.log('Registro: ', registro);
  }








  guardar_avance() {

    var hora = this.obtener_hora();
    //sessionStorage.usuarioNumberOT= sessionStorage.usuarioRut+"|"+hora;

    //var datos= this.numeroOt;
    //var rut_ok = datos.split("|");
    //alert("Rut OK: "+rut_ok[0]);
    var estado = "1";
    var fecha = this.funcionesService.fecha_hora_actual();

    this.dataLocalCantidad.guardarRegistroCantidad(

      this.numeroOtCantidad = this.numeroOt,
      this.rutCantidad = sessionStorage.usuarioRut,
      this.unidadCantidad = this.tipoTratoUnidadMedida,
      this.cantidadCantidad,
      this.fechaCantidad = fecha,
      this.nombreProductoCantidad,
      this.cantidadProductoCantidad,
      this.estadoCantidad = estado,

      this.nombreProductoQuimico1 = null,
      this.nombreProductoQuimico2 = null,
      this.nombreProductoQuimico3 = null,
      this.nombreProductoQuimico4 = null,
      this.nombreProductoQuimico5 = null,
      this.nombreProductoQuimico6 = null,
      this.nombreProductoQuimico7 = null,
      this.nombreProductoQuimico8 = null,
      this.nombreProductoQuimico9 = null,
      this.cantidadProductoQuimico1 = null,
      this.cantidadProductoQuimico2 = null,
      this.cantidadProductoQuimico3 = null,
      this.cantidadProductoQuimico4 = null,
      this.cantidadProductoQuimico5 = null,
      this.cantidadProductoQuimico6 = null,
      this.cantidadProductoQuimico7 = null,
      this.cantidadProductoQuimico8 = null,
      this.cantidadProductoQuimico9 = null,
      this.medidaProductoQuimico1 = null,
      this.medidaProductoQuimico2 = null,
      this.medidaProductoQuimico3 = null,
      this.medidaProductoQuimico4 = null,
      this.medidaProductoQuimico5 = null,
      this.medidaProductoQuimico6 = null,
      this.medidaProductoQuimico7 = null,
      this.medidaProductoQuimico8 = null,
      this.medidaProductoQuimico9 = null,
      this.numeroBombada = null,
      this.litrosAguaxHectarea = null

    );

    //this.limpiar_form();

    alert("Usted agregó la cantidad de: " + this.cantidadCantidad + " al rut: " + this.rutCantidad);

    //borrar
    this.numeroOtCantidad = "",
      this.rutCantidad = "",
      this.unidadCantidad = "",
      this.cantidadCantidad = "",
      this.fechaCantidad = ""

    this.router.navigate(['/entrada']);

    this.segmento = "inicial";
    this.listadoRegistros = true;



  }


  obtener_hora() {
    /*sacar fecha*/
    var meses = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
    var f = new Date();
    var fec = (f.getFullYear() + "" + meses[f.getMonth()] + "" + f.getDate());
    /**/

    /*sacar hora*/
    this.today = new Date();
    var h = this.today.getHours();
    var m = this.today.getMinutes();
    var s = this.today.getSeconds();
    var mm = this.today.getMilliseconds();
    var h1 = 0;
    var m1 = 0;
    var s1 = 0;

    if (h < 10) { h1 = 0 + h; } else { h1 = h; }
    if (m < 10) { m1 = 0 + m; } else { m1 = m; }
    if (s < 10) { s1 = 0 + s; } else { s1 = s; }
    var hora = h1 + ":" + m1 + ":" + s1 + ":" + mm;
    /**/

    return fec + "|" + hora;
  }


  guardar_avance_interna2(x, i, a) {
    alert(x + " - " + i + " - " + a);
    console.log(x + " - " + i + " - " + a);
  }


  guardar_avance_interna3(x, i, a) {
    alert(x + " - " + i + " - " + a);
    console.log(x + " - " + i + " - " + a);
  }


  guardar_avance_interna(x, i) {


    var cc = (<HTMLInputElement>document.getElementById(i)).value;

    alert("Usted agregó la cantidad de: " + cc + " al rut: " + x.text);


    (<HTMLInputElement>document.getElementById(i)).disabled = true;

    (<HTMLInputElement>document.getElementById(x.text)).className = "borrar";


    var estado = "1";
    var fecha = this.funcionesService.fecha_hora_actual();

    this.dataLocalCantidad.guardarRegistroCantidad(

      this.numeroOtCantidad = this.numeroOt,
      this.rutCantidad = x.text,
      this.unidadCantidad = this.tipoTratoUnidadMedida,
      this.cantidadCantidad = cc,
      this.fechaCantidad = fecha,
      this.nombreProductoCantidad,
      this.cantidadProductoCantidad,
      this.estadoCantidad = estado,

      this.nombreProductoQuimico1 = null,
      this.nombreProductoQuimico2 = null,
      this.nombreProductoQuimico3 = null,
      this.nombreProductoQuimico4 = null,
      this.nombreProductoQuimico5 = null,
      this.nombreProductoQuimico6 = null,
      this.nombreProductoQuimico7 = null,
      this.nombreProductoQuimico8 = null,
      this.nombreProductoQuimico9 = null,
      this.cantidadProductoQuimico1 = null,
      this.cantidadProductoQuimico2 = null,
      this.cantidadProductoQuimico3 = null,
      this.cantidadProductoQuimico4 = null,
      this.cantidadProductoQuimico5 = null,
      this.cantidadProductoQuimico6 = null,
      this.cantidadProductoQuimico7 = null,
      this.cantidadProductoQuimico8 = null,
      this.cantidadProductoQuimico9 = null,
      this.medidaProductoQuimico1 = null,
      this.medidaProductoQuimico2 = null,
      this.medidaProductoQuimico3 = null,
      this.medidaProductoQuimico4 = null,
      this.medidaProductoQuimico5 = null,
      this.medidaProductoQuimico6 = null,
      this.medidaProductoQuimico7 = null,
      this.medidaProductoQuimico8 = null,
      this.medidaProductoQuimico9 = null,
      this.numeroBombada = null,
      this.litrosAguaxHectarea = null
    );


    return false;
  }




  volver() {

    //en caso de que este parado en segmento inicial vuelve a playpause, de lo contrario
    //se mantiene en la seccion entrada.
    if (this.segmento == "inicial") {
      this.router.navigate(['/playpause']);
    } else {
      this.numeroOtCantidad = "";
      this.cierreDescripcion = "";
      this.cierreTratoFinal = "";

      this.router.navigate(['/entrada']);
      this.segmento = "inicial";
      this.listadoRegistros = true;
      this.botonesEntrada = true;
    }

  }






  async presentAlertPrompt(rut) {
    const alert = await this.alertController.create({
      header: 'Datos Mojamiento ' + rut,
      inputs: [
        {
          name: 'nombreProductoCantidad',
          type: 'text',
          placeholder: 'Nombre del Producto:'
        },
        {
          name: 'cantidadProductoCantidad',
          type: 'text',
          placeholder: 'Cantidad de Producto:'
        },
        {
          name: 'unidadCantidad',
          type: 'text',
          placeholder: 'Medida:'
        },
        {
          name: 'cantidadCantidad',
          type: 'text',
          placeholder: 'Mojamiento:'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (aa) => {

            //evita que se envien los datos con datos en blanco...
            if (aa.nombreProductoCantidad == "" || aa.cantidadProductoCantidad == "" || aa.unidadCantidad == "" || aa.cantidadCantidad == "") {
              return false;
            }
            //elimina la linea despues de agregar los datos.
            (<HTMLInputElement>document.getElementById(rut)).className = "borrar";

            //guarda los datos
            console.log('Confirm Ok ' + aa.cantidadCantidad);

            var estado = "1";
            var fecha = this.funcionesService.fecha_hora_actual();

            this.dataLocalCantidad.guardarRegistroCantidad(
              this.numeroOtCantidad = this.numeroOt,
              this.rutCantidad = rut,
              aa.unidadCantidad,
              aa.cantidadCantidad,
              this.fechaCantidad = fecha,
              aa.nombreProductoCantidad,
              aa.cantidadProductoCantidad,
              this.estadoCantidad = estado,

              this.nombreProductoQuimico1 = null,
              this.nombreProductoQuimico2 = null,
              this.nombreProductoQuimico3 = null,
              this.nombreProductoQuimico4 = null,
              this.nombreProductoQuimico5 = null,
              this.nombreProductoQuimico6 = null,
              this.nombreProductoQuimico7 = null,
              this.nombreProductoQuimico8 = null,
              this.nombreProductoQuimico9 = null,
              this.cantidadProductoQuimico1 = null,
              this.cantidadProductoQuimico2 = null,
              this.cantidadProductoQuimico3 = null,
              this.cantidadProductoQuimico4 = null,
              this.cantidadProductoQuimico5 = null,
              this.cantidadProductoQuimico6 = null,
              this.cantidadProductoQuimico7 = null,
              this.cantidadProductoQuimico8 = null,
              this.cantidadProductoQuimico9 = null,
              this.medidaProductoQuimico1 = null,
              this.medidaProductoQuimico2 = null,
              this.medidaProductoQuimico3 = null,
              this.medidaProductoQuimico4 = null,
              this.medidaProductoQuimico5 = null,
              this.medidaProductoQuimico6 = null,
              this.medidaProductoQuimico7 = null,
              this.medidaProductoQuimico8 = null,
              this.medidaProductoQuimico9 = null,
              this.numeroBombada = null,
              this.litrosAguaxHectarea = null

            );
          }
        }
      ]
    });

    await alert.present();
  }






  acordeon_labores(bb, xx) {

    console.log(this.itemLaboresAbiertas + " - " + xx);

    var x = bb + xx;

    //document.getElementById(x).innerText = "hola";
    var estado = document.getElementById(x).style.display;

    if (estado == "none") {
      document.getElementById(x).style.display = "block";
    } else {
      document.getElementById(x).style.display = "none";
      // this.itemLaboresAbiertas = false;
    }



  }








  guardar_avance_quimica(x, ii, aplicador) {

    var i = ii + 1;

    var litrosAgua = (<HTMLInputElement>document.getElementById(i)).value;

    //console.log(x+" "+i+" "+litrosAgua);



    //return false;

    var cantidadProductoQuimico1 = "";
    var cantidadProductoQuimico2 = "";
    var cantidadProductoQuimico3 = "";
    var cantidadProductoQuimico4 = "";
    var cantidadProductoQuimico5 = "";
    var cantidadProductoQuimico6 = "";
    var cantidadProductoQuimico7 = "";
    var cantidadProductoQuimico8 = "";
    var cantidadProductoQuimico9 = "";
    var medidaProductoQuimico1 = "";
    var medidaProductoQuimico2 = "";
    var medidaProductoQuimico3 = "";
    var medidaProductoQuimico4 = "";
    var medidaProductoQuimico5 = "";
    var medidaProductoQuimico6 = "";
    var medidaProductoQuimico7 = "";
    var medidaProductoQuimico8 = "";
    var medidaProductoQuimico9 = "";

    // seleccionar numero de aplicador


    for (var a = 1; a <= 9; a++) {
      if (a == i) {
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico1-" + a)) {
          cantidadProductoQuimico1 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico1-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico2-" + a)) {
          cantidadProductoQuimico2 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico2-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico3-" + a)) {
          cantidadProductoQuimico3 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico3-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico4-" + a)) {
          cantidadProductoQuimico4 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico4-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico5-" + a)) {
          cantidadProductoQuimico5 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico5-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico6-" + a)) {
          cantidadProductoQuimico6 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico6-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico7-" + a)) {
          cantidadProductoQuimico7 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico7-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico8-" + a)) {
          cantidadProductoQuimico8 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico8-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico9-" + a)) {
          cantidadProductoQuimico9 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico9-" + a)).value;
        }

        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico1-" + a)) {
          medidaProductoQuimico1 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico1-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico2-" + a)) {
          medidaProductoQuimico2 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico2-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico3-" + a)) {
          medidaProductoQuimico3 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico3-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico4-" + a)) {
          medidaProductoQuimico4 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico4-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico5-" + a)) {
          medidaProductoQuimico5 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico5-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico6-" + a)) {
          medidaProductoQuimico6 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico6-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico7-" + a)) {
          medidaProductoQuimico7 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico7-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico8-" + a)) {
          medidaProductoQuimico8 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico8-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico9-" + a)) {
          medidaProductoQuimico9 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico9-" + a)).value;
        }
      }
    }







    /*
    console.log(litrosAgua+" "+cantidadProductoQuimico1+" "+cantidadProductoQuimico2+" "+cantidadProductoQuimico3+" "+cantidadProductoQuimico4+" "+cantidadProductoQuimico5+" "+cantidadProductoQuimico6+" "+cantidadProductoQuimico7+" "+cantidadProductoQuimico8+" "+cantidadProductoQuimico9);
    console.log(litrosAgua+" "+medidaProductoQuimico1+" "+medidaProductoQuimico2+" "+medidaProductoQuimico3+" "+medidaProductoQuimico4+" "+medidaProductoQuimico5+" "+medidaProductoQuimico6+" "+medidaProductoQuimico7+" "+medidaProductoQuimico8+" "+medidaProductoQuimico9);
    return false;
    */




    //var litrosAgua = (<HTMLInputElement>document.getElementById(i)).value;
    //console.log(litrosAgua+" "+cantidadProductoQuimico1+" "+cantidadProductoQuimico2);
    //console.log(x+" "+ii+" "+litrosAgua);

    //return false;

    //var cc = (<HTMLInputElement>document.getElementById(i)).value;

    alert("Usted agregó la cantidad de: " + litrosAgua + " litros al rut: " + aplicador);


    (<HTMLInputElement>document.getElementById(aplicador)).style.display = "none";

    // (<HTMLInputElement>document.getElementById(x.text)).className ="borrar";


    var estado = "1";
    var fecha = this.funcionesService.fecha_hora_actual();

    this.dataLocalCantidad.guardarRegistroCantidad(

      this.numeroOtCantidad = this.numeroOt,
      this.rutCantidad = aplicador,
      this.unidadCantidad = this.tipoTratoUnidadMedida,
      this.cantidadCantidad,
      this.fechaCantidad = fecha,
      this.nombreProductoCantidad,
      this.cantidadProductoCantidad,
      this.estadoCantidad = estado,

      this.nombreProductoQuimico1 = x.nombreProductoQuimico1,
      this.nombreProductoQuimico2 = x.nombreProductoQuimico2,
      this.nombreProductoQuimico3 = x.nombreProductoQuimico3,
      this.nombreProductoQuimico4 = x.nombreProductoQuimico4,
      this.nombreProductoQuimico5 = x.nombreProductoQuimico5,
      this.nombreProductoQuimico6 = x.nombreProductoQuimico6,
      this.nombreProductoQuimico7 = x.nombreProductoQuimico7,
      this.nombreProductoQuimico8 = x.nombreProductoQuimico8,
      this.nombreProductoQuimico9 = x.nombreProductoQuimico9,
      this.cantidadProductoQuimico1 = cantidadProductoQuimico1,
      this.cantidadProductoQuimico2 = cantidadProductoQuimico2,
      this.cantidadProductoQuimico3 = cantidadProductoQuimico3,
      this.cantidadProductoQuimico4 = cantidadProductoQuimico4,
      this.cantidadProductoQuimico5 = cantidadProductoQuimico5,
      this.cantidadProductoQuimico6 = cantidadProductoQuimico6,
      this.cantidadProductoQuimico7 = cantidadProductoQuimico7,
      this.cantidadProductoQuimico8 = cantidadProductoQuimico8,
      this.cantidadProductoQuimico9 = cantidadProductoQuimico9,
      this.medidaProductoQuimico1 = medidaProductoQuimico1,
      this.medidaProductoQuimico2 = medidaProductoQuimico2,
      this.medidaProductoQuimico3 = medidaProductoQuimico3,
      this.medidaProductoQuimico4 = medidaProductoQuimico4,
      this.medidaProductoQuimico5 = medidaProductoQuimico5,
      this.medidaProductoQuimico6 = medidaProductoQuimico6,
      this.medidaProductoQuimico7 = medidaProductoQuimico7,
      this.medidaProductoQuimico8 = medidaProductoQuimico8,
      this.medidaProductoQuimico9 = medidaProductoQuimico9,
      this.numeroBombada = null,
      this.litrosAguaxHectarea = litrosAgua
    );


    return false;
  }








  guardar_avance_quimica_contratista(x, ii, aplicador) {

    var i = ii + 1;

    var litrosAgua = (<HTMLInputElement>document.getElementById(i)).value;

    //console.log(x+" "+i+" "+litrosAgua);



    //return false;

    var cantidadProductoQuimico1 = null;
    var cantidadProductoQuimico2 = null;
    var cantidadProductoQuimico3 = null;
    var cantidadProductoQuimico4 = null;
    var cantidadProductoQuimico5 = null;
    var cantidadProductoQuimico6 = null;
    var cantidadProductoQuimico7 = null;
    var cantidadProductoQuimico8 = null;
    var cantidadProductoQuimico9 = null;
    var medidaProductoQuimico1 = null;
    var medidaProductoQuimico2 = null;
    var medidaProductoQuimico3 = null;
    var medidaProductoQuimico4 = null;
    var medidaProductoQuimico5 = null;
    var medidaProductoQuimico6 = null;
    var medidaProductoQuimico7 = null;
    var medidaProductoQuimico8 = null;
    var medidaProductoQuimico9 = null;

    // seleccionar numero de aplicador


    for (var a = 1; a <= 9; a++) {
      if (a == i) {
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico1-" + a)) {
          cantidadProductoQuimico1 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico1-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico2-" + a)) {
          cantidadProductoQuimico2 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico2-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico3-" + a)) {
          cantidadProductoQuimico3 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico3-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico4-" + a)) {
          cantidadProductoQuimico4 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico4-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico5-" + a)) {
          cantidadProductoQuimico5 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico5-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico6-" + a)) {
          cantidadProductoQuimico6 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico6-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico7-" + a)) {
          cantidadProductoQuimico7 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico7-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico8-" + a)) {
          cantidadProductoQuimico8 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico8-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("cantidadProductoQuimico9-" + a)) {
          cantidadProductoQuimico9 = (<HTMLInputElement>document.getElementById("cantidadProductoQuimico9-" + a)).value;
        }

        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico1-" + a)) {
          medidaProductoQuimico1 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico1-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico2-" + a)) {
          medidaProductoQuimico2 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico2-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico3-" + a)) {
          medidaProductoQuimico3 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico3-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico4-" + a)) {
          medidaProductoQuimico4 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico4-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico5-" + a)) {
          medidaProductoQuimico5 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico5-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico6-" + a)) {
          medidaProductoQuimico6 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico6-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico7-" + a)) {
          medidaProductoQuimico7 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico7-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico8-" + a)) {
          medidaProductoQuimico8 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico8-" + a)).value;
        }
        if (<HTMLInputElement>document.getElementById("medidaProductoQuimico9-" + a)) {
          medidaProductoQuimico9 = (<HTMLInputElement>document.getElementById("medidaProductoQuimico9-" + a)).value;
        }
      }
    }







    /*
    console.log(litrosAgua+" "+cantidadProductoQuimico1+" "+cantidadProductoQuimico2+" "+cantidadProductoQuimico3+" "+cantidadProductoQuimico4+" "+cantidadProductoQuimico5+" "+cantidadProductoQuimico6+" "+cantidadProductoQuimico7+" "+cantidadProductoQuimico8+" "+cantidadProductoQuimico9);
    console.log(litrosAgua+" "+medidaProductoQuimico1+" "+medidaProductoQuimico2+" "+medidaProductoQuimico3+" "+medidaProductoQuimico4+" "+medidaProductoQuimico5+" "+medidaProductoQuimico6+" "+medidaProductoQuimico7+" "+medidaProductoQuimico8+" "+medidaProductoQuimico9);
    return false;
    */




    //var litrosAgua = (<HTMLInputElement>document.getElementById(i)).value;
    //console.log(litrosAgua+" "+cantidadProductoQuimico1+" "+cantidadProductoQuimico2);
    //console.log(x+" "+ii+" "+litrosAgua);

    //return false;

    //var cc = (<HTMLInputElement>document.getElementById(i)).value;

    alert("Usted agregó la cantidad de: " + litrosAgua + " litros al rut: " + aplicador);


    (<HTMLInputElement>document.getElementById(aplicador)).style.display = "none";

    // (<HTMLInputElement>document.getElementById(x.text)).className ="borrar";


    var estado = "1";
    var fecha = this.funcionesService.fecha_hora_actual();

    this.dataLocalCantidad.guardarRegistroCantidad(

      this.numeroOtCantidad = this.numeroOt,
      this.rutCantidad = aplicador,
      this.unidadCantidad = this.tipoTratoUnidadMedida,
      this.cantidadCantidad,
      this.fechaCantidad = fecha,
      this.nombreProductoCantidad,
      this.cantidadProductoCantidad,
      this.estadoCantidad = estado,

      this.nombreProductoQuimico1 = x.nombreProductoQuimico1,
      this.nombreProductoQuimico2 = x.nombreProductoQuimico2,
      this.nombreProductoQuimico3 = x.nombreProductoQuimico3,
      this.nombreProductoQuimico4 = x.nombreProductoQuimico4,
      this.nombreProductoQuimico5 = x.nombreProductoQuimico5,
      this.nombreProductoQuimico6 = x.nombreProductoQuimico6,
      this.nombreProductoQuimico7 = x.nombreProductoQuimico7,
      this.nombreProductoQuimico8 = x.nombreProductoQuimico8,
      this.nombreProductoQuimico9 = x.nombreProductoQuimico9,
      this.cantidadProductoQuimico1 = cantidadProductoQuimico1,
      this.cantidadProductoQuimico2 = cantidadProductoQuimico2,
      this.cantidadProductoQuimico3 = cantidadProductoQuimico3,
      this.cantidadProductoQuimico4 = cantidadProductoQuimico4,
      this.cantidadProductoQuimico5 = cantidadProductoQuimico5,
      this.cantidadProductoQuimico6 = cantidadProductoQuimico6,
      this.cantidadProductoQuimico7 = cantidadProductoQuimico7,
      this.cantidadProductoQuimico8 = cantidadProductoQuimico8,
      this.cantidadProductoQuimico9 = cantidadProductoQuimico9,
      this.medidaProductoQuimico1 = medidaProductoQuimico1,
      this.medidaProductoQuimico2 = medidaProductoQuimico2,
      this.medidaProductoQuimico3 = medidaProductoQuimico3,
      this.medidaProductoQuimico4 = medidaProductoQuimico4,
      this.medidaProductoQuimico5 = medidaProductoQuimico5,
      this.medidaProductoQuimico6 = medidaProductoQuimico6,
      this.medidaProductoQuimico7 = medidaProductoQuimico7,
      this.medidaProductoQuimico8 = medidaProductoQuimico8,
      this.medidaProductoQuimico9 = medidaProductoQuimico9,
      this.numeroBombada = null,
      this.litrosAguaxHectarea = litrosAgua
    );


    return false;
  }










}
