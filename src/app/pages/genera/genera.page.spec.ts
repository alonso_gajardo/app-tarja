import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneraPage } from './genera.page';

describe('GeneraPage', () => {
  let component: GeneraPage;
  let fixture: ComponentFixture<GeneraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneraPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
