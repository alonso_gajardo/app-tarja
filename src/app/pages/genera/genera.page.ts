import { Component, OnInit } from '@angular/core';
import { DataLocalLaboresService } from '../../services/data-local-labores.service';
import { FuncionesService } from 'src/app/services/funciones.service';

@Component({
  selector: 'app-genera',
  templateUrl: './genera.page.html',
  styleUrls: ['./genera.page.scss'],
})
export class GeneraPage implements OnInit {

laboresInternas:string;

labores={
  idLabor:"",
  laborNombre:"",
  laborDescripcion:"",
  laborValorTratoInicial:"",
  laborValorTratoFinal:"",
  laborObjetivo:"",
  laborLat:"",
  laborLng:"",
  laborFecha:"",
  laborIconoAsignacion:"",
  laborIcono:"",
  laborCuartel:"",
  laborDescripcionCierre:"",
  estado_idEstado:"",
  tipoLabor_idTipoLabor:"",
  laborAplicacionQ_idLaborAplicacionQ:"",
  contratista_idContratista:"",
  usuario_idUsuario:""

};



  constructor(
    public dataLocalLabor: DataLocalLaboresService,
    public funcionesService: FuncionesService
    ) { }

  ngOnInit() {
  }

  onSubmitTemplate(){
    console.log("Form submit");
    console.log(this.labores);
  }



agregarLabor(){


//1 = activo 2= inactivo

  this.dataLocalLabor.guardarRegistroLabores(
this.labores.idLabor,
this.labores.laborNombre,
this.labores.laborDescripcion,
this.labores.laborValorTratoInicial,
this.labores.laborValorTratoFinal,
this.labores.laborObjetivo,
this.labores.laborLat,
this.labores.laborLng,
this.labores.laborFecha,
this.labores.laborIconoAsignacion,
this.labores.laborIcono,
this.labores.laborCuartel,
this.labores.laborDescripcionCierre,
this.labores.estado_idEstado = "1",
this.labores.tipoLabor_idTipoLabor = "1",
this.labores.laborAplicacionQ_idLaborAplicacionQ,
this.labores.contratista_idContratista,
this.labores.usuario_idUsuario
/*
    this.labores.nombre, 
    this.labores.tipo, 
    this.labores.descripcion,  
    "1",
    this.labores.createdLabor = this.funcionesService.fecha_hora_actual(),
    this.labores.numeroOT = sessionStorage.usuarioNumberOT,
   this.labores.tipificacionLabor = "Labor General",

   this.labores.nombreContratista="nombreContratista",


   
   this.labores.tipoTratoValor="tipoTratoValo",
   this.labores.tipoTratoUnidadMedida="tipoTratoUnidadMedida",

   this.labores.nombreCuartel = "nombreCuartel",
this.labores.nombreMaquina = "nombreMaquina",

this.labores.cantidadAgua="",

this.labores.nombreProductoQuimico1="",
this.labores.cantidadProductoQuimico1="",
this.labores.medidaProductoQuimico1="",
this.labores.cierreSobrante1="",

this.labores.nombreProductoQuimico2="",
this.labores.cantidadProductoQuimico2="",
this.labores.medidaProductoQuimico2="",
this.labores.cierreSobrante2="",

this.labores.nombreProductoQuimico3="",
this.labores.cantidadProductoQuimico3="",
this.labores.medidaProductoQuimico3="",
this.labores.cierreSobrante3="",

this.labores.nombreProductoQuimico4="",
this.labores.cantidadProductoQuimico4="",
this.labores.medidaProductoQuimico4="",
this.labores.cierreSobrante4="",

this.labores.nombreProductoQuimico5="",
this.labores.cantidadProductoQuimico5="",
this.labores.medidaProductoQuimico5="",
this.labores.cierreSobrante5="",

this.labores.nombreProductoQuimico6="",
this.labores.cantidadProductoQuimico6="",
this.labores.medidaProductoQuimico6="",
this.labores.cierreSobrante6="",

this.labores.nombreProductoQuimico7="",
this.labores.cantidadProductoQuimico7="",
this.labores.medidaProductoQuimico7="",
this.labores.cierreSobrante7="",

this.labores.nombreProductoQuimico8="",
this.labores.cantidadProductoQuimico8="",
this.labores.medidaProductoQuimico8="",
this.labores.cierreSobrante8="",

this.labores.nombreProductoQuimico9="",
this.labores.cantidadProductoQuimico9="",
this.labores.medidaProductoQuimico9="",
this.labores.cierreSobrante9="",

this.labores.iconoLabor="",
this.labores.iconoAsignacion="",

this.labores.cierreTratoFinal="",
this.labores.cierreDescripcion="",
this.labores.estadoCierre="1"
*/
    );
/*
  this.labores.nombre="";

  this.labores.descripcion="";
  this.labores.tipo="";

  console.log(this.dataLocalLabor);
*/
  //this.dataLocalLabor.guardarRegistroLabores(nombreLabor, tipoLabor, descripcionLabor,  estadoLabor);


}


abrirRegistro(registro){

  alert(
    "Nombre :"+
    registro.nombreLabor
    +"\nDescripción :"+
    registro.descripcionLabor
    +"\nEstado :"+
    registro.estadoLabor
    +"\nTipo :"+
    registro.tipoLabor
    +"\nCreación :"+
    registro.createdLabor
    
    );
  console.log('Registro: ',registro);
}




cerrarLabor(index:number){

//alert(index);
  //1 = activo 2= inactivo
    //this.dataLocalLabor.splice(index, 4);

    //console.log(this.dataLocalLabor);
    this.dataLocalLabor.borrarRegistroLabores(index);
  }



}
