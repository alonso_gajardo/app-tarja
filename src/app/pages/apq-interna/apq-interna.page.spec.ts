import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApqInternaPage } from './apq-interna.page';

describe('ApqInternaPage', () => {
  let component: ApqInternaPage;
  let fixture: ComponentFixture<ApqInternaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApqInternaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApqInternaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
