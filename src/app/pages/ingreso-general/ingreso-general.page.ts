import { Component, OnInit } from '@angular/core';
import { DBlaboresService } from 'src/app/services/data-bd-labores.service';
import { AvanceLaborGDB } from 'src/app/services/avancelaborgDB.service';
import { FuncionesService } from 'src/app/services/funciones.service';
import { Router } from '@angular/router';
import { DBrutUsuariosService } from 'src/app/services/data-db-rut-usuarios.service';
import { DataLocalService } from 'src/app/services/data-local.service';
import { ListaNegraDB } from 'src/app/services/listanegraDB.service';
import { AvanceLaborGLocal } from 'src/app/services/avancelaborg.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { Proseso } from "../../provedor/proseso";
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-ingreso-general',
  templateUrl: './ingreso-general.page.html',
  styleUrls: ['./ingreso-general.page.scss'],
})
export class IngresoGeneralPage implements OnInit {

  show: boolean;
  radio: boolean;
  seleccionRadioButton: string;
  etapa2: boolean;
  etapa2_2: boolean;
  etapa3: boolean;
  numero: string;

  check_1_general: boolean;
  check_2_general: boolean;

  labores = {
    idLabor: "",
    laborNombre: "",
    laborDescripcion: "",
    laborValorTratoInicial: "",
    laborValorTratoFinal: "",
    laborObjetivo: "",
    laborLat: "",
    laborLng: "",
    laborFecha: "",
    laborIconoAsignacion: "",
    laborIcono: "",
    laborCuartel: "",
    laborDescripcionCierre: "",
    estado_idEstado: "",
    tipoLabor_idTipoLabor: "",
    laborAplicacionQ_idLaborAplicacionQ: "",
    contratista_idContratista: "",
    usuario_idUsuario: ""

  };


  laborObjetivo: string;
  responseId: string;


  escanearBoton: boolean;
  grupoBoton: boolean;
  manualBoton: boolean;
  caja_rut: boolean = false;
  boton_rut: boolean = true;
  caja_rut_datos: boolean = false;

  datosUsuario = {
    usuarioNombre: '',
    usuarioApellidoPaterno: '',
    usuarioCelular: ''
  }


  datosUsuario2 = {
    rut2: ''
  };


  listaNegra = "";

  // listaRut:string [] = [];
  mostrarIngresarLabor: boolean = false;

  listaRut = [{
    rut: "",
    idLabor: "",
    nombre: "",
    apellido: "",
    formatoEntrada: "",
    celular: "",
    idFecha: "",
    estado: ""
  }];


  listaRut2 = [];


  avanceLaborGCantidad: string;
  avanceLaborGRutTrabajador: string;
  avanceLaborGObjetivoLabor: string;
  avanceLaborGLat: string;
  avanceLaborGLng: string;
  avanceLaborGFecha: string;
  avanceLaborGCantGrupo: string;
  avanceLaborGFormato: string;
  labor_idLabor: string;
  envio: string;

  cantidadCantidad: string;
  tipoIngreso: boolean;


  usuarioNombre: string;
  usuarioApellidoPaterno: string;
  usuarioCelular: string;

  idUsuario: string = sessionStorage.idUsuario;
  geo = "GPS x Tiempo";
  lat: number;
  lon: number;

  proce: string = "1";
  estado1: string = "1";
  constructor(
    private dblaboresService: DBlaboresService,
    private avancelaborgdb: AvanceLaborGDB,
    private funcionesService: FuncionesService,
    private router: Router,
    private dbrutusuarios: DBrutUsuariosService,
    public dataLocal: DataLocalService,
    public listanegra: ListaNegraDB,
    private avancelaborglocal: AvanceLaborGLocal,
    public barcodeScanner: BarcodeScanner,
    public loadingController: LoadingController,
    private geolocation: Geolocation,
    private prosecnt: Proseso
  ) {


    sessionStorage.idlabor = "";
    this.listaRut = [];
    this.tipoIngreso = true;
    this.validar_tipo_de_ingreso();
  }

  ngOnInit() {
    this.radio = true;
    this.etapa2 = false;
    this.etapa2_2 = true;
    this.etapa3 = false;

    this.check_1_general = false;
    this.check_2_general = false;

    this.avanceLaborGCantGrupo = "";
    this.avanceLaborGRutTrabajador = "";
    this.laborObjetivo = "";
    this.responseId = "";

    this.cantidadCantidad = "";

    this.validar_tipo_de_ingreso();

    this.usuarioNombre = "";
    this.usuarioApellidoPaterno = "";
    this.usuarioCelular = "";

  }
  botones_estado_latlong(x) {
    //CREAR LATLONG LOCAL


    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        const gpsUsuarioLatLngLat = `${resp.coords.latitude}`;
        const gpsUsuarioLatLngLng = `${resp.coords.longitude}`;
        const gpsUsuarioLatLngFecha = this.funcionesService.fecha_hora_actual();

        const usuario_idUsuario = sessionStorage.idUsuario;
        const estadoLatLong = "1";


      })
      .catch((error) => {
        console.log("Error getting location", error);
      });
  }

  validar_tipo_de_ingreso() {

    if (sessionStorage.tipoLabor == "2") {
      this.tipoIngreso = false;

      this.seleccionRadioButton = 'Rut';
      this.etapa2 = true;
    }

  }

  seleccion(x) {
    this.seleccionRadioButton = x;
    this.show = true;
    this.etapa2 = false;
  }

  avanzar_a_2() {
    this.show = false;
    this.radio = false;
    this.etapa2 = true;
    this.etapa2_2 = true;
    this.numero = "";
    this.check_1_general = true;
  }

  avanzar_a_3(x) {
    this.radio = false;
    this.seleccionRadioButton = x;
    this.etapa2 = true;
    this.etapa2_2 = false;
    this.etapa3 = true;
  }

  restaurar1() {
    this.radio = true;
    this.etapa2 = false;
    this.etapa3 = false;
    this.check_1_general = false;
    this.mostrarIngresarLabor = false;
    this.numero = "";
  }

  restaurar2() {
    this.radio = true;
    this.etapa2 = false;
    this.etapa3 = false;
    this.check_2_general = false;
    this.mostrarIngresarLabor = false;
    this.numero = "";
  }

  restaurar3() {
    this.etapa3 = false;
    this.avanzar_a_2()
  }

  crearLabor() {

    let data_cruda = JSON.parse(sessionStorage.getItem('data_cruda_labor'));
    console.log(data_cruda);
    this.laborObjetivo = data_cruda.laborObjetivo;

    //return false
    //Se guardan los datos. En una nueva labor en BD
    this.dblaboresService.CrearPostLabores(data_cruda).subscribe(
      response => {
        console.log("CrearPostLabores " + response.id);
        this.responseId = response.id;
        sessionStorage.idlabor = response.id;
        this.limpiar_form();
        //alert UTIL
        //alert("Labor creada exitosamente. Actualice el Listado de labores.");



        //////////////agregar avance//////////////
        if (this.seleccionRadioButton == 'Grupo') {
          this.avanceLaborGCantGrupo = this.numero;
        }

        if (this.seleccionRadioButton == 'Rut') {
          this.avanceLaborGCantGrupo = "";
          this.avanceLaborGRutTrabajador = ""; //falta agregar valor
        }

        this.funcionesService.obtenerGeolocalizacion();



        let info: any = new FormData();

        info.append('avanceLaborGCantidad', '0'); //se pone cero porque es la labor inicial
        info.append('avanceLaborGRutTrabajador', this.avanceLaborGRutTrabajador);
        info.append('avanceLaborGObjetivoLabor', this.laborObjetivo);
        info.append('avanceLaborGLat', sessionStorage.lat);
        info.append('avanceLaborGLng', sessionStorage.lng);
        info.append('avanceLaborGFecha', this.funcionesService.fecha_hora_actual());
        info.append('avanceLaborGCantGrupo', this.avanceLaborGCantGrupo);
        info.append('avanceLaborGFormato', 'INICIAL');
        info.append('labor_idLabor', this.responseId);

        var object = {};
        info.forEach((value, key) => { object[key] = value });
        var data_cruda_avance = object;

        //Se guardan los datos. En una nueva labor en BD
        this.avancelaborgdb.CrearPostAvance(data_cruda_avance).subscribe(
          response => {
            console.log("CrearPostAvance" + response);
            this.limpiar_form();
            //alert UTIL 
            //alert("Avance creado exitosamente. Actualice el Listado de labores.");
            this.router.navigate(['/listado-labores']);
          },
          error => {
            console.log("CrearPostAvance error " + error);
            this.limpiar_form();
            alert("No se pudo crear el Avance. Hubo un error en el proceso de creación.");
          }
        )
        ////////////////fin agregar avance////////////////////////////




      },
      error => {
        console.log("CrearPostLabores error " + error);
        this.limpiar_form();
        alert("No se pudo crear la Labor. Hubo un error en el proceso de creación.");
      }
    )




  }




  limpiar_form() {
    this.labores.idLabor = "",
      this.labores.laborNombre = "",
      this.labores.laborDescripcion = "",
      this.labores.laborValorTratoInicial = "",
      this.labores.laborValorTratoFinal = "",
      this.labores.laborObjetivo = "",
      this.labores.laborLat = "",
      this.labores.laborLng = "",
      this.labores.laborFecha = "",
      this.labores.laborIconoAsignacion = "",
      this.labores.laborIcono = "",
      this.labores.laborCuartel = "",
      this.labores.laborDescripcionCierre = "",
      this.labores.estado_idEstado = "",
      this.labores.tipoLabor_idTipoLabor = "",
      this.labores.laborAplicacionQ_idLaborAplicacionQ = "",
      this.labores.contratista_idContratista = "",
      this.labores.usuario_idUsuario = ""
  }




  validar_rut() {

    this.escanearBoton = true;
    this.grupoBoton = false;
    this.manualBoton = true;

    this.caja_rut = true;
    this.boton_rut = false;
  }




  leer_rutValEstado() {

    return sessionStorage.rutValEstado;

  }



  ingresar_rut(manual, idlabor, estado) {
    //alert(sessionStorage.tipoLabor);

    var rut = "";
    //alert(this.datosUsuario2.rut2);
    //alert(manual+" "+rut2+" "+laborOT+" "+estado);

    this.escanearBoton = true;
    this.grupoBoton = false;
    this.manualBoton = true;

    if (this.datosUsuario2.rut2) {
      rut = this.datosUsuario2.rut2;
    } else {
      alert("Ingrese un Rut válido");
      return false
    }
    //alert(rut);
    //validar Rut con BD lista negra
    //conectarse y verificar rut con lista negra rut
    this.dbrutusuarios.getRutUsuarios(rut)
      .subscribe(
        response => {

          console.log(response);





          this.listaNegra = response;

          //var respuesta = Object.values(response.usuario.usuarioRut);
          let usuarioRut = response.usuario.usuarioRut;
          let respuestaid = response.usuario.idUsuario;
          //var respuestax = response;
          let usuarioNombres = response.usuario.usuarioNombres;
          let usuarioApellidoPaterno = response.usuario.usuarioApellidoPaterno;

          console.log(respuestaid);



          //lista negra obtener respuesta de id
          this.listanegra.getListaNegra(respuestaid)
            .subscribe(
              response => {


                console.log(response);
                console.log(response.logListaNegraMotivo);

                if (response.idLogListaNegra != "1") {
                  alert("NO puede trabajar en la empresa está en LISTA NEGRA: \n"
                    + " Nº: " + response.idLogListaNegra
                    + " \nFecha: " + response.logListaNegraFecha
                    + " \nMotivo: " + response.logListaNegraMotivo
                  )
                } else {
                  //validar si está repetido el rut
                  var i = 0;
                  for (const key in this.listaRut) {
                    if (this.listaRut.hasOwnProperty(key)) {
                      const element = this.listaRut[key];
                      if (element.rut == usuarioRut) {
                        i++;
                      }
                    }
                  }

                  if (i > 0) {
                    alert("Existe en la BD");
                  } else {

                    //alert UTIL
                    //alert("SIN ANTECEDENTES, Puede Trabajar en la Empresa");
                    this.listaRut.push({
                      rut: usuarioRut,
                      idLabor: "",
                      nombre: usuarioNombres,
                      apellido: usuarioApellidoPaterno,
                      formatoEntrada: "",
                      celular: "",
                      idFecha: "",
                      estado: "1"
                    });
                    console.log(this.listaRut);
                    this.mostrarIngresarLabor = true;


                  }

                  //alert(this.dataLocal.guardados);
                  this.caja_rut = false;
                  this.boton_rut = true;
                  //Validar rut en BD

                }

              })

        },
        error => {
          console.log(error);
          console.log(error.registros_rut_usuarios);

          console.log(error.status);//404 rut no encontrado   // 0  sin internet
          console.log(error.statusText);//ok rut no encontrado  // Unknown Error sin internet

          if (error.status == 0 || error.statusText == "Unknown Error") {
            alert("No se puede confirmar RUT. No hay conexión con el Servidor");
            sessionStorage.rutValEstado = false;
            return false;
          }

          if (error.status == 404 || error.statusText == "Not Found") {

            //EVITA INGRESAR RUT DUPLICADO

            ////
            //validar si está repetido el rut
            var i = 0;
            for (const key in this.listaRut) {
              if (this.listaRut.hasOwnProperty(key)) {
                const element = this.listaRut[key];
                if (element.rut == rut) {
                  i++;
                }
              }
            }

            if (i > 0) {
              alert("Existe en la BD");
              return false;
            } else {
              //alert UTIL
              // alert("Este RUT es Nuevo en nuestra BD");
              this.datosUsuario.usuarioNombre = "";
              this.datosUsuario.usuarioApellidoPaterno = "";
              this.datosUsuario.usuarioCelular = "";

            }

            //alert(this.dataLocal.guardados);
            this.caja_rut = false;
            this.boton_rut = true;
            //Validar rut en BD

            //////


            //GUARDAR **NUEVO RUT** EN BD NOMBRE APELLIDO TELEFONO
            //this.info = {fechaLatLong:1,idLatLong:2,latitud:3,longitud:4,fechaEntrada:5};
            this.caja_rut_datos = true;




            //alert(this.dataLocal.guardados);
            //this.caja_rut = false;
            //this.boton_rut = true;
            //Validar rut en BD
            //this.dataLocal.guardarRegistro("Manual", rut, idlabor, estado);
            console.log(error);
            //return false;
          }


        })

    //

  }







  cerrarLabor(index: number) {
    //alert UTIL
    //alert(index);

    //1 = activo 2= inactivo
    //this.dataLocalLabor.splice(index, 4);
    //console.log(this.dataLocalLabor);
    // this.dataLocal.borrarRegistro(index);
    this.listaRut.splice(index, 1);

  }



  guardar_nuevo_rut_usuario(datosUsuario, datosUsuario2) {

    this.listaRut.push({
      rut: datosUsuario2,
      idLabor: "",
      nombre: datosUsuario.usuarioNombre,
      apellido: datosUsuario.usuarioApellidoPaterno,
      formatoEntrada: "Nuevo",
      celular: datosUsuario.usuarioCelular,
      idFecha: "",
      estado: "1"
    });
    this.caja_rut_datos = false;
    this.mostrarIngresarLabor = true;
    console.log(this.listaRut);

    //AGREGAR USUARIO NUEVO EN BD

    // ****************PARA PROXIMA VERSION CREAR AQUI****************************

    var data_cruda = {
      idUsuario: "",
      usuarioRut: datosUsuario2,
      usuarioPass: "",
      usuarioNombres: datosUsuario.usuarioNombre,
      usuarioApellidoPaterno: datosUsuario.usuarioApellidoPaterno,
      usuarioApellidoMaterno: "",
      usuarioFono: datosUsuario.usuarioCelular,
      usuarioMail: "",
      contrato_idContrato: "1",
      perfil_idPerfil: "10",
      dispositivo_idDispositivo: "1",
      estado_idEstado: "1",
      nivelAcceso_idNivelAcceso: "4",
      fundo_idFundo: "1",
      urlfoto: "",
      passvisible: ""
    };

    this.dbrutusuarios.CrearPostRutUsuarios(data_cruda).subscribe(
      response => {
        //alert UTIL
        //alert("Agregado a BD Exitosamente");
        console.log("Respuesta ingreso de rut usuario: " + response);
        this.caja_rut_datos = false;
        datosUsuario = null;
        data_cruda = null;

      },
      error => {
        console.log("Error ingreso de rut usuario: " + error);
      }
    )

    //FIN AGREGAR USUARIO NUEVO EN BD

  }









  ingresarLaborGeneralRut(listaRut, numero) {
    console.log(numero + " - " + listaRut.length);

    //return false;

    if (numero > 0) {
      this.cargando();
      console.log(numero);
      this.avanceLaborGCantGrupo = numero;

      this.mostrarIngresarLabor = false;
      let data_cruda = JSON.parse(sessionStorage.getItem('data_cruda_labor'));
      console.log(data_cruda);
      this.laborObjetivo = data_cruda.laborObjetivo;

      //return false
      //Se guardan los datos. En una nueva labor en BD
      this.dblaboresService.CrearPostLabores(data_cruda).subscribe(
        response => {
          //se obtiene id de la labor
          console.log("CrearPostLabores " + response.id);
          this.responseId = response.id;
          sessionStorage.idlabor = response.id;
          //alert UTIL
          //alert("Labor creada exitosamente. Actualice el Listado de labores."+response.id);



          //////////////agregar avance//////////////

          /**/

          var formato = "interno";
          this.funcionesService.obtenerGeolocalizacion();
          //alert("Usted agregó la cantidad de: "+num+" Id Labor: "+ labor+" Usuario Rut: "+rut);
          //return  false;                           
          var fecha = this.funcionesService.fecha_hora_actual();

          this.avancelaborglocal.guardarAvanceLaborG(
            this.avanceLaborGCantidad = "",
            this.avanceLaborGRutTrabajador = sessionStorage.usuarioRut,
            this.avanceLaborGObjetivoLabor = null,
            this.avanceLaborGLat = sessionStorage.lat,
            this.avanceLaborGLng = sessionStorage.lng,
            this.avanceLaborGFecha = fecha,
            this.avanceLaborGCantGrupo = this.numero,
            this.avanceLaborGFormato = formato + "-INICIAL",
            this.labor_idLabor = sessionStorage.idlabor,
            this.envio = "1"
          );
          this.cantidadCantidad = "";


          this.limpiar_form();

          if (this.numero != "") {
            //this.limpiar_form(); 
            this.numero = "";
            this.restaurar1();
            this.router.navigate(['/listado-labores']);
            return false;
          }

          /**/

          /*
                                          for (const key in listaRut) {
                                            
                                            if (listaRut.hasOwnProperty(key)) {
                                              let element = listaRut[key];
                                                console.log(element.rut);
          
                                                this.funcionesService.obtenerGeolocalizacion();
                                    
                                                let info: any = new FormData();
                                          
                                                  info.append('avanceLaborGCantidad','0'); //se pone cero porque es la labor inicial
                                                  info.append('avanceLaborGRutTrabajador', element.rut);
                                                  info.append('avanceLaborGObjetivoLabor',this.laborObjetivo);
                                                  info.append('avanceLaborGLat',sessionStorage.lat);
                                                  info.append('avanceLaborGLng',sessionStorage.lng);
                                                  info.append('avanceLaborGFecha', this.funcionesService.fecha_hora_actual());
                                                  info.append('avanceLaborGCantGrupo',this.avanceLaborGCantGrupo);
                                                  info.append('avanceLaborGFormato','INICIAL');
                                                  info.append('labor_idLabor', this.responseId);
                                          
                                                var object = {};
                                                info.forEach((value, key) => { object[key] = value });
                                                var data_cruda_avance = object;           
          
                                            //Se guardan los datos. En una nueva labor en BD
                                            this.avancelaborgdb.CrearPostAvance(data_cruda_avance).subscribe(
                                              response => {
                                                console.log("CrearPostAvance"+response);
                                                this.limpiar_form();  
                                                alert("Avance creado exitosamente. Actualice el Listado de labores.");
                                                this.router.navigate(['/listado-labores']);
                                              },
                                              error => {
                                                console.log("CrearPostAvance error "+error);
                                                this.limpiar_form();  
                                                alert("No se pudo crear el Avance. Hubo un error en el proceso de creación.");
                                              }
                                            )
          
          
                                            }
                                          }
                                          */
          ////////////////fin agregar avance////////////////////////////

          ////////////////localStorage

          for (let key in listaRut) {
            if (listaRut.hasOwnProperty(key)) {
              let element = listaRut[key];
              // localStorage.listaRut.Push(element);

              //agregar el valor del id que se obtiene luego de crearse en la BD
              this.listaRut2.push({
                rut: element.rut,
                idLabor: String(this.responseId),
                nombre: element.nombre,
                apellido: element.apellido,
                formatoEntrada: element.formatoEntrada,
                celular: element.celular,
                idFecha: sessionStorage.idFecha,
                estado: element.estado
              });
            } //fin for
          }
          console.log(this.listaRut2);


          //////////leer desde localStorage listaRut
          let data_cruda2 = JSON.parse(localStorage.getItem('listaRut'));
          console.log(data_cruda2);

          //si data cruda2 está vacío entonces guardfe solamente la tabla con datos. listaRut2
          if (!data_cruda2) {

            this.listaRut2.sort().reverse();
            //La lista concatenada data_cruda3 se ingresa al localStorage
            localStorage.listaRut = JSON.stringify(this.listaRut2);

          } else {
            //concateno las 2 listas la temporal listaRut y la respaldada en LocalStorage
            let data_cruda3 = this.listaRut2.concat(data_cruda2);
            //ORDENA LA LISTA EN FORMA DESCENDENTE
            data_cruda3.sort().reverse();

            //La lista concatenada data_cruda3 se ingresa al localStorage
            localStorage.listaRut = JSON.stringify(data_cruda3);
          }


        },
        error => {
          console.log("CrearPostLabores error " + error);
          this.limpiar_form();
          alert("No se pudo crear la Labor. Hubo un error en el proceso de creación.");
        }
      )


    }




    if (listaRut.length > 0) {
      this.cargando();
      console.log(listaRut.length);
      /*inicio listarut*/
      //obtener idLabor
      //crear uno por uno los avances iniciales para cada rut.
      console.log(listaRut);

      this.mostrarIngresarLabor = false;
      let data_cruda = JSON.parse(sessionStorage.getItem('data_cruda_labor'));
      console.log(data_cruda);
      this.laborObjetivo = data_cruda.laborObjetivo;

      //return false
      //Se guardan los datos. En una nueva labor en BD
      this.dblaboresService.CrearPostLabores(data_cruda).subscribe(
        response => {
          //se obtiene id de la labor
          console.log("CrearPostLabores " + response.id);
          this.responseId = response.id;
          sessionStorage.idlabor = response.id;
          this.limpiar_form();
          //alert UTIL
          //alert("Labor creada exitosamente. Actualice el Listado de labores."+response.id);

          if (this.numero != "") {
            //this.limpiar_form(); 
            this.numero = "";
            this.restaurar1();
            this.router.navigate(['/listado-labores']);
            //return false;
          }

          //////////////agregar avance//////////////

          for (const key in listaRut) {
            if (listaRut.hasOwnProperty(key)) {
              let element = listaRut[key];
              console.log(element.rut);

              this.funcionesService.obtenerGeolocalizacion();

              let info: any = new FormData();

              info.append('avanceLaborGCantidad', '0'); //se pone cero porque es la labor inicial
              info.append('avanceLaborGRutTrabajador', element.rut);
              info.append('avanceLaborGObjetivoLabor', this.laborObjetivo);
              info.append('avanceLaborGLat', sessionStorage.lat);
              info.append('avanceLaborGLng', sessionStorage.lng);
              info.append('avanceLaborGFecha', this.funcionesService.fecha_hora_actual());
              info.append('avanceLaborGCantGrupo', this.avanceLaborGCantGrupo);
              info.append('avanceLaborGFormato', 'INICIAL');
              info.append('labor_idLabor', this.responseId);

              var object = {};
              info.forEach((value, key) => { object[key] = value });
              var data_cruda_avance = object;

              //Se guardan los datos. En una nueva labor en BD
              this.avancelaborgdb.CrearPostAvance(data_cruda_avance).subscribe(
                response => {
                  console.log("CrearPostAvance" + response);
                  this.limpiar_form();
                  //alert("Avance creado exitosamente. Actualice el Listado de labores.");
                  this.router.navigate(['/listado-labores']);
                },
                error => {
                  console.log("CrearPostAvance error " + error);
                  this.limpiar_form();
                  alert("No se pudo crear el Avance. Hubo un error en el proceso de creación.");
                }
              )


            }
          }
          ////////////////fin agregar avance////////////////////////////

          ////////////////localStorage

          for (let key in listaRut) {
            if (listaRut.hasOwnProperty(key)) {
              let element = listaRut[key];
              // localStorage.listaRut.Push(element);

              //agregar el valor del id que se obtiene luego de crearse en la BD
              this.listaRut2.push({
                rut: element.rut,
                idLabor: String(this.responseId),
                nombre: element.nombre,
                apellido: element.apellido,
                formatoEntrada: element.formatoEntrada,
                celular: element.celular,
                idFecha: sessionStorage.idFecha,
                estado: element.estado
              });
            } //fin for
          }
          console.log(this.listaRut2);


          //////////leer desde localStorage listaRut
          let data_cruda2 = JSON.parse(localStorage.getItem('listaRut'));
          console.log(data_cruda2);

          //si data cruda2 está vacío entonces guardfe solamente la tabla con datos. listaRut2
          if (!data_cruda2) {

            this.listaRut2.sort().reverse();
            //La lista concatenada data_cruda3 se ingresa al localStorage
            localStorage.listaRut = JSON.stringify(this.listaRut2);

          } else {
            //concateno las 2 listas la temporal listaRut y la respaldada en LocalStorage
            let data_cruda3 = this.listaRut2.concat(data_cruda2);
            //ORDENA LA LISTA EN FORMA DESCENDENTE
            data_cruda3.sort().reverse();

            //La lista concatenada data_cruda3 se ingresa al localStorage
            localStorage.listaRut = JSON.stringify(data_cruda3);
          }
        },
        error => {
          console.log("CrearPostLabores error " + error);
          this.limpiar_form();
          alert("No se pudo crear la Labor. Hubo un error en el proceso de creación.");
        }
      )

      //
      /*fin listarut*/
    }




  }











  ingresarLaborGeneralGrupo(numero) {

    var num = numero;

    var formato = "interno";

    this.funcionesService.obtenerGeolocalizacion();

    //alert("Usted agregó la cantidad de: "+num+" Id Labor: "+ labor+" Usuario Rut: "+rut);
    //return  false;

    var fecha = this.funcionesService.fecha_hora_actual();

    this.avancelaborglocal.guardarAvanceLaborG(

      this.avanceLaborGCantidad = "INICIAL",
      this.avanceLaborGRutTrabajador = sessionStorage.usuarioRut,
      this.avanceLaborGObjetivoLabor = null,
      this.avanceLaborGLat = sessionStorage.lat,
      this.avanceLaborGLng = sessionStorage.lng,
      this.avanceLaborGFecha = fecha,
      this.avanceLaborGCantGrupo = num,
      this.avanceLaborGFormato = formato,
      this.labor_idLabor = "",
      this.envio = "1"
    );
    this.cantidadCantidad = "";

    //this.volver();
    this.router.navigate(['/listado-labores']);

  }



















  abrirRegistro(registro) {
    /*
    alert(
      "Rut :"+
      registro.rut
      +" \nId Labor :"+
      registro.idLabor
      +" \nNombre :"+
      registro.nombre
      +" "+
      registro.apellido
      +" \nFormato :"+
      registro.formatoEntrada
      );*/
    alert(
      "Rut :" +
      registro.rut
      + " \nNombre :" +
      registro.nombre
      + " " +
      registro.apellido
    );

    console.log('Registro: ', registro);
  }





  scan(laborOT) {

    //alert("scan");

    this.barcodeScanner.scan().then(barcodeData => {
      console.log('barcode data', barcodeData);

      if (!barcodeData.cancelled) {


        //
        var rut1 = Array.from(barcodeData.text);
        var rutOk = "";

        for (var i = 0; i <= rut1.length; i++) {
          var xx = rut1[i];

          if ((i >= 52 && i <= 62) && (xx != "=" && xx != "&" && xx != "-" && xx != "t" && xx != "<" && xx != "L")) {
            console.log(i + " - " + rut1[i]);
            rutOk += rut1[i];
            console.log(i + " - " + rut1[i] + " - " + rutOk);
          }

        }
        var rut = rutOk;
        var estado = "1";
        //conectarse y verificar rut con lista negra rut


        var rut_formateado = this.formato_rut(rut);

        /////
        var rut = "";
        //alert(this.datosUsuario2.rut2);
        //alert(manual+" "+rut2+" "+laborOT+" "+estado);

        this.escanearBoton = true;
        this.grupoBoton = false;
        this.manualBoton = true;

        //alert(rut);
        //validar Rut con BD lista negra
        //conectarse y verificar rut con lista negra rut
        this.dbrutusuarios.getRutUsuarios(rut_formateado)
          .subscribe(
            response => {

              console.log(response);



              this.listaNegra = response;

              //var respuesta = Object.values(response.usuario.usuarioRut);
              let usuarioRut = response.usuario.usuarioRut;
              let respuestaid = response.usuario.idUsuario;
              //var respuestax = response;

              console.log(respuestaid);



              //lista negra obtener respuesta de id
              this.listanegra.getListaNegra(respuestaid)
                .subscribe(
                  response => {


                    console.log(response);
                    console.log(response.logListaNegraMotivo);

                    if (response.idLogListaNegra != "1") {
                      alert("NO puede trabajar en la empresa está en LISTA NEGRA: \n"
                        + " Nº: " + response.idLogListaNegra
                        + " \nFecha: " + response.logListaNegraFecha
                        + " \nMotivo: " + response.logListaNegraMotivo
                      )
                    } else {
                      //validar si está repetido el rut
                      var i = 0;
                      for (const key in this.listaRut) {
                        if (this.listaRut.hasOwnProperty(key)) {
                          const element = this.listaRut[key];
                          if (element.rut == usuarioRut) {
                            i++;
                          }
                        }
                      }

                      if (i > 0) {
                        alert("Existe en la BD");
                      } else {
                        //alert UTIL
                        //alert("SIN ANTECEDENTES, Puede Trabajar en la Empresa");
                        this.listaRut.push({
                          rut: usuarioRut,
                          idLabor: "",
                          nombre: "",
                          apellido: "",
                          formatoEntrada: "",
                          celular: "",
                          idFecha: "",
                          estado: "1"
                        });
                        console.log(this.listaRut);
                        this.mostrarIngresarLabor = true;

                      }

                      //alert(this.dataLocal.guardados);
                      this.caja_rut = false;
                      this.boton_rut = true;
                      //Validar rut en BD

                    }

                  })

            },
            error => {
              console.log(error);
              console.log(error.registros_rut_usuarios);

              console.log(error.status);//404 rut no encontrado   // 0  sin internet
              console.log(error.statusText);//ok rut no encontrado  // Unknown Error sin internet

              if (error.status == 0 || error.statusText == "Unknown Error") {
                alert("No se puede confirmar RUT. No hay conexión con el Servidor");
                sessionStorage.rutValEstado = false;
                return false;
              }

              if (error.status == 404 || error.statusText == "Not Found") {

                //EVITA INGRESAR RUT DUPLICADO

                ////
                //validar si está repetido el rut
                var i = 0;
                for (const key in this.listaRut) {
                  if (this.listaRut.hasOwnProperty(key)) {
                    const element = this.listaRut[key];
                    if (element.rut == rut) {
                      i++;
                    }
                  }
                }

                if (i > 0) {
                  alert("Existe en la BD");
                  return false;
                } else {
                  //alert UTIL
                  //alert("Este RUT es Nuevo en nuestra BD");
                  this.datosUsuario.usuarioNombre = "";
                  this.datosUsuario.usuarioApellidoPaterno = "";
                  this.datosUsuario.usuarioCelular = "";
                }

                //alert(this.dataLocal.guardados);
                this.caja_rut = false;
                this.boton_rut = true;
                //Validar rut en BD

                //////


                //GUARDAR **NUEVO RUT** EN BD NOMBRE APELLIDO TELEFONO
                //this.info = {fechaLatLong:1,idLatLong:2,latitud:3,longitud:4,fechaEntrada:5};
                this.caja_rut_datos = true;
                //alert(this.dataLocal.guardados);
                //this.caja_rut = false;
                //this.boton_rut = true;
                //Validar rut en BD
                //this.dataLocal.guardarRegistro("Manual", rut, idlabor, estado);
                console.log(error);
                //return false;
              }


            })

        //
        ///////
        this.escanearBoton = true;
        this.grupoBoton = false;
        this.manualBoton = true;

      }

    }).catch(err => {


      console.log('Error', err);
      //let estado = "";
      alert("Error al escanear: " + err);
      //this.dataLocal.guardarRegistro('QRCode', 'https://www.digital5.cl', sessionStorage.usuarioNumberOT, estado);

      this.escanearBoton = true;
      this.grupoBoton = false;
      this.manualBoton = false;

    });



  }


  formato_rut(rut) {
    let value = rut.replace(/\./g, '').replace('-', '');

    if (value.match(/^(\d{2})(\d{3}){2}(\w{1})$/)) {
      value = value.replace(/^(\d{2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4');
    }
    else if (value.match(/^(\d)(\d{3}){2}(\w{0,1})$/)) {
      value = value.replace(/^(\d)(\d{3})(\d{3})(\w{0,1})$/, '$1.$2.$3-$4');
    }
    else if (value.match(/^(\d)(\d{3})(\d{0,2})$/)) {
      value = value.replace(/^(\d)(\d{3})(\d{0,2})$/, '$1.$2.$3');
    }
    else if (value.match(/^(\d)(\d{0,2})$/)) {
      value = value.replace(/^(\d)(\d{0,2})$/, '$1.$2');
    }
    return value;
  }





  async cargando() {
    const loading = await this.loadingController.create({
      message: 'Procesando...',
      duration: 3000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }



  iniciarGeolocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude
      this.lon = resp.coords.longitude

      console.log(resp.coords);

      let watch = this.geolocation.watchPosition();
      watch.subscribe((data) => {
        // data can be a set of coordinates, or an error (if an error occurred).
        // data.coords.latitude
        // data.coords.longitude

        this.lat = data.coords.latitude
        this.lon = data.coords.longitude
        console.log('Watch: ', data);


      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });

    60000
  }


  agregar1() {
    return new Promise((resolve) => {
      let body = {
        datos: "insertar",
        longitud: this.lon,
        latitud: this.lat,

        proce1: this.proce,
        user1: this.idUsuario,
        estado1: this.estado1,

        geo1: this.geo,
        fecha: this.funcionesService.fecha_hora_actual()
      };

      this.prosecnt.postData(body, "geo2.php").subscribe((data) => {
        console.log("OK");
      });
    });
  }





}
