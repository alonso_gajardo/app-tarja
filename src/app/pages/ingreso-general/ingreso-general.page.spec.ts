import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoGeneralPage } from './ingreso-general.page';

describe('IngresoGeneralPage', () => {
  let component: IngresoGeneralPage;
  let fixture: ComponentFixture<IngresoGeneralPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoGeneralPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoGeneralPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
