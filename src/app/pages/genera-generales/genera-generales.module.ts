import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GeneraGeneralesPage } from './genera-generales.page';

const routes: Routes = [
  {
    path: '',
    component: GeneraGeneralesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GeneraGeneralesPage]
})
export class GeneraGeneralesPageModule {}
