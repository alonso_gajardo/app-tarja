import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Lislabor2PageRoutingModule } from './lislabor2-routing.module';

import { Lislabor2Page } from './lislabor2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Lislabor2PageRoutingModule
  ],
  declarations: [Lislabor2Page]
})
export class Lislabor2PageModule {}
