import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Lislabor2Page } from './lislabor2.page';

describe('Lislabor2Page', () => {
  let component: Lislabor2Page;
  let fixture: ComponentFixture<Lislabor2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lislabor2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Lislabor2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
