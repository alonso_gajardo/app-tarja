import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataLocalLaboresService } from '../../services/data-local-labores.service';
//import { Icon } from 'ionicons/dist/types/icon/icon';
import { FuncionesService } from 'src/app/services/funciones.service';
import { DBcontratistaService } from 'src/app/services/data-bd-contratista.services';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DBlaboresService } from 'src/app/services/data-bd-labores.service';
import { RegistroLabores } from 'src/app/models/registroLabores.model';
import {Storage} from '@ionic/storage';
import { AlertPage } from '../alert/alert.page';
import { LoadingController } from '@ionic/angular';
import {Proseso} from "../../provedor/proseso";
@Component({
  selector: 'app-lislabor2',
  templateUrl: './lislabor2.page.html',
  styleUrls: ['./lislabor2.page.scss'],
})
export class Lislabor2Page implements OnInit {

  geo = "GPS x Tiempo";
  lat: number;
  lon: number;

  proce: string = "1";
  estado: string = "1";
  arregloLocalVista: any[]=[] = JSON.parse( localStorage.getItem('arregloLocal') ) ;
  arregloLocalVista2: any[]=[];

  segmento: string;

  listaContratista: any[] = [];

  menuContratista: any[]=[];

  textoBuscar = '';

  permiso: string;



   idUsuario: string = sessionStorage.idUsuario;
  
   guardadosLabores: RegistroLabores[] =[];


  labores = {
    idLabor:"",
    laborNombre: "",
    laborDescripcion: "",
    laborValorTratoInicial: "",
    laborValorTratoFinal: "",
    laborObjetivo: "",
    laborLat: "",
    laborLng: "",
    laborFecha: "",
    laborIconoAsignacion: "",
    laborIcono: "",
    laborCuartel: "",
    laborDescripcionCierre: "",
    estado_idEstado: "",
    tipoLabor_idTipoLabor: "",
    laborAplicacionQ_idLaborAplicacionQ: "",
    contratista_idContratista: "",
    usuario_idUsuario: ""

  };

  actualizar:number = 0;


  contratista = {
    idContratista:"",
    contratistaRut:"",
    contratistaNombre:"",
    contratistaGiro:"",
    contratistaFono:"",
    contratistaMail:"",
    contratistaDireccion:"",
    contratistaRazonSocial:"",
    estado_idEstado:""
  };

  formGeneral:boolean;

  listadoRegistros:boolean;
  botonesEntrada:boolean;


  //carga antes de cargar la página completa
  ionViewWillEnter() {
    sessionStorage.laboresNombre = this.labores.laborNombre;
    sessionStorage.laboresDescripcion = this.labores.laborDescripcion;
    console.log(this.labores.laborNombre + " - " + this.labores.laborDescripcion);
  }


  constructor(
    private router: Router,
    public dataLocalLaborGeneral: DataLocalLaboresService,
    public funcionesService: FuncionesService,
    public dbcontratistaService: DBcontratistaService,
    private geolocation: Geolocation,
    private dblaboresService: DBlaboresService,
    private storage: Storage,
    public loadingController: LoadingController,
    private prosecnt: Proseso

  ) { 

    this.formGeneral = true;

    this.listadoRegistros = false;
    this.botonesEntrada = false;
   
  
     }

     botones_estado_latlong(x) {
      //CREAR LATLONG LOCAL
  
  
      this.geolocation
        .getCurrentPosition()
        .then((resp) => {
          const gpsUsuarioLatLngLat = `${resp.coords.latitude}`;
          const gpsUsuarioLatLngLng = `${resp.coords.longitude}`;
          const gpsUsuarioLatLngFecha = this.funcionesService.fecha_hora_actual();
   
          const usuario_idUsuario = sessionStorage.idUsuario;
          const estadoLatLong = "1";
  
 
        })
        .catch((error) => {
          console.log("Error getting location", error);
        });
    }

     ngOnInit() {
  
      this.permiso = sessionStorage.usuarioNivelPermiso;
  
      this.contratista = JSON.parse( localStorage.getItem('contratistaLocal') );
  
    }

    guardar_nombre(nombre) {
      sessionStorage.laboresNombre = nombre;
    }
    guardar_descripcion(descripcion) {
      sessionStorage.laboresDescripcion = descripcion;
    }
    
    async ruta_laborGeneral(tipo){
       
      //this.formGeneral = false;
      
  
      //CREAR labores EN DB {
      this.funcionesService.obtenerGeolocalizacion();
  

     var idContratista = this.contratista.idContratista;
     var laborValorTratoInicial = this.labores.laborValorTratoInicial;
     var tipificacionLabor = "";
  
  switch(tipo) {
    case 'autoasignada':
      this.cargando();
      tipificacionLabor = "1";
      idContratista = "1";//nulo
      laborValorTratoInicial = "";//nulo
      sessionStorage.tipoLabor="1";
      break;
    case 'interna':
      tipificacionLabor = "2";
      idContratista = "1";//nulo
      laborValorTratoInicial = "";//nulo
      sessionStorage.tipoLabor="2";
      break;
    case 'contratista':
      tipificacionLabor = "3";
      sessionStorage.tipoLabor="3";
      break;
    default:
      // code block
  }
  
  
     //1 = activo 2= inactivo
     var estado = "1";
     var lat = sessionStorage.lat;
     var lng = sessionStorage.lng;
  
     //console.log(this.contratista.idContratista);
     //return false;
  
    
  
  console.log(tipificacionLabor+" - "+idContratista);
    //return false;
  
        //formData
        let info: any = new FormData();
  
        info.append('laborNombre', this.labores.laborNombre);
        info.append('laborDescripcion', this.labores.laborDescripcion);
        info.append('laborValorTratoInicial',laborValorTratoInicial);
        info.append('laborValorTratoFinal',"");
        info.append('laborObjetivo', this.labores.laborObjetivo);
        info.append('laborLat', lat);
        info.append('laborLng', lng);
        info.append('laborFecha', this.funcionesService.fecha_hora_actual());
        info.append('laborIconoAsignacion',  this.funcionesService.obtener_iconoAsignacion(tipificacionLabor));
        info.append('laborIcono',  this.funcionesService.obtener_iconoLabor(tipificacionLabor));
        info.append('laborCuartel',this.labores.laborCuartel);
        info.append('laborDescripcionCierre','sin descripcion cierre');
        info.append('estado_idEstado', estado);
        info.append('tipoLabor_idTipoLabor', tipificacionLabor);
        info.append('laborAplicacionQ_idLaborAplicacionQ',1);
        info.append('contratista_idContratista',idContratista);
        info.append('usuario_idUsuario', sessionStorage.idUsuario);
  
  
        var object = {};
        info.forEach((value, key) => { object[key] = value });
        var data_cruda = object;
        console.log(data_cruda);
  
        sessionStorage.data_cruda_labor = JSON.stringify(data_cruda);
  /*
  this.router.navigate(['/ingreso-general']);
  */
  
  if(tipificacionLabor == '1'){
          //se guardan los datos. Es una nueva labor en BD
          this.dblaboresService.CrearPostLabores(data_cruda).subscribe(
            response => {
              
              console.log("CrearPostLabores "+response);
              this.limpiar_form();  
              alert("Labor creada exitosamente.");
              
              
              //this.ir_a_ingreso_general(tipificacionLabor);
              this.router.navigate(['/listado-labores']);
  
             //this.actualizar_listado_arregloLocal();
              
            },
            error => {
             // this.actualizar_listado_arregloLocal();
              console.log("CrearPostLabores error "+error);
              this.limpiar_form();  
              alert("No se pudo crear la Labor. Hubo un error en el proceso de creación.");
            }
          )
  }else{
    this.ir_a_ingreso_general(tipificacionLabor);
    //this.router.navigate(['/ingreso-general']);
  }
  
  
  
  }
  

  
ir_a_ingreso_general(x){

  //alert(x);
  this.limpiar_form();  
  //sessionStorage.tipoLabor=x;
  //return false;
  this.router.navigate(['/ingreso-general']);
}








  limpiar_form() {
    this.labores.idLabor ="",
    this.labores.laborNombre = "",
      this.labores.laborDescripcion = "",
      this.labores.laborValorTratoInicial = "",
      this.labores.laborValorTratoFinal = "",
      this.labores.laborObjetivo = "",
      this.labores.laborLat = "",
      this.labores.laborLng = "",
      this.labores.laborFecha = "",
      this.labores.laborIconoAsignacion = "",
      this.labores.laborIcono = "",
      this.labores.laborCuartel = "",
      this.labores.laborDescripcionCierre = "",
      this.labores.estado_idEstado = "",
      this.labores.tipoLabor_idTipoLabor = "",
      this.labores.laborAplicacionQ_idLaborAplicacionQ = "",
      this.labores.contratista_idContratista = "",
      this.labores.usuario_idUsuario = ""

  }





  async cargando() {
    const loading = await this.loadingController.create({
      message: 'Procesando...',
      duration: 3000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }






  actualizar_listado_arregloLocal(event){
    //actualiza la tabla genial
    console.log("actualizar tabla arregloLocal");
    this.actualizar = 1;
    this.obtener_laboresDB();
    

setTimeout(() => {
  this.arregloLocalVista = JSON.parse( localStorage.getItem('arregloLocal') );
  event.target.complete();
  this.actualizar = 0;
 // this.volver();
 },1500);
    //this.doRefresh(event.isTrusted);
//actualiza la tabla genial  fin
  }







  obtener_laboresDB(){

  
    this.cargando();
  var idUsuario = sessionStorage.idUsuario;
    //console.log(idUsuario);
    const arregloLocal =[];

        //DB LLAMADA a tabla LABORES x idUsuario y agregar datos en BD LOCAL 100%
        this.dblaboresService.getLaboresXidCliente(idUsuario).subscribe(
          response => {
    
            for (let i = 0; i < response.registros_labores.length; i++) {
              const element = response.registros_labores[i];
             console.log(response.registros_labores.length);


    
              if(element.estado_idEstado == "1"){

                arregloLocal.push({
                  idLabor: element.idLabor,
                  laborNombre: element.laborNombre,
                  laborDescripcion: element.laborDescripcion,
                  laborValorTratoInicial: element.laborValorTratoInicial,
                  laborValorTratoFinal: element.laborValorTratoFinal,
                  laborObjetivo: element.laborObjetivo,
                  laborLat: element.laborLat,
                  laborLng: element.laborLng,
                  laborFecha: element.laborFecha,
                  laborIconoAsignacion: element.laborIconoAsignacion,
                  laborIcono: element.laborIcono,
                  laborCuartel: element.laborCuartel,
                  laborDescripcionCierre: element.laborDescripcionCierre,
                  estado_idEstado: element.estado_idEstado,
                  tipoLabor_idTipoLabor: element.tipoLabor_idTipoLabor,
                  laborAplicacionQ_idLaborAplicacionQ: element.laborAplicacionQ_idLaborAplicacionQ,
                  contratista_idContratista: element.contratista_idContratista,
                  usuario_idUsuario: element.usuario_idUsuario});


              }



            }
            //ORDENA LA LISTA EN FORMA DESCENDENTE
            arregloLocal.sort().reverse();          
            localStorage.arregloLocal = JSON.stringify(arregloLocal);
            this.arregloLocalVista2 = arregloLocal;
           // this.actualizar = 1;
          },
          error => {
            console.log(error);
          }
        );

    }



    volver(){

      this.cargando();
      //en caso de que este parado en segmento inicial vuelve a playpause, de lo contrario
      //se mantiene en la seccion listado-labores .
        if(this.segmento=="inicial"){
          this.router.navigate(['/playpause']);
        }else{
          //this.numeroOtCantidad = "";
          //this.cierreDescripcion = "";
          //this.cierreTratoFinal = "";
          this.router.navigate(['/listado-labores']);
          this.segmento="inicial";
          this.listadoRegistros= true;
          this.botonesEntrada=true;
        }
  
      }






      INTERMO(){
        this.router.navigate(['/lislabor2']);
      }
    


      iniciarGeolocation(){
        this.geolocation.getCurrentPosition().then((resp) => {
          this.lat =   resp.coords.latitude
          this.lon = resp.coords.longitude
    
            console.log(resp.coords);
    
            let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
     // data can be a set of coordinates, or an error (if an error occurred).
     // data.coords.latitude
     // data.coords.longitude
    
     this.lat =   data.coords.latitude
     this.lon = data.coords.longitude
     console.log('Watch: ', data);
    
    
    });
           }).catch((error) => {
             console.log('Error getting location', error);
           });
    
           60000    
      }
    
    
      agregar1() {
        return new Promise((resolve) => {
          let body = {
            datos: "insertar",
            longitud: this.lon,
            latitud: this.lat,
         
            proce1: this.proce,
            user1 :this.idUsuario,
            estado1: this.estado,
           
            geo1: this.geo,
            fecha: this.funcionesService.fecha_hora_actual()
          };
    
          this.prosecnt.postData(body, "geo2.php").subscribe((data) => {
            console.log("OK");
          });
        });
    }
}
