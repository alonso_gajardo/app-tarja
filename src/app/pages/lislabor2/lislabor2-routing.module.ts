import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Lislabor2Page } from './lislabor2.page';

const routes: Routes = [
  {
    path: '',
    component: Lislabor2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Lislabor2PageRoutingModule {}
