import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalContratistaPageRoutingModule } from './modal-contratista-routing.module';

import { ModalContratistaPage } from './modal-contratista.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalContratistaPageRoutingModule
  ],
  declarations: [ModalContratistaPage]
})
export class ModalContratistaPageModule {}
