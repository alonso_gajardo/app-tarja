import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalContratistaPage } from './modal-contratista.page';

const routes: Routes = [
  {
    path: '',
    component: ModalContratistaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalContratistaPageRoutingModule {}
