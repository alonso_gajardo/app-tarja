import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IngresoQuimicaPage } from './ingreso-quimica.page';

const routes: Routes = [
  {
    path: '',
    component: IngresoQuimicaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngresoQuimicaPageRoutingModule {}
