import { Component, OnInit } from '@angular/core';
import { DBlaboresService } from 'src/app/services/data-bd-labores.service';
import { AvanceLaborQDB  } from 'src/app/services/avancelaborqDB.service';
import { FuncionesService } from 'src/app/services/funciones.service';
import { Router } from '@angular/router';
import { DBrutUsuariosService } from 'src/app/services/data-db-rut-usuarios.service';
import { DataLocalService } from 'src/app/services/data-local.service';
import { ListaNegraDB } from 'src/app/services/listanegraDB.service';
import { AvanceLaborQLocal } from 'src/app/services/avancelaborq.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ProductosQFusionLocal } from 'src/app/services/productosQFusion.service';
import {Proseso} from "../../provedor/proseso";
import { Geolocation } from '@ionic-native/geolocation/ngx';
@Component({
  selector: 'app-ingreso-quimica',
  templateUrl: './ingreso-quimica.page.html',
  styleUrls: ['./ingreso-quimica.page.scss'],
})
export class IngresoQuimicaPage implements OnInit {


  
  show: boolean;
  radio: boolean;
  seleccionRadioButton: string;
  etapa2: boolean;
  etapa2_2:boolean;
  etapa3: boolean;
  numero: string;

  check_1_general:boolean;
  check_2_general:boolean;

  labores = {
    idLabor:"",
    laborNombre: "",
    laborDescripcion: "",
    laborValorTratoInicial: "",
    laborValorTratoFinal: "",
    laborObjetivo: "",
    laborLat: "",
    laborLng: "",
    laborFecha: "",
    laborIconoAsignacion: "",
    laborIcono: "",
    laborCuartel: "",
    laborDescripcionCierre: "",
    estado_idEstado: "",
    tipoLabor_idTipoLabor: "",
    laborAplicacionQ_idLaborAplicacionQ: "",
    contratista_idContratista: "",
    usuario_idUsuario: ""

  };

  
  laborObjetivo:string;
  responseId:string;


  escanearBoton: boolean;
  grupoBoton: boolean;
  manualBoton: boolean;
  caja_rut: boolean = false;
  boton_rut: boolean = true;
  caja_rut_datos: boolean = false;

 datosUsuario ={
  usuarioNombre:'',
  usuarioApellidoPaterno:'',
  usuarioCelular:''
 }


  datosUsuario2 = {
    rut2: ''
  };


  listaNegra = "";

 // listaRut:string [] = [];
  mostrarIngresarLabor: boolean = false;
 
  listaRut = [{
    rut: "",
    idLabor: "",
    nombre:"",
    apellido: "",
    formatoEntrada: "",
    celular: "",
    idFecha:"",
    estado:""
  }];


  listaRut2 = [];

  listaFusionQlocalRut = [];
  listaFusionQLocal = [];
  productosQLocal = [];

  productosQ = [];




listaRutlocalRut: {
    rut: string;
    idLabor: string;
    nombre: string;
    apellido: string;
    formatoEntrada: string;
    celular: string;
    idFecha: string;
}



  avanceLaborGCantidad:string;
  avanceLaborGRutTrabajador:string;
  avanceLaborGObjetivoLabor:string;
  avanceLaborGLat:string;
  avanceLaborGLng:string;
  avanceLaborGFecha:string;
  avanceLaborGCantGrupo:string;
  avanceLaborGFormato:string;
  labor_idLabor:string;
  envio:string;





  rut: string;
  idLabor: string;
  nombre: string;
  apellido: string;
  formatoEntrada: string;
  celular: string;
  idFecha: string;
  productosQCantidad: string;
  productosQNombre: string;
  unidadmedida_idUnidadMedida: string;
  estado: string;  

  tipoLaborNombre:string;
  usuarioRut:string;

  idUsuario: string = sessionStorage.idUsuario;
  geo = "GPS x Tiempo";
  lat: number;
  lon: number;

  proce: string = "1";
  estado1: string = "1";
  constructor(
    private dblaboresService: DBlaboresService,
    private avancelaborqdb: AvanceLaborQDB,
    private funcionesService: FuncionesService,
    private router: Router,
    private dbrutusuarios: DBrutUsuariosService,
    public dataLocal: DataLocalService,
    public listanegra: ListaNegraDB,
    private avancelaborqlocal: AvanceLaborQLocal,
    public barcodeScanner: BarcodeScanner,
    public storage: Storage,
    public loadingController: LoadingController,
    private productosQFusion: ProductosQFusionLocal,
    private geolocation: Geolocation,
    private prosecnt: Proseso
  ) {

    sessionStorage.idlabor ="";
    this.listaRut=[];
    this.tipoLaborNombre = sessionStorage.tipoLaborNombre;
    this.usuarioRut = sessionStorage.usuarioRut;

  }

  ngOnInit() {
    this.radio = true;
    this.etapa2 = false;
    this.etapa2_2 = true;
    this.etapa3 = false;

    this.check_1_general = false;
    this.check_2_general = false;

    this.avanceLaborGCantGrupo ="";
    this.avanceLaborGRutTrabajador ="";
    this.laborObjetivo="";
    this.responseId = "";

    this.scan();
    
  }
  botones_estado_latlong(x) {
    //CREAR LATLONG LOCAL


    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        const gpsUsuarioLatLngLat = `${resp.coords.latitude}`;
        const gpsUsuarioLatLngLng = `${resp.coords.longitude}`;
        const gpsUsuarioLatLngFecha = this.funcionesService.fecha_hora_actual();
 
        const usuario_idUsuario = sessionStorage.idUsuario;
        const estadoLatLong = "1";


      })
      .catch((error) => {
        console.log("Error getting location", error);
      });
  }

  seleccion(x){
    this.seleccionRadioButton = x;
    this.show = true;
    this.etapa2 = false;
  }

  avanzar_a_2(){
    this.show = false;
    this.radio = false;
    this.etapa2 = true;
    this.etapa2_2 = true;
    this.numero = "";
    this.check_1_general = true;
   }

  avanzar_a_3(x){
    this.radio = false;
    this.seleccionRadioButton = x;
    this.etapa2 = true;
    this.etapa2_2 = false;
    this.etapa3 = true;
  }

  restaurar1(){
    this.radio = true;
    this.etapa2 = false;
    this.etapa3 = false;
    this.check_1_general = false;
    this.mostrarIngresarLabor = false;
    this.numero = "";
  }

  restaurar2(){
    this.radio = true;
    this.etapa2 = false;
    this.etapa3 = false;
    this.check_2_general = false;
    this.mostrarIngresarLabor = false;
    this.numero = "";
  }

  restaurar3(){
    this.etapa3 = false;
    this.avanzar_a_2()
  }



/*

  crearLabor(){

let data_cruda = JSON.parse( sessionStorage.getItem('data_cruda_labor') );
console.log(data_cruda);
this.laborObjetivo = data_cruda.laborObjetivo;

//return false
        //Se guardan los datos. En una nueva labor en BD
        this.dblaboresService.CrearPostLabores(data_cruda).subscribe(
          response => {
            console.log("CrearPostLabores "+response.id);
            this.responseId = response.id;
            sessionStorage.idlabor = response.id;
            this.limpiar_form();  
            alert("Labor creada exitosamente. Actualice el Listado de labores.");



//////////////agregar avance//////////////
            if(this.seleccionRadioButton == 'Grupo'){
              this.avanceLaborGCantGrupo = this.numero;
            }
      
            if(this.seleccionRadioButton == 'Rut'){
              this.avanceLaborGCantGrupo = "";
              this.avanceLaborGRutTrabajador = ""; //falta agregar valor
            }
      
            this.funcionesService.obtenerGeolocalizacion();
      
      
      
            let info: any = new FormData();
      
              info.append('avanceLaborGCantidad','0'); //se pone cero porque es la labor inicial
              info.append('avanceLaborGRutTrabajador', this.avanceLaborGRutTrabajador);
              info.append('avanceLaborGObjetivoLabor',this.laborObjetivo);
              info.append('avanceLaborGLat',sessionStorage.lat);
              info.append('avanceLaborGLng',sessionStorage.lng);
              info.append('avanceLaborGFecha', this.funcionesService.fecha_hora_actual());
              info.append('avanceLaborGCantGrupo',this.avanceLaborGCantGrupo);
              info.append('avanceLaborGFormato','INICIAL');
              info.append('labor_idLabor', this.responseId);
      
            var object = {};
            info.forEach((value, key) => { object[key] = value });
            var data_cruda_avance = object;
      
              //Se guardan los datos. En una nueva labor en BD
              this.avancelaborgdb.CrearPostAvance(data_cruda_avance).subscribe(
                response => {
                  console.log("CrearPostAvance"+response);
                  this.limpiar_form();  
                  alert("Avance creado exitosamente. Actualice el Listado de labores.");
                  this.router.navigate(['/listado-labores']);
                },
                error => {
                  console.log("CrearPostAvance error "+error);
                  this.limpiar_form();  
                  alert("No se pudo crear el Avance. Hubo un error en el proceso de creación.");
                }
              )
////////////////fin agregar avance////////////////////////////




          },
          error => {
            console.log("CrearPostLabores error "+error);
            this.limpiar_form();  
            alert("No se pudo crear la Labor. Hubo un error en el proceso de creación.");
          }
        )

/* avance general */

      //formData
      //this.seleccionRadioButton;
      //this.numero;

/*
      if(this.seleccionRadioButton == 'Grupo'){
        this.avanceLaborGCantGrupo = this.numero;
      }

      if(this.seleccionRadioButton == 'Rut'){
        this.avanceLaborGCantGrupo = "";
        this.avanceLaborGRutTrabajador = ""; //falta agregar valor
      }

      this.funcionesService.obtenerGeolocalizacion();



      let info: any = new FormData();

        info.append('avanceLaborGCantidad','0'); //se pone cero porque es la labor inicial
        info.append('avanceLaborGRutTrabajador', this.avanceLaborGRutTrabajador);
        info.append('avanceLaborGObjetivoLabor',this.laborObjetivo);
        info.append('avanceLaborGLat',sessionStorage.lat);
        info.append('avanceLaborGLng',sessionStorage.lng);
        info.append('avanceLaborGFecha', this.funcionesService.fecha_hora_actual());
        info.append('avanceLaborGCantGrupo',this.avanceLaborGCantGrupo);
        info.append('avanceLaborGFormato','INICIAL');
        info.append('labor_idLabor', this.responseId);

      var object = {};
      info.forEach((value, key) => { object[key] = value });
      var data_cruda_avance = object;

        //Se guardan los datos. En una nueva labor en BD
        this.avancelaborgdb.CrearPostAvance(data_cruda_avance).subscribe(
          response => {
            console.log("CrearPostAvance"+response);
            this.limpiar_form();  
            alert("Avance creado exitosamente. Actualice el Listado de labores.");
          },
          error => {
            console.log("CrearPostAvance error "+error);
            this.limpiar_form();  
            alert("No se pudo crear el Avance. Hubo un error en el proceso de creación.");
          }
        )
*/

/*
}
*/



  limpiar_form() {
    this.labores.idLabor ="",
    this.labores.laborNombre = "",
      this.labores.laborDescripcion = "",
      this.labores.laborValorTratoInicial = "",
      this.labores.laborValorTratoFinal = "",
      this.labores.laborObjetivo = "",
      this.labores.laborLat = "",
      this.labores.laborLng = "",
      this.labores.laborFecha = "",
      this.labores.laborIconoAsignacion = "",
      this.labores.laborIcono = "",
      this.labores.laborCuartel = "",
      this.labores.laborDescripcionCierre = "",
      this.labores.estado_idEstado = "",
      this.labores.tipoLabor_idTipoLabor = "",
      this.labores.laborAplicacionQ_idLaborAplicacionQ = "",
      this.labores.contratista_idContratista = "",
      this.labores.usuario_idUsuario = ""
  }




  validar_rut() {

    this.escanearBoton = true;
    this.grupoBoton = false;
    this.manualBoton = true;

    this.caja_rut = true;
    this.boton_rut = false;
  }




  leer_rutValEstado(){

    return sessionStorage.rutValEstado;

 }



  ingresar_rut(manual, idlabor, estado) {
    //alert(sessionStorage.tipoLabor);
        
    var rut = "";
    //alert(this.datosUsuario2.rut2);
    //alert(manual+" "+rut2+" "+laborOT+" "+estado);

    this.escanearBoton = true;
    this.grupoBoton = false;
    this.manualBoton = true;

    if (this.datosUsuario2.rut2) {
      rut = this.datosUsuario2.rut2;
    } else {
      alert("Ingrese un Rut válido");
      return false
    }
    //alert(rut);
    //validar Rut con BD lista negra
    //conectarse y verificar rut con lista negra rut
    this.dbrutusuarios.getRutUsuarios(rut)
      .subscribe(
        response => {

          console.log(response);
          
          

          this.listaNegra = response;

          //var respuesta = Object.values(response.usuario.usuarioRut);
          let usuarioRut = response.usuario.usuarioRut;
          let respuestaid = response.usuario.idUsuario;
          let respuestaPerfil = response.usuario.perfil_idPerfil;

          let usuarioNombres = response.usuario.usuarioNombres;
          let usuarioApellidoPaterno = response.usuario.usuarioApellidoPaterno;
          let usuarioApellidoMaterno = response.usuario.usuarioApellidoMaterno;

          let respuestaNivelAcceso = response.usuario.nivelAcceso_idNivelAcceso;




          //var respuestax = response;

          console.log(respuestaid+" - "+respuestaPerfil);
          


          //lista negra obtener respuesta de id
          this.listanegra.getListaNegra(respuestaid)
          .subscribe(
            response => {


              console.log(response);
                console.log(response.logListaNegraMotivo);

                if(response.idLogListaNegra != "1"){
                    alert("NO puede trabajar en la empresa está en LISTA NEGRA: \n"
                    +" Nº: "+ response.idLogListaNegra
                    +" \nFecha: "+ response.logListaNegraFecha
                    +" \nMotivo: "+ response.logListaNegraMotivo
                    )
                }else{                 
                  //validar si está repetido el rut
                  var i = 0;
                  for (const key in this.listaRut) {
                    if (this.listaRut.hasOwnProperty(key)) {
                      const element = this.listaRut[key];
                        if(element.rut == usuarioRut){
                          i ++;
                        }
                    }
                  }

                  if(i > 0){
                    //alert UTIL
                    //alert("Existe en la BD");                   
                  }else{

                    //alert(respuestaPerfil);
                    //alert(respuestaNivelAcceso);

                   if(respuestaNivelAcceso === "3"){
                     //alert UTIL
                      //alert("Autorizado para trabajar en Aplicaciones Químicas");
                    }else{
                      //alert UTIL
                      alert("NO ESTA Autorizado para trabajar en Aplicaciones Químicas");
                      return false;
                    };

                    //alert UTIL
                    //alert("SIN ANTECEDENTES, Puede Trabajar en la Empresa");
                    this.listaRut.push({
                      rut: usuarioRut,
                      idLabor: "",
                      nombre:usuarioNombres,
                      apellido:usuarioApellidoPaterno,
                      formatoEntrada: "",
                      celular:"",
                      idFecha:"",
                      estado:""
                    });
                    console.log(this.listaRut);
                    this.mostrarIngresarLabor = true;


                  }
 
            //alert(this.dataLocal.guardados);
            this.caja_rut = false;
            this.boton_rut = true;
            //Validar rut en BD

                }

            })

        },
        error => {
          console.log(error);
          console.log(error.registros_rut_usuarios);

          console.log(error.status);//404 rut no encontrado   // 0  sin internet
          console.log(error.statusText);//ok rut no encontrado  // Unknown Error sin internet

          if(error.status == 0 || error.statusText == "Unknown Error"){
            alert("No se puede confirmar RUT. No hay conexión con el Servidor");
            sessionStorage.rutValEstado = false;
            return false;
          }

          if(error.status == 404 || error.statusText == "Not Found"){

              //EVITA INGRESAR RUT DUPLICADO
              
////
                  //validar si está repetido el rut
                  var i = 0;
                  for (const key in this.listaRut) {
                    if (this.listaRut.hasOwnProperty(key)) {
                      const element = this.listaRut[key];
                        if(element.rut == rut){
                          i ++;
                        }
                    }
                  }

                  if(i > 0){
                    alert("Existe en la BD");  
                    return false;                 
                  }else{
                   // alert("Este RUT es Nuevo en nuestra BD");
                    alert("NO FIGURA EN NUESTROS REGISTROS, NO ESTA Autorizado, para trabajar en Aplicaciones Químicas");
                   return false;
                  }
 
            //alert(this.dataLocal.guardados);
            this.caja_rut = false;
            this.boton_rut = true;
            //Validar rut en BD

//////


              //GUARDAR **NUEVO RUT** EN BD NOMBRE APELLIDO TELEFONO
              //this.info = {fechaLatLong:1,idLatLong:2,latitud:3,longitud:4,fechaEntrada:5};

              //this.caja_rut_datos = true;

              //alert(this.dataLocal.guardados);
              this.caja_rut = false;
              this.boton_rut = true;
              //Validar rut en BD
              //this.dataLocal.guardarRegistro("Manual", rut, idlabor, estado);
              console.log(error);
              //return false;
          }
          

        })

    //

  }







  cerrarLabor(index: number) {

    alert(index);
    //1 = activo 2= inactivo
    //this.dataLocalLabor.splice(index, 4);
    //console.log(this.dataLocalLabor);
   // this.dataLocal.borrarRegistro(index);
   this.listaRut.splice( index, 1 );

  }



  guardar_nuevo_rut_usuario(datosUsuario, datosUsuario2){

    this.listaRut.push({
      rut: datosUsuario2,
      idLabor: "",
      nombre: datosUsuario.usuarioNombre,
      apellido: datosUsuario.usuarioApellidoPaterno,
      formatoEntrada: "Nuevo",
      celular: datosUsuario.usuarioCelular,
      idFecha:"",
      estado:"1"
    });
    this.caja_rut_datos = false;
    //console.log(this.listaRut);

  }







  ingresarLaborQuimicaRutAutoasignado(usuarioRut){

    this.listaRut.push({
      rut: usuarioRut,
      idLabor: "",
      nombre:"",
      apellido: "",
      formatoEntrada: "",
      celular:"",
      idFecha:"",
      estado:"1"
    });
    //console.log(this.listaRut);

    this.ingresarLaborQuimicaRut(this.listaRut);

  }





  ingresarLaborQuimicaRut(listaRut){






    this.cargando();
   console.log(listaRut);

//return false;

this.mostrarIngresarLabor=false;
let data_cruda = JSON.parse( sessionStorage.getItem('data_cruda_labor') );
console.log(data_cruda);
this.laborObjetivo = data_cruda.laborObjetivo;

//return false
        //Se guardan los datos. En una nueva labor en BD
        this.dblaboresService.CrearPostLabores(data_cruda).subscribe(
          response => {
            console.log("CrearPostLabores "+response.id);
            this.responseId = response.id;
            sessionStorage.idlabor = response.id;
            this.limpiar_form();  
            //alert("Labor creada exitosamente. Actualice el Listado de labores."+response.id);

//return false;

//////////////agregar avance//////////////
var lat = sessionStorage.lat;
var lng = sessionStorage.lng;

            for (const key in listaRut) {
              if (listaRut.hasOwnProperty(key)) {
                let element = listaRut[key];
                  console.log(element.rut);

                  this.funcionesService.obtenerGeolocalizacion();
      
                  let info: any = new FormData();

                  info.append('avanceLaborQCantidad','0'); //se pone cero porque es la labor inicial
                  info.append('avanceLaborQProducto','0');
                  info.append('avanceLaborQBombada','0');
                  info.append('avanceLaborQLat',lat);
                  info.append('avanceLaborQLng',lng);
                  info.append('avanceLaborQFecha', this.funcionesService.fecha_hora_actual());
                  info.append('avanceLaborQRutTrabajador',element.rut);
                  info.append('avanceLaborQUnidadMedida','0');
                  info.append('labor_idLabor', this.responseId);
            
                  var object = {};
                  info.forEach((value, key) => { object[key] = value });
                  var data_cruda_avance = object;           

              //Se guardan los datos. En una nueva labor en BD
              this.avancelaborqdb.CrearPostAvance(data_cruda_avance).subscribe(
                response => {
                  console.log("CrearPostAvance"+response);
                  this.limpiar_form();  
                  //alert("Avance creado exitosamente. Actualice el Listado de labores.");
                  this.router.navigate(['/listado-labores']);
                },
                error => {
                  //console.log("CrearPostAvance error "+error);
                  this.limpiar_form();  
                  //alert("No se pudo crear el Avance. Hubo un error en el proceso de creación.");
                }
              )




              }
            }
////////////////fin agregar avance////////////////////////////

////////////////localStorage - MODIFICA LA TABLA LOCAL PRODUCTOS, AGREGA EL idLabor que faltaba.
var idLaborx = String(this.responseId);
var idFechax = sessionStorage.idFecha;
this.obtener_lista_productosQ_agregar_idLabor(idFechax, idLaborx);
////////////////localStorage - FIN:::::::::MODIFICA LA TABLA LOCAL PRODUCTOS, AGREGA EL idLabor que faltaba.


////////////////localStorage
for (let key in listaRut) {
  if (listaRut.hasOwnProperty(key)) {
    let element = listaRut[key];
  // localStorage.listaRut.Push(element);

  //agregar el valor del id que se obtiene luego de crearse en la BD
  this.listaRut2.push({
    rut: element.rut ,
    idLabor: String(this.responseId),
    nombre: element.nombre,
    apellido: element.apellido,
    formatoEntrada: element.formatoEntrada,
    celular: element.celular,
    idFecha:sessionStorage.idFecha,
    estado:element.estado
  });
} //fin for
  }
  console.log(this.listaRut2);


//////////leer desde localStorage listaRut
let data_cruda2 = JSON.parse( localStorage.getItem('listaRut') );
console.log(data_cruda2);

//si data cruda2 está vacío entonces guarde solamente la tabla con datos. listaRut2
if(!data_cruda2){

  this.listaRut2.sort().reverse();     
//La lista concatenada data_cruda3 se ingresa al localStorage
localStorage.listaRut = JSON.stringify(this.listaRut2);



}else{
  //concateno (unir) las 2 listas la temporal listaRut y la respaldada en LocalStorage
let data_cruda3 = this.listaRut2.concat(data_cruda2);
//ORDENA LA LISTA EN FORMA DESCENDENTE
data_cruda3.sort().reverse();     

//La lista concatenada data_cruda3 se ingresa al localStorage
localStorage.listaRut = JSON.stringify(data_cruda3);
}





          },
          error => {
            console.log("CrearPostLabores error "+error);
            this.limpiar_form();  
            alert("No se pudo crear la Labor. Hubo un error en el proceso de creación.");
          }
        )

setTimeout(() => {
  this.crearListaFusionRutQlocal();
}, 5000);

  }







   crearListaFusionRutQlocal(){
    
    //this.cargando();

    //console.log(this.obtener_lista_productosQ());
  
    this.obtenerListaRut();


   }






   obtenerListaRut(){
    
    let idFecha = sessionStorage.idFecha;

      let key = 'productosQLocal';
      //leer data local
      this.storage.get(key).then((val) => {
       // console.log('get ' + key + ' ', val);

        if (val == null) {
          console.log("productosQLocal sin datos")
          return false;
        }

        //recorrer data local
        for (var i = 0; i < val.length; i++) {
          if (val[i].estado == '1' && val[i].idFecha == idFecha) {
            //console.log("si:"+i);
/*
              const productosQCantidad = val[i].productosQCantidad;
              const productosQNombre = val[i].productosQNombre;
              const labor_idLabor = val[i].labor_idLabor;
              const unidadmedida_idUnidadMedida = val[i].unidadmedida_idUnidadMedida;
              const idFecha = val[i].idFecha;
              const estado = val[i].estado;

              console.log(productosQCantidad+" "+productosQNombre+" "+labor_idLabor+" "+unidadmedida_idUnidadMedida+" "+idFecha+" "+estado);
*/

              this.productosQ.push({
                productosQCantidad: val[i].productosQCantidad,
                productosQNombre: val[i].productosQNombre,
                labor_idLabor: val[i].labor_idLabor,
                unidadmedida_idUnidadMedida: val[i].unidadmedida_idUnidadMedida,
                idFecha: val[i].idFecha,
                estado: val[i].estado
              })


          }
        }
/*
//listado PRODUCTOSQ    
        for (const key in this.productosQ) {
          if (this.productosQ.hasOwnProperty(key)) {
            const elementQ = this.productosQ[key];
            console.log(elementQ);
          }
        }
*/




        const listaRut = JSON.parse( localStorage.getItem('listaRut'));

        for (let key in listaRut) {
          if (listaRut.hasOwnProperty(key)) {
            let element = listaRut[key];
        
          if(element.idFecha == idFecha ){

              this.listaFusionQlocalRut.push({
                rut: element.rut,
                idLabor: element.idLabor,
                nombre: element.nombre,
                apellido: element.apellido,
                formatoEntrada: element.formatoEntrada,
                celular: element.celular,
                idFecha:element.idFecha
              })

          }

        } //fin if
          }//fin for

              //listado LOCALRUT
                for (const key in this.listaFusionQlocalRut) {
                  if (this.listaFusionQlocalRut.hasOwnProperty(key)) {
                    const elementQrut = this.listaFusionQlocalRut[key];
                    console.log(elementQrut);

                    if(elementQrut.idFecha == idFecha){



                        //listado PRODUCTOSQ    
                        for (const key2 in this.productosQ) {
                          if (this.productosQ.hasOwnProperty(key2)) {
                            const elementQ = this.productosQ[key2];
                            console.log(elementQ);

                                if(elementQrut.idFecha == elementQ.idFecha){

                                  


                                  console.log(elementQrut.idFecha+" "+elementQ.idFecha);
                                        //guardar la FUSION
                                        this.productosQFusion.guardarProductosQFusion(

                                        this.rut = elementQrut.rut,
                                        this.idLabor = elementQrut.idLabor,
                                        this.nombre = elementQrut.nombre,
                                        this.apellido = elementQrut.apellido,
                                        this.formatoEntrada = elementQrut.formatoEntrada,
                                        this.celular = elementQrut.celular,
                                        this.idFecha = elementQrut.idFecha,
                                        this.productosQCantidad = elementQ.productosQCantidad,
                                        this.productosQNombre = elementQ.productosQNombre,
                                        this.unidadmedida_idUnidadMedida = elementQ.unidadmedida_idUnidadMedida,
                                        this.estado = elementQ.estado

                                        );




                                }


                          }
                        }

                      
                    }


                  }
                }





      }).catch((error) => {
        console.log('get error for ' + key + '', error);
        return false;
      }).then(function(){






      }, function () {
        console.log('Not fired due to the catch');
      });

   }



   obtener_lista_productosQ(){
        
    const arregloQ =[];

      //LEER LATLONG LOCAL ENVIAR A BD Y BORRAR LOCAL
      let key = 'productosQLocal';
      //leer data local
      this.storage.get(key).then((val) => {
       console.log('get ' + key + ' ', val);

        if (val == null) {
          console.log("registros productosQLocal sin datos")
          return false;
        }else{
          console.log("registros productosQLocal: "+val.length);

          for (let i = 0; i < val.length; i++) {
            const element = val[i];
            arregloQ.push({
              productosQCantidad: element.productosQCantidad,
              productosQNombre: element.productosQNombre,
              labor_idLabor: element.labor_idLabor,
              unidadmedida_idUnidadMedida: element.unidadmedida_idUnidadMedida,
              idFecha: element.idFecha,
              estado: element.estado
            });

        }
      //ORDENA LA LISTA EN FORMA DESCENDENTE
      arregloQ.sort().reverse();          
      this.productosQLocal = arregloQ;

      return this.productosQLocal;
        }


      }).catch((error) => {
        console.log('get error for ' + key + '', error);
        return false;
      });

   
  }


//PROBAR PARA AGREGAR IDlABOR
  obtener_lista_productosQ_agregar_idLabor(fecha,idLabor){
        
    //var fecha = "2020-04-13 18:02:52";
    //var idLabor = "999999999";
    var coincidencias=0;

    const arregloQ =[];

      //LEER LATLONG LOCAL ENVIAR A BD Y BORRAR LOCAL
      let key = 'productosQLocal';
      //leer data local
      this.storage.get(key).then((val) => {
       console.log('get ' + key + ' ', val);

        if (val == null) {
          console.log("registros productosQLocal sin datos")
          return false;
        }else{
          console.log("registros productosQLocal: "+val.length);

          for (let i = 0; i < val.length; i++) {
            const element = val[i];

            if(element.idFecha == fecha){

              coincidencias++;

              arregloQ.push({
                productosQCantidad: element.productosQCantidad,
                productosQNombre: element.productosQNombre,
                labor_idLabor: idLabor,
                unidadmedida_idUnidadMedida: element.unidadmedida_idUnidadMedida,
                idFecha: element.idFecha,
                estado: "1" //se pasa a 1 para que lo detecte el settimeout y lo envíe.
              });

            }else{

              arregloQ.push({
                productosQCantidad: element.productosQCantidad,
                productosQNombre: element.productosQNombre,
                labor_idLabor: element.labor_idLabor,
                unidadmedida_idUnidadMedida: element.unidadmedida_idUnidadMedida,
                idFecha: element.idFecha,
                estado: element.estado
              });

            }



        }
      //ORDENA LA LISTA EN FORMA DESCENDENTE
      arregloQ.sort().reverse();          
      this.productosQLocal = arregloQ;

      console.log("Coincidencias: "+coincidencias);
      console.log(this.productosQLocal);

      this.storage.set('productosQLocal',this.productosQLocal);

      return false;
      return this.productosQLocal;
        }


      }).catch((error) => {
        console.log('get error for ' + key + '', error);
        return false;
      });

   
  }








  abrirRegistro(registro){
    
/*    alert(
      "Rut :"+
      registro.rut
      +" \nId Labor :"+
      registro.idLabor
      +" \nNombre :"+
      registro.nombre
      +" "+
      registro.apellido
      +" \nFormato :"+
      registro.formatoEntrada
      );*/
      alert(
        "Rut :"+
        registro.rut
        +" \nNombre :"+
        registro.nombre
        +" "+
        registro.apellido
        );

    console.log('Registro: ',registro);
  }





  scan() {

    //laborOT

    //alert("scan");

    this.barcodeScanner.scan().then(barcodeData => {
      console.log('barcode data', barcodeData);

      if (!barcodeData.cancelled) {


        //
        var rut1 = Array.from(barcodeData.text);
        var rutOk = "";

        for (var i = 0; i <= rut1.length; i++) {
          var xx = rut1[i];

          if ((i >= 52 && i <= 62) && (xx != "=" && xx != "&" && xx != "-" && xx != "t" && xx != "<" && xx != "L")) {
            console.log(i + " - " + rut1[i]);
            rutOk += rut1[i];
            console.log(i + " - " + rut1[i] + " - " + rutOk);
          }

        }
        var rut = rutOk;
        var estado = "1";
        //conectarse y verificar rut con lista negra rut


        var rut_formateado = this.formato_rut(rut);

/////
var rut = "";
//alert(this.datosUsuario2.rut2);
//alert(manual+" "+rut2+" "+laborOT+" "+estado);

this.escanearBoton = true;
this.grupoBoton = false;
this.manualBoton = true;

//alert(rut);
//validar Rut con BD lista negra
//conectarse y verificar rut con lista negra rut
this.dbrutusuarios.getRutUsuarios(rut_formateado)
  .subscribe(
    response => {

      console.log(response);
      
      

      this.listaNegra = response;

      //var respuesta = Object.values(response.usuario.usuarioRut);
      let usuarioRut = response.usuario.usuarioRut;
      let respuestaid = response.usuario.idUsuario;
      //var respuestax = response;

      console.log(respuestaid);
      


      //lista negra obtener respuesta de id
      this.listanegra.getListaNegra(respuestaid)
      .subscribe(
        response => {


          console.log(response);
            console.log(response.logListaNegraMotivo);

            if(response.idLogListaNegra != "1"){
                alert("NO puede trabajar en la empresa está en LISTA NEGRA: \n"
                +" Nº: "+ response.idLogListaNegra
                +" \nFecha: "+ response.logListaNegraFecha
                +" \nMotivo: "+ response.logListaNegraMotivo
                )
            }else{                 
              //validar si está repetido el rut
              var i = 0;
              for (const key in this.listaRut) {
                if (this.listaRut.hasOwnProperty(key)) {
                  const element = this.listaRut[key];
                    if(element.rut == usuarioRut){
                      i ++;
                    }
                }
              }

              if(i > 0){
                alert("Existe en la BD");                   
              }else{
                alert("SIN ANTECEDENTES, Puede Trabajar en la Empresa");
                this.listaRut.push({
                  rut: usuarioRut,
                  idLabor: "",
                  nombre:"",
                  apellido: "",
                  formatoEntrada: "",
                  celular:"",
                  idFecha:"",
                  estado:""
                });
                console.log(this.listaRut);
                this.mostrarIngresarLabor = true;
              }

        //alert(this.dataLocal.guardados);
        this.caja_rut = false;
        this.boton_rut = true;
        //Validar rut en BD

            }

        })

    },
    error => {
      console.log(error);
      console.log(error.registros_rut_usuarios);

      console.log(error.status);//404 rut no encontrado   // 0  sin internet
      console.log(error.statusText);//ok rut no encontrado  // Unknown Error sin internet

      if(error.status == 0 || error.statusText == "Unknown Error"){
        alert("No se puede confirmar RUT. No hay conexión con el Servidor");
        sessionStorage.rutValEstado = false;
        return false;
      }

      if(error.status == 404 || error.statusText == "Not Found"){

          //EVITA INGRESAR RUT DUPLICADO
          
////
              //validar si está repetido el rut
              var i = 0;
              for (const key in this.listaRut) {
                if (this.listaRut.hasOwnProperty(key)) {
                  const element = this.listaRut[key];
                    if(element.rut == rut){
                      i ++;
                    }
                }
              }

              if(i > 0){
                alert("Existe en la BD");  
                return false;                 
              }else{
                //alert("Este RUT es Nuevo en nuestra BD");
                                  alert("NO FIGURA EN NUESTROS REGISTROS, NO ESTA Autorizado, para trabajar en Aplicaciones Químicas");
                                  return false;
              }

        //alert(this.dataLocal.guardados);
        this.caja_rut = false;
        this.boton_rut = true;
        //Validar rut en BD

//////


          //GUARDAR **NUEVO RUT** EN BD NOMBRE APELLIDO TELEFONO
          //this.info = {fechaLatLong:1,idLatLong:2,latitud:3,longitud:4,fechaEntrada:5};

          //this.caja_rut_datos = true;

          //alert(this.dataLocal.guardados);
          this.caja_rut = false;
          this.boton_rut = true;
          //Validar rut en BD
          //this.dataLocal.guardarRegistro("Manual", rut, idlabor, estado);
          console.log(error);
          //return false;
      }
      

    })

//
///////
      this.escanearBoton = true;
      this.grupoBoton = false;
      this.manualBoton = true;

      }

    }).catch(err => {


      //console.log('Error', err);

      //let estado = "";
      alert("Error al escanear: " + err);
      //this.dataLocal.guardarRegistro('QRCode', 'https://www.digital5.cl', sessionStorage.usuarioNumberOT, estado);

      this.escanearBoton = true;
      this.grupoBoton = false;
      this.manualBoton = false;

      return false;

    });


    
  }


  formato_rut(rut){
    let value = rut.replace(/\./g, '').replace('-', '');

    if (value.match(/^(\d{2})(\d{3}){2}(\w{1})$/)) {
      value = value.replace(/^(\d{2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4');
    }
    else if (value.match(/^(\d)(\d{3}){2}(\w{0,1})$/)) {
      value = value.replace(/^(\d)(\d{3})(\d{3})(\w{0,1})$/, '$1.$2.$3-$4');
    }
    else if (value.match(/^(\d)(\d{3})(\d{0,2})$/)) {
      value = value.replace(/^(\d)(\d{3})(\d{0,2})$/, '$1.$2.$3');
    }
    else if (value.match(/^(\d)(\d{0,2})$/)) {
      value = value.replace(/^(\d)(\d{0,2})$/, '$1.$2');
    }
    return value;
  }





  async cargando() {
    const loading = await this.loadingController.create({
      message: 'Procesando...',
      duration: 3000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }




  iniciarGeolocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat =   resp.coords.latitude
      this.lon = resp.coords.longitude

        console.log(resp.coords);

        let watch = this.geolocation.watchPosition();
watch.subscribe((data) => {
 // data can be a set of coordinates, or an error (if an error occurred).
 // data.coords.latitude
 // data.coords.longitude

 this.lat =   data.coords.latitude
 this.lon = data.coords.longitude
 console.log('Watch: ', data);


});
       }).catch((error) => {
         console.log('Error getting location', error);
       });

       60000    
  }


  agregar1() {
    return new Promise((resolve) => {
      let body = {
        datos: "insertar",
        longitud: this.lon,
        latitud: this.lat,
     
        proce1: this.proce,
        user1 :this.idUsuario,
        estado1: this.estado1,
       
        geo1: this.geo,
        fecha: this.funcionesService.fecha_hora_actual()
      };

      this.prosecnt.postData(body, "geo2.php").subscribe((data) => {
        console.log("OK");
      });
    });
}






}

