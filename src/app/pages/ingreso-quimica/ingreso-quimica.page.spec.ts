import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IngresoQuimicaPage } from './ingreso-quimica.page';

describe('IngresoQuimicaPage', () => {
  let component: IngresoQuimicaPage;
  let fixture: ComponentFixture<IngresoQuimicaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoQuimicaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IngresoQuimicaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
