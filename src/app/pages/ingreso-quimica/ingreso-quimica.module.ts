import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IngresoQuimicaPageRoutingModule } from './ingreso-quimica-routing.module';

import { IngresoQuimicaPage } from './ingreso-quimica.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IngresoQuimicaPageRoutingModule
  ],
  declarations: [IngresoQuimicaPage]
})
export class IngresoQuimicaPageModule {}
