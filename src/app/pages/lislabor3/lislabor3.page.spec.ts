import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Lislabor3Page } from './lislabor3.page';

describe('Lislabor3Page', () => {
  let component: Lislabor3Page;
  let fixture: ComponentFixture<Lislabor3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lislabor3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Lislabor3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
