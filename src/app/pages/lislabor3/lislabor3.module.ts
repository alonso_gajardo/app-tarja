import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Lislabor3PageRoutingModule } from './lislabor3-routing.module';

import { Lislabor3Page } from './lislabor3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Lislabor3PageRoutingModule
  ],
  declarations: [Lislabor3Page]
})
export class Lislabor3PageModule {}
