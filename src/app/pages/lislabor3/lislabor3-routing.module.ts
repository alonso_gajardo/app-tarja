import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Lislabor3Page } from './lislabor3.page';

const routes: Routes = [
  {
    path: '',
    component: Lislabor3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Lislabor3PageRoutingModule {}
