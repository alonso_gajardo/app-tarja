import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AplicacionQuimicaPage } from './aplicacion-quimica.page';

describe('AplicacionQuimicaPage', () => {
  let component: AplicacionQuimicaPage;
  let fixture: ComponentFixture<AplicacionQuimicaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AplicacionQuimicaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AplicacionQuimicaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
