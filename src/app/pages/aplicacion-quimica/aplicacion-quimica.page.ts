import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataLocalLaboresService } from '../../services/data-local-labores.service';
import { FuncionesService } from 'src/app/services/funciones.service';
import { DBcontratistaService } from 'src/app/services/data-bd-contratista.services';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DBlaboresService } from 'src/app/services/data-bd-labores.service';
import { RegistroLabores } from 'src/app/models/registroLabores.model';
import {Storage} from '@ionic/storage';
import { AlertPage } from '../alert/alert.page';
import { ProductosQLocal } from 'src/app/services/productosQ.service';
import { LoadingController, ModalController } from '@ionic/angular';
import { DBcontratistaOKService } from 'src/app/services/contratista.service';
import { DBlaboraplicacionqService } from 'src/app/services/laboraplicacionqDB.service';
import {Proseso} from "../../provedor/proseso";



@Component({
  selector: 'app-aplicacion-quimica',
  templateUrl: './aplicacion-quimica.page.html',
  styleUrls: ['./aplicacion-quimica.page.scss'],
})
export class AplicacionQuimicaPage implements OnInit {

  segmento: string;

  listaContratista: any[] = [];

  menuContratista: any[]=[];

  textoBuscar = '';

  permiso: string;

  

  
  datosquimica1: boolean;
  avance_1_estado: boolean;
  check_1_quimica: boolean;

  datosquimica2_titulo: boolean;
  avance_2_estado: boolean;
  check_2_quimica: boolean;
  datosquimica2: boolean;

  datosquimica3_titulo: boolean;
  avance_3_estado: boolean;
  check_3_quimica: boolean;
  datosquimica3: boolean;

  boton_agregar_quimica:boolean;

  formQuimica1:boolean;
  formQuimica2:boolean;
  formQuimica3:boolean;

  listado_1:boolean;
  listado_2:boolean;
  listado_3:boolean;
  listado_4:boolean;
  listado_5:boolean;
  listado_6:boolean;
  listado_7:boolean;
  listado_8:boolean;
  listado_9:boolean;
  posicion:string;

  /*
    valorLabor: any;
    segmentChanged(ev: any) {
      console.log('Segment changed', ev);
      console.log(ev.returnValue);
      console.log(ev.detail.value);
  
      this.valorLabor = ev.detail.value;
  
    }
    */

   idUsuario: string = sessionStorage.idUsuario;
  
   guardadosLabores: RegistroLabores[] =[];


  labores = {
    idLabor:"",
    laborNombre: "",
    laborDescripcion: "",
    laborValorTratoInicial: "",
    laborValorTratoFinal: "",
    laborObjetivo: "",
    laborLat: "",
    laborLng: "",
    laborFecha: "",
    laborIconoAsignacion: "",
    laborIcono: "",
    laborCuartel: "",
    laborDescripcionCierre: "",
    estado_idEstado: "",
    tipoLabor_idTipoLabor: "",
    laborAplicacionQ_idLaborAplicacionQ: "",
    contratista_idContratista: "",
    usuario_idUsuario: ""

  };


  laboraplicacionq = {
    laborAplicacionQMaquina: "",
    laborAplicacionQMojamiento:"",
    laborAplicacionQSuperficie:""
  };

  actualizar:number = 0;


  contratista = {
    idContratista:"",
    contratistaRut:"",
    contratistaNombre:"",
    contratistaGiro:"",
    contratistaFono:"",
    contratistaMail:"",
    contratistaDireccion:"",
    contratistaRazonSocial:"",
    estado_idEstado:""
  };

  datosContratista= {
    idContratista:"",
    contratistaRut:"",
    contratistaNombre:"",
    contratistaGiro:"",
    contratistaFono:"",
    contratistaMail:"",
    contratistaDireccion:"",
    contratistaRazonSocial:"",
    estado_idEstado:"",
    fundo_idFundo:""
  };


  formGeneral:boolean;

  productosQCantidad:string;
  productosQNombre:string;
  labor_idLabor:string;
  unidadmedida_idUnidadMedida:string;
  idFecha:string;
  estado:string;
  itemContratista:boolean;
  itemContratistaSel:string;
  listaRadioQuimica:boolean;

  tipoLaborNombre:string;
  caja_contratista:boolean;

  datosUsuario2 = {
    rut2: ''
  };

  posicionBoton:string;

  inicio_quimica:boolean;

  listadoUnidadMedida={
    idUnidadMedida:"",
    unidadMedidaNombre:""
  }


  //carga antes de cargar la página completa
  ionViewWillEnter() {
    sessionStorage.laboresNombre = this.labores.laborNombre;
    sessionStorage.laboresDescripcion = this.labores.laborDescripcion;
    console.log(this.labores.laborNombre + " - " + this.labores.laborDescripcion);
  }
  geo = "GPS x Tiempo";
  lat: number;
  lon: number;

  proce: string = "1";
  estado1: string = "1";

  constructor(
    private router: Router,
    public dataLocalLaborGeneral: DataLocalLaboresService,
    public funcionesService: FuncionesService,
    public dbcontratistaService: DBcontratistaService,
    private geolocation: Geolocation,
    private dblaboresService: DBlaboresService,
    private storage: Storage,
    private productosQ: ProductosQLocal,
    public loadingController: LoadingController,
    private dbcontratistaService2: DBcontratistaOKService,
    public modalController: ModalController,
    public dblaboraplicacionq: DBlaboraplicacionqService,
    private prosecnt: Proseso


  ) { 



 this.idFecha ="";

 this.formGeneral = true;
 this.datosquimica1 = true;
 this.avance_1_estado = false;
 this.check_1_quimica = false;

 this.datosquimica2_titulo = false;
 this.datosquimica2 = false;
 this.avance_2_estado = false;
 this.check_2_quimica = false;

 this.datosquimica3_titulo = false;
 this.datosquimica3 = false;
 this.avance_3_estado = false;
 this.check_3_quimica = false;

this.boton_agregar_quimica = false;

 sessionStorage.numeroListadoQuimica = 0;

 this.formQuimica1 = false;
 this.formQuimica2 = false;
 this.formQuimica3 = false;

 this.listado_1=false;
 this.listado_2=false;
 this.listado_3=false;
 this.listado_4=false;
 this.listado_5=false;
 this.listado_6=false;
 this.listado_7=false;
 this.listado_8=false;
 this.listado_9=false;
 this.posicion ='0';
  // localStorage.data_cruda_productosQ ="";
 this.itemContratista=false;
 this.itemContratistaSel='';
 this.listaRadioQuimica=true;

 this.tipoLaborNombre="";

 

 this.limpiar_form_quimica();
 this.limpiar_avances();

sessionStorage.tipoLaborNombre="";

this.caja_contratista=false;

this.posicionBoton="";

this.inicio_quimica=true;

this.listadoUnidadMedida = JSON.parse(localStorage.unidadesMedida);

  }

  ngOnInit() {
/*
    this.dbcontratistaService.getContratista()
      .subscribe(listaContratista => {
        console.log(listaContratista);
        this.listaContratista = listaContratista;
      })
*/
    this.permiso = sessionStorage.usuarioNivelPermiso;
    this.contratista = JSON.parse( localStorage.getItem('contratistaLocal') );

    
 this.limpiar_form_quimica();
 this.limpiar_avances();
 sessionStorage.tipoLaborNombre="";
 this.caja_contratista=false;
 this.posicionBoton="";

 this.inicio_quimica=true;
  }


  prueba(){
    alert("prueba");
  }


  guardar_nombre(nombre) {
    sessionStorage.laboresNombre = nombre;
  }
  guardar_descripcion(descripcion) {
    sessionStorage.laboresDescripcion = descripcion;
  }


  async ruta_laborQuimica(tipo){

    sessionStorage.tipoLaborNombre = tipo;

    var tipoLaborNombre = sessionStorage.tipoLaborNombre;

    console.log(tipo, sessionStorage.tipoLaborNombre);

    //return false;

    this.listaRadioQuimica=false;

    this.idFecha = this.funcionesService.fecha_hora_actual();

    this.formGeneral = false;

    //CREAR labores EN DB {
    this.funcionesService.obtenerGeolocalizacion();

    /* tipificacionLabor
    1	- Labor General Autoasignada
    2	- Labor General Interna
    3	- Labor General Contratista
    4	- Aplicación Química Autoasignada
    5	- Aplicación Química Interna
    6	- Aplicación Química Contratista
    */
   var idContratista = this.contratista.idContratista;
   var laborValorTratoInicial = this.labores.laborValorTratoInicial;
   var tipificacionLabor = "";

  

switch(tipoLaborNombre) {
  case 'autoasignado':
    tipificacionLabor = "4";
    idContratista = "1";//nulo
    laborValorTratoInicial = "";//nulo
    sessionStorage.tipoLabor="1";
    break;
    
    case 'interna':
    tipificacionLabor = "5";
    //idContratista = "1";//nulo
   // laborValorTratoInicial = "";//nulo
    sessionStorage.tipoLabor="2";
    idContratista = "1";
    break;

    
  case 'contratista':
    tipificacionLabor = "6";
    sessionStorage.tipoLabor="3";
    
    break;
  default:
    // code block
}


   //1 = activo 2= inactivo
   var estado = "1";
   var lat = sessionStorage.lat;
   var lng = sessionStorage.lng;

   //console.log(this.contratista.idContratista);
   //return false;

  

console.log(tipificacionLabor+" - "+idContratista);
  //return false;

      //formData
      let info: any = new FormData();

      info.append('laborNombre', this.labores.laborNombre);
      info.append('laborDescripcion', this.labores.laborDescripcion);
      info.append('laborValorTratoInicial',laborValorTratoInicial);
      info.append('laborValorTratoFinal',"");
      info.append('laborObjetivo', this.labores.laborObjetivo);
      info.append('laborLat', lat);
      info.append('laborLng', lng);
      info.append('laborFecha', this.idFecha);
      info.append('laborIconoAsignacion',  this.funcionesService.obtener_iconoAsignacion(tipificacionLabor));
      info.append('laborIcono',  this.funcionesService.obtener_iconoLabor(tipificacionLabor));
      info.append('laborCuartel',this.labores.laborCuartel);
      info.append('laborDescripcionCierre','sin descripcion cierre');
      info.append('estado_idEstado', estado);
      info.append('tipoLabor_idTipoLabor', tipificacionLabor);
      info.append('laborAplicacionQ_idLaborAplicacionQ',1); //aqui poner id laboraplicacionQ
      info.append('contratista_idContratista',idContratista);
      info.append('usuario_idUsuario', sessionStorage.idUsuario);

      //alert("SE HIZO LABOR");

      var object = {};
      info.forEach((value, key) => { object[key] = value });
      var data_cruda = object;
      console.log(data_cruda);

      sessionStorage.data_cruda_labor = JSON.stringify(data_cruda);
/*
this.router.navigate(['/ingreso-general']);
*/
console.log(tipificacionLabor);

//return false
/*
if(tipificacionLabor == '4'){
  //se guardan los datos. Es una nueva labor en BD
  this.dblaboresService.CrearPostLabores(data_cruda).subscribe(
    response => {
      console.log("CrearPostLabores "+response);
     // this.limpiar_form_quimica();  
     // alert("Labor Química creada exitosamente. Actualice el Listado de labores.");
     this.limpiar_form_quimica();
     this.limpiar_avances();
     this.router.navigate(['/listado-labores']);
    },
    error => {
      console.log("CrearPostLabores error "+error);
      this.limpiar_form_quimica();  
      this.limpiar_avances();
      alert("No se pudo crear la Labor Química. Hubo un error en el proceso de creación.");
    }
  )
}else{
//this.router.navigate(['/ingreso-general']);
}
*/


this.avanzar_a_2();

}
botones_estado_latlong(x) {
  //CREAR LATLONG LOCAL


  this.geolocation
    .getCurrentPosition()
    .then((resp) => {
      const gpsUsuarioLatLngLat = `${resp.coords.latitude}`;
      const gpsUsuarioLatLngLng = `${resp.coords.longitude}`;
      const gpsUsuarioLatLngFecha = this.funcionesService.fecha_hora_actual();

      const usuario_idUsuario = sessionStorage.idUsuario;
      const estadoLatLong = "1";


    })
    .catch((error) => {
      console.log("Error getting location", error);
    });
}



async ruta_laborQuimica_2(tipo){


  //ingresar laboraplicacionq y obtener id desde DB
let info: any = new FormData();

info.append('laborAplicacionQMaquina', this.laboraplicacionq.laborAplicacionQMaquina);
info.append('laborAplicacionQMojamiento', this.laboraplicacionq.laborAplicacionQMojamiento);
info.append('laborAplicacionQSuperficie', this.laboraplicacionq.laborAplicacionQSuperficie);
//info.append('idFecha', this.idFecha);

var object = {};
info.forEach((value, key) => { object[key] = value });
var data_cruda = object;


this.crearLaborAplicacionQ(data_cruda);


//


/*
    //formData
    let info: any = new FormData();

    info.append('laborAplicacionQMaquina', this.laboraplicacionq.laborAplicacionQMaquina);
    info.append('laborAplicacionQMojamiento', this.laboraplicacionq.laborAplicacionQMojamiento);
    info.append('idFecha', this.idFecha);

    var object = {};
    info.forEach((value, key) => { object[key] = value });
    var data_cruda = object;
    console.log(data_cruda);

    //sessionStorage.data_cruda_laboraplicacionq = JSON.stringify(data_cruda);

    sessionStorage.setItem('data_cruda_laboraplicacionq', JSON.stringify(data_cruda));
    

*/


}





async ruta_laborQuimica_3(tipo){

  let total = sessionStorage.numeroListadoQuimica;
 /*const data_cruda_productosQ =[];*/
//alert(total);

 // return false;



  //formData
  //let info: any = new FormData();


for (let index = 1; index <= total; index++) {
  const nombre = document.getElementById("celda_"+index+"_1").innerText;
  const cantidad = document.getElementById("celda_"+index+"_2").innerText;
  const unidad = document.getElementById("celda_"+index+"_3").innerText;


 

    this.productosQ.guardarProductosQ(

      this.productosQCantidad =  cantidad,
      this.productosQNombre =  nombre,
      this.labor_idLabor =  "",
      this.unidadmedida_idUnidadMedida = unidad,
      this.idFecha =  this.idFecha,
      this.estado =  "2",//se crea con 2 para que después que se le agregue el idLabor cambie a 1 y se envíe.

      );

      console.log(cantidad);

 




/*
  data_cruda_productosQ.push({
    productosQCantidad: cantidad,
    productosQNombre: nombre,
    labor_idLabor: "",
    unidadmedida_idUnidadMedida: unidad,
    idFecha: this.idFecha

  });
  */

}

/*
data_cruda_productosQ.sort().reverse();
localStorage.data_cruda_productosQ = JSON.stringify(data_cruda_productosQ);
console.log(localStorage.getItem('data_cruda_productosQ'));
*/

//sessionStorage.setItem('data_cruda_productosQ',  list);



/*
          info.append('laborAplicacionQMaquina', this.laboraplicacionq.laborAplicacionQMaquina);
          info.append('laborAplicacionQMojamiento', this.laboraplicacionq.laborAplicacionQMojamiento);
*/



this.formQuimica3 = false;
this.datosquimica3 = false;
this.check_3_quimica = true;
this.avance_3_estado = true;

this.router.navigate(['/ingreso-quimica']);

}









  limpiar_form_quimica() {
    this.labores.idLabor ="",
    this.labores.laborNombre = "",
      this.labores.laborDescripcion = "",
      this.labores.laborValorTratoInicial = "",
      this.labores.laborValorTratoFinal = "",
      this.labores.laborObjetivo = "",
      this.labores.laborLat = "",
      this.labores.laborLng = "",
      this.labores.laborFecha = "",
      this.labores.laborIconoAsignacion = "",
      this.labores.laborIcono = "",
      this.labores.laborCuartel = "",
      this.labores.laborDescripcionCierre = "",
      this.labores.estado_idEstado = "",
      this.labores.tipoLabor_idTipoLabor = "",
      this.labores.laborAplicacionQ_idLaborAplicacionQ = "",
      this.labores.contratista_idContratista = "",
      this.labores.usuario_idUsuario = ""

  }

  limpiar_avances(){
    this.avance_1_estado = false;
    this.check_1_quimica = false;
    this.datosquimica1 = false;

    this.avance_2_estado = false;
    this.check_2_quimica = false;
    this.datosquimica2_titulo = false;
    this.datosquimica2 = false;

    this.avance_3_estado = false;
    this.check_3_quimica = false;
    this.datosquimica3_titulo = false;
    this.datosquimica3 = false;

    sessionStorage.numeroListadoQuimica = 0;
  }





  avanzar_a_2(){
    this.avance_1_estado = true;
    this.check_1_quimica = true;
    this.datosquimica1 = false;


    this.avance_2_estado = false;
    this.check_2_quimica = false;
    this.datosquimica2_titulo = true;
    this.datosquimica2 = true;
   }

   avanzar_a_3(){

 
    this.avance_2_estado = true;
    this.check_2_quimica = true;
    this.datosquimica2_titulo = true;
    this.datosquimica2 = false;

    this.avance_3_estado = false;
    this.check_3_quimica = false;
    this.datosquimica3_titulo = true;
    this.datosquimica3 = true;
   }


  restaurar1(){

    var x = sessionStorage.tipoLaborNombre;
//alert(x);
    if(x=="interna" || x =="autoasignado" || x=="contratista"){
    }else{
      return false;
    }

    this.listaRadioQuimica=false;
    this.avance_1_estado = false;
    this.check_1_quimica = false;
    this.datosquimica1 = true;


    this.datosquimica2_titulo = false;
    this.avance_2_estado = false;
    this.check_2_quimica = false;
    this.datosquimica2= false;

    this.datosquimica3_titulo = false;
    this.avance_3_estado = false;
    this.check_3_quimica = false;
    this.datosquimica3= false;
  }

  restaurar2(){
    this.avance_2_estado = false;
    this.check_2_quimica = false;
    this.datosquimica2= true;
    this.datosquimica2_titulo = true;

    this.datosquimica3_titulo = false;
    this.avance_3_estado = false;
    this.check_3_quimica = false;
    this.datosquimica3= false;
  }

  restaurar3(){
    this.avance_3_estado = false;
    this.check_3_quimica = false;
    this.datosquimica3= true;
    this.datosquimica3_titulo = true;

    sessionStorage.numeroListadoQuimica = 0;
    //localStorage.data_cruda_productosQ ="";

  }



 agregarElementoLista(){

  this.formQuimica3 = true;
   let num =   parseInt(sessionStorage.numeroListadoQuimica);
  num = num +1;
  let myContainer = <HTMLElement>document.getElementById('listaQuimica');
  // '<ion-grid><ion-row><ion-col style="background-color: #f7f7f7;border: solid 1px #ddd;padding: 2px;" width-33><ion-input required></ion-input></ion-col><ion-col style="background-color: #f7f7f7;border: solid 1px #ddd;padding: 2px;" width-33><ion-input required></ion-input></ion-col><ion-col style="background-color: #f7f7f7;border: solid 1px #ddd;padding: 2px;" width-33><ion-input required></ion-input></ion-col></ion-row></ion-grid>';
  
  /*
  myContainer.innerHTML +='<tr>'+
  '<td id="celda_'+num+'_1" style="border: 1px solid gray;width: 50%;word-wrap: break-word;text-align: center;padding: 5px;background-color: #ededed;" contenteditable="true"></td>'+
  '<td id="celda_'+num+'_2" style="border: 1px solid gray;width: 15%;word-wrap: break-word;text-align: center;padding: 5px;background-color: #ededed;" contenteditable="true"></td>'+
  '<td id="celda_'+num+'_3" style="border: 1px solid gray;width: 15%;word-wrap: break-word;text-align: center;padding: 5px;background-color: #ededed;" contenteditable="true"></td>'+
  '<td id="celda_'+num+'_4" style="border: 1px solid gray;width: 20%;word-wrap: break-word;text-align: center;padding: 5px;background-color: #ededed;" contenteditable="true"><input type="button" onclick="ruta_laborQuimica_4('+num+')"/></td>'+
  '</tr>';
*/

myContainer.innerHTML += '<ion-grid> '+
'<ion-row>'+
                '<ion-col size="12" align-self-baseline>'+
                    '<ion-item>'+
                        '<ion-label position="floating" >Nombre Producto</ion-label>'+
                        '<ion-input type="text" id="celda_1"  ></ion-input>'+
                    '</ion-item> '+                                           
                '</ion-col>'+

                '<ion-col  size="6" align-self-baseline>   '+                                   
                    '<ion-item>'+
                        '<ion-label position="floating" >Cantidad Producto</ion-label>'+
                        '<ion-input type="text" id="celda_2"  ></ion-input>'+
                    '</ion-item>  '+
                '</ion-col>'+

                '<ion-col size="6" align-self-baseline>'+
                    '<ion-item>'+
                        '<ion-label position="floating" >Cantidad Producto</ion-label>'+
                        '<ion-input type="text" id="celda_3" ></ion-input>'+
                    '</ion-item>  '+
                '</ion-col>'+

                '<ion-col size="12" align="right">'+
                    '<!--<ion-button  (click)="guardar_avance_general(idUsuario,idLabor,registro.rut, laborObjetivo, formato, registro, i)">Agregar</ion-button>-->'+
                    '<ion-button  (click)="ruta_laborQuimica_4()">Agregar</ion-button>'+
                '</ion-col>'+         
'</ion-row>'+
'</ion-grid>';


  sessionStorage.numeroListadoQuimica = num;

  

 }





    listado_producto(x){
          //alert(x);

          if( x=='1' ){
            this.posicionBoton='';
            this.listado_1 = true;
            this.posicion = '1';
          }
          if( x=='2' ){
            this.posicionBoton='';
            this.listado_2 = true;
            this.posicion = '2';
          }
          if( x=='3' ){
            this.posicionBoton='';
            this.listado_3 = true;
            this.posicion = '3';
          }
          if( x=='4' ){
            this.posicionBoton='';
            this.listado_4 = true;
            this.posicion = '4';
          }
          if( x=='5' ){
            this.posicionBoton='';
            this.listado_5 = true;
            this.posicion = '5';
          }
          if( x=='6' ){
            this.posicionBoton='';
            this.listado_6 = true;
            this.posicion = '6';
          }
          if( x=='7' ){
            this.posicionBoton='';
            this.listado_7 = true;
            this.posicion = '7';
          }
          if( x=='8' ){
            this.posicionBoton='';
            this.listado_8 = true;
            this.posicion = '8';
          }
          if( x=='9' ){
            this.posicionBoton='';
            this.listado_9 = true;
            this.posicion = '9';
          }




        //alert(x);
    }





    async ruta_laborQuimica_4(nombre, cantidad, medida, x){
 
//console.log(nombre+" - "+cantidad+" - "+medida+" - "+x);

      //return false;
      this.posicionBoton=x;

           this.productosQ.guardarProductosQ(
     
           this.productosQCantidad =  cantidad,
           this.productosQNombre =  nombre,
           this.labor_idLabor =  "",
           this.unidadmedida_idUnidadMedida = medida,
           this.idFecha =  this.idFecha,
           this.estado =  "2",//se crea con 2 para que después que se le agregue el idLabor cambie a 1 y se envíe.
     
           );
            


          if( x=='1' ){
            this.listado_1 = false;
          }
          if( x=='2' ){
            this.listado_2 = false;
          }
          if( x=='3' ){
            this.listado_3 = false;
          }
          if( x=='4' ){
            this.listado_4 = false;
          }
          if( x=='5' ){
            this.listado_5 = false;
          }
          if( x=='6' ){
            this.listado_6 = false;
          }
          if( x=='7' ){
            this.listado_7 = false;
          }
          if( x=='8' ){
            this.listado_8 = false;
          }
          if( x=='9' ){
            this.listado_9 = false;
          }

     


    }



   ir_paso_4(idFecha,tipoLaborNombre){

console.log(idFecha+" "+tipoLaborNombre);
      
      //console.log(idFecha);
      console.log(idFecha+" - "+tipoLaborNombre);
      
      tipoLaborNombre = sessionStorage.tipoLaborNombre;

      console.log(idFecha+" - "+tipoLaborNombre);

      sessionStorage.idFecha = idFecha;

      //Crear ingresar rut directo porque es autoasignado
      // inicio
     
      this.limpiar_avances();
      this.limpiar_form_quimica();

      setTimeout(() => {

      this.router.navigate(['/ingreso-quimica']);
        
      }, 250);
      
//return false;

      // fin 
    
    }




    radioQuimica(tipo){

      this.listaRadioQuimica=false;
      this.datosquimica1=true;

      sessionStorage.tipoLaborNombre=tipo;

      if(tipo=='autoasignado'){
        this.itemContratista=false;
        this.itemContratistaSel='autoasignado';
        sessionStorage.tipoLabor='4';
              } 

      if(tipo=='interna'){
this.itemContratista=false;
this.itemContratistaSel='interna';
sessionStorage.tipoLabor='5';
      }
      if(tipo=='contratista'){
this.itemContratista=true;
this.itemContratistaSel='contratista';
sessionStorage.tipoLabor='6';
      }

      
    }





    async cargando() {
                
      const loading = await this.loadingController.create({
        message: 'Procesando...',
        duration: 1500
      });
      await loading.present();
      
  
      const { role, data } = await loading.onDidDismiss();
      console.log('Loading dismissed!');

    }




    ir_a_crearContratista(){

      this.datosquimica1=false;

      this.caja_contratista=true;

      this.obtener_contratistaDB();

    }

    ir_a_crearContratista_volver(){
      this.caja_contratista=false;
      this.datosquimica1=true;
      this.datosContratista.contratistaRut="";
      this.datosContratista.contratistaNombre="";
      this.datosContratista.contratistaFono="";
      this.obtener_contratistaDB();
    }



    crearContratista(datosContratista, rut){



      console.log(rut);

//return false;
      var data_cruda = {
        idContratista:"",
        contratistaRut:rut,
        contratistaNombre:datosContratista.contratistaNombre,
        contratistaGiro:"faltan datos",
        contratistaFono:datosContratista.contratistaFono,
        contratistaMail:"faltan datos",
        contratistaDireccion:"faltan datos",
        contratistaRazonSocial:"faltan datos",
        estado_idEstado:"1",
        fundo_idFundo:"1"
      };
      
  
      this.dbcontratistaService2.CrearPostContratista(data_cruda).subscribe(
        response => {
          alert("Nuevo Contratista Agregado a BD Exitosamente");
          console.log("Respuesta ingreso de contratista: " + response);

          data_cruda = null;
          this.ir_a_crearContratista_volver();
          this.obtener_contratistaDB();
        },
        error => {
          console.log("Error ingreso de contratista: " + error);
          alert("Este rut ya existe en la BD");
          return false;
        }
      )

      


    }









    async obtener_contratistaDB(){

      const idFundo = sessionStorage.idFundo;
      //return  false;
      const contratistaLocal =[];
          //alert("obtener_laboresDB");
          //DB LLAMADA a tabla LABORES x idUsuario y agregar datos en BD LOCAL 100%
          await this.dbcontratistaService2.getContratista_x_fundo(idFundo).subscribe(
            response => {

              console.log(response);

              for (let i = 0; i < response.contratistas.length; i++) {
                const element = response.contratistas[i];
                console.log(element.idContratista);
      
                contratistaLocal.push({

                  idContratista: element.idContratista,
                  contratistaRut: element.contratistaRut,
                  contratistaNombre: element.contratistaNombre,
                  contratistaGiro: element.contratistaGiro,
                  contratistaFono: element.contratistaFono,
                  contratistaMail: element.contratistaMail,
                  contratistaDireccion: element.contratistaDireccion,
                  contratistaRazonSocial: element.contratistaRazonSocial,
                  estado_idEstado: element.estado_idEstado,
                  fundo_idFundo:element.fundo_idFundo

                });
              }
              //ORDENA LA LISTA EN FORMA DESCENDENTE
              contratistaLocal.sort().reverse();
              localStorage.contratistaLocal = JSON.stringify(contratistaLocal);

                //actualiza el listado
              this.contratista = JSON.parse( localStorage.getItem('contratistaLocal') );
              
            },
            error => {
              console.log(error);
            }
          );
      
      }





      async crearLaborAplicacionQ(data_cruda){


        this.dblaboraplicacionq.CrearPostLaborAplicacionQ(data_cruda).subscribe(
          response=>{
            console.log(response);


            let info: any = new FormData();

            info.append('laborAplicacionQMaquina', this.laboraplicacionq.laborAplicacionQMaquina);
            info.append('laborAplicacionQMojamiento', this.laboraplicacionq.laborAplicacionQMojamiento);
            info.append('laborAplicacionQSuperficie', this.laboraplicacionq.laborAplicacionQSuperficie);
            info.append('idFecha', this.idFecha);
            info.append('idLaborAplicacionQ', response.id);
        
            var object = {};
            info.forEach((value, key) => { object[key] = value });
            var data_cruda2 = object;
            console.log(data_cruda2);
        
            //sessionStorage.data_cruda_laboraplicacionq = JSON.stringify(data_cruda);
        
            sessionStorage.setItem('data_cruda_laboraplicacionq', JSON.stringify(data_cruda2));



           /***********cambiar valor laborAplicacionQ_idLaborAplicacionQ y guardar data_cruda*********/
           let data_cruda_labor = JSON.parse( sessionStorage.getItem('data_cruda_labor') );
           data_cruda_labor.laborAplicacionQ_idLaborAplicacionQ = response.id;
           sessionStorage.setItem('data_cruda_labor', JSON.stringify(data_cruda_labor));
           ////////////////////////////////////////////

            
            //return false;
            this.avanzar_a_3();
          },
          error=>{
            console.log(error);
          }
        )


/*
        this.dblaboraplicacionq.CrearPostLaborAplicacionQ(data_cruda).subscribe(
          response => {
             console.log(response);
             this.avanzar_a_3();
          },
          error => {
            console.log(error);
          }*/

      }




      iniciarGeolocation(){
        this.geolocation.getCurrentPosition().then((resp) => {
          this.lat =   resp.coords.latitude
          this.lon = resp.coords.longitude
    
            console.log(resp.coords);
    
            let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
     // data can be a set of coordinates, or an error (if an error occurred).
     // data.coords.latitude
     // data.coords.longitude
    
     this.lat =   data.coords.latitude
     this.lon = data.coords.longitude
     console.log('Watch: ', data);
    
    
    });
           }).catch((error) => {
             console.log('Error getting location', error);
           });
    
           60000    
      }
    
    
      agregar1() {
        return new Promise((resolve) => {
          let body = {
            datos: "insertar",
            longitud: this.lon,
            latitud: this.lat,
         
            proce1: this.proce,
            user1 :this.idUsuario,
            estado1: this.estado1,
           
            geo1: this.geo,
            fecha: this.funcionesService.fecha_hora_actual()
          };
    
          this.prosecnt.postData(body, "geo2.php").subscribe((data) => {
            console.log("OK");
          });
        });
    }
    





}
