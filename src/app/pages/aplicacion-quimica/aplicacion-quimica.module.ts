import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AplicacionQuimicaPageRoutingModule } from './aplicacion-quimica-routing.module';

import { AplicacionQuimicaPage } from './aplicacion-quimica.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AplicacionQuimicaPageRoutingModule
  ],
  declarations: [AplicacionQuimicaPage]
})
export class AplicacionQuimicaPageModule {}




