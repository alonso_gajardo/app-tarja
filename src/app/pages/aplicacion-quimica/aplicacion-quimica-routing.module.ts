import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AplicacionQuimicaPage } from './aplicacion-quimica.page';

const routes: Routes = [
  {
    path: '',
    component: AplicacionQuimicaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AplicacionQuimicaPageRoutingModule {}
