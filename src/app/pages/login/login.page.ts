import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//import { AppVersion } from '@ionic-native/app-version/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DataLocalLaboresLatLong } from 'src/app/services/data-local-latlong.service';
import { DBlatlongService } from '../../services/data-bd-latlong.service';
import { DBlatlongModel } from 'src/app/models/dblatLong.model';
import { Storage } from '@ionic/storage';
import { FuncionesService } from 'src/app/services/funciones.service';
import { DBrutService } from 'src/app/services/data-bd-rut.service';
import { DBcantidadService } from 'src/app/services/data-bd-cantidad.service';
import { DBlaboresService } from 'src/app/services/data-bd-labores.service';
import { DBrutUsuariosService } from 'src/app/services/data-db-rut-usuarios.service';
import { AvanceLaborGDB } from 'src/app/services/avancelaborgDB.service';
import { AvanceLaborQDB } from 'src/app/services/avancelaborqDB.service';
import { SobrantesQDB } from 'src/app/services/sobrantesqDB.service';
import { stringify } from 'querystring';
import { ProductosQDB } from 'src/app/services/productosQDB.service';
import { DBmedidaService } from 'src/app/services/data-bd-medida.service';
import { Proseso } from "../../provedor/proseso";


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  idUsuario: string = sessionStorage.idUsuario;
  usuario: string;

  info: any;

  datosUsuario = {
    rut: '',
    pass: ''
  };


  data_cruda_2 = {
    avanceLaborQCantidad: "",
    avanceLaborQProducto: "",
    avanceLaborQBombada: "",
    avanceLaborQLat: "",
    avanceLaborQLng: "",
    avanceLaborQFecha: "",
    avanceLaborQRutTrabajador: "",
    avanceLaborQUnidadMedida: "",
    labor_idLabor: ""
  }


  public permiso: "1";






  AppName: string;
  PackageName: string;
  VersionCode: string | number;
  VersionNumber: string;
  guardados: any;
  dblatlong: DBlatlongModel;

  lat: number;
  lon: number;
  total: string;
  geo = "GPS x Tiempo";

  geot2: string = "Termina de Trabajar";
  geot3: string = "Sale del Trabajo";
  proce: string = "1";
  estado: string = "1";

  constructor(
    private router: Router,
    //private appVersion: AppVersion,
    private geolocation: Geolocation,
    private latLong: DataLocalLaboresLatLong,
    private dblatlongService: DBlatlongService,
    public storage: Storage,
    public funcionesService: FuncionesService,
    private dbrutService: DBrutService,
    private dbcantidadService: DBcantidadService,
    private dblaboresService: DBlaboresService,
    private dbrutUsuariosService: DBrutUsuariosService,
    private avancelaborgdbService: AvanceLaborGDB,
    private avancelaborqdbService: AvanceLaborQDB,
    private sobrantesqdbService: SobrantesQDB,
    private productosqdb: ProductosQDB,
    private dbmedidaservice: DBmedidaService,
    private prosecnt: Proseso,
  ) {

    this.dblatlong = new DBlatlongModel(null, null, null, null, null);




    //
    setInterval(() => {
      //CREAR LATLONG LOCAL
      if (!sessionStorage.usuarioRut) {
        console.log("No ingresó rut...");
        return false;
      }

      this.geolocation.getCurrentPosition().then((resp) => {
        // resp.coords.latitude
        // resp.coords.longitude
        //const idgpsUsuarioLatLng = "";
        const gpsUsuarioLatLngLat = `${resp.coords.latitude}`;
        const gpsUsuarioLatLngLng = `${resp.coords.longitude}`;
        const gpsUsuarioLatLngFecha = this.funcionesService.fecha_hora_actual();
        const gpsUsuarioLatLngProcedencia = "login";
        const usuario_idUsuario = sessionStorage.idUsuario;
        const estadoLatLong = "1";
        /*
                const coords = `${resp.coords.latitude},${resp.coords.longitude}`;
                const lat = `${resp.coords.latitude}`;
                const long = `${resp.coords.longitude}`;
                const fecha = this.funcionesService.fecha_hora_actual();
                const estado = "1"; //1 sin enviar   //2 enviado
                const accion = "";
                const idUsuario = sessionStorage.idUsuario;
                console.log(coords);
        */
        //this.latLong.guardarRegistroLatLong(sessionStorage.usuarioRut, lat, long, fecha, estado, accion, idUsuario);
        this.latLong.guardarRegistroLatLong(gpsUsuarioLatLngLat, gpsUsuarioLatLngLng, gpsUsuarioLatLngFecha, gpsUsuarioLatLngProcedencia, usuario_idUsuario, estadoLatLong);
      }).catch((error) => {
        console.log('Error getting location', error);
      });
    }, 60000);


    //LAT LONG
    setInterval(() => {


      //LEER LATLONG LOCAL ENVIAR A BD Y BORRAR LOCAL
      let key = 'registrosLatLong';
      //leer data local
      this.storage.get(key).then((val) => {
        // console.log('get ' + key + ' ', val);

        if (val == null) {
          console.log("registros Lat Long sin datos")
          return false;
        }


        //recorrer data local
        for (var i = 0; i < val.length; i++) {
          if (val[i].estadoLatLong == '1') {
            //console.log("si:"+i);
            /*
                        const fechaLatLong = val[i].fechaLatLong;
                        const idUsuario = val[i].idUsuario;
                        const latitud = val[i].latitud;
                        const longitud = val[i].longitud;
                        const accionLatLong = val[i].accionLatLong;
            */

            const gpsUsuarioLatLngLat = val[i].gpsUsuarioLatLngLat;
            const gpsUsuarioLatLngLng = val[i].gpsUsuarioLatLngLng;
            const gpsUsuarioLatLngFecha = val[i].gpsUsuarioLatLngFecha;
            const gpsUsuarioLatLngProcedencia = val[i].gpsUsuarioLatLngProcedencia;
            const usuario_idUsuario = val[i].usuario_idUsuario;
            //const estadoLatLong = val[i].estadoLatLong;


            this.crearPostDB(
              gpsUsuarioLatLngLat,
              gpsUsuarioLatLngLng,
              gpsUsuarioLatLngFecha,
              gpsUsuarioLatLngProcedencia,
              usuario_idUsuario
            )
              .then(res => {

                //remove latlong local (enviada)
                this.storage.remove(key).then((val) => {
                  console.log('removed ' + key);
                  val[key] = "";
                }).catch((error) => {
                  //console.log('removed error for ' + key + '', error);
                });

              }).catch(error => {
                console.log('removed error for ' + key + '', error);
              });

          }
        }

      }).catch((error) => {
        console.log('get error for ' + key + '', error);
        return false;
      });

    }, 30000)
    //}, 120000)






    setInterval(() => {

      //LEER registroSobrantesQLOCAL ENVIAR A BD Y BORRAR LOCAL
      let key = 'registroSobrantesQ';
      //leer data local
      this.storage.get(key).then((val) => {
        // console.log('get ' + key + ' ', val);

        if (val == null) {
          console.log("registros registroSobrantesQ sin datos")
          return false;
        }
        //recorrer data local
        for (var i = 0; i < val.length; i++) {
          if (val[i].estado == '1') {
            //console.log("si:"+i);

            const sobrantesQCantidad = val[i].sobrantesQCantidad;
            const sobrantesQNombreProducto = val[i].sobrantesQNombreProducto;
            const sobrantesQObservacion = val[i].sobrantesQObservacion;
            const labor_idLabor = val[i].labor_idLabor;

            console.log(sobrantesQCantidad, sobrantesQNombreProducto, sobrantesQObservacion, labor_idLabor);

            this.crearPostSobrantesDB(
              sobrantesQCantidad,
              sobrantesQNombreProducto,
              sobrantesQObservacion,
              labor_idLabor
            )
              .then(res => {

                //remove latlong local (enviada)
                this.storage.remove(key).then((val) => {
                  console.log('removed ' + key);
                  val[key] = "";
                }).catch((error) => {
                  //console.log('removed error for ' + key + '', error);
                });

              }).catch(error => {
                console.log('removed error for ' + key + '', error);
              });

          }
        }

      }).catch((error) => {
        console.log('get error for ' + key + '', error);
        return false;
      });

    }, 35000)











    setInterval(() => {

      // 1- leer avanceLaborGLocal
      let key = 'productosQLocal';
      //leer data local
      this.storage.get(key).then((val) => {

        if (val == null) {
          console.log("1 registros productosQLocal sin datos")
          return false;
        } else {

          for (let i = 0; i < val.length; i++) {
            // 2- verificar estado == 1
            if (val[i].estado == '1') {
              const productosQCantidad = val[i].productosQCantidad;
              const productosQNombre = val[i].productosQNombre;
              const labor_idLabor = val[i].labor_idLabor;
              const unidadmedida_idUnidadMedida = val[i].unidadmedida_idUnidadMedida;
              const idFecha = val[i].idFecha;
              const estado = val[i].estado;

              console.log(productosQCantidad, productosQNombre, labor_idLabor, unidadmedida_idUnidadMedida, idFecha, estado);

              // 3- enviar a BD Registros_labores/registros_labores_insertar/
              this.crearProductosQDB(
                productosQCantidad,
                productosQNombre,
                labor_idLabor,
                unidadmedida_idUnidadMedida,
                idFecha,
                estado
              )
                .then(res => {
                  /*
                                      //remove latlong local (enviada)
                                      this.storage.remove(key).then((val) => {
                                        console.log('removed ' + key);
                                        val[key] = "";
                                      }).catch((error) => {
                                        //console.log('removed error for ' + key + '', error);
                                      });
                                      */

                  //modificar estado cantidad local (enviada)
                  // 4- cambiar estado a avanceLaborGLocal, estado == 2.
                  this.storage.get(key).then(valueStr => {
                    console.log(valueStr);
                    let value = valueStr;
                    console.log("Estado Envío: " + value[i].estado);
                    // Modify just that property
                    value[i].estado = "2";

                    // Save the entire data again
                    this.storage.set(key, value);

                    console.log("Modificado Estado Envío local " + value);
                  });




                }).catch(error => {
                  console.log('error for ' + key + '', error);
                });
            } else {

              // 5- después de un dia (24 horas), borrar de avanceLaborGLocal los estados  == 2.

              let fecha_hoy = this.funcionesService.fecha_hora_actual();
              let fecha_hoy2 = fecha_hoy.substr(0, 10);
              //let xx = (val[i].avanceLaborGFecha).substr(0, 10);
              //console.log(  val[i].envio+" "+ fecha_hoy2 +" "+   xx )

              if (val[i].estado == "2" && fecha_hoy2 != (val[i].idFecha).substr(0, 10)) {
                this.storage.remove(key).then((val) => {
                  console.log('Borrados - Envios de más de 1 día: ' + key);
                  val[key] = "";
                }).catch((error) => {
                  //console.log('removed error for ' + key + '', error);
                });
              }

            }//fin else

          }

        }//fin else val null

      }).catch((error) => {
        console.log('get error for ' + key + '', error);
      });


    }, 100000);


















    // 1- leer avanceLaborGLocal
    // 2- verificar estado == 1
    // 3- enviar a BD Registros_labores/registros_labores_insertar/
    // 4- cambiar estado a avanceLaborGLocal, estado == 2.
    // 5- después de un dia borrar de avanceLaborGLocal los estados  == 2.

    setInterval(() => {

      // 1- leer avanceLaborGLocal
      let key = 'avanceLaborGLocal';
      //leer data local
      this.storage.get(key).then((val) => {
        // console.log('get ' + key + ' ', val);

        if (val == null) {
          console.log("1 registros avanceLaborGLocal sin datos")
          return false;
        } else {

          //recorrer data local
          for (let i = 0; i < val.length; i++) {
            // 2- verificar estado == 1
            if (val[i].envio == '1') {
              console.log("Sin enviar: " + val[i].labor_idLabor + " - " + val[i].envio + " = " + i);

              const avanceLaborGCantidad = val[i].avanceLaborGCantidad;
              const avanceLaborGRutTrabajador = val[i].avanceLaborGRutTrabajador;
              const avanceLaborGObjetivoLabor = val[i].avanceLaborGObjetivoLabor;
              const avanceLaborGLat = val[i].avanceLaborGLat;
              const avanceLaborGLng = val[i].avanceLaborGLng;
              const avanceLaborGFecha = val[i].avanceLaborGFecha;
              const avanceLaborGCantGrupo = val[i].avanceLaborGCantGrupo;
              const avanceLaborGFormato = val[i].avanceLaborGFormato;
              const labor_idLabor = val[i].labor_idLabor;

              // 3- enviar a BD Registros_labores/registros_labores_insertar/
              this.crearAvanceLaborGeneralDB(
                avanceLaborGCantidad,
                avanceLaborGRutTrabajador,
                avanceLaborGObjetivoLabor,
                avanceLaborGLat,
                avanceLaborGLng,
                avanceLaborGFecha,
                avanceLaborGCantGrupo,
                avanceLaborGFormato,
                labor_idLabor
              )
                .then(res => {

                  //remove latlong local (enviada)
                  this.storage.remove(key).then((val) => {
                    console.log('removed ' + key);
                    val[key] = "";
                  }).catch((error) => {
                    //console.log('removed error for ' + key + '', error);
                  });



                }).catch(error => {
                  console.log('error for ' + key + '', error);
                });
            } else {

              // 5- después de un dia (24 horas), borrar de avanceLaborGLocal los estados  == 2.

              let fecha_hoy = this.funcionesService.fecha_hora_actual();
              let fecha_hoy2 = fecha_hoy.substr(0, 10);
              //let xx = (val[i].avanceLaborGFecha).substr(0, 10);
              //console.log(  val[i].envio+" "+ fecha_hoy2 +" "+   xx )

              if (val[i].envio == "2" && fecha_hoy2 != (val[i].avanceLaborGFecha).substr(0, 10)) {
                this.storage.remove(key).then((val) => {
                  console.log('Borrados - Envios de más de 1 día: ' + key);
                  val[key] = "";
                }).catch((error) => {
                  //console.log('removed error for ' + key + '', error);
                });
              }

            }//fin else

          }

        }//fin else val null

      }).catch((error) => {
        console.log('get error for ' + key + '', error);
      });





    }, 10000);
    //60000








    // 1- leer avanceLaborGLocal
    // 2- verificar estado == 1
    // 3- enviar a BD Registros_labores/registros_labores_insertar/
    // 4- cambiar estado a avanceLaborGLocal, estado == 2.
    // 5- después de un dia borrar de avanceLaborGLocal los estados  == 2.

    setInterval(() => {

      // 1- leer avanceLaborGLocal
      let key = 'avanceLaborQLocal';
      //leer data local
      this.storage.get(key).then((val) => {
        // console.log('get ' + key + ' ', val);

        if (val == null) {
          console.log("1 registros avanceLaborQLocal sin datos")
          return false;
        } else {

          // console.log("total datos avanceLaborQLocal: "+val.length);

          //recorrer data local


          for (let i = 0; i < val.length; i++) {
            // 2- verificar estado == 1
            if (val[i].envio == '1') {
              console.log("Sin enviar: " + val[i].labor_idLabor + " - " + val[i].envio + " = " + i);

              const avanceLaborQCantidad = val[i].avanceLaborQCantidad;
              const avanceLaborQProducto = val[i].avanceLaborQProducto;
              const avanceLaborQBombada = val[i].avanceLaborQBombada;
              const avanceLaborQLat = val[i].avanceLaborQLat;
              const avanceLaborQLng = val[i].avanceLaborQLng;
              const avanceLaborQFecha = val[i].avanceLaborQFecha;
              const avanceLaborQRutTrabajador = val[i].avanceLaborQRutTrabajador;
              const avanceLaborQUnidadMedida = val[i].avanceLaborQUnidadMedida;
              const labor_idLabor = val[i].labor_idLabor;
              const envio = val[i].envio;

              // 3- enviar a BD Registros_labores/registros_labores_insertar/
              this.crearAvanceLaborQDB(
                avanceLaborQCantidad,
                avanceLaborQProducto,
                avanceLaborQBombada,
                avanceLaborQLat,
                avanceLaborQLng,
                avanceLaborQFecha,
                avanceLaborQRutTrabajador,
                avanceLaborQUnidadMedida,
                labor_idLabor
              )
                .then(res => {
                  /*
                                      //remove latlong local (enviada)
                                      this.storage.remove(key).then((val) => {
                                        console.log('removed ' + key);
                                        val[key] = "";
                                      }).catch((error) => {
                                        //console.log('removed error for ' + key + '', error);
                                      });
                                      */

                  //modificar estado cantidad local (enviada)
                  // 4- cambiar estado a avanceLaborGLocal, estado == 2.
                  this.storage.get(key).then(valueStr => {
                    console.log(valueStr);
                    let value = valueStr;
                    console.log("Estado Envío: " + value[i].envio);
                    // Modify just that property
                    value[i].envio = "2";

                    // Save the entire data again
                    this.storage.set(key, value);

                    console.log("Modificado Estado Envío local " + value);
                  });




                }).catch(error => {
                  console.log('error for ' + key + '', error);
                });
            } else {

              // 5- después de un dia (24 horas), borrar de avanceLaborGLocal los estados  == 2.

              let fecha_hoy = this.funcionesService.fecha_hora_actual();
              let fecha_hoy2 = fecha_hoy.substr(0, 10);
              //let xx = (val[i].avanceLaborGFecha).substr(0, 10);
              //console.log(  val[i].envio+" "+ fecha_hoy2 +" "+   xx )

              if (val[i].envio == "2" && fecha_hoy2 != (val[i].avanceLaborQFecha).substr(0, 10)) {
                this.storage.remove(key).then((val) => {
                  console.log('Borrados - Envios de más de 1 día: ' + key);
                  val[key] = "";
                }).catch((error) => {
                  //console.log('removed error for ' + key + '', error);
                });
              }

            }//fin else

          }

        }//fin else val null

      }).catch((error) => {
        console.log('get error for ' + key + '', error);
      });


    }, 5000);
  }//fin constructor




  ngOnInit() {

    sessionStorage.usuarioRut = "";
    sessionStorage.idFundo = "";

    //LLAMADA LAT LONG SIMPLE GET 100%
    this.dblatlongService.getLatLong().subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log(error);
      }
    );


    //LLAMADA UNIDADES MEDIDA ALL GET 100%
    this.dbmedidaservice.getMedida().subscribe(
      response => {
        console.log(response);
        localStorage.unidadesMedida = JSON.stringify(response);
        //this.listadoUnidadMedida = JSON.parse(localStorage.unidadesMedida);

      },
      error => {
        console.log(error);
      }
    );
  }

  //CREAR LATLONG EN DB
  async crearPostDB(
    gpsUsuarioLatLngLat,
    gpsUsuarioLatLngLng,
    gpsUsuarioLatLngFecha,
    gpsUsuarioLatLngProcedencia,
    usuario_idUsuario
  ) {

    //alert("Crear post db");
    //console.log(fechaLatLong+" "+idUsuario+" "+latitud+" "+longitud+" "+accionLatLong);
    //return false;
    //
    const fecha = this.funcionesService.fecha_hora_actual();
    //formData
    let info: any = new FormData();


    //info.append('idGpsUsuario', '');
    info.append('gpsUsuarioLatLngLat', gpsUsuarioLatLngLat);
    info.append('gpsUsuarioLatLngLng', gpsUsuarioLatLngLng);
    info.append('gpsUsuarioLatLngFecha', gpsUsuarioLatLngFecha);
    info.append('gpsUsuarioLatLngProcedencia', 1);
    info.append('usuario_idUsuario', usuario_idUsuario);



    var object = {};
    info.forEach((value, key) => { object[key] = value });
    var data_cruda = object;
    console.log("data_cruda: " + data_cruda);
    //console.log(info);

    //this.info = {fechaLatLong:1,idLatLong:2,latitud:3,longitud:4,fechaEntrada:5};
    this.dblatlongService.CrearPostLatLong(data_cruda).subscribe(
      response => {
        console.log(response);
      },

      error => {
        console.log(error);
      }

    )

    //

  }



  //CREAR LATLONG EN DB
  async crearPostSobrantesDB(
    sobrantesQCantidad,
    sobrantesQNombreProducto,
    sobrantesQObservacion,
    labor_idLabor
  ) {

    //const fecha = this.funcionesService.fecha_hora_actual();
    //formData
    let info: any = new FormData();

    info.append('sobrantesQCantidad', sobrantesQCantidad,);
    info.append('sobrantesQNombreProducto', sobrantesQNombreProducto,);
    info.append('sobrantesQObservacion', sobrantesQObservacion,);
    info.append('labor_idLabor', labor_idLabor);

    var object = {};
    info.forEach((value, key) => { object[key] = value });
    var data_cruda = object;
    console.log("data_cruda: " + data_cruda);

    this.sobrantesqdbService.CrearPostSobrantes(data_cruda).subscribe(
      response => {
        console.log(response);
      },

      error => {
        console.log(error);
      }

    )

    //

  }


  //CREAR crearAvanceLaborGeneralDB EN DB
  async crearAvanceLaborGeneralDB(
    avanceLaborGCantidad,
    avanceLaborGRutTrabajador,
    avanceLaborGObjetivoLabor,
    avanceLaborGLat,
    avanceLaborGLng,
    avanceLaborGFecha,
    avanceLaborGCantGrupo,
    avanceLaborGFormato,
    labor_idLabor
  ) {

    const fecha = this.funcionesService.fecha_hora_actual();

    let info: any = new FormData();
    info.append('avanceLaborGCantidad', avanceLaborGCantidad);
    info.append('avanceLaborGRutTrabajador', avanceLaborGRutTrabajador);
    info.append('avanceLaborGObjetivoLabor', avanceLaborGObjetivoLabor);
    info.append('avanceLaborGLat', avanceLaborGLat);
    info.append('avanceLaborGLng', avanceLaborGLng);
    info.append('avanceLaborGFecha', avanceLaborGFecha);
    info.append('avanceLaborGCantGrupo', avanceLaborGCantGrupo);
    info.append('avanceLaborGFormato', avanceLaborGFormato);
    info.append('labor_idLabor', labor_idLabor);

    var object = {};
    info.forEach((value, key) => { object[key] = value });
    var data_cruda = object;
    console.log("data_cruda: " + data_cruda);

    this.avancelaborgdbService.CrearPostAvance(data_cruda).subscribe(
      response => {
        console.log(response);
      },

      error => {
        console.log(error);
      }

    )

  }




  //CREAR crearAvanceLaborGeneralDB EN DB
  async crearAvanceLaborQDB_old(
    avanceLaborQCantidad,
    avanceLaborQProducto,
    avanceLaborQBombada,
    avanceLaborQLat,
    avanceLaborQLng,
    avanceLaborQFecha,
    avanceLaborQRutTrabajador,
    avanceLaborQUnidadMedida,
    labor_idLabor
  ) {

    alert("enviando");
    this.data_cruda_2 = {
      avanceLaborQCantidad: avanceLaborQCantidad,
      avanceLaborQProducto: avanceLaborQProducto,
      avanceLaborQBombada: avanceLaborQBombada,
      avanceLaborQLat: avanceLaborQLat,
      avanceLaborQLng: avanceLaborQLng,
      avanceLaborQFecha: avanceLaborQFecha,
      avanceLaborQRutTrabajador: avanceLaborQRutTrabajador,
      avanceLaborQUnidadMedida: avanceLaborQUnidadMedida,
      labor_idLabor: labor_idLabor
    }

    this.avancelaborqdbService.CrearPostAvance(this.data_cruda_2).subscribe(
      response => {
        console.log(response);
      },

      error => {
        console.log(error);
      }

    )

  }








  //CREAR crearAvanceLaborGeneralDB EN DB
  async crearProductosQDB(
    productosQCantidad,
    productosQNombre,
    labor_idLabor,
    unidadmedida_idUnidadMedida,
    idFecha,
    estado

  ) {

    // const fecha = this.funcionesService.fecha_hora_actual();

    let info: any = new FormData();

    info.append('productosQCantidad', productosQCantidad);
    info.append('productosQNombre', productosQNombre);
    info.append('labor_idLabor', labor_idLabor);
    info.append('unidadmedida_idUnidadMedida', unidadmedida_idUnidadMedida);
    info.append('idFecha', idFecha);
    info.append('estado', estado);


    var object = {};
    info.forEach((value, key) => { object[key] = value });
    var data_cruda = object;
    console.log("data_cruda: " + data_cruda);

    this.productosqdb.CrearPostProductosQDB(data_cruda).subscribe(
      response => {
        //no mostrar el error cuando quiere ingresar un dato repetido en BD
        console.log(response);
      },

      error => {
        //no mostrar el error cuando quiere ingresar un dato repetido en BD
        console.log(error);
      }

    )

  }


  //CREAR crearAvanceLaborGeneralDB EN DB
  async crearAvanceLaborQDB(
    avanceLaborQCantidad,
    avanceLaborQProducto,
    avanceLaborQBombada,
    avanceLaborQLat,
    avanceLaborQLng,
    avanceLaborQFecha,
    avanceLaborQRutTrabajador,
    avanceLaborQUnidadMedida,
    labor_idLabor,
  ) {

    const fecha = this.funcionesService.fecha_hora_actual();

    let info: any = new FormData();

    info.append('avanceLaborQCantidad', avanceLaborQCantidad);
    info.append('avanceLaborQProducto', avanceLaborQProducto);
    info.append('avanceLaborQBombada', avanceLaborQBombada);
    info.append('avanceLaborQLat', avanceLaborQLat);
    info.append('avanceLaborQLng', avanceLaborQLng);
    info.append('avanceLaborQFecha', avanceLaborQFecha);
    info.append('avanceLaborQRutTrabajador', avanceLaborQRutTrabajador);
    info.append('avanceLaborQUnidadMedida', avanceLaborQUnidadMedida);
    info.append('labor_idLabor', labor_idLabor);


    var object = {};
    info.forEach((value, key) => { object[key] = value });
    var data_cruda = object;
    console.log("data_cruda: " + data_cruda);

    this.avancelaborqdbService.CrearPostAvance(data_cruda).subscribe(
      response => {
        //no mostrar el error cuando quiere ingresar un dato repetido en BD
        // console.log(response);
      },

      error => {
        //no mostrar el error cuando quiere ingresar un dato repetido en BD
        // console.log(error);
      }

    )

  }






  //CREAR RUT EN DB
  async crearRutDB(idlabor, text, format, created) {
    //
    //const fecha = this.funcionesService.fecha_hora_actual();
    //formData
    let info: any = new FormData();
    info.append('idlabor', idlabor);
    info.append('text', text);
    info.append('format', format);
    info.append('created', created);

    var object = {};
    info.forEach((value, key) => { object[key] = value });
    var data_cruda = object;
    //console.log(data_cruda);
    //console.log(info);

    //this.info = {fechaLatLong:1,idLatLong:2,latitud:3,longitud:4,fechaEntrada:5};
    this.dbrutService.CrearPostRut(data_cruda).subscribe(
      response => {
        // console.log(response);
      },
      error => {
        // console.log(error);
      }
    )

    //

  }

  validarUsuario(formularioUsuario) {
    console.log("Validar")
    console.log(formularioUsuario.value.pass + " " + formularioUsuario.value.rut);


    var data_cruda = {
      usuarioRut: formularioUsuario.value.rut,
      usuarioPass: formularioUsuario.value.pass
    };



    this.dbrutUsuariosService.ValidarPostRut(data_cruda).subscribe(
      response => {

        console.log(response.data);
        console.log('ACA!!')

        if ((formularioUsuario.value.pass == response.data.usuarioPass) && (response.data.estado_idEstado == "1") && (response.data.estado_idEstado == "1")) {
          sessionStorage.usuarioRut = formularioUsuario.value.rut;
          sessionStorage.usuarioNivelPermiso = response.data.nivelAcceso_idNivelAcceso;
          sessionStorage.usuarioCategoria = response.data.perfil_idPerfil;
          sessionStorage.usuarioEstado = response.data.estado_idEstado;
          sessionStorage.usuarioNombre = response.data.usuarioNombres;
          sessionStorage.usuarioApellidoPaterno = response.data.usuarioApellidoPaterno;
          sessionStorage.idUsuario = response.data.idUsuario;
          sessionStorage.idFundo = response.data.fundo_idFundo;




          this.router.navigate(['/lovi']);
        } else {
          alert("Clave inválida, o Usuario inhabilitado para ingresar, hable con el administrador por favor.");
          return false;
        }


      },
      error => {
        console.log("error en http post a restfull_2")
        console.log("UpdatePostLabores error " + error);
      }
    )

  }
  iniciarGeolocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude
      this.lon = resp.coords.longitude

      console.log(resp.coords);

      let watch = this.geolocation.watchPosition();
      watch.subscribe((data) => {
        // data can be a set of coordinates, or an error (if an error occurred).
        // data.coords.latitude
        // data.coords.longitude

        this.lat = data.coords.latitude
        this.lon = data.coords.longitude
        console.log('Watch: ', data);


      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });

    60000
  }

  agregar1() {
    return new Promise((resolve) => {
      let body = {
        datos: "insertar",
        longitud: this.lon,
        latitud: this.lat,

        proce1: this.proce,
        user1: this.idUsuario,
        estado1: this.estado,
        geo1: this.geo,
        fecha: this.funcionesService.fecha_hora_actual()
      };

      this.prosecnt.postData(body, "geo2.php").subscribe((data) => {
        console.log("OK");
      });
    });
  }


}



