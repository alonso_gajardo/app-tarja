import { Component, OnInit } from '@angular/core';
import { Router, Routes } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { FuncionesService } from 'src/app/services/funciones.service';
import { DataLocalLaboresLatLong } from 'src/app/services/data-local-latlong.service';
import { DBlaboresService } from 'src/app/services/data-bd-labores.service';
import { DataLocalLaboresService } from '../../services/data-local-labores.service';
import {Storage} from '@ionic/storage';
import { RegistroLabores } from 'src/app/models/registroLabores.model';
import { DBcontratistaOKService } from 'src/app/services/contratista.service';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-playpause',
  templateUrl: './playpause.page.html',
  styleUrls: ['./playpause.page.scss'],
})
export class PlaypausePage implements OnInit {
  botonIngresar: boolean = false;
  botonPlay: boolean =  false;
  botonPausa: boolean =  true;
  botonSalida: boolean =  true;
  disabledIngresar: boolean =  false;
  disabledPlay: boolean =  true;
  disabledPausa: boolean =  false;
  disabledSalida: boolean =  false;
  textoIngresar: boolean =  true;

  disabledEntrada: boolean = true;

  on:boolean = false;
  off:boolean = true;
  idUsuario: string = sessionStorage.idUsuario;
  
  guardadosLabores: RegistroLabores[] =[];

  //cargar datos de tabla arreglo local
arregloLocal2:[] = JSON.parse( localStorage.getItem('arregloLocal') ) ;

  labores = {
    idLabor:"",
    laborNombre: "",
    laborDescripcion: "",
    laborValorTratoInicial: "",
    laborValorTratoFinal: "",
    laborObjetivo: "",
    laborLat: "",
    laborLng: "",
    laborFecha: "",
    laborIconoAsignacion: "",
    laborIcono: "",
    laborCuartel: "",
    laborDescripcionCierre: "",
    estado_idEstado: "",
    tipoLabor_idTipoLabor: "",
    laborAplicacionQ_idLaborAplicacionQ: "",
    contratista_idContratista: "",
    usuario_idUsuario: ""
  };


  constructor(
    private router: Router,
    private geolocation: Geolocation,
    public funcionesService: FuncionesService,
    private latLong: DataLocalLaboresLatLong,
    private dblaboresService: DBlaboresService,
    public dataLocalLaborGeneral: DataLocalLaboresService,
    private storage: Storage,
    private dbcontratistaService: DBcontratistaOKService,
    public loadingController: LoadingController
    ) { }

  ngOnInit() {
    /*
        //DB LLAMADA a tabla LABORES x idUsuario y agregar datos en BD LOCAL 100%
        this.dblaboresService.getLaboresXidCliente(this.idUsuario).subscribe(
          response => {

            for (let i = 0; i <= response.registros_labores.length; i++) {
              const element = response.registros_labores[i];
              console.log(element.idLabor);
              //console.log(element.laborNombre);
              var idLabor = element.idLabor;
              var laborNombre = element.laborNombre;
              var laborDescripcion = element.laborDescripcion;
              var laborValorTratoInicial = element.laborValorTratoInicial;
              var laborValorTratoFinal = element.laborValorTratoFinal;
              var laborObjetivo = element.laborObjetivo;
              var laborLat = element.laborLat;
              var laborLng = element.laborLng;
              var laborFecha = element.laborFecha;
              var laborIconoAsignacion = element.laborIconoAsignacion;
              var laborIcono = element.laborIcono;
              var laborCuartel = element.laborCuartel;
              var laborDescripcionCierre = element.laborDescripcionCierre;
              var estado_idEstado = element.estado_idEstado;
              var tipoLabor_idTipoLabor = element.tipoLabor_idTipoLabor;
              var laborAplicacionQ_idLaborAplicacionQ = element.laborAplicacionQ_idLaborAplicacionQ;
              var contratista_idContratista = element.contratista_idContratista;
              var usuario_idUsuario = element.usuario_idUsuario;

              const nuevoRegistroLabores = new RegistroLabores(
                idLabor,
                laborNombre,
                laborDescripcion,
                laborValorTratoInicial,
                laborValorTratoFinal,
                laborObjetivo,
                laborLat,
                laborLng,
                laborFecha,
                laborIconoAsignacion,
                laborIcono,
                laborCuartel,
                laborDescripcionCierre,
                estado_idEstado,
                tipoLabor_idTipoLabor,
                laborAplicacionQ_idLaborAplicacionQ,
                contratista_idContratista,
                usuario_idUsuario
                );
          
              //ordena y deja el ultimo ingresado en primer lugar
              this.guardadosLabores.unshift(nuevoRegistroLabores);
              //guarda los datos en local
              this.storage.set('registrosLabores',this.guardadosLabores);
            }
           
          },
          error => {
            console.log(error);
          }
        );
*/
this.obtener_laboresDB();

this.obtener_contratistaDB();

  }//fin on init




  enviar_playpause(x){



if(x == 'botonIngresar'){
  this.botonIngresar = this.on;
  this.botonPlay = this.on;
  this.botonPausa = this.off;
  this.botonSalida = this.on;

  this.disabledIngresar = true;
  this.disabledPlay = false;
  this.disabledPausa = true;
  this.disabledSalida = false;

  this.disabledEntrada = true;

  this.botones_estado_latlong("ingresar");

}

if(x == 'botonPlay'){
  this.botonIngresar = this.on;
  this.botonPlay = this.off;
  this.botonPausa = this.on;
  this.botonSalida = this.on;

  this.disabledIngresar = true;
  this.disabledPlay = false;
  this.disabledPausa = false;
  this.disabledSalida = false;

  this.disabledEntrada = false;
  this.botones_estado_latlong("play");
}

if(x == 'botonPausa'){
  this.botonIngresar = this.on;
  this.botonPlay = this.on;
  this.botonPausa = this.off;
  this.botonSalida = this.on;

  this.disabledIngresar = true;
  this.disabledPlay = false;
  this.disabledPausa = false;
  this.disabledSalida = false;

  this.disabledEntrada = true;
  this.botones_estado_latlong("pausa");
}

if(x == 'botonSalida'){
  this.botonIngresar = this.on;
  this.botonSalida = this.on;

  this.disabledIngresar = false;
  this.disabledPlay = true;
  this.disabledSalida = true;

  this.disabledEntrada = true;
  this.botones_estado_latlong("salida");
}

  }

 ingreso_a_principal(){
  //this.router.navigate(['/entrada']);
  this.cargando();

  this.router.navigate(['/listado-labores']);
 }




        botones_estado_latlong(x){
          //CREAR LATLONG LOCAL
        if(!sessionStorage.usuarioRut){
        console.log("No ingresó rut...");
        return false;
        }

        this.geolocation.getCurrentPosition().then((resp) => {
        // resp.coords.latitude
        // resp.coords.longitude
        /*
        const coords =`${resp.coords.latitude},${resp.coords.longitude}`;
        const lat =`${resp.coords.latitude}`;
        const long =`${resp.coords.longitude}`;
        const fecha = this.funcionesService.fecha_hora_actual();
        const estado ="1"; //1 sin enviar   //2 enviado
        const accion =x;
        const idUsuario = sessionStorage.idUsuario;
        console.log(coords);
        */

       const gpsUsuarioLatLngLat = `${resp.coords.latitude}`;
       const gpsUsuarioLatLngLng = `${resp.coords.longitude}`;
       const gpsUsuarioLatLngFecha = this.funcionesService.fecha_hora_actual();
       const gpsUsuarioLatLngProcedencia= "playpause";
       const usuario_idUsuario = sessionStorage.idUsuario;
       const estadoLatLong = "1";


        //this.latLong.guardarRegistroLatLong(sessionStorage.usuarioRut, lat, long, fecha, estado, accion, idUsuario);
        this.latLong.guardarRegistroLatLong(gpsUsuarioLatLngLat, gpsUsuarioLatLngLng, gpsUsuarioLatLngFecha, gpsUsuarioLatLngProcedencia, usuario_idUsuario, estadoLatLong);

        }).catch((error) => {
          console.log('Error getting location', error);
        });
        }




        obtener_laboresDB(){

          //return  false;
          const arregloLocal =[];
              //alert("obtener_laboresDB");
              //DB LLAMADA a tabla LABORES x idUsuario y agregar datos en BD LOCAL 100%
              this.dblaboresService.getLaboresXidCliente(this.idUsuario).subscribe(
                response => {
                  for (let i = 0; i < response.registros_labores.length; i++) {
                    const element = response.registros_labores[i];
                   // console.log(element.idLabor);
          
                    arregloLocal.push({
                      idLabor: element.idLabor,
                      laborNombre: element.laborNombre,
                      laborDescripcion: element.laborDescripcion,
                      laborValorTratoInicial: element.laborValorTratoInicial,
                      laborValorTratoFinal: element.laborValorTratoFinal,
                      laborObjetivo: element.laborObjetivo,
                      laborLat: element.laborLat,
                      laborLng: element.laborLng,
                      laborFecha: element.laborFecha,
                      laborIconoAsignacion: element.laborIconoAsignacion,
                      laborIcono: element.laborIcono,
                      laborCuartel: element.laborCuartel,
                      laborDescripcionCierre: element.laborDescripcionCierre,
                      estado_idEstado: element.estado_idEstado,
                      tipoLabor_idTipoLabor: element.tipoLabor_idTipoLabor,
                      laborAplicacionQ_idLaborAplicacionQ: element.laborAplicacionQ_idLaborAplicacionQ,
                      contratista_idContratista: element.contratista_idContratista,
                      usuario_idUsuario: element.usuario_idUsuario});
                  }
                  //ORDENA LA LISTA EN FORMA DESCENDENTE
                  arregloLocal.sort().reverse();
                  localStorage.arregloLocal = JSON.stringify(arregloLocal);
                  
                },
                error => {
                  console.log(error);
                }
              );
              //location.reload();
              //alert("Sincronizando con BD, esto tardará unos segundos");
              //this.router.navigate(['/entrada']);
          
          }





          obtener_contratistaDB(){

            const idFundo = sessionStorage.idFundo;

            //return  false;
            const contratistaLocal =[];
                //alert("obtener_laboresDB");
                //DB LLAMADA a tabla LABORES x idUsuario y agregar datos en BD LOCAL 100%
                this.dbcontratistaService.getContratista_x_fundo(idFundo).subscribe(
                  response => {

                    console.log(response);

                    //return  false;


                    for (let i = 0; i < response.contratistas.length; i++) {
                      const element = response.contratistas[i];
                      console.log(element.idContratista);
            
                      contratistaLocal.push({
                        /*
                        idLabor: element.idLabor,
                        laborNombre: element.laborNombre,
                        laborDescripcion: element.laborDescripcion,
                        laborValorTratoInicial: element.laborValorTratoInicial,
                        laborValorTratoFinal: element.laborValorTratoFinal,
                        laborObjetivo: element.laborObjetivo,
                        laborLat: element.laborLat,
                        laborLng: element.laborLng,
                        laborFecha: element.laborFecha,
                        laborIconoAsignacion: element.laborIconoAsignacion,
                        laborIcono: element.laborIcono,
                        laborCuartel: element.laborCuartel,
                        laborDescripcionCierre: element.laborDescripcionCierre,
                        estado_idEstado: element.estado_idEstado,
                        tipoLabor_idTipoLabor: element.tipoLabor_idTipoLabor,
                        laborAplicacionQ_idLaborAplicacionQ: element.laborAplicacionQ_idLaborAplicacionQ,
                        contratista_idContratista: element.contratista_idContratista,
                        usuario_idUsuario: element.usuario_idUsuario*/

idContratista: element.idContratista,
contratistaRut: element.contratistaRut,
contratistaNombre: element.contratistaNombre,
contratistaGiro: element.contratistaGiro,
contratistaFono: element.contratistaFono,
contratistaMail: element.contratistaMail,
contratistaDireccion: element.contratistaDireccion,
contratistaRazonSocial: element.contratistaRazonSocial,
estado_idEstado: element.estado_idEstado,
fundo_idFundo: element.fundo_idFundo

                      });
                    }
                    //ORDENA LA LISTA EN FORMA DESCENDENTE
                    contratistaLocal.sort().reverse();
                    localStorage.contratistaLocal = JSON.stringify(contratistaLocal);
                    
                  },
                  error => {
                    console.log(error);
                  }
                );
                //location.reload();
                //alert("Sincronizando con BD, esto tardará unos segundos");
                //this.router.navigate(['/entrada']);
            
            }





            async cargando() {
              const loading = await this.loadingController.create({
                message: 'Procesando...',
                duration: 3000
              });
              await loading.present();
          
              const { role, data } = await loading.onDidDismiss();
              console.log('Loading dismissed!');
            }



}
