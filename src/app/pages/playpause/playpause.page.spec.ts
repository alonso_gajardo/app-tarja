import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaypausePage } from './playpause.page';

describe('PlaypausePage', () => {
  let component: PlaypausePage;
  let fixture: ComponentFixture<PlaypausePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaypausePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaypausePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
