import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Lislabor1Page } from './lislabor1.page';

describe('Lislabor1Page', () => {
  let component: Lislabor1Page;
  let fixture: ComponentFixture<Lislabor1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lislabor1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Lislabor1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
