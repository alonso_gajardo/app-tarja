import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Lislabor1PageRoutingModule } from './lislabor1-routing.module';

import { Lislabor1Page } from './lislabor1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Lislabor1PageRoutingModule
  ],
  declarations: [Lislabor1Page]
})
export class Lislabor1PageModule {}
