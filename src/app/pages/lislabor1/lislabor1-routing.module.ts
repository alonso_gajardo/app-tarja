import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Lislabor1Page } from './lislabor1.page';

const routes: Routes = [
  {
    path: '',
    component: Lislabor1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Lislabor1PageRoutingModule {}
