import { Component, OnInit } from '@angular/core';
import { DBlatlongService } from 'src/app/services/data-bd-latlong.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.page.html',
  styleUrls: ['./principal.page.scss'],
})
export class PrincipalPage implements OnInit {
  listado: Object;


  constructor(
    private dataService: DBlatlongService
  ) { }

  ngOnInit() {
   
this.dataService.getLatLong().subscribe( posts =>{
console.log(posts);
this.listado = posts;
});

  }


}
