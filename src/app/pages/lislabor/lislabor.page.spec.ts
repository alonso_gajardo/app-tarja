import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LislaborPage } from './lislabor.page';

describe('LislaborPage', () => {
  let component: LislaborPage;
  let fixture: ComponentFixture<LislaborPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LislaborPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LislaborPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
