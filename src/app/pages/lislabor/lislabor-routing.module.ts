import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LislaborPage } from './lislabor.page';

const routes: Routes = [
  {
    path: '',
    component: LislaborPage,
    children: [
      {
        path:'Asignado',
        loadChildren: () => import('../lislabor1/lislabor1.module').then( m => m.Lislabor1PageModule)
      },
      {
        path:'Interna',
        loadChildren: () => import('../lislabor2/lislabor2.module').then( m => m.Lislabor2PageModule)
      },
      {
        path:'contratista',
        loadChildren: () => import('../lislabor3/lislabor3.module').then( m => m.Lislabor3PageModule)
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LislaborPageRoutingModule {}
