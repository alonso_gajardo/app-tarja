import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LislaborPageRoutingModule } from './lislabor-routing.module';

import { LislaborPage } from './lislabor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LislaborPageRoutingModule
  ],
  declarations: [LislaborPage]
})
export class LislaborPageModule {}
