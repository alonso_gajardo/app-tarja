import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralesContratistaPage } from './generales-contratista.page';

describe('GeneralesContratistaPage', () => {
  let component: GeneralesContratistaPage;
  let fixture: ComponentFixture<GeneralesContratistaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralesContratistaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralesContratistaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
