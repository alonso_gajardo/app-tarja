import { Component, OnInit } from "@angular/core";
import { ToastController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { Geolocation, Geoposition } from "@ionic-native/geolocation/ngx";
import { FuncionesService } from "../../services/funciones.service";
import { DataLocalLaboresLatLong } from "../../services/data-local-latlong.service";
import { DBlaboresService } from "../../services/data-bd-labores.service";
import { DataLocalLaboresService } from "../../services/data-local-labores.service";
import { RegistroLabores } from "../../models/registroLabores.model";
import { DBcontratistaOKService } from "../../services/contratista.service";
import { LoadingController, AlertController } from "@ionic/angular";
import { Router } from "@angular/router";
import { ProductosQFusionLocal } from "src/app/services/productosQFusion.service";
import { AvanceLaborQLocal } from "src/app/services/avancelaborq.service";
import { RegistroSobrantesQ } from "src/app/models/sobrantesQ.model";
import { DataLocalLaboresSobrantesQ } from "src/app/services/data-local-sobrantesQ.service";
import { DBrutUsuariosService } from "src/app/services/data-db-rut-usuarios.service";
import {Proseso} from "../../provedor/proseso";

import { AvanceLaborGLocal } from "src/app/services/avancelaborg.service";
@Component({
  selector: 'app-listado-labores',
  templateUrl: './listado-labores.page.html',
  styleUrls: ['./listado-labores.page.scss'],
})
export class ListadoLaboresPage implements OnInit {
  botonIngresar: boolean = false;
  botonPlay: boolean = false;
  botonPausa: boolean = true;
  botonSalida: boolean = true;
  disabledIngresar: boolean = false;
  disabledPlay: boolean = true;
  disabledPausa: boolean = false;
  disabledSalida: boolean = false;
  textoIngresar: boolean = true;

  disabledEntrada: boolean = true;

  on: boolean = false;
  off: boolean = true;
  idUsuario: string = sessionStorage.idUsuario;

  guardadosLabores: RegistroLabores[] = [];

  //cargar datos de tabla arreglo local
  arregloLocal2: [] = JSON.parse(localStorage.getItem("arregloLocal"));

  labores = {
    idLabor: "",
    laborNombre: "",
    laborDescripcion: "",
    laborValorTratoInicial: "",
    laborValorTratoFinal: "",
    laborObjetivo: "",
    laborLat: "",
    laborLng: "",
    laborFecha: "",
    laborIconoAsignacion: "",
    laborIcono: "",
    laborCuartel: "",
    laborDescripcionCierre: "",
    estado_idEstado: "",
    tipoLabor_idTipoLabor: "",
    laborAplicacionQ_idLaborAplicacionQ: "",
    contratista_idContratista: "",
    usuario_idUsuario: "",
  };

  // inicio termina aki

  items: any[] = [];

  usuarioRut: string = sessionStorage.usuarioRut;
  arregloLocalVista: any[] = ([] = JSON.parse(
    localStorage.getItem("arregloLocal")
  ));
  arregloLocalVista2: any[] = [];
  botonesEntrada: boolean = true;
  permiso: string;
  actualizar: number = 0;
  itemLaboresAbiertas: boolean = true;
  gpsUsuarioLatLngProcedencia: "2";
  segmento: string;

  nombre: string;
  laborObjetivo: string;
  descripcion: string;
  listadoRegistros: boolean;
  tipoLabor_idTipoLabor: string;
  idlabor: string;
  nombreContratista: string;
  tipoTratoValor: string;
  laborFecha: string;
  laborValorTratoInicial: string;

  avanceLaborGCantidad: string;
  avanceLaborGRutTrabajador: string;
  avanceLaborGObjetivoLabor: string;
  avanceLaborGLat: string;
  avanceLaborGLng: string;
  avanceLaborGFecha: string;
  avanceLaborGCantGrupo: string;
  avanceLaborGFormato: string;
  labor_idLabor: string;
  envio: string;

  avanceLaborGdata = {
    avanceLaborGCantidad: "",
    avanceLaborGRutTrabajador: "",
    avanceLaborGObjetivoLabor: "",
    avanceLaborGLat: "",
    avanceLaborGLng: "",
    avanceLaborGFecha: "",
    avanceLaborGCantGrupo: "",
    avanceLaborGFormato: "",
    labor_idLabor: "",
    envio: "",
  };

  cantidadCantidad: string;
  id_usuario: string;
  idLabor: string;
  tituloListadoLabores: string;

  contratista = {
    idContratista: "",
    contratistaRut: "",
    contratistaNombre: "",
    contratistaGiro: "",
    contratistaFono: "",
    contratistaMail: "",
    contratistaDireccion: "",
    contratistaRazonSocial: "",
    estado_idEstado: "",
  };

  listaRut = [];
  listaRut2 = [];
  data_cruda_productosQ = [];

  arregloQFusion = [];

  arregloQFusion2 = [];

  laborNombre: string;
  laborDescripcion: string;
  cierreDescripcion: string;

  i: string;

  productosQLocal: any[] = [];

  productosQFusionLocal: any[] = [];

  mostrarBoton: boolean;

  cierre_1: boolean;
  cierre_2: boolean;
  cierre_3: boolean;
  cierre_4: boolean;
  cierre_5: boolean;
  cierre_6: boolean;
  cierre_7: boolean;
  cierre_8: boolean;
  cierre_9: boolean;
  cierre_10: boolean;

  btn_cierre_1: boolean;
  btn_cierre_2: boolean;
  btn_cierre_3: boolean;
  btn_cierre_4: boolean;
  btn_cierre_5: boolean;
  btn_cierre_6: boolean;
  btn_cierre_7: boolean;
  btn_cierre_8: boolean;
  btn_cierre_9: boolean;
  btn_cierre_10: boolean;

  btn_cerrarLaborQuimica: boolean;
  estadoGrupo1: boolean;
  estadoGrupo2: boolean;

  rutaLogo: string;

  estadoBotonInterna: boolean;
  estadoBotonContratista: boolean;
  botonAgregarQuimica: boolean;

  listaBombada = [];

  totalBombadas: number;


  lat: number;
  lon: number;
  total: string;
  geo = "";
  geolocal: string = "iniciogeo local";
  geot2: string = "Termina de Trabajar";
  geot3: string = "Sale del Trabajo";
  proce: string = "1";
  estado: string = "1";
  constructor(
    private toastController: ToastController,
    private router: Router,
    private storage: Storage,

    private geolocation: Geolocation,
    public funcionesService: FuncionesService,
    private latLong: DataLocalLaboresLatLong,
    private dblaboresService: DBlaboresService,
    public dataLocalLaborGeneral: DataLocalLaboresService,

    private dbcontratistaService: DBcontratistaOKService,
    public loadingController: LoadingController,

    // otra pagina

    private avancelaborglocal: AvanceLaborGLocal,

    public productosQFusion: ProductosQFusionLocal,

    public avancelaborq: AvanceLaborQLocal,
    public sobrantesq: DataLocalLaboresSobrantesQ,
    public dbrutusuarios: DBrutUsuariosService,
    public alertController: AlertController,
// otra sensor
    private prosecnt: Proseso,
  ) {
    this.iniciarGeolocation();
 
    this.obtener_laboresDB();
    this.listaRut = JSON.parse(localStorage.getItem("listaRut"));

    this.obtener_lista_productosQ();
    this.obtener_productosQFusionLocal();

    this.mostrarBoton = true;

    this.cierre_1 = false;
    this.cierre_2 = false;
    this.cierre_3 = false;
    this.cierre_4 = false;
    this.cierre_5 = false;
    this.cierre_6 = false;
    this.cierre_7 = false;
    this.cierre_8 = false;
    this.cierre_9 = false;
    this.cierre_10 = false;

    this.btn_cierre_1 = true;
    this.btn_cierre_2 = false;
    this.btn_cierre_3 = false;
    this.btn_cierre_4 = false;
    this.btn_cierre_5 = false;
    this.btn_cierre_6 = false;
    this.btn_cierre_7 = false;
    this.btn_cierre_8 = false;
    this.btn_cierre_9 = false;
    this.btn_cierre_10 = false;

    this.btn_cerrarLaborQuimica = false;
    this.estadoGrupo1 = false;
    this.estadoGrupo2 = false;

    this.rutaLogo = "";
    this.estadoBotonInterna = false;
    this.estadoBotonContratista = false;
    this.cierreDescripcion = "";
    this.tituloListadoLabores = "Listado de Labores";

    this.botonAgregarQuimica = false;
    this.totalBombadas = 0;
  }

  ngOnInit() {
    this.obtener_contratistaDB();
    this.obtener_laboresDBS();
    // otra Pgina
    this.segmento = "inicial";
    this.obtener_laboresDB();
    console.log(this.idUsuario);
    this.contratista = JSON.parse(localStorage.getItem("contratistaLocal"));
    this.permiso = sessionStorage.usuarioNivelPermiso;

    this.estadoBotonInterna = false;
    this.estadoBotonContratista = false;

    this.cierreDescripcion = "";
    this.tituloListadoLabores = "Listado de Labores";

    this.botonAgregarQuimica = false;
    this.totalBombadas = 0;
  }
  enviar_playpause(x) {
    if (x == "botonIngresar") {
      this.botonIngresar = this.off;
      this.botonPlay = this.on;
      this.botonPausa = this.off;
      this.botonSalida = this.on;

      this.disabledIngresar = true;
      this.disabledPlay = false;
      this.disabledPausa = true;
      this.disabledSalida = false;

      this.disabledEntrada = true;

      this.botones_estado_latlong("ingresar");
      this.geo = "Inicio Jornada Laboral";
      this.agregar1();
    }
    if (x == "botonPlay") {
      this.botonIngresar = this.off;
      this.botonPlay = this.off;
      this.botonPausa = this.on;
      this.botonSalida = this.off;

      this.disabledIngresar = true;
      this.disabledPlay = true;
      this.disabledPausa = false;
      this.disabledSalida = true;

      this.disabledEntrada = false;
      this.botones_estado_latlong("play");
      this.geo = "Entro a Trabajar";
      this.agregar1();
    }
    if (x == "botonPausa") {
      this.botonIngresar = this.off;
      this.botonPlay = this.on;
      this.botonPausa = this.off;
      this.botonSalida = this.on;

      this.disabledIngresar = true;
      this.disabledPlay = false;
      this.disabledPausa = true;
      this.disabledSalida = false;

      this.disabledEntrada = true;
      this.botones_estado_latlong("pausa");
      this.geo = "Pausa en el Trabajo";
      this.agregar1();
    }

    if (x == "botonSalida") {
      this.botonIngresar = this.on;
      this.botonPlay = this.off;
      this.botonPausa = this.off;
      this.botonSalida = this.off;

      this.disabledIngresar = false;
      this.disabledPlay = true;
      this.disabledPausa = true;
      this.disabledSalida = true;

      this.disabledEntrada = true;
      this.botones_estado_latlong("salida");
      this.geo = "Termina de Trabajar";
      this.agregar1();
    }

    
  }

  ingreso_a_principal() {
    //this.router.navigate(['/entrada']);
    this.router.navigate(["/listar-labor"]);
    this.cargando();
  }
  ingreso_a_principal2() {
    //this.router.navigate(['/entrada']);
    this.router.navigate(["/lislabor"]);
    this.cargando();
  }
  ingreso_a_principal3() {
    //this.router.navigate(['/entrada']);
    this.router.navigate(["/aplicacion-quimica"]);
    this.cargando();
  }
  botones_estado_latlong(x) {
    //CREAR LATLONG LOCAL
    if (!sessionStorage.usuarioRut) {
      console.log("No ingresó rut...");
      return false;
    }

    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        const gpsUsuarioLatLngLat = `${resp.coords.latitude}`;
        const gpsUsuarioLatLngLng = `${resp.coords.longitude}`;
        const gpsUsuarioLatLngFecha = this.funcionesService.fecha_hora_actual();
        const gpsUsuarioLatLngProcedencia = "lovi";
        const usuario_idUsuario = sessionStorage.idUsuario;
        const estadoLatLong = "1";

        //this.latLong.guardarRegistroLatLong(sessionStorage.usuarioRut, lat, long, fecha, estado, accion, idUsuario);
        this.latLong.guardarRegistroLatLong(
          gpsUsuarioLatLngLat,
          gpsUsuarioLatLngLng,
          gpsUsuarioLatLngFecha,
          gpsUsuarioLatLngProcedencia,
          usuario_idUsuario,
          estadoLatLong
        );
      })
      .catch((error) => {
        console.log("Error getting location", error);
      });
  }
  obtener_laboresDBS() {
    //return  false;
    const arregloLocal = [];
    //alert("obtener_laboresDB");
    //DB LLAMADA a tabla LABORES x idUsuario y agregar datos en BD LOCAL 100%
    this.dblaboresService.getLaboresXidCliente(this.idUsuario).subscribe(
      (response) => {
        for (let i = 0; i < response.registros_labores.length; i++) {
          const element = response.registros_labores[i];
          // console.log(element.idLabor);

          arregloLocal.push({
            idLabor: element.idLabor,
            laborNombre: element.laborNombre,
            laborDescripcion: element.laborDescripcion,
            laborValorTratoInicial: element.laborValorTratoInicial,
            laborValorTratoFinal: element.laborValorTratoFinal,
            laborObjetivo: element.laborObjetivo,
            laborLat: element.laborLat,
            laborLng: element.laborLng,
            laborFecha: element.laborFecha,
            laborIconoAsignacion: element.laborIconoAsignacion,
            laborIcono: element.laborIcono,
            laborCuartel: element.laborCuartel,
            laborDescripcionCierre: element.laborDescripcionCierre,
            estado_idEstado: element.estado_idEstado,
            tipoLabor_idTipoLabor: element.tipoLabor_idTipoLabor,
            laborAplicacionQ_idLaborAplicacionQ:
              element.laborAplicacionQ_idLaborAplicacionQ,
            contratista_idContratista: element.contratista_idContratista,
            usuario_idUsuario: element.usuario_idUsuario,
          });
        }
        //ORDENA LA LISTA EN FORMA DESCENDENTE
        arregloLocal.sort().reverse();
        localStorage.arregloLocal = JSON.stringify(arregloLocal);
      },
      (error) => {
        console.log(error);
      }
    );
    //location.reload();
    //alert("Sincronizando con BD, esto tardará unos segundos");
    //this.router.navigate(['/entrada']);
  }
  obtener_contratistaDB() {
    const idFundo = sessionStorage.idFundo;

    //return  false;
    const contratistaLocal = [];
    //alert("obtener_laboresDB");
    //DB LLAMADA a tabla LABORES x idUsuario y agregar datos en BD LOCAL 100%
    this.dbcontratistaService.getContratista_x_fundo(idFundo).subscribe(
      (response) => {
        console.log(response);

        //return  false;

        for (let i = 0; i < response.contratistas.length; i++) {
          const element = response.contratistas[i];
          console.log(element.idContratista);

          contratistaLocal.push({
            idContratista: element.idContratista,
            contratistaRut: element.contratistaRut,
            contratistaNombre: element.contratistaNombre,
            contratistaGiro: element.contratistaGiro,
            contratistaFono: element.contratistaFono,
            contratistaMail: element.contratistaMail,
            contratistaDireccion: element.contratistaDireccion,
            contratistaRazonSocial: element.contratistaRazonSocial,
            estado_idEstado: element.estado_idEstado,
            fundo_idFundo: element.fundo_idFundo,
          });
        }
        //ORDENA LA LISTA EN FORMA DESCENDENTE
        contratistaLocal.sort().reverse();
        localStorage.contratistaLocal = JSON.stringify(contratistaLocal);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  async cargando() {
    const loading = await this.loadingController.create({
      message: "Procesando...",
      duration: 3000,
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log("Loading dismissed!");
  }

  // otra paGINA
  doRefresh(event) {
    //console.log(event);
    this.actualizar = 1;
    this.obtener_laboresDB();

    setTimeout(() => {
      this.arregloLocalVista = JSON.parse(localStorage.getItem("arregloLocal"));
      event.target.complete();
      this.actualizar = 0;
    }, 1500);
  }

  obtener_laboresDB() {
    this.cargando();
    var idUsuario = sessionStorage.idUsuario;
    //console.log(idUsuario);
    const arregloLocal = [];

    //DB LLAMADA a tabla LABORES x idUsuario y agregar datos en BD LOCAL 100%
    this.dblaboresService.getLaboresXidCliente(idUsuario).subscribe(
      (response) => {
        for (let i = 0; i < response.registros_labores.length; i++) {
          const element = response.registros_labores[i];
          console.log(response.registros_labores.length);

          if (element.estado_idEstado == "1") {
            arregloLocal.push({
              idLabor: element.idLabor,
              laborNombre: element.laborNombre,
              laborDescripcion: element.laborDescripcion,
              laborValorTratoInicial: element.laborValorTratoInicial,
              laborValorTratoFinal: element.laborValorTratoFinal,
              laborObjetivo: element.laborObjetivo,
              laborLat: element.laborLat,
              laborLng: element.laborLng,
              laborFecha: element.laborFecha,
              laborIconoAsignacion: element.laborIconoAsignacion,
              laborIcono: element.laborIcono,
              laborCuartel: element.laborCuartel,
              laborDescripcionCierre: element.laborDescripcionCierre,
              estado_idEstado: element.estado_idEstado,
              tipoLabor_idTipoLabor: element.tipoLabor_idTipoLabor,
              laborAplicacionQ_idLaborAplicacionQ:
                element.laborAplicacionQ_idLaborAplicacionQ,
              contratista_idContratista: element.contratista_idContratista,
              usuario_idUsuario: element.usuario_idUsuario,
            });
          }
        }
        //ORDENA LA LISTA EN FORMA DESCENDENTE
        arregloLocal.sort().reverse();
        localStorage.arregloLocal = JSON.stringify(arregloLocal);
        this.arregloLocalVista2 = arregloLocal;
        // this.actualizar = 1;
        //this.arregloLocalVista = JSON.parse( localStorage.getItem('arregloLocal') );
      },
      (error) => {
        console.log(error);
      }
    );
  }

  acordeon_labores(bb, xx) {
    console.log(this.itemLaboresAbiertas + " - " + xx);

    var x = bb + xx;

    //document.getElementById(x).innerText = "hola";
    var estado = document.getElementById(x).style.display;

    if (estado == "none") {
      document.getElementById(x).style.display = "block";
    } else {
      document.getElementById(x).style.display = "none";
      // this.itemLaboresAbiertas = false;
    }
  }

  prueba(idLabor, idFecha) {
    //alert(idLabor+" - "+idFecha);
    this.arregloQFusion2 = [];
    //this.mostrarBoton=false;

    //LEER LATLONG LOCAL ENVIAR A BD Y BORRAR LOCAL
    let key = "productosQFusionLocal";
    //leer data local
    this.storage
      .get(key)
      .then((val) => {
        console.log("get " + key + " ", val);

        if (val == null) {
          console.log("productosQFusionLocal sin datos");
          return false;
        }

        console.log(
          "productosQFusionLocal - Largo " +
            val.length +
            " " +
            val[0].estado +
            " " +
            val[0].idFecha +
            " " +
            val[0].idLabor +
            " " +
            val[0].rut
        );
        console.log(
          "productosQFusionLocal - Largo " +
            val.length +
            " " +
            "1" +
            " " +
            idFecha +
            " " +
            idLabor
        );
        //recorrer data local
        for (var i = 0; i < val.length; i++) {
          if (
            val[i].estado == "1" &&
            val[i].idFecha == idFecha &&
            val[i].idLabor == idLabor
          ) {
            console.log("si:" + i);
            const element = val[i];

            this.arregloQFusion2.push({
              rut: element.rut,
              idLabor: element.idLabor,
              nombre: element.nombre,
              apellido: element.apellido,
              formatoEntrada: element.formatoEntrada,
              celular: element.celular,
              idFecha: element.idFecha,
              productosQCantidad: element.productosQCantidad,
              productosQNombre: element.productosQNombre,
              unidadmedida_idUnidadMedida: element.unidadmedida_idUnidadMedida,
              estado: element.estado,
            });
          }
        }

        console.log(this.arregloQFusion2);
        return this.arregloQFusion2;
      })
      .catch((error) => {
        console.log("get error for " + key + "", error);
        return false;
      });
  }

  prueba_2(idLabor, idFecha, rut, i) {
    // alert(idLabor+" - "+idFecha+" - "+rut+" - "+i);
    this.arregloQFusion2 = [];
    //this.mostrarBoton=false;

    (<HTMLInputElement>(
      document.getElementById("boton_" + rut + i)
    )).disabled = true;

    //LEER LATLONG LOCAL ENVIAR A BD Y BORRAR LOCAL
    let key = "productosQFusionLocal";
    //leer data local
    this.storage
      .get(key)
      .then((val) => {
        // console.log('get ' + key + ' ', val);

        if (val == null) {
          console.log("productosQFusionLocal sin datos");
          return false;
        }

        //recorrer data local
        for (var i = 0; i < val.length; i++) {
          if (
            val[i].estado == "1" &&
            val[i].idFecha == idFecha &&
            val[i].idLabor == idLabor &&
            val[i].rut == rut
          ) {
            //console.log("si:"+i);
            const element = val[i];

            this.arregloQFusion2.push({
              rut: element.rut,
              idLabor: element.idLabor,
              nombre: element.nombre,
              apellido: element.apellido,
              formatoEntrada: element.formatoEntrada,
              celular: element.celular,
              idFecha: element.idFecha,
              productosQCantidad: element.productosQCantidad,
              productosQNombre: element.productosQNombre,
              unidadmedida_idUnidadMedida: element.unidadmedida_idUnidadMedida,
              estado: element.estado,
            });
          }
        }

        return this.arregloQFusion2;
      })
      .catch((error) => {
        console.log("get error for " + key + "", error);
        return false;
      });
  }

  obtener_lista_productosQFusionold2(idLabor, idFecha) {
    console.log(idLabor + " " + idFecha);
    let formularioFusion = "";
    const arregloQFusion = [];

    const arregloQFusionRut = [];
    //this.productosQFusion.cargarProductosQFusion();

    //LEER LATLONG LOCAL ENVIAR A BD Y BORRAR LOCAL
    let key = "productosQFusionLocal";
    //leer data local
    this.storage
      .get(key)
      .then((val) => {
        // console.log('get ' + key + ' ', val);

        if (val == null) {
          console.log("productosQFusionLocal sin datos");
          return false;
        }

        let data_cruda2 = JSON.parse(localStorage.getItem("listaRut"));

        console.log(data_cruda2);

        //FOR 1 recorrer data local
        for (var i = 0; i < data_cruda2.length; i++) {
          console.log(data_cruda2[i].idLabor) + " " + data_cruda2.length;

          if (
            data_cruda2[i].idFecha == idFecha &&
            data_cruda2[i].idLabor == idLabor
          ) {
            console.log(data_cruda2[i].rut);
            var rutx = data_cruda2[i].rut;

            const dom = <HTMLElement>document.getElementById("form_fusion");

            dom.innerHTML +=
              '<ion-list style="margin-bottom:10px;background-color: #f2f2f2;">' +
              "<ion-grid>" +
              "<ion-row>" +
              '<ion-col size="6">' +
              '<input id="" type="text" style="width:80%;font-weight:bold;font-size:20px;border-width: 0px;text-align: center;" value="' +
              rutx +
              '" disabled >' +
              "</ion-col>" +
              '<ion-col size="6">' +
              '<p style="font-size:18px;">Bombada: </p>' +
              '<input id="" style="width:80%;font-weight:bold;font-size:20px;text-align: center;" type="number" >' +
              "</ion-col>" +
              "</ion-row>" +
              "</ion-grid>";

            //FOR 2
            for (let i = 0; i < val.length; i++) {
              if (
                val[i].estado == "1" &&
                val[i].idFecha == idFecha &&
                val[i].idLabor == idLabor &&
                val[i].rut == rutx
              ) {
                /* */
                const element = val[i];

                dom.innerHTML +=
                  "<ion-grid>" +
                  "<ion-row>" +
                  '<ion-col size="8" align-self-baseline>' +
                  "<ion-item>" +
                  '<ion-label position="floating" >Nombre Producto</ion-label>' +
                  '<ion-input id="' +
                  idLabor +
                  "_nombre_" +
                  rutx +
                  '" type="text" ></ion-input>' +
                  "</ion-item>  " +
                  "</ion-col>" +
                  '<ion-col size="4" align-self-baseline>' +
                  "<ion-item>" +
                  '<ion-label position="floating" >Cantidad Producto</ion-label>' +
                  '<ion-input id="' +
                  idLabor +
                  "_cantidad_" +
                  rutx +
                  '" type="text" ></ion-input>' +
                  "</ion-item>" +
                  "</ion-col>" +
                  '<ion-col size="6" align-self-baseline>' +
                  "<ion-item>" +
                  '<ion-label position="floating" >Unidad</ion-label>' +
                  '<ion-input id="' +
                  idLabor +
                  "_unidad_" +
                  rutx +
                  '" type="text" ></ion-input>' +
                  "</ion-item>" +
                  "</ion-col>" +
                  '<ion-col size="6" align="right">' +
                  '<ion-button  (click)="guardar_avance_quimica(idLabor, rutx);">Agregar</ion-button>' +
                  '<button type="button" onclick="guardar_avance_quimica();">Enviar</button>' +
                  "</ion-col>" +
                  "</ion-row>" +
                  "</ion-grid>";
              } //fin if 2
            } //fin for 2

            dom.innerHTML += "</ion-list>";

            //console.log(dom);

            //ORDENA LA LISTA EN FORMA DESCENDENTE

            /*
        arregloQFusion.sort().reverse();  
        this.productosQFusionLocal = arregloQFusion;
        return this.productosQFusionLocal;
        */
          } //fin if 1
        } //fin for 1
      })
      .catch((error) => {
        console.log("get error for " + key + "", error);
        return false;
      });
  }

  eliminarObjetosDuplicados(arreglo, propiedad) {
    var nuevoArray = [];
    var lookup = {};

    for (var i in arreglo) {
      lookup[arreglo[i][propiedad]] = arreglo[i];
    }

    for (i in lookup) {
      nuevoArray.push(lookup[i]);
    }

    return nuevoArray;
  }

  obtener_productosQFusionLocal() {
    const arregloQ = [];

    //LEER LATLONG LOCAL ENVIAR A BD Y BORRAR LOCAL
    let key = "productosQFusionLocal";
    //leer data local
    this.storage
      .get(key)
      .then((val) => {
        console.log("get " + key + " ", val);

        if (val == null) {
          console.log("registros productosQFusionLocal sin datos");
          return false;
        } else {
          console.log("registros productosQFusionLocal: " + val.length);

          for (let i = 0; i < val.length; i++) {
            const element = val[i];
            arregloQ.push({
              rut: element.rut,
              idLabor: element.idLabor,
              nombre: element.nombre,
              apellido: element.apellido,
              formatoEntrada: element.formatoEntrada,
              celular: element.celular,
              idFecha: element.idFecha,
              productosQCantidad: element.productosQCantidad,
              productosQNombre: element.productosQNombre,
              unidadmedida_idUnidadMedida: element.unidadmedida_idUnidadMedida,
              estado: element.estado,
            });
          }
          //ORDENA LA LISTA EN FORMA DESCENDENTE
          arregloQ.sort().reverse();
          this.productosQFusionLocal = arregloQ;

          return this.productosQFusionLocal;
        }

        /*
            //recorrer data local
            for (var i = 0; i < val.length; i++) {
              if (val[i].estadoLatLong == '1') {

                const gpsUsuarioLatLngLat = val[i].gpsUsuarioLatLngLat;
                const gpsUsuarioLatLngLng = val[i].gpsUsuarioLatLngLng;
                const gpsUsuarioLatLngFecha = val[i].gpsUsuarioLatLngFecha;
                const gpsUsuarioLatLngProcedencia = val[i].gpsUsuarioLatLngProcedencia;
                const usuario_idUsuario = val[i].usuario_idUsuario;
    
              }
            }
arregloLocalVista.length
            */
      })
      .catch((error) => {
        console.log("get error for " + key + "", error);
        return false;
      });
  }

  obtener_lista_productosQ() {
    const arregloQ = [];

    //LEER LATLONG LOCAL ENVIAR A BD Y BORRAR LOCAL
    let key = "productosQLocal";
    //leer data local
    this.storage
      .get(key)
      .then((val) => {
        console.log("get " + key + " ", val);

        if (val == null) {
          console.log("registros productosQLocal sin datos");
          return false;
        } else {
          console.log("registros productosQLocal: " + val.length);

          for (let i = 0; i < val.length; i++) {
            const element = val[i];
            arregloQ.push({
              productosQCantidad: element.productosQCantidad,
              productosQNombre: element.productosQNombre,
              labor_idLabor: element.labor_idLabor,
              unidadmedida_idUnidadMedida: element.unidadmedida_idUnidadMedida,
              idFecha: element.idFecha,
              estado: element.estado,
            });
          }
          //ORDENA LA LISTA EN FORMA DESCENDENTE
          arregloQ.sort().reverse();
          this.productosQLocal = arregloQ;

          return this.productosQLocal;
        }

      
      })
      .catch((error) => {
        console.log("get error for " + key + "", error);
        return false;
      });
  }

  abrirRegistroGeneral(registro) {
    this.total_bombadas(registro.idLabor);

    this.botonAgregarQuimica = false;

    this.listaRut = JSON.parse(localStorage.getItem("listaRut"));
    console.log(registro);
    console.log(this.listaRut);

    //this.obtener_lista_productosQFusion(registro.laborFecha);
    this.listaRut = JSON.parse(localStorage.getItem("listaRut"));
    //this.data_cruda_productosQ = JSON.parse( localStorage.getItem('data_cruda_productosQ') );
    //this.productosQLocal = this.obtener_lista_productosQ();
    //console.log(registro);
    //return false;
    this.listadoRegistros = false;
    this.botonesEntrada = false;
    this.estadoGrupo1 = false;
    this.estadoGrupo2 = false;

    if (registro.tipoLabor_idTipoLabor == "1") {
      this.segmento = "autoasignado";

      this.nombre = registro.laborNombre;
      this.laborObjetivo = registro.laborObjetivo;
      this.descripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      this.idUsuario = registro.usuario_idUsuario;
      this.usuarioRut = sessionStorage.usuarioRut;
      this.idLabor = registro.idLabor;
      this.laborFecha = registro.laborFecha;

      this.tituloListadoLabores = "Avance Autoasignado";

      //alert(tipoLabor_idTipoLabor+" "+this.nombre);

      //return "leaf-outline";
    }

    if (registro.tipoLabor_idTipoLabor == "2") {
      this.segmento = "interna";

      this.nombre = registro.laborNombre;
      this.laborObjetivo = registro.laborObjetivo;
      this.descripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      this.idUsuario = registro.usuario_idUsuario;
      this.usuarioRut = sessionStorage.usuarioRut;
      this.idLabor = registro.idLabor;
      this.laborFecha = registro.laborFecha;

      this.tituloListadoLabores = "Avance MO Interna";

      //validar si es Rut o Grupo
      var i = 0;
      for (const key in this.listaRut) {
        if (this.listaRut.hasOwnProperty(key)) {
          const element = this.listaRut[key];
          if (element.idLabor == this.idLabor) {
            i++;
          }
        }
      }

      if (i == 0) {
        this.estadoGrupo1 = false;
        this.estadoGrupo2 = true;
      } else {
        this.estadoGrupo1 = true;
        this.estadoGrupo2 = false;
      }

      // alert("idLabor: "+" "+this.idLabor);
    }

    if (registro.tipoLabor_idTipoLabor == "3") {
      this.segmento = "contratista";

      this.nombre = registro.laborNombre;
      this.laborObjetivo = registro.laborObjetivo;
      this.descripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      this.idUsuario = registro.usuario_idUsuario;
      this.usuarioRut = sessionStorage.usuarioRut;
      this.idLabor = registro.idLabor;
      this.laborFecha = registro.laborFecha;
      this.tituloListadoLabores = "Avance Contratista";

      //validar si es Rut o Grupo
      var i = 0;
      for (const key in this.listaRut) {
        if (this.listaRut.hasOwnProperty(key)) {
          const element = this.listaRut[key];
          if (element.idLabor == this.idLabor) {
            i++;
          }
        }
      }

      if (i == 0) {
        this.estadoGrupo1 = false;
        this.estadoGrupo2 = true;
      } else {
        this.estadoGrupo1 = true;
        this.estadoGrupo2 = false;
      }
    }

    if (registro.tipoLabor_idTipoLabor == "4") {
      this.segmento = "auto_interna_quimica_contratista";

      this.nombre = registro.laborNombre;
      this.laborObjetivo = registro.laborObjetivo;
      this.descripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      this.idUsuario = registro.usuario_idUsuario;
      this.usuarioRut = sessionStorage.usuarioRut;
      this.idLabor = registro.idLabor;
      this.laborFecha = registro.laborFecha;
      this.tituloListadoLabores = "Avance Autoasignado Química";

      this.rutaLogo = "assets/person-outline.svg";

      //leer de nuevo lista rut
      //this.prueba(idLabor, idFecha, rut);
      this.prueba(this.idLabor, this.laborFecha);
    }

    if (registro.tipoLabor_idTipoLabor == "5") {
      this.segmento = "auto_interna_quimica_contratista";

      this.nombre = registro.laborNombre;
      this.laborObjetivo = registro.laborObjetivo;
      this.descripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      this.idUsuario = registro.usuario_idUsuario;
      this.usuarioRut = sessionStorage.usuarioRut;
      this.idLabor = registro.idLabor;
      this.laborFecha = registro.laborFecha;
      this.rutaLogo = "assets/people-outline.svg";
      this.tituloListadoLabores = "Avance MO Interna Química";

      this.prueba(this.idLabor, this.laborFecha);
    }

    if (registro.tipoLabor_idTipoLabor == "6") {
      this.segmento = "auto_interna_quimica_contratista";

      this.nombre = registro.laborNombre;
      this.laborObjetivo = registro.laborObjetivo;
      this.descripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      this.idUsuario = registro.usuario_idUsuario;
      this.usuarioRut = sessionStorage.usuarioRut;
      this.idLabor = registro.idLabor;
      this.laborFecha = registro.laborFecha;
      this.rutaLogo = "assets/construct-outline.svg";

      this.tituloListadoLabores = "Avance Contratista Química";

      //this.prueba(registro.idLabor, registro.laborFecha, );

      this.prueba(this.idLabor, this.laborFecha);
    }
  }

  guardar_avance_general_autoasignada(
    idLabor,
    usuarioRut,
    cantidadCantidad,
    formato
  ) {
    this.cargando();

    //var formato ="autoasignado";

    this.funcionesService.obtenerGeolocalizacion();

    //alert UTIL
    //  alert("Usted agregó la cantidad de: "+cantidadCantidad+" Id Labor: "+ idLabor+" Usuario Rut: "+usuarioRut);

    //return  false;

    var fecha = this.funcionesService.fecha_hora_actual();

    this.avancelaborglocal.guardarAvanceLaborG(
      (this.avanceLaborGCantidad = cantidadCantidad),
      (this.avanceLaborGRutTrabajador = usuarioRut),
      (this.avanceLaborGObjetivoLabor = null),
      (this.avanceLaborGLat = sessionStorage.lat),
      (this.avanceLaborGLng = sessionStorage.lng),
      (this.avanceLaborGFecha = fecha),
      (this.avanceLaborGCantGrupo = null),
      (this.avanceLaborGFormato = formato),
      (this.labor_idLabor = idLabor),
      (this.envio = "1")
    );
    this.cantidadCantidad = "";

    this.volver();
    // this.limpiar();
    // this.router.navigate(['/listado-labores']);

    // this.segmento="inicial";
    // this.listadoRegistros= true;
  }

  guardar_avance_general(
    idUsuario,
    idLabor,
    usuarioRut,
    laborObjetivo,
    formato,
    x,
    i
  ) {
    var cc = (<HTMLInputElement>document.getElementById(i)).value;

    if (!cc) {
      alert("Ingrese un valor...");
      return false;
    }

    this.estadoBotonInterna = true;
    this.estadoBotonContratista = true;

    //alert("Usted agregó la cantidad de: "+cc+" al rut: "+ x.text);

    (<HTMLInputElement>document.getElementById(i)).disabled = true;

    (<HTMLInputElement>document.getElementById(usuarioRut + "" + i)).className =
      "borrar";

    formato = "rut";
    this.funcionesService.obtenerGeolocalizacion();
    //alert UTIL
    //alert("Usted agregó la cantidad de: "+cc+" al id Usuario: "+ idUsuario+" Id Labor: "+ idLabor+" Usuario Rut: "+usuarioRut);
    //return  false;

    var fecha = this.funcionesService.fecha_hora_actual();

    this.avancelaborglocal.guardarAvanceLaborG(
      (this.avanceLaborGCantidad = cc),
      (this.avanceLaborGRutTrabajador = usuarioRut),
      (this.avanceLaborGObjetivoLabor = laborObjetivo),
      (this.avanceLaborGLat = sessionStorage.lat),
      (this.avanceLaborGLng = sessionStorage.lng),
      (this.avanceLaborGFecha = fecha),
      (this.avanceLaborGCantGrupo = "0"),
      (this.avanceLaborGFormato = formato),
      (this.labor_idLabor = idLabor),
      (this.envio = "1")
    );
    // this.limpiar();
    // this.router.navigate(['/listado-labores']);
    // this.segmento="inicial";
    // this.listadoRegistros= true;
  }

  guardar_avance_quimica(idLabor, rut, i) {
    this.botonAgregarQuimica = true;
    // (<HTMLInputElement>document.getElementById("boton_"+rut+i)).style.display="none";

    var x1 = "";
    var x2 = "";
    var x3 = "";
    var x4 = "";

    //alert(idLabor+" - "+rut);
    // console.log(idLabor+" - "+rut);
    var bombada = (<HTMLInputElement>document.getElementById("bombada_" + rut))
      .value;

    var nombre = (<HTMLInputElement>(
      document.getElementById("nombre_" + rut + "" + i)
    )).value;
    var cantidad = (<HTMLInputElement>(
      document.getElementById("cantidad_" + rut + "" + i)
    )).value;
    var unidad = (<HTMLInputElement>(
      document.getElementById("unidad_" + rut + "" + i)
    )).value;

    //console.log(nombre+" - "+cantidad+" - "+unidad+" - "+i);
    //console.log(nombre+" - ");

    if (nombre == "" || cantidad == "" || unidad == "" || bombada == "") {
      if (nombre == "") {
        x1 = " Nombre";
      }
      if (cantidad == "") {
        x2 = " Cantidad";
      }
      if (unidad == "") {
        x3 = " Unidad";
      }
      if (bombada == "") {
        x4 = " Bombada";
      }
      alert("Faltan datos en:" + x1 + x2 + x3 + x4);
      return false;
    } else {
      document.getElementById(rut + "" + i).style.display = "none";
      (<HTMLInputElement>(
        document.getElementById("bombada_" + rut)
      )).disabled = true;

      console.log(nombre + " - " + cantidad + " - " + unidad + " - " + bombada);

      //guardar avance quimica
      this.geolocation
        .getCurrentPosition()
        .then((resp) => {
          const avanceLaborQCantidad = cantidad;
          const avanceLaborQProducto = nombre;
          const avanceLaborQBombada = bombada;
          const avanceLaborQLat = `${resp.coords.latitude}`;
          const avanceLaborQLng = `${resp.coords.longitude}`;
          const avanceLaborQFecha = this.funcionesService.fecha_hora_actual();
          const avanceLaborQRutTrabajador = rut;
          const avanceLaborQUnidadMedida = unidad;
          const labor_idLabor = idLabor;
          const envio = "1";

          //this.latLong.guardarRegistroLatLong(sessionStorage.usuarioRut, lat, long, fecha, estado, accion, idUsuario);
          this.avancelaborq.guardarAvanceLaborQ(
            avanceLaborQCantidad,
            avanceLaborQProducto,
            avanceLaborQBombada,
            avanceLaborQLat,
            avanceLaborQLng,
            avanceLaborQFecha,
            avanceLaborQRutTrabajador,
            avanceLaborQUnidadMedida,
            labor_idLabor,
            envio
          );
        })
        .catch((error) => {
          console.log("Error en guardarAvanceLaborQ ", error);
        });
      //fin avance quimica
    }
  }

  cierre_avance_quimica(x, idLabor) {
    var x1 = "";
    var x2 = "";
    var x3 = "";

    var nombre = (<HTMLInputElement>(
      document.getElementById("nombre_cierre_" + x)
    )).value;
    var cantidad = (<HTMLInputElement>(
      document.getElementById("cantidad_cierre_" + x)
    )).value;
    var observacion = (<HTMLInputElement>(
      document.getElementById("observacion_cierre_" + x)
    )).value;
    var estado = "1";

    console.log(nombre + " - " + cantidad + " - " + observacion);

    if (nombre == "" || cantidad == "" || observacion == "") {
      if (nombre == "") {
        x1 = " Nombre";
      }
      if (cantidad == "") {
        x2 = " Cantidad";
      }
      if (observacion == "") {
        x3 = " Observación";
      }
      alert("Faltan datos en:" + x1 + x2 + x3);
      return false;
    } else {
      /* */

      //this.latLong.guardarRegistroLatLong(sessionStorage.usuarioRut, lat, long, fecha, estado, accion, idUsuario);
      this.sobrantesq.guardarRegistroSobrantesQ(
        cantidad,
        nombre,
        observacion,
        idLabor,
        estado
      );

      /* */
      this.ocultar_labor_quimica(x);
      this.btn_cerrarLaborQuimica = true;
    }
  }

  cierre_avance_quimica_descripcion(idLabor, cierreDescripcion) {
    /**/

    console.log(idLabor);

    // 1) Para "labor" de acuerdo a "idLabor", hacer update a tabla "labor" cambiar "estado" "abierto" a "cerrado".
    let info: any = new FormData();
    //info.append('idregistros_labores', idregistros_labores);
    info.append("idLabor", idLabor);
    info.append("laborDescripcionCierre", cierreDescripcion);
    info.append("estado_idEstado", 2);
    //
    var object = {};
    info.forEach((value, key) => {
      object[key] = value;
    });
    var data_cruda2 = object;
    console.log(data_cruda2);

    //Se guardan los datos. En una nueva labor en BD
    this.dblaboresService.UpdatePostLabores(data_cruda2).subscribe(
      (response) => {
        console.log("UpdatePostLabores: " + response);
        this.limpiar_general();
        //alert UTIL
        //alert("Labor Actualizada Exitosamente.");

        this.nombre = "";
        this.laborObjetivo = "";
        this.descripcion = "";
        this.tipoLabor_idTipoLabor = "";
        this.idUsuario = "";
        this.usuarioRut = "";
        this.idLabor = "";
        this.laborFecha = "";
        this.ver_cierre("0");

        this.actualizar_listado_arregloLocal();
        this.cierreDescripcion = "";
      },
      (error) => {
        console.log("CrearPostLabores error " + error);
        this.limpiar_general();
        alert(
          "No se pudo crear la Labor. Hubo un error en el proceso de creación." +
            error
        );
        this.volver();
      }
    );

    /**/
  }

  actualizar_listado_arregloLocal() {
    //actualiza la tabla genial
    this.actualizar = 1;
    this.obtener_laboresDB();

    setTimeout(() => {
      this.arregloLocalVista = JSON.parse(localStorage.getItem("arregloLocal"));
      //event.target.complete();
      this.actualizar = 0;
      this.volver();
    }, 1500);
    //this.doRefresh(event.isTrusted);
    //actualiza la tabla genial  fin
  }

  guardar_avance_quimicaold(idLabor, rut) {
    console.log(idLabor + " - " + rut);

    return false;
     var usuarioRut = document.getElementById("rut_" + rut).innerText;

    console.log(usuarioRut);

    var nombre = (<HTMLInputElement>(
      document.getElementById(idLabor + "_nombre_" + rut)
    )).value;
    var cantidad = (<HTMLInputElement>(
      document.getElementById(idLabor + "_cantidad_" + rut)
    )).value;
    var unidad = (<HTMLInputElement>(
      document.getElementById(idLabor + "_unidad_" + rut)
    )).value;
    //var nombre = document.getElementById("nombre_"+ii).innerText;
    //var cantidad = document.getElementById("cantidad_"+ii).innerText;
    //var unidad = document.getElementById("unidad_"+ii).innerText;
    var bombada = (<HTMLInputElement>(
      document.getElementById(idLabor + "_bombada_" + rut)
    )).value;
    var rut2 = (<HTMLInputElement>document.getElementById("rut_" + rut)).value;

    console.log(
      idLabor +
        " - " +
        nombre +
        " - " +
        cantidad +
        " - " +
        unidad +
        " - " +
        bombada +
        " - " +
        rut
    )

  }

  limpiar() {
    (this.cantidadCantidad = ""),
      (this.avanceLaborGCantidad = ""),
      (this.avanceLaborGRutTrabajador = ""),
      (this.avanceLaborGObjetivoLabor = ""),
      (this.avanceLaborGLat = ""),
      (this.avanceLaborGLng = ""),
      (this.avanceLaborGFecha = ""),
      (this.avanceLaborGCantGrupo = ""),
      (this.avanceLaborGFormato = ""),
      (this.labor_idLabor = ""),
      (this.envio = "");
  }

  cerrarRegistro(registro) {
    console.log(registro);
    //return false;

    this.listaRut = JSON.parse(localStorage.getItem("listaRut"));
    /*
                  this.dataLocalLabor.borrarRegistroLabores(
                    this.numeroOtCantidad = registro.numeroOt
                  );
              */

    //console.log(this.listaRut);
    //console.log(registro);
    //console.log(registro.tipoLabor_idTipoLabor);

    this.listadoRegistros = false;

    if (registro.tipoLabor_idTipoLabor == "1") {
      this.segmento = "cerrarAutoasignado";
      this.idLabor = registro.idLabor;
      this.laborNombre = registro.laborNombre;
      //this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      this.laborDescripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      this.tituloListadoLabores = "Cerrar Autoasignado";

      //alert(tipoLabor_idTipoLabor+" "+this.nombre);

      //return "leaf-outline";
    }

    if (registro.tipoLabor_idTipoLabor == "2") {
      this.segmento = "cerrarInterna";
      this.idLabor = registro.idLabor;
      this.laborNombre = registro.laborNombre;
      //this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      this.laborDescripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      this.tituloListadoLabores = "Cerrar MO Interna";
    }

    if (registro.tipoLabor_idTipoLabor == "3") {
      this.segmento = "cerrarContratista";
      this.idLabor = registro.idLabor;
      this.laborNombre = registro.laborNombre;

      this.laborDescripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      //this.numeroOt = registro.numeroOt;

      this.nombreContratista = registro.contratista_idContratista;
      this.tipoTratoValor = registro.tipoTratoValor;
      this.tituloListadoLabores = "Cerrar Contratista";
      //this.tipoTratoUnidadMedida = registro.tipoTratoUnidadMedida;
      //alert(registro.tipoLabor_idTipoLabor);
      // return "leaf-outline";
    }

    if (registro.tipoLabor_idTipoLabor == "4") {
      this.segmento = "cerrarQuimica_auto_interna_contratista";
      this.idLabor = registro.idLabor;
      this.laborNombre = registro.laborNombre;
      this.laborDescripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      this.nombreContratista = registro.contratista_idContratista;
      this.laborValorTratoInicial = registro.laborValorTratoInicial;
      this.rutaLogo = "name=alarm-outline";
      this.tituloListadoLabores = "Cerrar Autoasignado Química";
    }

    if (registro.tipoLabor_idTipoLabor == "5") {
      this.segmento = "cerrarQuimica_auto_interna_contratista";
      this.idLabor = registro.idLabor;
      this.laborNombre = registro.laborNombre;
      this.laborDescripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      this.nombreContratista = registro.contratista_idContratista;
      this.laborValorTratoInicial = registro.laborValorTratoInicial;
      this.rutaLogo = "assets/people-outline.svg";
      this.tituloListadoLabores = "Cerrar MO Interna Química";
    }

    if (registro.tipoLabor_idTipoLabor == "6") {
      this.segmento = "cerrarQuimica_auto_interna_contratista";
      this.idLabor = registro.idLabor;
      this.laborNombre = registro.laborNombre;
      this.laborDescripcion = registro.laborDescripcion;
      this.tipoLabor_idTipoLabor = registro.tipoLabor_idTipoLabor;
      this.nombreContratista = registro.contratista_idContratista;
      this.laborValorTratoInicial = registro.laborValorTratoInicial;
      this.rutaLogo = "assets/construct-outline.svg";
      this.tituloListadoLabores = "Cerrar Contratista Química";
    }
  }

  volver() {
    this.cargando();
    //en caso de que este parado en segmento inicial vuelve a playpause, de lo contrario
    //se mantiene en la seccion listado-labores .
    if (this.segmento == "inicial") {
      this.router.navigate(["/lovi"]);
    } else {
      //this.numeroOtCantidad = "";
      this.cierreDescripcion = "";
      //this.cierreTratoFinal = "";
      this.router.navigate(["/listado-labores"]);
      this.segmento = "inicial";
      this.listadoRegistros = true;
      this.botonesEntrada = true;
      this.estadoBotonInterna = false;
      this.estadoBotonContratista = false;
      this.tituloListadoLabores = "Listado de Labores";
    }
  }

  async total_bombadas(idLabor) {
    var sum = 0;
    let data_cruda2 = JSON.parse(localStorage.getItem("listaBombada"));

    if (data_cruda2) {
      for (let i = 0; i < data_cruda2.length; i++) {
        console.log(data_cruda2[i].labor);

        if (idLabor == data_cruda2[i].labor) {
          sum++;
        }
      }
      this.totalBombadas = sum;
    }
  }

  volver_bombada(idLabor) {
    console.log(idLabor);

    this.cargando();
    //en caso de que este parado en segmento inicial vuelve a playpause, de lo contrario
    //se mantiene en la seccion listado-labores .
    if (this.segmento == "inicial") {
      this.router.navigate(["/playpause"]);
    } else {
      //this.numeroOtCantidad = "";
      this.cierreDescripcion = "";
      //this.cierreTratoFinal = "";
      this.router.navigate(["/listado-labores"]);
      this.segmento = "inicial";
      this.listadoRegistros = true;
      this.botonesEntrada = true;
      this.estadoBotonInterna = false;
      this.estadoBotonContratista = false;
      this.tituloListadoLabores = "Listado de Labores";

      let data_cruda2 = JSON.parse(localStorage.getItem("listaBombada"));

      this.listaBombada.push({
        labor: idLabor,
      });

      //console.log(JSON.stringify(this.listaBombada));
      if (!data_cruda2) {
        localStorage.listaBombada = JSON.stringify({ labor: idLabor });
      } else {
        let data_cruda3 = [{ labor: idLabor }].concat(data_cruda2);

        localStorage.listaBombada = JSON.stringify(data_cruda3);
      }
    }
  }

  cerrarAvanceGeneral(rut, idLabor, i) {
    console.log(rut + " - " + idLabor + " - " + this.i);
    var cc = (<HTMLInputElement>document.getElementById(i)).value;

    //alert("Usted agregó la cantidad de: "+cc+" al rut: "+ x.text);
    (<HTMLInputElement>document.getElementById(i)).disabled = true;

    (<HTMLInputElement>document.getElementById(rut + "" + i)).className =
      "borrar";

    var cc = (<HTMLInputElement>document.getElementById(i)).value;
    var avanceLaborGRutTrabajador = rut;
    console.log(cc);

    var formato = "rut";
    this.funcionesService.obtenerGeolocalizacion();

    // alert("Usted agregó la cantidad de: "+cc+" al id Usuario: "+ idUsuario+" Id Labor: "+ idLabor+" Usuario Rut: "+usuarioRut);
    //return  false;
    var fecha = this.funcionesService.fecha_hora_actual();

    console.log("envio: " + cc);

    this.avancelaborglocal.guardarAvanceLaborG(
      (this.avanceLaborGCantidad = cc),
      (this.avanceLaborGRutTrabajador = avanceLaborGRutTrabajador),
      (this.avanceLaborGObjetivoLabor = null),
      (this.avanceLaborGLat = sessionStorage.lat),
      (this.avanceLaborGLng = sessionStorage.lng),
      (this.avanceLaborGFecha = fecha),
      (this.avanceLaborGCantGrupo = null),
      (this.avanceLaborGFormato = formato),
      (this.labor_idLabor = idLabor),
      (this.envio = "1")
    );
  }

  cerrarRegistroLabor(idLabor, cierreDescripcion) {
    //console.log(listaRut[0].rut+" - "+idLabor+" - "+laborNombre+" - "+tipoLabor_idTipoLabor+" - "+laborDescripcion+" - "+listaRut.length);
    //var ii;
    //var total =0;
    console.log(idLabor);

    // 1) Para "labor" de acuerdo a "idLabor", hacer update a tabla "labor" cambiar "estado" "abierto" a "cerrado".
    let info: any = new FormData();
    //info.append('idregistros_labores', idregistros_labores);
    info.append("idLabor", idLabor);
    info.append("laborDescripcionCierre", cierreDescripcion);
    info.append("estado_idEstado", 2);
    //
    var object = {};
    info.forEach((value, key) => {
      object[key] = value;
    });
    var data_cruda2 = object;
    console.log(data_cruda2);

    //Se guardan los datos. En una nueva labor en BD
    this.dblaboresService.UpdatePostLabores(data_cruda2).subscribe(
      (response) => {
        console.log("UpdatePostLabores: " + response);

        this.limpiar_general();
        this.cierreDescripcion = "";
        //alert UTIL
        //alert("Labor Actualizada Exitosamente. Actualice el Listado de labores.");
        //this.volver();
        this.actualizar_listado_arregloLocal();
      },
      (error) => {
        console.log("CrearPostLabores error " + error);
        this.limpiar_general();
        alert(
          "No se pudo crear la Labor. Hubo un error en el proceso de creación." +
            error
        );
        this.volver();
      }
    );
  }

  cerrarRegistroLaborOld(
    registro,
    listaRut,
    idLabor,
    laborNombre,
    tipoLabor_idTipoLabor,
    laborDescripcion
  ) {
    console.log(
      listaRut[0].rut +
        " - " +
        idLabor +
        " - " +
        laborNombre +
        " - " +
        tipoLabor_idTipoLabor +
        " - " +
        laborDescripcion +
        " - " +
        listaRut.length
    );
    var ii;
    var total = 0;

    //este for obtiene el numero total de registros del listado
    for (let index = 0; index < listaRut.length; index++) {
      if (listaRut[index].idLabor == idLabor) {
        total++;
      }
    }

    //obtiene uno por uno las cantidades de cierre de cada registro.
    for (ii = 0; ii < total; ii++) {
      var cc = (<HTMLInputElement>document.getElementById(ii)).value;
      var avanceLaborGRutTrabajador = listaRut[ii].rut;
      console.log(cc);

      var formato = "rut";
      this.funcionesService.obtenerGeolocalizacion();

      // alert("Usted agregó la cantidad de: "+cc+" al id Usuario: "+ idUsuario+" Id Labor: "+ idLabor+" Usuario Rut: "+usuarioRut);
      //return  false;
      var fecha = this.funcionesService.fecha_hora_actual();

      console.log("envio: " + cc);

      this.avancelaborglocal.guardarAvanceLaborG(
        (this.avanceLaborGCantidad = cc),
        (this.avanceLaborGRutTrabajador = avanceLaborGRutTrabajador),
        (this.avanceLaborGObjetivoLabor = "0"),
        (this.avanceLaborGLat = sessionStorage.lat),
        (this.avanceLaborGLng = sessionStorage.lng),
        (this.avanceLaborGFecha = fecha),
        (this.avanceLaborGCantGrupo = "0"),
        (this.avanceLaborGFormato = formato),
        (this.labor_idLabor = idLabor),
        (this.envio = "1")
      );
    }

    return false;

    //////////leer desde localStorage listaRut
     let data_cruda = JSON.parse(localStorage.getItem("listaRut"));
      console.log(data_cruda);

    //concateno las 2 listas la temporal listaRut y la respaldada en LocalStorage
    let data_cruda3 = this.listaRut2.concat(data_cruda);
    //ORDENA LA LISTA EN FORMA DESCENDENTE
    data_cruda3.sort().reverse();

    //La lista concatenada data_cruda3 se ingresa al localStorage
    localStorage.listaRut = JSON.stringify(data_cruda3);

    return false;



    if (tipoLabor_idTipoLabor == "1") {
    }

    if (tipoLabor_idTipoLabor == "2") {
    }

    if (tipoLabor_idTipoLabor == "3") {
    }

    if (tipoLabor_idTipoLabor == "4") {
    }

    if (tipoLabor_idTipoLabor == "5") {
    }

    if (tipoLabor_idTipoLabor == "6") {
    }

    // 1) Para "labor" de acuerdo a "idLabor", hacer update a tabla "labor" cambiar "estado" "abierto" a "cerrado".
    let info: any = new FormData();
    //info.append('idregistros_labores', idregistros_labores);
    info.append("idLabor", idLabor);
    info.append("laborDescripcionCierre", "CERRADO");
    info.append("estado_idEstado", 2);
    //
    var object = {};
    info.forEach((value, key) => {
      object[key] = value;
    });
    var data_cruda2 = object;
    console.log(data_cruda2);

    //Se guardan los datos. En una nueva labor en BD
    this.dblaboresService.UpdatePostLabores(data_cruda2).subscribe(
      (response) => {
        console.log("UpdatePostLabores " + idLabor);
        this.limpiar_general();
        //alert UTIL
        //alert("Labor Actualizada Exitosamente. Actualice el Listado de labores.");
      },
      (error) => {
        console.log("CrearPostLabores error " + error);
        this.limpiar_general();
        alert(
          "No se pudo crear la Labor. Hubo un error en el proceso de creación."
        );
      }
    );
  }

  limpiar_general() {
    this.laborObjetivo = "";
    this.cierreDescripcion = "";
    this.cantidadCantidad = "";
    this.idLabor = "";
    this.laborNombre = "";
    this.tipoLabor_idTipoLabor = "";
    this.laborDescripcion = "";
  }

  ver_cierre(x) {
    // alert(x);
    if (x == "0") {
      this.cierre_1 = false;
      this.cierre_2 = false;
      this.cierre_3 = false;
      this.cierre_4 = false;
      this.cierre_5 = false;
      this.cierre_6 = false;
      this.cierre_7 = false;
      this.cierre_8 = false;
      this.cierre_9 = false;
      this.cierre_10 = false;

      this.btn_cierre_1 = true;
      this.btn_cierre_2 = false;
      this.btn_cierre_3 = false;
      this.btn_cierre_4 = false;
      this.btn_cierre_5 = false;
      this.btn_cierre_6 = false;
      this.btn_cierre_7 = false;
      this.btn_cierre_8 = false;
      this.btn_cierre_9 = false;
      this.btn_cierre_10 = false;
    }

    if (x == "1") {
      this.cierre_1 = true;
      this.btn_cierre_1 = false;
      this.btn_cierre_2 = true;
    }

    if (x == "2") {
      this.cierre_2 = true;
      this.btn_cierre_2 = false;
      this.btn_cierre_3 = true;
    }

    if (x == "3") {
      this.cierre_3 = true;
      this.btn_cierre_3 = false;
      this.btn_cierre_4 = true;
    }

    if (x == "4") {
      this.cierre_4 = true;
      this.btn_cierre_4 = false;
      this.btn_cierre_5 = true;
    }

    if (x == "5") {
      this.cierre_5 = true;
      this.btn_cierre_5 = false;
      this.btn_cierre_6 = true;
    }

    if (x == "6") {
      this.cierre_6 = true;
      this.btn_cierre_6 = false;
      this.btn_cierre_7 = true;
    }

    if (x == "7") {
      this.cierre_7 = true;
      this.btn_cierre_7 = false;
      this.btn_cierre_8 = true;
    }

    if (x == "8") {
      this.cierre_8 = true;
      this.btn_cierre_8 = false;
      this.btn_cierre_9 = true;
    }

    if (x == "9") {
      this.cierre_9 = true;
      this.btn_cierre_9 = false;
      this.btn_cierre_10 = true;
    }

    if (x == "10") {
      this.cierre_10 = true;
      this.btn_cierre_10 = false;
    }
  }

  ocultar_labor_quimica(x) {
    // alert(x);

    if (x == "1") {
      this.cierre_1 = false;
      this.btn_cierre_1 = false;
    }

    if (x == "2") {
      this.cierre_2 = false;
      this.btn_cierre_2 = false;
    }

    if (x == "3") {
      this.cierre_3 = false;
      this.btn_cierre_3 = false;
    }

    if (x == "4") {
      this.cierre_4 = false;
      this.btn_cierre_4 = false;
    }

    if (x == "5") {
      this.cierre_5 = false;
      this.btn_cierre_5 = false;
    }

    if (x == "6") {
      this.cierre_6 = false;
      this.btn_cierre_6 = false;
    }

    if (x == "7") {
      this.cierre_7 = false;
      this.btn_cierre_7 = false;
    }

    if (x == "8") {
      this.cierre_8 = false;
      this.btn_cierre_8 = false;
    }

    if (x == "9") {
      this.cierre_9 = false;
      this.btn_cierre_9 = false;
    }

    if (x == "10") {
      this.cierre_10 = false;
      this.btn_cierre_10 = false;
    }
  }

  async modificar_rut_usuario(labor, rut) {
    const alert = await this.alertController.create({
      header: "IMPORTANTE!!!",
      message:
        '<strong>Mensaje Importante:</strong><br>Usted <strong>desvinculará</strong> al usuario: <br><div style="text-align:center"><strong><h4>' +
        rut +
        "</h4></strong></div>",
      buttons: [
        {
          text: "CANCELAR",
          role: "cancel",
          cssClass: "secondaryCancel",
          handler: (blah) => {
            console.log("Confirm Cancel: blah");
          },
        },

        {
          text: "OK",
          cssClass: "secondaryOk",
          handler: () => {
            //console.log('Confirm Okay');
            // aqui instrucciones
            let data_cruda2 = JSON.parse(localStorage.getItem("listaRut"));
            console.log(data_cruda2);

            for (var i = 0; i < data_cruda2.length; i++) {
              if (
                data_cruda2[i].rut == rut &&
                data_cruda2[i].idLabor == labor
              ) {
                console.log(data_cruda2[i].rut);
                data_cruda2[i].estado = "2";

                //
                var nombre = data_cruda2[i].nombre;
                var apellido = data_cruda2[i].apellido;
                var celular = data_cruda2[i].celular;
                var estado = data_cruda2[i].estado;

                this.modificar_usuario_db(
                  nombre,
                  apellido,
                  celular,
                  estado,
                  rut
                );
              }
            }

            //console.log(data_cruda2);
            data_cruda2.sort().reverse();

            localStorage.listaRut = JSON.stringify(data_cruda2);

            this.listaRut = JSON.parse(localStorage.getItem("listaRut"));
            //fin instrucciones
          },
        },
      ],
    });

    await alert.present();

    //console.log(labor, rut);

    //alert("Usted desvinculará al usuario:"+rut);

    //return false;
  }

  modificar_usuario_db(nombre, apellido, celular, estado, rut) {
    //console.log(datosUsuario+" "+rut+" "+i);
    //return false;
    console.log(
      nombre + " " + apellido + " " + celular + " " + estado + " " + rut
    );

    //MODIFICAR USUARIO NUEVO EN BD

    // ****************PARA PROXIMA VERSION CREAR AQUI****************************

    var data_cruda = {
      usuarioRut: rut,
      usuarioPass: "",
      usuarioNombres: nombre,
      usuarioApellidoPaterno: apellido,
      usuarioApellidoMaterno: "",
      usuarioFono: celular,
      usuarioMail: "",
      contrato_idContrato: "1",
      perfil_idPerfil: "10",
      dispositivo_idDispositivo: "1",
      estado_idEstado: estado,
      nivelAcceso_idNivelAcceso: "4",
      fundo_idFundo: "1",
      urlfoto: "",
      passvisible: "",
    };

    this.dbrutusuarios.ModificarPostRutUsuarios(data_cruda).subscribe(
      (response) => {
        //alert UTIL
        //alert("Modificado en BD Exitosamente");
        console.log("Respuesta ingreso de rut usuario: " + response);
        // this.caja_rut_datos = false;
        //datosUsuario = null;
        // data_cruda = null;
      },
      (error) => {
        console.log("Error ingreso de rut usuario: " + error);
      }
    );

    //FIN AGREGAR USUARIO NUEVO EN BD
  }

  async confirmarEliminarRut(rut) {
    const alert = await this.alertController.create({
      header: "IMPORTANTE!!!",
      message:
        '<strong>Mensaje Importante:</strong><br>Usted <strong>desvinculará</strong> al usuario: <br><div style="text-align:center"><strong><h4>' +
        rut +
        "</h4></strong></div>",
      buttons: [
        {
          text: "CANCELAR",
          role: "cancel",
          cssClass: "secondaryCancel",
          handler: (blah) => {
            console.log("Confirm Cancel: blah");
            var respuesta = false;
            return respuesta;
          },
        },

        {
          text: "OK",
          cssClass: "secondaryOk",
          handler: () => {
            //console.log('Confirm Okay');
            // aqui instrucciones
            var respuesta = true;
            return respuesta;
            //fin instrucciones
          },
        },
      ],
    });

    await alert.present();
  }
  async cerrar() {
    this.storage.clear();
    this.router.navigate([""]);
    const toast = await this.toastController.create({
      message: "sesion cerrada con existoso.",
      duration: 2000,
    });
    toast.present();

    this.geo = "Sale del Trabajo";
    this.agregar1();
  }



  iniciarGeolocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat =   resp.coords.latitude
      this.lon = resp.coords.longitude

        console.log(resp.coords);

        let watch = this.geolocation.watchPosition();
watch.subscribe((data) => {
 // data can be a set of coordinates, or an error (if an error occurred).
 // data.coords.latitude
 // data.coords.longitude

 this.lat =   data.coords.latitude
 this.lon = data.coords.longitude
 console.log('Watch: ', data);


});
       }).catch((error) => {
         console.log('Error getting location', error);
       });

       60000    
  }



 


  agregar1() {
    return new Promise((resolve) => {
      let body = {
        datos: "insertar",
        longitud: this.lon,
        latitud: this.lat,
     
        proce1: this.proce,
        user1 :this.idUsuario,
        estado1: this.estado,
       
        geo1: this.geo,
        fecha: this.funcionesService.fecha_hora_actual()
      };

      this.prosecnt.postData(body, "geo2.php").subscribe((data) => {
        console.log("OK");
      });
    });
  }


  
}