import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GeneraAPQPage } from './genera-apq.page';

const routes: Routes = [
  {
    path: '',
    component: GeneraAPQPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GeneraAPQPage]
})
export class GeneraAPQPageModule {}
