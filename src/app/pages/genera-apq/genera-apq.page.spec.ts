import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneraAPQPage } from './genera-apq.page';

describe('GeneraAPQPage', () => {
  let component: GeneraAPQPage;
  let fixture: ComponentFixture<GeneraAPQPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneraAPQPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneraAPQPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
